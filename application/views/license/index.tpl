<div class="ui-widget ui-widget-content">
	<div class="ui-widget-header">
		<h4>Terminal Licenses</h4>
	</div>

	<form>
		Show
		<select id="show_only" name="show_only">
			<option value="ALL">all</option>
			<option value="CURRENT">current</option>
			<option value="TERMINATED">terminated</option>
			<option value="PASTDUE">past due</option>
			<option value="INVALID">invalid</option>
			<option value="UNVERIFIED" selected="selected">unverified</option>
		</select>
		Licenses
	</form>

	<table id="term_table" cellspacing="0" cellpadding="0" width="80%" style="border:2px solid black;">
		<thead>
			<tr style="background-color:black; color:white;">
				<th align="left">Terminal Id</th>
				<th align="left">Terminal Name</th>
				<th align="left">Business Id</th>
				<th align="left">Database</th>
				<th align="left">Status</th>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<tbody id="lic_tbody">
			<?php foreach ($terminals as $term): ?>
				<tr>
					<td><?php print $term->term_id; ?></td>
					<td><?php print $term->term_name; ?></td>
					<td><?php print $term->bus_id; ?></td>
					<td><?php print $term->database; ?></td>
					<td>
						<select name="term_status_<?php print $term->id; ?>" class="status_sel" id="term_status_<?php print $term->id; ?>">
							<option value="CURRENT"<?php if($term->status == 'CURRENT'){print ' selected="selected"';} ?>>current</option>
							<option value="TERMINATED"<?php if($term->status == 'TERMINATED'){print ' selected="selected"';} ?>>terminated</option>
							<option value="PASTDUE"<?php if($term->status == 'PASTDUE'){print ' selected="selected"';} ?>>past due</option>
							<option value="INVALID"<?php if($term->status == 'INVALID'){print ' selected="selected"';} ?>>invalid</option>
							<option value="UNVERIFIED"<?php if($term->status == 'UNVERIFIED'){print ' selected="selected"';} ?>>unverified</option>
						</select>
					</td>
					<td align="left">
						<button id="save_status_<?php print $term->id; ?>" class="save_btn ui-button ui-widget ui-corner-all ui-state-default ui-button-text-icon-primary" style="margin: 5px; padding: 2px 10px;">
							<span class="ui-button-text">Save</span>
							<span class="ui-button-icon-primary ui-icon ui-icon-disk" />
						</button>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</div>
