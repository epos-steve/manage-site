<table width=100% cellspacing=0 cellpadding=0 style="border:2px solid black;">
	<tr bgcolor=black>
		<td width=10%>
			&nbsp;<font color=white><b>Program ID</b></font>
		</td>
		<td width=30%>
			&nbsp;<font color=white><b>Client</b></font>
		</td>
		<td width=20%>
			&nbsp;<font color=white><b>POS Type</b></font>
		</td>
		<td width=20%>
			&nbsp;<font color=white><b>Last Action</b></font>
		</td>
		<td width=10%>
			&nbsp;<font color=white><b>New Action</b></font>
		</td>
		<td align=right width=10%>
			<font color=white><b>Status</b></font>&nbsp;
		</td>
	</tr>
	<tr style="border-bottom: 2px solid black;" onMouseOver="this.bgColor='#CCCCCC'" onMouseOut="this.bgColor='white'" id="<?php echo $client->client_programid; ?>">
		<td>&nbsp;<?php echo $client->client_programid; ?></td>
		<td>&nbsp;<?php if ($client->hasChildren()) echo '&nbsp;<img src="plus.png" class="expand">'; ?>&nbsp;<a href="managedetail.php?pid=<?php echo $client->client_programid; ?>" style="<?php echo $style; ?>"><font color=blue><?php echo $client->name; ?></font></a></td>
		<td>&nbsp;<?php echo $client->posType()->name; ?></td>
		<td>&nbsp;<?php echo $client->showSchedule(); ?><span class="recv_status"><?php echo (empty($client->receiveStatus()->name))? 'None' : $client->receiveStatus()->name,' ',$client->receiveTime(); ?></span></td>
		<td>
			&nbsp;<a target='_blank' href="receive/data/<?php echo $newfile; ?>"><img src="folder.gif" border="0" height="16" width="16" alt="View Data" title="View Data"></a>
			<a href="update.php?pid=<?php echo $client->client_programid; ?>"><img src="wrench.gif" border="0" height="17" width="16" alt="Send Updates" title="Send Updates"></a>
			<a href="update.php?pid=<?php echo $client->client_programid; ?>&action=1"><img src="calendar.gif" border="0" height="16" width="16" alt="Request Data" title="Request Data"></a>
			<a href="update.php?pid=<?php echo $client->client_programid; ?>&action=2"><img src="clear.gif" border="0" height="16" width="16" alt="Cancel Request" title="Cancel Request"></a>
			<img class="errors" style="display: <?php echo ($client->hasErrors())? 'inline' : 'none'; ?>;" src="error.gif" border="0" height="16" width="16" alt="View Errors" title="View Errors" />
		</td>
		<td align=right><span class="timer"><?php echo $client->timeDiff(); ?></span><img class="status" src="<?php echo $client->statusImg(); ?>" border="0" height="16" width="16"><img class="current" src="<?php echo $client->showSendStatus2(); ?>" border="0" height="16" width="16">&nbsp;</td>
	</tr>
	<tr><td colspan="7" height="2" style="background-color: black;"></td></tr>
</table>
<div id="dialog"></div>