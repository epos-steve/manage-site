<script type="text/javascript">
$(document).ready(function () {
	$('.delete').click(function (event) {
		if (!confirm('Are you sure you want to delete this update?')) {
			event.preventDefault();
		}
	});
});
</script>
<div class="ui-widget ui-widget-content">
	<div class="ui-widget-header">
		<h4>Available Upgrades</h4>
	</div>
	<div class="ui-widget-header ui-corner-all" style="margin: 5px; padding: 2px">
		<div class="left"><strong>File Name</strong></div>
		<div class="left"><strong>Workgroup</strong></div>
		<div class="right"><strong>Created Date</strong></div>
		<br />
	</div>
<?php foreach ($upgrades as $upgrade): ?>
	<div class="ui-widget ui-widget-content ui-corner-all" style="margin: 5px; padding: 2px;">
		<div class="left">
			<a href="aerispos/delete/<?php echo $upgrade->Id; ?>" class="delete"><img src="delete.gif" alt="delete" title="delete" /></a>
			<a href="aerispos/upgrade/<?php echo $upgrade->Id; ?>"><?php echo basename($upgrade->FileName); ?></a>
		</div>
		<div class="left"><?php echo (empty($upgrade->Workgroup))? '<em>None</em>' : $upgrade->Workgroup; ?></div>
		<div class="right"><?php echo $upgrade->Created; ?></div>
		<br />
	</div>
<?php endforeach; ?>
</div>
<br />
<?php if ($user->permissions & MANAGE_PERMISSION_UPLOAD) include('aerispos/upload.tpl'); ?>