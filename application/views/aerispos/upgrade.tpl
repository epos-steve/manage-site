<script type="text/javascript">
$(document).ready(function () {
	$('#add').submit(function (event) {
		event.preventDefault();
		$.getJSON($(this).attr('action'), function (data) {
			$('<div></div>').html(data['html']).dialog({
				height: $(window).height()-100,
				modal: true,
				title: 'Add Clients',
				width: $(window).width()-150
			});
		});
	});

	$('#clients #name').click(function () { sortItems(this, '.name'); }).click();
	$('#clients #added').click(function () { sortItems(this, '.added'); });
	$('#clients #received').click(function () { sortItems(this, '.received'); });
	$('#clients #status').click(function () { sortItems(this, '.status'); });
});

function sortItems(element, type)
{
	list = $('.client');
	items = list.children(type).get();

	items.sort(function (a,b) {
		var A = $(a).text().toLowerCase();
		var B = $(b).text().toLowerCase();
		return((A < B)? -1 : (A > B)? 1 : 0);
	});

	$.each(items, function (idx, itm) { $(list).parent().append($(itm).parent()); });
	$('#clients').children().each(function (idx, itm) { $(itm).children('.sorted').remove(); });
	$(element).append($('<span></span>').addClass('sorted ui-icon ui-icon-circle-triangle-s').css({'float': 'left'}));
}
</script>
<div class="ui-widget ui-widget-content ui-corner-all">
	<div class="ui-widget-header">
		<h3 class="left"><?php echo basename($upgrade->FileName); ?></h3>
		<h3 class="right">Created: <?php echo $upgrade->Created; ?></h3>
		<br />
	</div>
	<br />
	<form id="add" method="get" action="aerispos/add/<?php echo $upgrade->Id; ?>">
		<button type="submit" class="ui-button ui-button-text-icon-primary">
			<span class="ui-button-text">Add Clients</span>
			<span class="ui-button-icon-primary ui-icon ui-icon-plusthick"></span>
		</button>
	</form>
	<div>
		<div id="clients" class="ui-widget ui-widget-header ui-corner-all ui-state-default" style="margin: 5px; padding: 2px;">
			<label id="name" class="clickable" style="display: block; float: left; width: 250px;">Client Name</label>
			<div id="added" class="clickable" style="float: left; width: 200px;">Date Added</div>
			<div id="received" class="clickable" style="float: left; width: 200px;">Date Received</div>
			<span id="status" class="clickable">Status</span>
		</div>
<?php if (!empty($upgrade->Clients)): foreach ($upgrade->Clients as $client): ?>
		<div class="client ui-widget ui-widget-content ui-corner-all" style="margin: 5px; padding: 2px;">
			<label class="name" style="display: block; float: left; width: 250px;"><?php echo $client->ClientProgram->name; ?></label>
			<div class="added" style="float: left; width: 200px;"><?php echo $client->Added; ?></div>
			<div class="received" style="float: left; width: 200px;"><?php echo (empty($client->Received))? 'Not Received' : $client->Received; ?></div>
			<span class="status"><?php echo $client->getStatus(); ?>&nbsp;</span>
		</div>
<?php endforeach; endif; ?>
	</div>
</div>