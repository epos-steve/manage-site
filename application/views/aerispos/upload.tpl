<script type="text/javascript">
$(document).ready(function () {
	$('#upload').submit(function (e) {
		$('<div></div>').html('Your files are being uploaded... please wait...').dialog({
			beforeClose: function (event) {
				event.preventDefault();
			},
			closeOnEscape: false,
			draggable: false,
			modal: true,
			resizable: false,
			title: 'Uploading...'
		});
		return(true);
	});
});
</script>
<div class="ui-widget ui-widget-content ui-corner-all">
	<div class="ui-widget-header"><h4>Upload New XPI</h4></div>
	<?php if (isset($error)): ?>
	<div class="ui-state-error ui-corner-all" style="margin: 5px 15px; padding: .2em .5em;">
			<span class="ui-icon ui-icon-alert" style="float: left;"></span>
			<span><?php echo $error; ?></span>
	</div>
	<?php endif; ?>
	<form action="aerispos/upload/" method="post" enctype="multipart/form-data" id="upload">
		<p>
			<input type="file" class="multi" maxlength="5" accept="xpi" name="files[]" />
		</p>
		<p>
			<label for="workgroup">Workgroup</label>
			<input type="text" id="workgroup" name="workgroup" value="" /><br />
		<sub>Leave blank for no workgroup. All AerisPOS systems will install upgrades with no workgroup.</sub>
		</p>
		<button class="ui-button ui-button-text-icon-primary" type="submit">
			<span class="ui-button-text">Add New</span>
			<span class="ui-button-icon-primary ui-icon ui-icon-plusthick"></span>
		</button>
	</form>
</div>