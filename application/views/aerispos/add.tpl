<div class="ui-widget ui-widget-content ui-corner-all">
	<div class="ui-widget-header">
		<h3 class="left"><?php echo basename($upgrade->FileName); ?></h3>
		<h3 class="right">Created: <?php echo $upgrade->Created; ?></h3>
		<h4 style="clear: both;">Clients to Update</h4>
	</div>
	<button type="button" class="ui-button ui-button-text-icon-primary" onclick="$('.update').attr('checked', 'checked');">
		<span class="ui-button-text">Select All</span>
		<span class="ui-button-icon-primary ui-icon ui-icon-plus"></span>
	</button>
	<button type="button" class="ui-button ui-button-text-icon-primary" onclick="$('.update').attr('checked', '');">
		<span class="ui-button-text">Un-Select All</span>
		<span class="ui-button-icon-primary ui-icon ui-icon-minus"></span>
	</button>
	<form method="post" action="aerispos/add/<?php echo $upgrade->Id; ?>">
		<div class="ui-state-error">
			<div class="ui-icon ui-icon-alert" style="float: left;"></div>
			<span class="ui-state-error-text">Once you add a client, it cannot be removed!</span>
		</div>
<?php foreach ($clients as $client): ?>
		<div class="ui-widget ui-widget-content" style="margin: 5px; padding: 2px;">
			<div>
				<input type="checkbox" class="update" name="update[]" value="<?php echo $client->client_programid; ?>"<?php if (ViviposUpgradeClientModel::get('ClientProgram = '.$client->client_programid.' AND ViviposUpdate = '.$upgrade->Id, true)) echo ' checked="checked"' ?> />
				<strong>Client Name:</strong> <?php echo $client->name; ?>
			</div>
		</div>
<?php endforeach; ?>
		<button type="submit" class="ui-button ui-button-text-icon-primary">
			<span class="ui-button-text">Save</span>
			<span class="ui-button-icon-primary ui-icon ui-icon-disk"></span>
		</button>
	</form>
</div>