<?=$boundary?>
Content-Type: multipart/related; boundary="<?=$boundary_related_header?>"

<?=$boundary_related?>
Content-Type: text/html; charset="utf-8"

<table style='border-collapse: collapse; margin: 1em 4em; font-family: sans-serif;'>
	<tr style='border: solid 1px black; padding: 0.6em; height: 160px;'>
		<td style='width: 320px;'>
			<img src='cid:logo.jpeg@eemail' alt='<?=$site_name?>' style='vertical-align: middle;' />
		</td>
		<td>
			<h3 style='display: inline; vertical-align: middle;'><?=$business_name?></h3>
		</td>
	</tr>
	<tr style='border: solid 1px black;'><td style='padding: 0.4em' colspan='2'>
		<p>Thank you for registering a new account with <?=$site_name?>!</p>
		<p style='text-align: justify;'>
			Please log in to your new account at <a href='<?=$login_address?>'><?=$login_address?></a>.
			Your username is the email address you registered with, and a temporary password has been assigned to your account.
			You will be asked to create a new password when you log in, as well as fill out additional account information.
		</p>
		<?=$promo_reminder == '' ? '' : ( '<p style="font-weight: bold;">' . $promo_reminder . '</p>' )?>
		<table style='margin-left: 24px;'>
			<tr>
				<td style='font-weight: bold; padding: 0.3em; background-color: #dddddd; border: solid 1px white;'>Account Number</td>
				<td><?=$scancode?></td>
			</tr>
			<tr>
				<td style='font-weight: bold; padding: 0.3em; background-color: #dddddd; border: solid 1px white;'>Username</td>
				<td><?=$username?></td>
			</tr>
			<tr>
				<td style='font-weight: bold; padding: 0.3em; background-color: #dddddd; border: solid 1px white;'>Password</td>
				<td><?=$password?>
			</tr>
		</table>
		<p>Thank you again for using <?=$site_name?> at <?=$business_name?>!</p>
	</td></tr>
	<tr><td style='text-align: center;' colspan='2'>
		<p style='color: #444444; font-size: 80%; margin: 0.2em 0; width: 80%; margin-left: 10%;'>
			Please do not reply to this automatically generated email. This email was automatically sent 
			on behalf of <?=$site_name?> in response to an account being registered with the email 
			address <span style='white-space: nowrap;'><?=$username?></span>. If you have an inquiry for <?=$site_name?>, please visit the contact page at 
			<a href='<?=$support_address?>' style='white-space: nowrap;'><?=$support_address?></a>.
		</p>
	</td></tr>
</table>

<?=$boundary_related?>
Content-Type: image/jpeg; name="logo.jpeg"
Content-Transfer-Encoding: base64
Content-ID: <logo.jpeg@eemail>
Content-Disposition: inline; filename="logo.jpeg"

<?php 
	echo chunk_split( base64_encode( file_get_contents( $image_path ) ) );
?>