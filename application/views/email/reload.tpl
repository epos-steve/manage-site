<?php echo $sep."\n"; ?>
Content-Type: text/html; charset=ISO-8859-1

<img src="cid:PHP-CID-CKIMG" />
<h3><?php echo $business; ?></h3>
<pre>
<?php
$subTotal = 0;
$taxTotal = 0;

echo str_pad('Date: '.date('Y-m-d H:i:s', $order['created']), 40, ' ', STR_PAD_RIGHT).str_pad('Auth Code: '.$order['auth_code'], 40, ' ', STR_PAD_LEFT)."\n";
echo str_pad('Reference Number: '.$order['ref_number'], 80, ' ', STR_PAD_LEFT)."\n\n";
echo str_pad('CK Card Reload from Credit', 70, ' ', STR_PAD_RIGHT).str_pad('$'.number_format($order['amount'], 2).' ', 10, ' ', STR_PAD_LEFT)."\n";
echo str_repeat('-', 80)."\n";
echo str_pad('TOTAL', 70, ' ', STR_PAD_RIGHT).str_pad('$'.number_format($order['amount'], 2).' ', 10, ' ', STR_PAD_LEFT)."\n";
if ($order['amount'] > 0){
echo str_repeat('-', 80)."\n";
echo str_pad('CREDIT CARD PAYMENT - RELOAD', 70, ' ', STR_PAD_RIGHT).str_pad('($'.number_format($order['amount'], 2).')', 10, ' ', STR_PAD_LEFT)."\n\n";
}
?>
</pre>

<?php echo $sep,"\n"; ?>
Content-Type: image/jpeg
Content-Transfer-Encoding: base64
Content-ID: <PHP-CID-CKIMG>

<?php
echo chunk_split(base64_encode(file_get_contents(SITECH_APP_PATH.'/../htdocs/ck.jpg')));
?>