<?php echo $sep."\n"; ?>
Content-Type: text/html; charset=ISO-8859-1

<img src="cid:PHP-CID-CKIMG" />
<h3><?php echo $business; ?></h3>
<pre>
<?php
$subTotal = (double) 0.0;

echo str_pad('Date: '.date('Y-m-d H:i:s', $order['created']), 40, ' ', STR_PAD_RIGHT).str_pad('Auth Code: '.$order['auth_code'], 40, ' ', STR_PAD_LEFT)."\n";
echo str_pad('Reference Number: '.$order['ref_number'], 80, ' ', STR_PAD_LEFT)."\n\n";

foreach ($items as $item):
	$subTotal += (double) $item['current_subtotal'];
	if ($item['current_qty'] > 1) {
		echo str_pad($item['product_name'].' '.$item['current_qty'].'@ $'.number_format($item['current_price'], 2), 70, ' ', STR_PAD_RIGHT).str_pad('$'.number_format($item['current_subtotal'], 2).' ', 10, ' ', STR_PAD_LEFT)."\n";
	} else {
		echo str_pad($item['product_name'], 70, ' ', STR_PAD_RIGHT).str_pad('$'.number_format($item['current_subtotal'], 2).' ', 10, ' ', STR_PAD_LEFT)."\n";
	}
endforeach;

echo str_repeat('-', 80)."\n";
echo str_pad('SUB-TOTAL', 70, ' ', STR_PAD_RIGHT).str_pad('$'.number_format($subTotal, 2).' ', 10, ' ', STR_PAD_LEFT)."\n";
echo str_pad('Tax', 70, ' ', STR_PAD_RIGHT).str_pad('$'.number_format($taxTotal, 2).' ', 10, ' ', STR_PAD_LEFT)."\n";
echo str_repeat('-', 80)."\n";
echo str_pad('TOTAL', 70, ' ', STR_PAD_RIGHT).str_pad('$'.number_format($total, 2).' ', 10, ' ', STR_PAD_LEFT)."\n";
if ($order['amount'] > 0){
echo str_repeat('-', 80)."\n";
echo str_pad('CREDIT CARD PAYMENT - Aeris2', 70, ' ', STR_PAD_RIGHT).str_pad('($'.number_format($order['amount'], 2).')', 10, ' ', STR_PAD_LEFT)."\n\n";
}
?>
</pre>

<?php echo $sep,"\n"; ?>
Content-Type: image/jpeg
Content-Transfer-Encoding: base64
Content-ID: <PHP-CID-CKIMG>

<?php
echo chunk_split(base64_encode(file_get_contents(SITECH_APP_PATH.'/../htdocs/ck.jpg')));
?>
