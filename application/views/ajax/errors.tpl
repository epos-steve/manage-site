					<table>
						<thead>
							<tr>
								<td width="10%">Date</td>
								<td width="40%">Error Message</td>
								<td width="40%">Stack Trace</td>
							</tr>
						</thead>
						<tbody>
<?php
if (empty($errors)):
?>
							<tr>
								<td colspan="3" class="ui-state-error ui-state-error-text"><span class="ui-icon ui-icon-alert" style="float: left;"></span>No errors found for this client</td>
							</tr>
<?php
else:
	foreach ($errors as $error):
?>
							<tr>
								<td><?php echo $error->error_time; ?></td>
								<td><?php echo $error->message(); ?></td>
								<td><?php echo $error->trace(); ?></td>
							</tr>
<?php
	endforeach;
endif;
?>
						</tbody>
					</table>