<script type="text/javascript">
$.strPad = function(i,l,s) {
	var o = i.toString();
	if (!s) { s = '0'; }
	while (o.length < l) {
		o = s + o;
	}
	return o;
};

function expand()
{
	var id = $(this).parent().parent().attr('id');

	$.getJSON('ajax/getChildren/'+id, function (data) {
		$.each(data, function (i, item) {
			// ID for client
			var tr = $('<tr></tr>')
				.attr('bgcolor', '#eee')
				.mouseover(function(){$(this).attr('bgcolor', '#ccc')})
				.mouseout(function(){$(this).attr('bgcolor', '#eee')})
				.addClass('parent-'+id).attr('id', item[0]);

			var tdid = $('<td></td>').attr('width', '10%').html('&nbsp;'+item[0]);
			$(tr).append(tdid);

			// Name and expand if children
			var myParent = id;
			var tdname = $('<td></td>').attr('width', '30%').html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
			do {
				if ($('tr#'+myParent).attr('class').indexOf('parent-') == -1) {
					myParent = null;
				} else {
					myParent = $('tr#'+myParent).attr('class').substr(7);
					$(tdname).append('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
				}
			} while(myParent != null);

			if (item[9]) {
				$(tdname).append($('<img />').attr('src', 'plus.png').addClass('click expand').bind('click', expand));
				$(tdname).append($('<span></span>').css('color', 'blue').html('&nbsp; ' + item[1]));
			} else {
				$(tdname).append($('<a></a>').attr('href', 'dashboard/manage/'+item[0]).attr('name', item[0]).text(item[1]));
				if (item[12]) {
					$(tdname).append($('<span></span>').css({'font-size': '0.8em'}).html('&nbsp;IP: '+item[12]));
				}
			}

			$(tr).append(tdname);

			// POS Type
			var tdType = $('<td></td>').html('&nbsp;'+item[2]);
			$(tr).append(tdType);

			// Last Action
			if (item[4].length == 0 && !item[9]) item[4] = 'None';
			var tdLast = $('<td></td>').html('&nbsp;'+item[3]);
			if (!item[9]) tdLast.append($('<span></span>').addClass('recv_status').html(item[4]+'&nbsp;'+item[5]));
			$(tr).append(tdLast);

			// Actions
			var tdActions = $('<td></td>').html('&nbsp;');
			<?php if ($user->isAdmin()): ?>
			if (!item[9]) {
				if (item[13] == '1') {
					$(tdActions).append("\n"+'<a target="_blank" href=dashboard/data/'+item[0]+'><img src="folder.gif" border="0" height="16" width="16" alt="View Data" title="View Data"></a>&nbsp;');
				}

				if (!item[11]) {
					$(tdActions).append('<a href="update.php?pid='+item[0]+'"><img src="wrench.gif" border="0" height="17" width="16" alt="Send Updates" title="Send Updates"></a>&nbsp;');
				}

				if (item[13] == '1') {
					$(tdActions).append(
							'<a href="update.php?pid='+item[0]+'&action=1"><img src="calendar.gif" border="0" height="16" width="16" alt="Request Data" title="Request Data"></a>&nbsp;' +
							'<a href="update.php?pid=' +item[0]+ '&action=2"><img src="clear.gif" border="0" height="16" width="16" alt="Cancel Request" title="Cancel Request"></a>&nbsp;'
					);
				}

				if (item[11]) {
					$(tdActions).append("\n"+'<a href="update.php?pid='+item[0]+'&action=3"><img src="restart.png" height="16" width="16" alt="Restart Kiosk" title="Restart Kiosk" /></a>');
				}
				$(tdActions).append($('<img />').addClass('errors click').css('display', ((item[10] == '1')? 'inline' : 'none')).attr('src', 'error.gif').bind('click', showErrors));
				$(tdActions).append('&nbsp;<a href="/remote/vnc/' + item[0] + '" target="_blank"><img src="icon-vnc.png" border="0" height="16" width="16" alt="Connect via VNC" title="Connect via VNC" /></a>')
							.append('&nbsp;<a href="/remote/ssh/' + item[0] + '" target="_blank"><img src="icon-ssh.png" border="0" height="16" width="16" alt="Connect via SSH" title="Connect via SSH" /></a>')
							.append('&nbsp;<a href="/remote/logs/' + item[0] + '" target="_blank"><img src="icon-logs.png" border="0" height="16" width="16" alt="Download Logs" title="Download Logs" /></a>');
			}
			<?php endif; ?>
			$(tr).append(tdActions);

			// Status
			var tdStatus = $('<td></td>').attr('align','right').html('');
			if (!item[9]) {
				$(tdStatus)
					.append($('<span></span>').addClass('timer').text(item[6]))
					.append($('<img />').attr('src', item[7]).addClass('status').attr('height', 16).attr('width', 16))
					.append(item[8]).append('&nbsp;');
			}
			$(tr).append(tdStatus);

			$('tr#'+id).after(tr);
			$('tr#'+item[0]+' img.expand').click();
		});
	});

	this.src = 'minus.png';
	$(this).unbind('click', expand);
	$(this).bind('click', collapse);
}

function collapse()
{
	var id  = $(this).parent().parent().attr('id');
	$('tr.parent-'+id).each(removeChildren);
	this.src = 'plus.png';
	$(this).unbind('click', collapse);
	$(this).bind('click',expand);
}

function removeChildren()
{
	var id = $(this).attr('id');

	$('tr.parent-'+id).each(removeChildren);
	$(this).remove();
}

function showErrors()
{
	var clientId = $(this).parent().parent().attr('id');
	$.getJSON('ajax/getErrors/' + clientId, function (data) {
		var errorHtml = '<table width="100%" cellspacing="0"><tr><th width="180">Date/Time</th><th>Message</th><th>Stack Trace</th></tr>';
		$.each(data, function (i, item) {
			errorHtml += '<tr><td valign="top">' + item[0] + '</td><td valign="top">' + item[1] + '</td><td valign="top">' + item[2] + '</td></tr>';
		});
		errorHtml += '</table>';
		$('<div></div>').html(errorHtml).dialog({
			buttons: {
				'OK': function () { $(this).dialog("close"); }
			},
			dialogbeforeclose: function(event, ui) {
				$('tr#' + clientId + ' img.errors').hide();
			},
			height: 400,
			title: 'Client Errors',
			width: 800
		});
	});
}

function updateTimer(i)
{
	var sp = $.strPad;
	$('span.timer').each( function () {
		var $this = $(this);
		var diff = $this.data('time');
		var seconds = 0;
		var minutes = 0;
		var hours = 0;

		if (diff === undefined || diff === null) {
			diff = $this.html();
			if (diff.length == 0) return;
			diff = diff.split(':');
			seconds = (((Number(diff[0]) * 60) + Number(diff[1]) * 1) * 60) + diff[2] * 1;
		} else {
			seconds = diff;
		}

		seconds++;
		$this.data('time', seconds);

		var secs = seconds % 60;
		seconds -= secs;
		minutes = seconds / 60;
		var mins = minutes % 60;
		minutes -= mins;
		hours = minutes / 60;
		
		$this.text(sp(hours, 2) + ':' + sp(mins, 2) + ':' + sp(secs, 2));
	});
}

$(document).ready(function () {
	$('img.expand').bind('click', expand);
	$('img.expand').each(function() {
		if ($(this).parent().parent().attr('id') <?php echo (IN_PRODUCTION)? '!=' : '=='; ?> 22) {
			$(this).trigger('click');
		}
	});
	$(document).everyTime(1000, updateTimer);
	$(document).everyTime(60000, function(i) {
		$.getJSON('ajax/updateTimers', function (data) {
			$.each(data, function (x, item) {
				if ($(this).find('#parent-'+item[0])[0] == undefined) {
					var element = $('#'+item[0]+' span.timer');
					$(element).data('time', undefined);
					$(element).text(item[1]);
					$('#'+item[0]+' span.recv_status').text(item[2] + ' ' + item[3]);
					$('#'+item[0]+' img.status').attr('src', item[4]);
					$('#'+item[0]+' img.current').attr('src', item[5]);
					if (item[6] == '1') {
						$('#' + item[0] + ' img.errors').show();
					} else {
						$('#' + item[0] + ' img.errors').hide();
					}
				}
			});
		});
	});
	$('#add-machine').click(function (e) {
		e.preventDefault();
		$.get('dashboard/add', function (html) {
			$('<div id="add-dialog" />').html(html).dialog({
				buttons: {
					'Save': function () { $('form#add').submit(); },
					'Cancel': function () { $('#add-dialog').dialog('destroy'); }
				},
				modal: true,
				width: '400px',
				title: 'Add Machine'
			});
		});
	});
});
</script>
<?php if ($user->isAdmin() && $user->loginid != 16 && $user->loginid != 12): ?>
<form method="post" action="aerispos" style="display: inline;">
	<button class="ui-button ui-widget ui-corner-all ui-state-default ui-button-text-icon-primary" style="margin: 5px; padding: 5px 10px;">
		<span class="ui-button-text">AerisPOS Management</span>
		<span class="ui-button-icon-primary ui-icon ui-icon-arrowreturnthick-1-s"></span>
	</button>
</form>
<button id="add-machine" class="ui-button ui-widget ui-corner-all ui-state-default ui-button-text-icon-primary" style="margin: 5px; padding: 5px 10px;">
	<span class="ui-button-text">Add New Machine</span>
	<span class="ui-button-icon-primary ui-icon ui-icon-plusthick" />
</button>


<form method="post" action="license" style="display: inline;">
	<button id="add-machine" class="ui-button ui-widget ui-corner-all ui-state-default ui-button-text-icon-primary" style="margin: 5px; padding: 5px 10px;">
		<span class="ui-button-text">License Manager</span>
		<span class="ui-button-icon-primary ui-icon ui-icon-wrench" />
	</button>
</form>


<?php endif; ?>
<table width=100% cellspacing=0 cellpadding=0 style="border:2px solid black;">
	<tr bgcolor=black>
		<td width=10%>
			&nbsp;<font color=white><b>Program ID</b></font>
		</td>
		<td width=30%>
			&nbsp;<font color=white><b>Client</b></font>
		</td>
		<td width=20%>
			&nbsp;<font color=white><b>POS Type</b></font>
		</td>
		<td width=20%>
			&nbsp;<font color=white><b>Last Action</b></font>
		</td>
		<td width=10%>
			&nbsp;<font color=white><b>New Action</b></font>
		</td>
		<td align=right width=10%>
			<font color=white><b>Status</b></font>&nbsp;
		</td>
	</tr>
<?php
foreach ($clients as $client):
	$newfile = $client->client_programid;
	$newfile.=".old.txt";
?>
	<tr style="border-bottom: 2px solid black;" onMouseOver="this.bgColor='#CCCCCC'" onMouseOut="this.bgColor='white'" id="<?php echo $client->client_programid; ?>">
		<td>&nbsp;<?php echo $client->client_programid; ?></td>
		<td>
			&nbsp;<?php if ($client->hasChildren()) echo '&nbsp;<img src="plus.png" class="expand click">'; ?>&nbsp;<?php if ($user->isAdmin() && !$client->hasChildren()): ?><a name="<?php echo $client->client_programid; ?>" href="dashboard/manage/<?php echo $client->client_programid; ?>" style=""><?php endif; ?><font color=blue><?php echo $client->name; ?></font></a>
			<?php if ($user->isAdmin() && !$client->hasChildren() && !empty($client->client_ip)): ?><span style="font-size: 0.8em;">&nbsp;IP: <?php echo $client->client_ip; ?></span><?php endif; ?>
		</td>
		<td>&nbsp;<?php echo $client->posType()->name; if ($client->posType() == '7') echo 'Sync Version: '.$client->sync_version; ?></td>
		<td>&nbsp;<?php if (!$client->hasChildren()): echo $client->showSchedule(); ?><span class="recv_status"><?php echo (empty($client->receiveStatus()->name))? 'None' : $client->receiveStatus()->name,' ',$client->receiveTime(); ?></span><?php endif; ?></td>
		<td>
			&nbsp;
			<?php if ($user->isAdmin() && !$client->hasChildren()): ?>
			<a target='_blank' href="dashboard/data/<?php echo $client->client_programid; ?>"><img src="folder.gif" border="0" height="16" width="16" alt="View Data" title="View Data"></a>
			<a href="update.php?pid=<?php echo $client->client_programid; ?>"><img src="wrench.gif" border="0" height="17" width="16" alt="Send Updates" title="Send Updates"></a>
<?php if ($client->sync_version == '1'): ?>
			<a href="update.php?pid=<?php echo $client->client_programid; ?>&action=1"><img src="calendar.gif" border="0" height="16" width="16" alt="Request Data" title="Request Data"></a>
			<a href="update.php?pid=<?php echo $client->client_programid; ?>&action=2"><img src="clear.gif" border="0" height="16" width="16" alt="Cancel Request" title="Cancel Request"></a>
<?php endif; ?>
			<?php if ($client->isKiosk()): ?>
			<a href="update.php?pid=<?php echo $client->client_programid; ?>&action=3"><img src="restart.png" height="16" width="16" alt="Restart Kiosk" title="Restart Kiosk" /></a>
			<?php endif; ?>
			<img class="errors click" style="display: <?php echo ($client->hasErrors())? 'inline' : 'none'; ?>;" src="error.gif" border="0" height="16" width="16" alt="View Errors" title="View Errors" />
			<?php endif; ?>
		</td>
		<td align=right><?php if(!$client->hasChildren()): ?><span class="timer"><?php echo $client->timeDiff(); ?></span><img class="status" src="<?php echo $client->statusImg(); ?>" border="0" height="16" width="16"><img class="current" src="<?php echo $client->showSendStatus2(); ?>" border="0" height="16" width="16"><?php endif; ?>&nbsp;</td>
	</tr>
	<tr><td colspan="7" height="2" style="background-color: black;"></td></tr>
<?php endforeach; ?>
</table>
