	<div id="client_mapping">
			<form method="post" action="mapping.php">
				<input type="hidden" name="client_programid" value="<?php echo $client->client_programid; ?>" />
				<div class="ui-widget ui-widget-header ui-corner-all">
					<h3 style="float: right;"><input type="submit" value="Save" /></h3>
					<h3>Mapping - <?php echo $client->name; ?> <a href="/receive/data/<?php echo $client->client_programid; ?>.old.txt" target="_blank"><img src="folder.gif" alt="View Data File" title="View Data File" /></a></h3>
				</div>
				<div class="tabs">
					<ul>
<?php
foreach($mappingTypes as $mapType):
?>
						<li><a href="#mapping_<?php echo $mapType->id; ?>"><?php echo $mapType->name; ?></a></li>
<?php
endforeach;
?>
					</ul>
<?php
foreach($mappingTypes as $mapType):
?>
					<div id="mapping_<?php echo $mapType->id; ?>">
						<table>
							<tr>
								<th width="45%">Name</th>
								<th>&nbsp;</th>
								<th width="25%">Credits</th>
								<th width="25%">Debits</th>
							</tr>
<?php
	foreach ($mapType->getMappingByClient($client) as $map):
?>
							<tr>
								<td width="45%">(<?php echo $map->posid.') '.$map->name; ?></td>
								<td>
									<a href="editmap.php?pid=<?php echo $client->client_programid; ?>&id=<?php echo $map->id; ?>&do=2" title="Delete Mapping" onclick="javascript:return confirm('Are you sure?');"><img src="delete.gif" alt="Delete Mapping" height="16" width="16" border="0"></a>
<?php if($map->localid == 0): ?>
									<img src="error.gif" height="16" width="16">
<?php endif; ?>
								</td>
								<td width="25%">
									<select name="credit[<?php echo $map->id; ?>]">
										<option value="0">(None)</option>
<?php foreach ($credits as $credit): ?>
										<option value="<?php echo $credit['creditid']; if($map->localid == $credit['creditid'] && $map->tbl->id == 1) echo '" selected="selected'; ?>"><?php echo $credit['creditname']; ?> (<?php echo $credit['creditid']; ?>)</option>
<?php endforeach; ?>
								</td>
								<td width="25%">
									<select name="debit[<?php echo $map->id; ?>]">
										<option value="0">(None)</option>
<?php foreach ($debits as $debit): ?>
										<option value="<?php echo $debit['debitid']; if($map->localid == $debit['debitid'] && $map->tbl->id == 2) echo '" selected="selected'; ?>"><?php echo $debit['debitname']; ?> (<?php echo $debit['debitid']; ?>)</option>
<?php endforeach; ?>
									</select>
								</td>
							</tr>
<?php
	endforeach;
?>
						</table>
					</div>
<?php
endforeach;
?>
			</div>
		</form>
	</div>