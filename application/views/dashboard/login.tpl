<p><br><p><br><p><br>
<form action="dashboard" method="post" name="login">
<div class="ui-widget ui-widget-content" style="width: 400px; text-align: center; margin: 0 auto;">
	<div class="ui-widget-header"><h4>Login</h4></div>
	<div>
		<label for="username">Username:</label>
		<input type="text" name="username" id="username" size="20">
	</div>
	<div>
		<label for="password">Password:</label>
		<input type="password" name="password" size="20">
	</div>
	<button type="submit" class="ui-button ui-button-text-icon-primary">
		<span class="ui-button-text">Login</span>
		<span class="ui-button-icon-primary ui-icon ui-icon-check" />
	</button>
</div>
</form>
