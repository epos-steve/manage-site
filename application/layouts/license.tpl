<html>
	<head>
		<base href="<?php echo $baseUrl; ?>" />
		<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.8.11.custom.min.js"></script>
		<script src="js/jquery.waffle.js"></script>
		<script src="js/license.js"></script>
		<link rel="stylesheet" href="css/cupertino/jquery-ui-1.8.11.custom.css" />
		<link rel="stylesheet" href="css/aerispos.css" />
		<link href="css/jquery.waffle.css" rel="stylesheet" />
		<style type="text/css">
a:link, a:visited {
	color: blue;
	text-decoration: none;
}

h2 a:link, h2 a:visited {
	color: black;
}

td.children {
	padding-left: 50px;
}

img.click {
	cursor: pointer;
}

tr.woo th {
	color: white;
	text-shadow: black 1px 1px 1px;
}

tr.woo td {
	border-bottom: 2px solid black;
	color: white;
	padding: 2px;
}

tbody tr:nth-child(odd) {
   background-color: #ccc;
}
		</style>
		<title>Manage POS Systems</title>
	</head>
	<body>
		<div class="ui-widget ui-widget-content">
			<div class="ui-widget-header">
				<form method="post" action="dashboard" style="float: right; margin: 12px;">
					<button type="submit" class="ui-button ui-button-text-icon-primary">
						<span class="ui-button-text">Back to Dashboard</span>
						<span class="ui-button-icon-primary ui-icon ui-icon-arrowreturnthick-1-n"></span>
					</button>
				</form>
				<h2><a href="aerispos">Welcome to AerisPOS management.</a></h2>
			</div>
		</div>
		<br />
		<?php echo $content; ?>
	</body>
</html>
