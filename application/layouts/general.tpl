<html>
	<head>
		<base href="<?php echo $baseUrl; ?>" />
		<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
		<script type="text/javascript" src="js/jquery.timers-1.2.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.8.11.custom.min.js"></script>
		<link rel="stylesheet" href="css/cupertino/jquery-ui-1.8.11.custom.css" />
		<style type="text/css">
a:link, a:visited {
	color: blue;
	text-decoration: none;
}

td.children {
	padding-left: 50px;
}

img.click {
	cursor: pointer;
}

tr.woo th {
	color: white;
	text-shadow: black 1px 1px 1px;
}

tr.woo td {
	border-bottom: 2px solid black;
	color: white;
	padding: 2px;
}
		</style>
		<title>Manage POS Systems</title>
	</head>
	<body>
		<?php echo $content; ?>
	</body>
</html>
