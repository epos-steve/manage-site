<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clientprograms
 *
 * @author eric
 */
class ClientProgramsModel extends Manage_Model_Abstract
{
	/**
	 * Holder for the client database connection.
	 *
	 * @var Manage_DB
	 */
	protected $_cdb;

	public $companyId;

	protected $_belongsTo = array(
		'pos_type' => array('class' => 'PosType', 'foreignKey' => 'pos_type')
	);

	protected $_hasMany = array(
		'schedules' => array(
			'class' => 'ClientSchedule',
			'foreignKey' => 'client_programid'
		)
	);

	protected static $_pk = 'client_programid';

	protected static $_table = 'client_programs';

	public function __get($name)
	{
		if ($name == 'db') {
			if (!is_object($this->_cdb)) {
				Manage_DB_Client::$client = $this;
				$this->_cdb = new Manage_DB_Client();
				$this->_cdb->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}

			return($this->_cdb);
		} elseif ($name == 'pos_type') {
			$val = parent::__get($name);
			if (!is_object($val)) {
				$val = new stdClass;
				$val->name = '';
				$val->pos_type = 0;
			}
			return($val);
		} else {
			return(parent::__get($name));
		}
	}

	public function canSkip()
	{
		return(($this->skip_client == 1));
	}

	public static function getAll()
	{
		$stmnt = self::db()->query('SELECT * FROM client_programs');
		$stmnt->setFetchMode(PDO::FETCH_CLASS, 'ClientProgramsModel');
		return($stmnt->fetchAll());
	}

	/**
	 * Fetch an array from the database of all clients.
	 *
	 * @param int $parent Parent ID to fetch from. Default: 0 (top level)
	 */
	public static function getClients($parent = 0)
	{
		$user = LoginModel::getCurrentUser();
		if ($parent !== 0) {
			$args = array(':parent' => $parent);
		} else {
			$args = array();
		}

		if ($user->isAdmin() || $parent != 0) {
			$stmnt = self::db()->prepare('SELECT * FROM client_programs WHERE parent '.(($parent !== 0)? '= :parent' : 'IS NULL').' ORDER BY client_programid');
		} else {
			$stmnt = self::db()->prepare('
				SELECT
					c.*
				FROM
					client_programs AS c
					LEFT JOIN parentAccess as p
						ON p.client_id = c.client_programid
				WHERE
					c.parent = '.(($parent !== 0)? '= :parent' : 'IS NULL').'
					AND p.login_id = :login
				ORDER BY client_programid');
			$args[':login'] = $user->loginid;
		}

		$stmnt->execute($args);
		$stmnt->setFetchMode(PDO::FETCH_CLASS, 'ClientProgramsModel');
		return($stmnt->fetchAll());
	}

	public function getCompanyId()
	{
		if (empty($this->companyId)) {
			$stmnt = $this->db->prepare('SELECT companyid FROM business WHERE businessid = ?');
			$stmnt->execute(array($this->businessid));
			$this->companyId = $stmnt->fetchColumn(0);
			$stmnt->closeCursor();
		}

		return($this->companyId);
	}

	public function getCompanyName()
	{
		if (empty($this->companyName)) {
			$stmnt = $this->db->prepare('SELECT companyname FROM business WHERE businessid = ?');
			$stmnt->execute(array($this->businessid));
			$this->companyName = $stmnt->fetchColumn(0);
			$stmnt->closeCursor();
		}

		return($this->companyName);
	}

	public function getCredits()
	{
		$ret = array();

		try {
			$sql = 'SELECT creditid,creditname FROM credits WHERE businessid = '.$this->businessid.' ORDER BY credittype,creditname';
			$stmnt = $this->db->prepare($sql);
			$stmnt->execute();
			$ret = $stmnt->fetchAll(PDO::FETCH_ASSOC);
		} catch (PDOException $e) {
			if (isset($stmnt)) {
				$stmnt->closeCursor();
			}
		}

		return($ret);
	}

	public function getDebits()
	{
		$ret = array();

		try {
			$sql = 'SELECT debitid,debitname FROM debits WHERE businessid = '.$this->businessid.' ORDER BY debittype,debitname';
			$stmnt = $this->db->prepare($sql);
			$stmnt->execute();
			$ret = $stmnt->fetchAll(PDO::FETCH_ASSOC);
		} catch (PDOException $e) {
			if (isset($stmnt)) {
				$stmnt->closeCursor();
			}
		}

		return($ret);
	}

	public function getErrors(array $args = array())
	{
		$sql = 'client_id = '.$this->client_programid;
		$page = (!empty($args['page']) && is_numeric($args['page']))? $args['page'] : 1;
		$max = (!empty($args['max']) && is_numeric($args['max']))? $args['max'] : 10;

		SiTech_Loader::loadModel('ClientErrors');

		if (!empty($args['start_date'])) {
			$sql .= ' AND UNIX_TIMESTAMP(error_time) >= '.strtotime($args['start_date']);
		}

		if (!empty($args['end_date'])) {
			$sql .= ' AND UNIX_TIMESTAMP(error_time) <= '.strtotime($args['end_date']);
		}

		$sql .= ' ORDER BY error_time DESC LIMIT '.$max.' OFFSET '.($page * $max);
		$errorTotal = ClientErrorsModel::getCount((int)$this->client_programid);

		return(array(
			'current' => $page,
			'pages' => ceil($max / (($errorTotal > 0)? $errorTotal : 1)),
			'list' => ClientErrorsModel::get($sql)
		));
	}

	public function getSchedules()
	{
		$schedules = new Manage_Schedule();
		return($schedules->getByClient($this->client_programid));
	}

	public function getViviposSiblings()
	{
		if ($this->pos_type->pos_type != '7') return(false);

		$sql = 'SELECT client_programid FROM client_programs WHERE businessid = ? AND pos_type = 7 AND db_name = ?';
		$stmnt = $this->db()->prepare($sql);
		$stmnt->execute(array($this->businessid, $this->db_name));
		$stmnt->setFetchMode(PDO::FETCH_COLUMN, 0);
		return(implode(',', $stmnt->fetchAll()));
	}

	/**
	 * Check if the current client has any children records. If there are it will
	 * return true, otherwise false.
	 *
	 * @return bool
	 */
	public function hasChildren()
	{
		$stmnt = $this->_db->prepare('SELECT COUNT(client_programid) AS total FROM client_programs WHERE parent = :id');
		$stmnt->execute(array(':id' => $this->client_programid));
		$ret = $stmnt->fetchColumn();
		$stmnt->closeCursor();
		return((bool)$ret);
	}

	public function hasErrors()
	{
		$stmnt = $this->_db->prepare('SELECT COUNT(id) FROM client_errors WHERE client_id = ? AND isRead = 0');
		$stmnt->execute(array($this->client_programid));
		$ret = $stmnt->fetchColumn();
		$stmnt->closeCursor();
		return((bool)$ret);
	}

	public function isKiosk()
	{
		return(($this->pos_type->pos_type == 6 || $this->pos_type->pos_type == 7)? true : false);
	}

	public function logError($msg, $trace)
	{
		$ret = false;

		$stmnt = $this->_db->prepare('INSERT INTO client_errors (client_id, error_message, error_trace) VALUES(?, ?, ?)');
		if ($stmnt->execute(array($this->client_programid, $msg, $trace))) {
			$ret = (bool)$stmnt->rowCount();
		}

		$stmnt->closeCursor();
		return($ret);
	}

	/**
	 * Create a new object for the pos_type field. This uses the PosType_Record
	 * and creates a new object using the ID.
	 *
	 * @return PosType_Record
	 */
	public function posType()
	{
		return($this->pos_type);
	}

	/**
	 * Create a new object from the receive_status field. This uses the
	 * ReceiveStatus_Record and creates the object from the ID stored here.
	 *
	 * @return ReceiveStatus_Record
	 */
	public function receiveStatus()
	{
		require_once('Manage/ReceiveStatus/Record.php');
		$status = new ReceiveStatus_Record($this->receive_status);
		return($status);
	}

	/**
	 * Just a helper function to format the receive_time field. If the timestamp
	 * is 0000-00-00 00:00:00 this returns an empty string.
	 *
	 * @return string
	 */
	public function receiveTime()
	{
		return($this->receive_time == '0000-00-00 00:00:00')? '' : $this->receive_time;
	}

	/**
	 * Update the table to show the client tried sending an update. Set $error
	 * to true if it failed.
	 *
	 * @param $error bool Set to true when an error occours.
	 * @return bool
	 */
	public function setSendStatus($error = false)
	{
		/*if ($error === true) {
			$sql = 'UPDATE client_programs SET receive_status = 3 WHERE client_programid = :id';
		} else {
			$sql = 'UPDATE client_programs SET send_status = 0, send_schedule = 0, receive_status = 2, receive_time = NOW(), inv = 0 WHERE client_programid = :id';
		}

		$stmnt = $this->_db->prepare($sql);
		$stmnt->execute(array(':id' => $this->client_programid));
		$ret = (bool)$stmnt->rowCount();
		$stmnt->closeCursor();
		return($ret);*/

		if ($error === true) {
			$this->receive_status = 3;
		} else {
			$this->send_status = 0;
			$this->send_schedule = 0;
			$this->receive_status = 2;
			$this->inv = 0;
		}
		$this->receive_time = date('Y-m-d H:i:s');
		return($this->save());
	}

	/**
	 * Doesn't do much of anything yet... just returns some HTML for an error
	 * image if no schedule is setup yet.
	 *
	 * @return string
	 */
	public function showSchedule()
	{
		$sql = "SELECT COUNT(id) AS totalcount FROM client_schedule WHERE client_programid = '$this->client_programid'";
		$stmnt = $this->_db->query($sql);

		$totalcount = $stmnt->fetchColumn();
		$stmnt->closeCursor();

		if ($totalcount == 0) return('<img src="error.gif" height="16" width="16" border="0" alt="No Schedule Setup">');
		else return('');
	}

	/**
	 * Return HTML for an image of the status type for the current client. This
	 * is based uppon the send_status field.
	 *
	 * @return string
	 */
	public function showSendStatus()
	{
		if ($this->send_status == 2) return('<img class="current" src="clock.gif" height="16" width="16" alt="Waiting for Data" title="Waiting for Data">');
		elseif ($this->send_status == 1) return('<img class="current" src="gear.gif" height="18" width="16" alt="Waiting for Update" title="Waiting for Update">');
		elseif ($this->send_status == 3) return('<img class="current" src="restart-icon.png" height="16" width="16" alt="Kiosk Restarting..." title="Kiosk Restarting...">');
		else return('<img class="current" src="blank.gif" height="16" width="16">');
		//else return('<a href="update.php?pid='.$this->client_programid.'"><img src="wrench.gif" border="0" height="17" width="16" alt="Send Updates" title="Send Updates"></a>');
	}

	public function showSendStatus2()
	{
		if ($this->send_status == 2) return('clock.gif');
		elseif ($this->send_status == 1) return('gear.gif');
		elseif ($this->send_status == 3) return('restart-icon.png');
		else return('blank.gif');
	}

	/**
	 * Return the image name for the status of the last run. This is based off
	 * of the time difference retreived from the timeDiff() method.
	 *
	 * @return string
	 * @see timeDiff
	 */
	public function statusImg()
	{
		$send = $this->timeDiff('send');
		$recv = $this->timeDiff('receive');
		if (substr($send,0,2)>=1 || substr($recv,0,2)>=48 || $this->send_time == "0000-00-00 00:00:00") return('reddot.gif');
		elseif(substr($send,3,2)>=15 || substr($recv,0,2)>=24) return('yellowdot.gif');
		else return('greendot.gif');
	}

	/**
	 * Run a query against the database to find the time difference between now
	 * and the last send time from the client.
	 *
	 * @return string
	 */
	public function timeDiff($field = 'send')
	{
		$sql = 'SELECT TIMEDIFF(now(),'.$field.'_time) AS status FROM client_programs WHERE client_programid = :id';
		$stmnt = $this->_db->prepare($sql);
		$stmnt->execute(array(':id' => $this->client_programid));
		$ret = $stmnt->fetchColumn();
		$stmnt->closeCursor();
		return($ret);
	}
}
