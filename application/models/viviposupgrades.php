<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of viviposupgrades
 *
 * @author Eric Gach <eric@essential-elements.net>
 */
class ViviposUpgradesModel extends Manage_Model_Abstract
{
	protected $_hasMany = array(
		'Clients' => array(
			'class' => 'ViviposUpgradeClient',
			'foreignKey' => 'ViviposUpdate'
		)
	);

	protected static $_pk = 'Id';
	protected static $_table = 'vivipos_upgrades';

	/**
	 * Return a full path to the file itself.
	 *
	 * @return string
	 */
	public function __toString()
	{
		return(SITECH_APP_PATH.'/upgrades/'.$this->FileName);
	}

	public function delete()
	{
		// Added functionality to delete the file too
		unlink((string)$this);
		parent::delete();
	}

	public function hasClient()
	{
		foreach ($this->Clients as $c) {
			if (!empty($c->ClientProgram->client_ip) && $c->ClientProgram->client_ip == $_SERVER['REMOTE_ADDR']) return(true);
		}

		return(false);
	}

	public static function getByFilename($filename)
	{
		return(self::get('FileName = '.self::db()->quote($filename), true));
	}
}
