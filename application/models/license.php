<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of license
 *
 * @author eric
 */
class LicenseModel extends \Manage\Model\Sentinel
{
	protected static $_pk = 'id';
	protected static $_table = 'registration';
	protected static $_table_shortname = 'r';	

	public static function getTerminals($show_only=null)
	{
		if(! $show_only)
		{
			/*$query = sprintf(
					'SELECT
						%1$s.id,
						%1$s.machine_id AS term_id,
						%1$s.status,
						if(%1$s.status="UNVERIFIED", 0, 1) as is_current,
						cp.name AS term_name,
						cp.businessid as bus_id,
						b.businessname as bus_name
					FROM %2$s AS %1$s
					LEFT OUTER JOIN mburris_manage.client_programs cp ON %1$s.machine_id = cp.client_programid
					//LEFT OUTER JOIN mburris_businesstrack.business b ON cp.businessid = b.businessid
					LEFT OUTER JOIN aerisposv2_ee.business b ON cp.businessid = b.businessid
					WHERE %1$s.status = "UNVERIFIED"
					ORDER BY is_current, %1$s.status, %1$s.machine_id;'
					, static::$_table_shortname, static::$_table
					);*/

			$query = sprintf(
				'SELECT
					%1$s.id,
					%1$s.machine_id AS term_id,
					%1$s.status,
					if(%1$s.status="UNVERIFIED", 0, 1) as is_current,
					cp.name AS term_name,
					cp.businessid as bus_id,
					cp.db_name as "database"
				FROM %2$s AS %1$s
				LEFT OUTER JOIN mburris_manage.client_programs cp ON %1$s.machine_id = cp.client_programid
				WHERE %1$s.status = "UNVERIFIED"
				ORDER BY is_current, %1$s.status, %1$s.machine_id;'
				, static::$_table_shortname, static::$_table
				);

			$stmnt = self::db()->prepare($query);
			$stmnt->setFetchMode(PDO::FETCH_CLASS, 'LicenseModel');
			$stmnt->execute();

			return $stmnt->fetchAll();

		} else {
			/*$sql = 'SELECT
						r.id,
						r.machine_id AS term_id,
						r.status,
						cp.name AS term_name,
						cp.businessid AS bus_id,
						b.businessname AS bus_name
					FROM registration AS r
					LEFT OUTER JOIN mburris_manage.client_programs cp ON r.machine_id = cp.client_programid
					LEFT OUTER JOIN aerisposv2_ee.business b ON cp.businessid = b.businessid';*/

			$sql = 'SELECT
						r.id,
						r.machine_id AS term_id,
						r.status,
						cp.name AS term_name,
						cp.businessid AS bus_id,
						cp.db_name as "database"
					FROM registration AS r
					LEFT OUTER JOIN mburris_manage.client_programs cp ON r.machine_id = cp.client_programid';

			if($show_only != 'ALL')
			{
				$sql .= ' WHERE r.status = ? ';
			}

			$sql .= ' ORDER BY r.status, r.machine_id;';

			$stmnt = self::db()->prepare($sql);

			if($show_only != 'ALL')
			{
				$stmnt->bindParam(1, $show_only, PDO::PARAM_STR);
			}

			$stmnt->execute();

			$terminals = $stmnt->fetchAll(PDO::FETCH_OBJ);

			$html = '';

			foreach ($terminals as $term)
			{
				$html .= '<tr>
							<td>'.$term->term_id.'</td>
							<td>'.$term->term_name.'</td>
							<td>'.$term->bus_id.'</td>
							<td>'.$term->database.'</td>
							<td>
								<select name="term_status_'.$term->id.'" class="status_sel" id="term_status_'.$term->id.'">
									<option value="CURRENT"';
										if($term->status == 'CURRENT'){$html .= ' selected="selected"';}
										$html .= '>current</option>
									<option value="TERMINATED"';
										if($term->status == 'TERMINATED'){$html .= ' selected="selected"';}
										$html .= '>terminated</option>
									<option value="PASTDUE"';
										if($term->status == 'PASTDUE'){$html .= ' selected="selected"';}
										$html .= '>past due</option>
									<option value="INVALID"';
										if($term->status == 'INVALID'){$html .= ' selected="selected"';}
										$html .= '>invalid</option>
									<option value="UNVERIFIED"';
										if($term->status == 'UNVERIFIED'){$html .= ' selected="selected"';}
										$html .= '>unverified</option>
								</select>
							</td>
							<td align="left">
								<button id="save_status_'.$term->id.'" class="save_btn ui-button ui-widget ui-corner-all ui-state-default ui-button-text-icon-primary" style="margin: 5px; padding: 2px 10px;">
									<span class="ui-button-text">Save</span>
									<span class="ui-button-icon-primary ui-icon ui-icon-disk" />
								</button>
							</td>
						</tr>';
			}

			if(!$html)
			{
				$html = '<tr><td colspan="6">No results for '.$show_only.'</td></tr>';
			}

			print $html;
		}
	}

	public static function saveTermStatus($termid=null, $status=null)
	{
		$sql = 'UPDATE registration as r
				SET r.status = ?
				WHERE r.id = ?';

		$stmnt = self::db()->prepare($sql);
		$stmnt->bindParam(1, $status, PDO::PARAM_STR);
		$stmnt->bindParam(2, $termid, PDO::PARAM_INT);
		$stmnt->execute();

		if($stmnt->rowCount() == 1)
		{
			print json_encode( array('success' => true, 'status' => $status) );
		} else {
			print json_encode( array('success' => false, 'status' => $status) );
		}
		
		$user = \EE\Controller\Base::getSessionCookieVariable('id');
		$get_username_sql = "SELECT username FROM mburris_manage.login WHERE loginid = :user LIMIT 1";
		$stmnt3 = self::db()->prepare($get_username_sql);
		$stmnt3->execute(array('user'=>$user));
		$username = $stmnt3->fetchColumn();
		
		$get_manage_id = "SELECT machine_id FROM registration WHERE id = :termid";
		$stmnt1 = self::db()->prepare($get_manage_id);
		$stmnt1->execute(array('termid'=>$termid));
		$manage_id = $stmnt1->fetchColumn();
		
		$audit_registration_sql = "INSERT IGNORE INTO ee_sentinel.audit_registration ( machine_id , username , registration_status , date_time ) VALUES ( :manage_id , :username , :status , now() )";
		$stmnt2 = self::db()->prepare($audit_registration_sql);
		$stmnt2->execute(array(
					'manage_id'=>$manage_id,
					'username'=>$username,
					'status'=>$status,
		));
		
	}
}
