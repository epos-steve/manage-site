<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class SoftwareVersionModel extends Manage_Model_Abstract
{
	protected static $_pk = 'SoftwareVersionId';
	protected static $_table = 'software_version';

	public function save()
	{
		return false;
	}

	public static function getLastest($type, $critical = false)
	{
		$query = 'Type = \'' . $type. '\'';
		if($critical)$query .= ' AND critical = 1';
		$query .= ' ORDER BY Version DESC';
		return SoftwareVersionModel::get($query, true);
	}
}
SoftwareVersionModel::db(new Manage_DB('database'));