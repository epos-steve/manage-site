<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of login
 *
 * @author eric
 */
class LoginModel extends Manage_Model_Abstract
{
	protected static $_pk = 'loginid';

	/**
	 * Holder for an instance of the current user.
	 *
	 * @var LoginModel
	 */
	protected static $_currentUser;

	public static function getCurrentUser()
	{
		if (empty(self::$_currentUser)) {
			if (!empty($_POST['username'])) {
				self::$_currentUser = LoginModel::getLogin($_POST['username'], $_POST['password']);
			} elseif (!empty($_COOKIE['id'])) {
				self::$_currentUser = LoginModel::getLogin($_COOKIE['id'], $_COOKIE['hash']);
			}
		}

		if (empty(self::$_currentUser)) {
			self::$_currentUser = new LoginModel();
		}

		return(self::$_currentUser);
	}

	public static function getLogin($user = 0, $pass = null)
	{
		if (ctype_digit($user)) {
			$sql = 'SELECT loginid, username, password, isAdmin, permissions FROM login WHERE loginid = ? AND MD5(CONCAT(username, password)) = ?';
			$stmnt = self::db()->prepare($sql);
			$stmnt->bindValue(1, $user, PDO::PARAM_INT);

		} else {
			$sql = 'SELECT loginid, username, password, isAdmin, permissions FROM login WHERE username = ? AND password = ?';
			$pass = sha1($pass);
			$stmnt = self::db()->prepare($sql);
			$stmnt->bindValue(1, $user, PDO::PARAM_STR);
		}

		$stmnt->bindValue(2, $pass, PDO::PARAM_STR);
		$stmnt->setFetchMode(PDO::FETCH_CLASS, 'LoginModel');
		$stmnt->execute();
		if (($obj = $stmnt->fetch()) !== false) {
			if (!isset($_COOKIE['id']))
			{
				setcookie('id', $obj->loginid);
			}
			if (!isset($_COOKIE['hash']))
			{
				setcookie('hash', md5($obj->username.$obj->password));
			}
			return($obj);
		}

		return(null);
	}

	public function isLoggedIn()
	{
		if (empty($this->loginid)) {
			return(false);
		} else {
			return(true);
		}
	}

	public function isAdmin()
	{
		return((bool)$this->isAdmin);
	}

	public function requireLogin()
	{
		if (!$this->isLoggedIn()) {
			header('Location: '.SITECH_BASEURI.'dashboard/login');
			exit;
		}
	}

}
