<?php

class SoftwareExceptionModel extends Manage_Model_Abstract
{
	const SOURCE_UNKNOWN = 'UNKNOWN';
	const SOURCE_NOT_SPECIFIED = 'NOT SPECIFIED';
	const SOURCE_WEBSERVICE = 'WEBSERVICE';

	private static $_sources = array(
		static::SOURCE_UNKNOWN,
		static::SOURCE_NOT_SPECIFIED,
		static::SOURCE_WEBSERVICE,
			);

	protected static $_pk = 'SoftwareExceptionId';
	protected static $_table = 'software_exception';


	public function validate()
	{
		if(empty($this->SourceFrom))
		{
			$this->SourceFrom = ReportedExceptionModel::SOURCE_NOT_SPECIFIED;
		}
		elseif(!in_array($this->SourceFrom, static::$_sources))
		{
			$this->SourceFrom = static::SOURCE_UNKNOWN;
		}

		if(!empty($this->{static::$_pk}))return false;

		$this->SourceIp = $_SERVER['REMOTE_ADDR'];
		$this->TimestampReported = time();

		parent::validate();
	}
}
SoftwareExceptionModel::db(new Manage_DB('database'));
