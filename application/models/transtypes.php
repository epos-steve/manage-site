<?php
/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of transtypes
 *
 * @author Eric Gach <eric@essential-elements.net>
 */
class TranstypesModel extends Manage_Model_Abstract
{
	protected static $_pk = 'Id';
	protected static $_table = 'mapping_cc_transactions_types';
}
