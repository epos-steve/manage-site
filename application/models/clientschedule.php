<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clientschedule
 *
 * @author eric
 */
class ClientScheduleModel extends Manage_Model_Abstract
{
	protected $_belongsTo = array(
		'client_programid' => array(
			'class' => 'ClientPrograms',
			'foreignKey' => 'client_programid'
		)
	);

	static public $_pk = 'id';

	static public $_table = 'client_schedule';

	public static function scheduleReady()
	{
		$now = time();
		$day = date('N', $now);
		$endtime = date('H:i:59', strtotime('-1 minute', $now));
		$starttime = date('H:i:00', strtotime('+5 minutes', $now));

		$sql = '
UPDATE
	client_schedule AS cs,
	client_programs AS cp
SET
	cp.send_status = 2,
	cp.send_schedule = cs.id
WHERE
	cp.client_programid = cs.client_programid
	AND (
		day = 0
		OR day = ?
	)
	AND `time` > ?
	AND `time` < ?
	AND sync_version = 1';

		$stmnt = self::db()->prepare($sql);
		$stmnt->bindParam(1, $day, PDO::PARAM_INT);
		$stmnt->bindParam(2, $endtime, PDO::PARAM_STR);
		$stmnt->bindParam(3, $starttime, PDO::PARAM_STR);
		$stmnt->execute();
		echo 'Set '.$stmnt->rowCount().' clients to update';
		$stmnt->closeCursor();
		return(false);
	}
}
