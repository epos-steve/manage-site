<?php
class ClientCheckinModel extends Manage_Model_Abstract
{
	protected static $_pk = 'client_programid';
	protected static $_table = 'client_checkin';

	public function save()
	{
		$save = static::db()->prepare($sql = 'INSERT INTO '.static::$_table.' VALUES(:cid, :total) ON DUPLICATE KEY UPDATE total = :total');
		return($save->execute(array('cid' => $this->client_programid, 'total' => $this->total)));
	}
}
