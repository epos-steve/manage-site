<?php

/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of mappingtype
 *
 * @author Eric Gach <eric@php-oop.net>
 */
class MappingTypeModel extends Manage_Model_Abstract
{
	protected static $_pk = 'id';
	protected static $_table = 'mapping_type';

	public static function get($where = null, $only_one = false) {
		if (strstr($where, 'ORDER BY') === false) {
			$where .= '`order` > 0 ORDER BY `order`, name';
		}
		
		return(parent::get($where, $only_one));
	}

	public function getMappingByClient($client)
	{
		if ($client instanceof ClientProgramsModel) {
			$client = $client->client_programid;
		} elseif (!is_numeric($client)) {
			return(false);
		}

		return(MappingModel::getByMappingType($this->id, $client));
	}
}
