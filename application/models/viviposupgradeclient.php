<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of viviposupgradeclients
 *
 * @author Eric Gach <eric@essential-elements.net>
 */
class ViviposUpgradeClientModel extends Manage_Model_Abstract
{
	protected $_belongsTo = array(
		'ClientProgram' => array(
			'class' => 'ClientPrograms',
			'foreignKey' => 'client_programid'
		),
		'ViviposUpgrade' => array(
			'class' => 'ViviposUpgrades',
			'foreignKey' => 'Id'
		)
	);

	protected static $_pk = array('ClientProgram', 'ViviposUpdate');
	protected static $_table = 'vivipos_upgrade_client';

	public function getStatus()
	{
		$status = array();

		if ((int)$this->Status & 1) {
			$status[] = 'Downloaded';
		}

		if ((int)$this->Status & 2) {
			$status[] = 'Installed';
		}

		return(implode(' & ', $status));
	}

	/*public function save()
	{
		$sql = 'REPLACE INTO '.self::$_table.' ';
		$fields = array_keys($this->_fields);
		$values = array_values($this->_fields);
		foreach ($values as &$val) {
			if (is_a($val, 'ClientProgramsModel')) $val = $val->client_programid;
			if (is_a($val, 'ViviposUpgradeModel')) $val = $val->Id;
			$val = $this->_db->quote($val);
		}
		$sql .= '('.implode(',', $fields).') VALUES('.implode(',', $values).')';
		return((bool)$this->_db->exec($sql));
	}*/
}
