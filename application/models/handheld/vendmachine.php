<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of loginroute
 *
 * @author Alexander
 */
class Handheld_VendMachineModel extends Manage_Model_Abstract
{
	protected static $_pk = 'machineid';
	protected static $_table = 'vend_machine';
	protected static $_table_shortname = 'vm';

	public static function getByLoginRouteIDAndBusinessId($login_routeid, $businessid = null)
	{
		$query = sprintf('SELECT %2$s.machineid as MachineId, mbl.ordertype as OrderType, %2$s.name as MachineName,
				mbl.businessid as BusinessId, mbl.client_programid as TerminalNo, mbl.machine_num as MachineNum
				FROM machine_bus_link AS mbl
				JOIN %1$s AS %2$s ON
				(
					%2$s.login_routeid = :login_routeid
					AND %2$s.machine_num = mbl.machine_num
					AND %2$s.is_deleted = 0',
				static::$_table, static::$_table_shortname);

		$values = array(':login_routeid' => intval($login_routeid));

		if($businessid != null)
		{
			$query .= ' AND mbl.businessid = :businessid';
			$values[':businessid'] = intval($businessid);
		}

		$query .= ') ORDER BY mbl.businessid, mbl.ordertype;'
		;

		$stmt = static::db()->prepare($query);
		$stmt->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Machine');
		$stmt->execute($values);

		return $stmt->fetchAll();
	}
}
Handheld_VendMachineModel::db(new Manage_DB('treat db'));
