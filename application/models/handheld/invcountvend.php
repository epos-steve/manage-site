<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of loginroute
 *
 * @author Alexander
 */
class Handheld_InvCountVendModel extends Manage_Model_Abstract
{
	protected static $_pk = 'id';
	protected static $_table = 'inv_count_vend';
	protected static $_table_shortname = 'icv';

	public static function SetUnitCost(Handheld_InvCountVendModel $inv, $businessid)
	{
		$query = sprintf('SELECT menu_items_price.cost FROM menu_items_price
					JOIN menu_items_new ON menu_items_new.id = menu_items_price.menu_item_id
					WHERE menu_items_new.businessid = %1$d AND menu_items_new.item_code = \'%2$s\'',
				$businessid, $inv->item_code);

		$stmnt = self::db()->query($query);
		$cost = $stmnt === false ? $stmnt->fetchColumn() : 0;
		$inv->unit_cost = $cost;
	}
}
Handheld_InvCountVendModel::db(new Manage_DB('treat db'));
