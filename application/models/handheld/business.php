<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of loginroute
 *
 * @author Alexander
 */
class Handheld_BusinessModel extends Manage_Model_Abstract
{
	protected static $_pk = 'businessid';
	protected static $_table = 'business';
	protected static $_table_shortname = 'b';

	public static function getByLoginRouteID($login_routeid)
	{
		$query = sprintf(
				'SELECT DISTINCT %1$s.businessid as BusinessId, %1$s.businessname AS BusinessName
				FROM %3$s AS %1$s
				JOIN machine_bus_link AS mbl
				JOIN vend_machine AS vm  ON
				(
					%1$s.businessid = mbl.businessid
					AND mbl.machine_num = vm.machine_num
					AND vm.login_routeid = %2$d
					AND vm.is_deleted = 0
				);'
				, static::$_table_shortname, $login_routeid, static::$_table
				);

		$stmt = static::db()->prepare($query);
		$stmt->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Business');
		$stmt->execute();
		return $stmt->fetchAll();
	}
}
Handheld_BusinessModel::db(new Manage_DB('treat db'));
