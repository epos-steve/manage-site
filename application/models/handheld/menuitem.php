<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of loginroute
 *
 * @author Alexander
 */
class Handheld_MenuItemModel extends Manage_Model_Abstract
{
	protected static $_pk = 'id';
	protected static $_table = 'menu_items_new';
	protected static $_table_shortname = 'min';

	public static function getMenuItemsByBusinessId($businessid)
	{
		set_time_limit(90);

		$query = sprintf(
				'SELECT
				%1$s.name AS Name,
				%1$s.businessid AS BusinessId,
				%1$s.item_code AS ItemCode,
				%1$s.upc AS Barcode,
				%1$s.pos_id AS PosId,
				%1$s.ordertype AS OrderType,
				%1$s.active AS Inactive,
				mbl.machine_num AS MachineNum,
				COALESCE(fillsTbl.prefills, 0) AS ExpectedFills
				FROM %2$s AS %1$s
				JOIN machine_bus_link AS mbl ON
				(
					%1$s.businessid = %4$d
					AND %1$s.businessid = mbl.businessid
					AND %1$s.ordertype = mbl.ordertype
				)
				LEFT JOIN
				(
					SELECT SUM(md.qty) AS prefills, md.menu_items_new_id AS id
					FROM machine_orderdetail AS md
					JOIN machine_order AS mo ON
					(
						mo.expected_delivery = CURDATE()
						AND md.orderid = mo.id
					)
					GROUP BY md.menu_items_new_id
				) fillsTbl
				ON
				(
					%1$s.active = 0
					AND %1$s.%3$s = fillsTbl.id
				)
				WHERE %1$s.item_code <> \'\' AND %1$s.item_code <> \'0\'
				ORDER BY %1$s.item_code, mbl.machine_num;',
				static::$_table_shortname,
				static::$_table,
				static::$_pk,
				$businessid
			);

		$stmt = static::db()->query($query);

		$items = $stmt->fetchAll(PDO::FETCH_CLASS, 'Item');

		foreach($items as $item)
		{
			if($item->Inactive != 0)continue;
			static::setOnHand($item);
			static::setFillsAndWaste($item);
			static::setSold($item);
			$item->ExpectedCount = intval($item->OnHand) + intval($item->Fills) - intval($item->Waste) - intval($item->Sold);
		}

		return $items;
	}

	protected static function setOnHand(Item $item)
	{
		$query = sprintf(
			'SELECT onhand, date_time
			FROM inv_count_vend
			WHERE machine_num = \'%1$s\'
			AND item_code = %2$d
			AND is_inv = 1;',
			$item->MachineNum,
			$item->ItemCode
		);

		$stmt = static::db()->query($query);

		$array = $stmt->fetch(PDO::FETCH_BOTH);
		$item->OnHand = $array[0] ?:  0;
		$item->LastCountDate = $array[1] ?: date('Y-m-d H:i:s', strtotime('-1 year'));
	}

	protected static function setFillsAndWaste(Item $item)
	{
		$query = sprintf(
			'SELECT SUM(waste) AS waste, SUM(fills) as fills
			FROM inv_count_vend
			WHERE machine_num = \'%1$s\'
			AND item_code = %2$d
			AND date_time BETWEEN \'%3$s\' AND NOW();',
			$item->MachineNum,
			$item->ItemCode,
			$item->LastCountDate
		);


		$stmt = static::db()->query($query);
		$array = $stmt->fetch(PDO::FETCH_BOTH);

		$item->Waste = $array[0] ?: 0;
		$item->Fills = $array[1] ?: 0;
	}

	protected static function setSold(Item $item)
	{
		$query = sprintf(
			'SELECT SUM(cd.quantity) AS sold
			FROM checkdetail AS cd
			JOIN checks AS c
			ON
			(
				cd.item_number = %1$d
				AND cd.bill_number = c.check_number
				AND cd.businessid = %2$d
				AND c.businessid = %2$d
				AND c.bill_datetime BETWEEN \'%3$s\' AND NOW()
			);',
			$item->PosId,
			$item->BusinessId,
			$item->LastCountDate ?: date('Y-m-d H:i:s', strtotime('-1 year'))
		);

		$stmt = static::db()->query($query);
		$array = $stmt->fetch(PDO::FETCH_BOTH);
		$item->Sold = $array[0];
	}

	public static function setActive($itemCode, $businessId)
	{
		$query = sprintf('UPDATE %1$s SET active = 0 WHERE businessid = %2$d AND item_code = \'%3$s\'',
				static::$_table, intval($businessId), $itemCode
				);

		static::db()->exec($query);
	}
}
Handheld_MenuItemModel::db(new Manage_DB('treat db'));
