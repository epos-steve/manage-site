<?php

/**
 * Description of loginroute
 *
 * @author Alexander
 */
class Handheld_LoginRouteModel extends Manage_Model_Abstract
{
	protected static $_pk = 'login_routeid';
	protected static $_table = 'login_route';
	protected static $_table_shortname = 'lr';

	public static function getByLogin($username, $password)
	{
		$query = sprintf('SELECT *, %2$s as Id FROM %1$s WHERE username=:username AND MD5(password)=:password;',
				static::$_table, static::$_pk
				);

		$stmt = static::db()->prepare($query);
		$stmt->setFetchMode(PDO::FETCH_LAZY);
		$stmt->execute(array(':username' => $username, ':password' => $password));

		return $stmt->fetch();
	}
}
Handheld_LoginRouteModel::db(new Manage_DB('treat db'));
