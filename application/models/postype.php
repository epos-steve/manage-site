<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of postype
 *
 * @author Eric Gach <eric@essential-elements.net>
 */
class PosTypeModel extends Manage_Model_Abstract
{
	protected static $_pk = 'pos_type';
	protected static $_table = 'pos_type';

	public function isRM()
	{
		return((substr($this->name, 0, 2) == 'RM')? true : false);
	}

	public function isCatapult()
	{
		return((strstr($this->name, 'Catapult'))? true : false);
	}

	public function isSLHS()
	{
		return((bool)$this->is_slhs);
	}

	public function isVolante()
	{
		return((strstr($this->name, 'Volante'))? true : false);
	}
}
