<?php
/**
 * Description of mapping
 *
 * @author Eric Gach <eric@essential-elements.net>
 */
class MappingModel extends Manage_Model_Abstract
{
	protected $_belongsTo = array(
		'tbl' => array('class' => 'MappingTable', 'foreignKey' => 'id'),
		'type' => array('class' => 'MappingType', 'foreignKey' => 'id')
	);

	protected static $_pk = 'id';
	protected static $_table = 'mapping';

	public static function getByClient($client)
	{
		if ($client instanceof ClientProgramsModel) {
			$client = $client->client_programid;
		} elseif (!is_numeric($client)) {
			return(false);
		}

		return(self::get('client_programid = '.self::db()->quote($client).' AND tbl > 0'));
	}

	public static function getByMappingType($type, $client = null)
	{
		if ($type instanceof MappingTypeModel) {
			$type = $type->id;
		} elseif (!is_numeric($type)) {
			return(false);
		}

		if ($client instanceof ClientProgramsModel) {
			$client = $client->client_programid;
		} elseif (!is_numeric($client)) {
			$client = null;
		}

		$where = 'type = '.self::db()->quote($type);
		if (!empty($client)) $where .= ' AND client_programid = '.self::db()->quote($client);
		return(self::get($where));
	}

	public static function getByPosId($pos_id, $type, $client)
	{
		if ($type instanceof MappingTypeModel) {
			$type = $type->id;
		} elseif (!is_numeric($type)) {
			return(false);
		}

		if ($client instanceof ClientProgramsModel) {
			$client = $client->client_programid;
		} elseif (!is_numeric($client)) {
			$client = null;
		}

		$where = 'posid = '.self::db()->quote($pos_id).' AND type = '.self::db()->quote($type).' AND client_programid = '.self::db()->quote($client);
		return(self::get($where, true));
	}
}
