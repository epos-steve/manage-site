<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of clienterrors
 *
 * @author Eric Gach <eric@essential-elements.net>
 */
class ClientErrorsModel extends Manage_Model_Abstract
{
	protected static $_pk = 'id';
	protected static $_table = 'client_errors';

	public function markRead()
	{
		$sql = 'UPDATE client_errors SET isRead = 1 WHERE id = ?';
		$this->db()->exec($sql, array($this->id));
	}

	public function message()
	{
		return(htmlentities($this->error_message));
	}

	public function trace()
	{
		return(htmlentities($this->error_trace));
	}
}
