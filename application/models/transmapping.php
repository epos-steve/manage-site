<?php
/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of transmapping
 *
 * @author Eric Gach <eric@php-oop.net>
 */
class TransMappingModel extends Manage_Model_Abstract
{
	protected static $_pk = 'id';
	protected static $_table = 'mapping_cc_transactions';

	public static function getByClient(ClientProgramsModel $client)
	{
		$types = array();
		$array = static::get('client_programid = '.$client->client_programid);
		foreach ($array as $type) {
			$types[$type->type] = $type;
		}
		return($types);
	}
}
