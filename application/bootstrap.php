<?php
defined('SITECH_APP_PATH') || define('SITECH_APP_PATH', dirname(__FILE__) );
defined('SITECH_LIB_PATH') || define('SITECH_LIB_PATH', dirname(SITECH_APP_PATH).'/lib');
defined('SITECH_VENDOR_PATH') || define('SITECH_VENDOR_PATH', dirname(SITECH_APP_PATH).'/vendors');

set_include_path(
	SITECH_LIB_PATH
    .PATH_SEPARATOR.SITECH_VENDOR_PATH.'/SiTech/lib'
    .PATH_SEPARATOR.SITECH_VENDOR_PATH.'/treat-lib'
    .PATH_SEPARATOR.SITECH_VENDOR_PATH.'/treat-lib/src'
    .PATH_SEPARATOR.SITECH_VENDOR_PATH.'/lib-treat/src'
    .PATH_SEPARATOR.SITECH_VENDOR_PATH.'/lib-treat/lib'
    .PATH_SEPARATOR.SITECH_VENDOR_PATH.'/Treat/lib'
    .PATH_SEPARATOR.SITECH_VENDOR_PATH.'/EE/library'
    .PATH_SEPARATOR.SITECH_VENDOR_PATH.'/Treat/htdocs/ta/lib'
	.PATH_SEPARATOR.get_include_path());

// Permissions
define('MANAGE_PERMISSION_UPLOAD', 1);

$composerDir = __DIR__ . '/../vendor/autoload.php';
if ( file_exists( $composerDir ) ) {
	require_once $composerDir;
}
require_once('EE/Loader.php');
\EE\Loader::registerAutoload();
$config = Manage_ConfigParser::singleton();
$base_url = $config->get('main', 'base_url');
if ($base_url[strlen($base_url) - 1] != '/') {
	$base_url .= '/';
}

$uri = new SiTech_Uri($base_url);
define('SITECH_BASEURI', $uri->getScheme().'://'.$uri->getHost().($uri->getPort() != '80'? ':'.$uri->getPort() : null).$uri->getPath());
define('IN_PRODUCTION', $config->getBool('main', 'production'));
define('MANAGE_VERSION', '1.0.0');
define('SITECH_DEFAULT_CONTROLLER', 'dashboard');

Manage_Model_Abstract::db(Manage_DB_Proxy::singleton());

# fix the config sections for the various model autoloaders
\EE\Model\Base::setDbConfigSection( 'treat db' );
\EE\Model\AbstractModel\Manage::setDbConfigSection( 'database' );
\EE\Model\AbstractModel\ProcessHost::setDbConfigSection( 'process_host' );
\EE\Model\AbstractModel\Treat::setDbConfigSection( 'treat db' );

\Essential\Treat\Db\ProxyOld::setConfigClass(
	$class = 'Treat_Config'
	,$method = 'singleton'
	,$section_method = 'getSection'
	,$section = 'treat db'
);
\Essential\Treat\Db\ProxyOld\ProcessHost::setConfigClass(
	$class = 'Treat_Config'
	,$method = 'singleton'
	,$section_method = 'getSection'
	,$section = 'process_host'
);

