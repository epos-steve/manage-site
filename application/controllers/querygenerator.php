<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of queryGenerator
 *
 * @author Eric Gach <eric@essential-elements.net>
 */
class queryGeneratorController extends Manage_Controller_Abstract
{
	public function index()
	{
		$this->_layout = 'general.tpl';
	}

	public function upload()
	{
		$out = array();
		if (($fp = fopen($_FILES['csvFile']['tmp_name'], 'r')) !== false) {
			while ($data = fgetcsv($fp, null, ',', '"')) {
				list($id, $bal) = $data;
				$out[] = "UPDATE ChargeCustomer SET CHG_Balance=$bal WHERE CHG_PK = (SELECT CUS_PK FROM Customers WHERE CUS_AccountNum = '$id');";
			}
		}

		$this->_view->setAttribute(EEF_Template::ATTR_DOWNLOAD_AS_ATTACHMENT, 'balances.sql');
		$this->_view->assign('out', $out);
	}
}
