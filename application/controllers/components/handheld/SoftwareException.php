<?php

class SoftwareException
{
	/**
	 * @var string
	 */
	public $SourceReported = '';

	/**
	 *
	 * @var string
	 */
	public $ExceptionType = '';

	/**
	 *
	 * @var string
	 */
	public $ExceptionDateTime = '';

	/**
	 *
	 * @var string
	 */
	public $ExceptionMessage = '';

	/**
	 *
	 * @var string
	 */
	public $ExceptionStackTrace = '';
}
?>
