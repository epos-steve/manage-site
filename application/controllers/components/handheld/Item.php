<?php

class Item
{
	/**
	 *
	 * @var string
	 */
	public $ItemCode = '';

	/**
	 *
	 * @var string
	 */
	public $Name = '';

	/**
	 *
	 * @var string
	 */
	public $Barcode = '';

	/**
	 *
	 * @var int
	 */
	public $BusinessId = 0;

	/**
	 *
	 * @var int
	 */
	public $OrderType = 0;

	/**
	 *
	 * @var int
	 */
	public $Inactive = 0;

	/**
	 *
	 * @var string
	 */
	public $MachineNum = '';

	/**
	 *
	 * @var float
	 */
	public $ExpectedFills = 0;

	/**
	 *
	 * @var float
	 */
	public $ExpectedCount = 0;

	public $PosId;
	public $OnHand;
	public $LastCountDate;
	public $Fills;
	public $Waste;
	public $Sold;
}
?>
