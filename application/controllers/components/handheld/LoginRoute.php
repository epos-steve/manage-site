<?php

class LoginRoute
{
	/**
	 * The id of the login route
	 *
	 * @var int
	 */
	public $LoginRouteId = 0;

	/**
	 * Username for the login route
	 *
	 * @var string
	 */
	public $Username = '';

	/**
	 * Password in MD5 encryption
	 *
	 * @var string
	 */
	public $Password = '';
}
?>
