<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ApplicationVersion
 * Created: Dec 21, 2011
 *
 * @author Alexander Petrides <alexander@essential-elements.net>
 */
class SoftwareVersion
{
	/**
	 *
	 * @var string
	 */
	public $LatestVersion = '';

	/**
	 *
	 * @var string
	 */
	public $MinimumVersionNeeded = '';

	/**
	 *
	 * @var string
	 */
	public $DownloadUrl = '';
}

?>
