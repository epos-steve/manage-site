<?php

class ItemCount
{
	/**
	 *
	 * @var string
	 */
	public $ItemCode = '';

	/**
	 *
	 * @var int
	 */
	public $BusinessId = 0;

	/**
	 *
	 * @var int
	 */
	public $OrderType = 0;

	/**
	 *
	 * @var string
	 */
	public $MachineNum = '';

	/**
	 *
	 * @var float
	 */
	public $Count = 0.0;

	/**
	 *
	 * @var int
	 */
	public $InventoryType = 0;

	/**
	 *
	 * @var string
	 */
	public $DateTimeModified = '';
}
?>
