<?php

class HandheldSoapClient
{
	public function __construct()
	{
	}

	/**
	 * Logs device into webservice and returns the route id for the login
	 *
	 * @param string $username The username for the account
	 * @param string $password The password for the account
	 * @return int
	 */
	public function Login($username, $password)
	{
		SiTech_Loader::loadModel('handheld/loginroute');
		$route = Handheld_LoginRouteModel::getByLogin($username, $password);

		if ($route == false)
		{
			throw new Exception('Invalid username/password combination supplied.', -999);
		}

		return $route->Id;
	}

	/**
	 * Returns list of businesses available to route
	 *
	 * @param int $loginRouteId
	 * @return Business[]
	 */
	public function GetBusinesses($loginRouteId)
	{
		SiTech_Loader::loadModel('handheld/business');
		$businesses = Handheld_BusinessModel::getByLoginRouteID($loginRouteId);

		if (empty($businesses))
		{
			throw new Exception('No Businesses Found.', -999);
		}

		return($businesses);
	}

	/**
	 * Returns list of machines available to provided route and optionally business
	 *
	 * @param int $loginRouteId
	 * @param int $businessId[optional]
	 * @return Machine[]
	 */
	public function GetMachines($loginRouteId, $businessId = null)
	{
		SiTech_Loader::loadModel('handheld/vendmachine');

		array_map($businessids);
		$machines = Handheld_VendMachineModel::getByLoginRouteIDAndBusinessId($loginRouteId, $businessId);

		if (empty($machines))
		{
			throw new Exception('No Machines Found.', -999);
		}

		return($machines);
	}

	/**
	 * Returns iist of items available at business
	 *
	 * @param int $businessId
	 * @return Item[]
	 */
	public function GetItems($businessId)
	{
		SiTech_Loader::loadModel('handheld/menuitem');
		$items = Handheld_MenuItemModel::getMenuItemsByBusinessId($businessId);

		if (empty($items))
		{
			throw new Exception('No Items Found.', -999);
		}

		return($items);
	}

	/**
	 * Returns the current time in RFC1123 format
	 *
	 * @return string
	 */
	public function GetCurrentTime()
	{
		return gmdate('D, d M Y H:i:s') . ' GMT';
	}

	/**
	 * Returns the most recent application
	 * software version available for download
	 *
	 * @return SoftwareVersion
	 */
	public function GetCurrentSoftwareVersion()
	{
		SiTech_Loader::loadModel('softwareversion');

		try
		{
			$latest = SoftwareVersionModel::getLastest('HANDHELD');
			$minimum = false;
			if($latest->Critical == 0)
			{
				$minimum = SoftwareVersionModel::getLastest('HANDHELD', true);
			}

			$verson = new SoftwareVersion();
			$version->LatestVersion = $latest->Version;
			$version->MinimumVersionNeeded = $minimum === false ? $latest->Version : $minimum->Version;

			$uri = new SiTech_Uri();
			$version->DownloadUrl = sprintf('%s:/%s/%s', $uri->getScheme(), $uri->getHost(), trim($latest->RelativePath, ' /'));

			return $version;
		}
		catch (Exception $ex)
		{
			throw new Exception('There was an error checking for updates, please try again.', -999);
		}
	}

	/**
	 * Accepts the inventory report and returns boolean indicating success
	 *
	 * @param ItemCount[] $counts
	 * @param bool $collected
	 * @return bool
	 */
	public function SendInventoryReport($counts, $collected)
	{
		SiTech_Loader::loadModel('handheld/invcountvend');

		$counts = (array)$counts;
		if(count($counts) == 0)
		{
			throw new Exception('Invalid counts sent.', -999);
		}

		$models = array();
		foreach($counts as $count)
		{
			if(!is_object($count))
			{
				throw new Exception('Invalid count data sent.', -999);
			}

			$icv = new Handheld_InvCountVendModel();
			$icv->machine_num = $count->MachineNum;
			$icv->date_time = $count->DateTimeModified;
			$icv->item_code = $count->ItemCode;
			$icv->onhand = 0;
			$icv->fills = 0;
			$icv->waste = 0;
			$icv->pulled = 0;

			switch($count->InventoryType)
			{
				case 1:
					$icv->onhand = $count->Count;
					$icv->is_inv = $count->InventoryType == 1;
					break;
				case 2:
					$icv->fills = $count->Count;
					$icv->is_inv = $count->InventoryType == 0;
					break;
				case 3:
					$icv->waste = $count->Count;
					$icv->is_inv = $count->InventoryType == 0;
					break;
				default:
					throw new Exception('Invalid inventory type on item count.', -999);
			}

			Handheld_InvCountVendModel::SetUnitCost($icv, $count->BusinessId);
			Handheld_MenuItemModel::setActive($icv->item_code, $count->BusinessId);

			$models[] = $icv;
		}

		foreach($models as $model)
		{
			$model->save();
		}

		return true;
	}

	/**
	 * Accepts exception generated from the handheld software
	 *
	 * @param SoftwareException[] $exceptions
	 * @return bool
	 */
	public function SendSoftwareExceptions($exceptions)
	{
		SiTech_Loader::loadModel('handheld/softwareexception');

		$exceptions = (array)$exceptions;
		foreach($exceptions as $key=>$val)
		{
			$ex = new ReportedExceptionModel();
			$ex->SourceFrom = ClientExceptionsModel::SOURCE_WEBSERVICE;
			$ex->SourceReport = $val->Identification;
			$ex->ExceptionType = $val->Type;
			$ex->ExceptionDateTime = $val->DateTime;
			$ex->ExceptionMessage = $val->Message;
			$exceptions[$key] = $ex;
		}

		foreach($exceptions as $exception)
		{
			$exception->save();
		}
	}
}

?>
