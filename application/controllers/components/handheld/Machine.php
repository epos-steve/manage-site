<?php

class Machine
{
	/**
	 *
	 * @var int
	 */
	public $MachineId = 0;

	/**
	 *
	 * @var string
	 */
	public $MachineNum = '';

	/**
	 *
	 * @var string
	 */
	public $MachineName = '';

	/**
	 *
	 * @var int
	 */
	public $OrderType = 0;

	/**
	 *
	 * @var int
	 */
	public $BusinessId = 0;

	/**
	 *
	 * @var int
	 */
	public $TerminalNo = 0;
}
