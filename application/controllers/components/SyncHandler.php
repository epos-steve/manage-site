<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SyncHandler
 *
 * @author eric
 */
class SyncHandler extends Manage_Component_Abstract
{
	public function getRequestMachineId()
	{
		return(getenv('HTTP_X_VIVIPOS_MACHINE_ID'));
	}

	public function getServerData($client, SiTech_Uri $uri)
	{
		$return = array(
			'default' => array(
				'datasource' => 'default',
				'count' => 0,
				'lastsynced' => 0,
				'sql' => ''
			),
			'extension' => array(
				'datasource' => 'extension',
				'count' => 0,
				'lastsynced' => 0,
				'sql' => ''
			)
		);
		// First grab the menu items
		/*
		$items = explode("\r\n", file_get_contents($uri->getScheme().$uri->getHost().'/kiosk/index/'.$client['machine_id']));
		foreach ($items as $item) {
			if (empty($item)) continue;
			list($no, $code, $barcode, $new_price, $name, $is_button, $price) = explode(',', $item);
			if (empty($no)) continue;
			if (!empty($new_price)) $price = $new_price;
			$uuid = $this->gen_uuid();
			$return['default']['sql'] .= 'INSERT OR REPLACE INTO syncs (crud, machine_id, from_machine_id, method_id, method_type, method_table, created, modified) VALUES(\'create\', \'EESync\', \'EESync\', \''.$uuid.'\', \'Products\', \'products\', \''.time().'\', \''.time().'\')'."\n";
			$return['default']['sql'] .= 'INSERT OR REPLACE INTO "products" ("id", "no", "cate_no", "name", "level_enable1", "price_level1", "barcode", "visible") VALUES(\''.$uuid.'\', \''.$no.'\', 1, \''.$name.'\', 1, '.(float)$price.', \''.$barcode.'\', '.(int)$is_button.');'."\n";
			$return['default']['count']++;
		}

		$items = explode("\r\n", file_get_contents($uri->getScheme().$uri->getHost().'/kiosk/index/'.$client['machine_id']));
		foreach ($items as $item) {
			if (empty($item)) continue;
			$item = explode(',', $item);
			$return['extension']['sql'] .= 'UPDATE "customers" SET "ck_card_balance" = "ck_card_balance" + '.$line[1].' WHERE "ck_card_number" = \'780'.$line[0].'\';'."\n";
			$return['extension']['count']++;
		}
		 */

		file_put_contents(realpath(SITECH_APP_PATH.'/files/').'/'.$client['machine_id'].'.queries.txt', var_export($return, true));

		return($return);
	}

	public function parseRequest($data, $type = 'php')
	{
		switch ($type) {
			case 'php':
				$result = unserialize(bzdecompress(base64_decode($data)));
				break;

			case 'json':
				$result = json_decode($data, true);
				break;
		}

		return($result);
	}

	/**
     *
     * @param mixed $result
     * @param string $type
     * @return string
     */
	public function prepareResponse($result, $type='php') {
		$response = "";

		switch($type) {
			case 'php':
				$response = base64_encode(bzcompress(serialize($result)));
				break;

			case 'json':
				$response = json_encode($result);
				break;

			case 'bgz_json':
				// base64 gzip json
				$response = base64_encode(gzdeflate(rawurlencode(json_encode($result))));
				break;
		}

		return($response);
	}

	public function saveLastSynced($machine_id, $data)
	{
		SiTech_Loader::loadModel('SyncRemoteMachines');
		foreach ($data as $item) {
			$sync = new SyncRemoteMachines();
			$sync->machine_id = $machine_id;
			$sync->sync_type = $item['datasource'];
			$sync->last_synced = $item['last_synced'];
			$sync->save();
		}
	}

	protected function gen_uuid() {
	    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
    	    // 32 bits for "time_low"
        	mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

			// 16 bits for "time_mid"
			mt_rand( 0, 0xffff ),

			// 16 bits for "time_hi_and_version",
			// four most significant bits holds version number 4
			mt_rand( 0, 0x0fff ) | 0x4000,

			// 16 bits, 8 bits for "clk_seq_hi_res",
			// 8 bits for "clk_seq_low",
			// two most significant bits holds zero and one for variant DCE1.1
			mt_rand( 0, 0x3fff ) | 0x8000,

			// 48 bits for "node"
			mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
		);
	}
}
