<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of detailcreation
 *
 * @author eric
 */
class DetailCreation
{
	protected $_client;
	public $data = array();
	protected $_fp;
	protected $_siblings;

	public function __destruct()
	{
		$this->_closeFile();
	}

	public function createChecksSection()
	{
		if (empty($this->_client)) throw new Exception ('FAIL');
		$checks = array();

		$this->_db->exec('USE vivipos_order');
		$stmnt = $this->_db->query(
			'(SELECT
				CAST(CONCAT(SUBSTRING(sequence, 2), terminal_no) AS UNSIGNED INTEGER) AS sequence,
				0 AS session,
				table_no,
				0 AS seat,
				0 AS emp_no,
				no_of_customers,
				FROM_UNIXTIME(modified) AS transaction_created,
				total,
				tax_subtotal,
				payment_subtotal,
				0 AS sale_type,
				IF (status != 1, 1, 0) AS is_void,
				IF (customer_card_number = "null", "", customer_card_number) AS customer_card_number,
				terminal_no,
				date_posted
			FROM
				orders
			WHERE
				date_posted >= FROM_UNIXTIME(:start)
				AND date_posted < FROM_UNIXTIME(:end)
				AND terminal_no IN (' . $this->_siblings . ')
				AND status = 1)
			UNION ALL
			(SELECT
				CAST(CONCAT(SUBSTRING(FROM_UNIXTIME(op.modified, \'%Y%m%d%H%i%s\'), 2), op.terminal_no) AS UNSIGNED INTEGER) AS sequence,
				0 AS session,
				0 AS table_no,
				0 AS seat,
				0 AS emp_no,
				1 AS no_of_customers,
				FROM_UNIXTIME(op.modified) AS transaction_created,
				op.order_total AS total,
				0 AS tax_subtotal,
				op.amount AS payment_subtotal,
				0 AS sale_type,
				0 AS is_void,
				IF (op.customer_card_number = "null", "", op.customer_card_number) AS customer_card_number,
				op.terminal_no,
				date_posted
			FROM
				order_payments AS op
			LEFT JOIN cc_transactions AS cc ON
				op.order_id = cc.cc_transaction_id
			WHERE
				op.date_posted >= FROM_UNIXTIME(:start)
				AND op.date_posted < FROM_UNIXTIME(:end)
				AND op.terminal_no IN ('. $this->_siblings .')
				AND op.name IN (\'CR\', \'CA\'))
			ORDER BY sequence',
			array(
				'start' => $this->start,
				'end' => $this->end
			)
		);

		$this->_writeSection('checks', $stmnt->fetchAll(PDO::FETCH_ASSOC));

		$stmnt->closeCursor();
		unset($stmnt);
	}

	public function createCheckDetailSection()
	{
		if (empty($this->_client)) throw new Exception ('FAIL');
		$detail = array();

		$this->_db->exec('USE vivipos_order');
		$stmnt = $this->_db->query(
			'SELECT
				CAST(CONCAT(SUBSTRING(o.sequence, 2), o.terminal_no) AS UNSIGNED INTEGER) AS sequence,
				oi.product_no,
				oi.cate_no,
				oi.current_qty,
				IF(oi.sale_unit = \'lb\', oi.current_subtotal + oi.current_discount, oi.current_price + oi.current_discount + oi.current_condiment) AS current_price,
				IF(void_clerk IS NULL, 0, 1) AS is_void
			FROM
				order_items AS oi
			JOIN orders AS o ON
				oi.order_id = o.id
			WHERE
				o.date_posted >= FROM_UNIXTIME(:start)
				AND o.date_posted < FROM_UNIXTIME(:end)
				AND o.terminal_no IN (' . $this->_siblings . ')
				AND o.status = 1
			ORDER BY o.sequence',
			array(
				'start' => $this->start,
				'end' => $this->end
			)
		);

		$this->_writeSection('check detail', $stmnt->fetchAll(PDO::FETCH_ASSOC));

		$stmnt->closeCursor();
		unset($stmnt);
	}

	public function createDiscountSection()
	{
		if (empty($this->_client)) throw new Exception('FAIL');
		$this->_db->exec('USE vivipos_order');
		$date = $this->start;
		$dates = array();

		$stmntDiscounts = $this->_db->query(
			'SELECT
				op.code AS ID,
				op.name AS NAME
			FROM
				order_promotions AS op
			JOIN orders AS o ON
				op.order_id = o.id
			WHERE
				o.date_posted >= FROM_UNIXTIME(:start)
				AND o.date_posted < FROM_UNIXTIME(:end)
				AND o.terminal_no IN ('.$this->_siblings.')
			GROUP BY op.code',
			array(
				'start' => $this->start,
				'end' => $this->end
			)
		);
		$discounts = $stmntDiscounts->fetchAll(PDO::FETCH_ASSOC);

		while ($date < $this->end) {
			foreach ($discounts as &$discount) {
				$stmnt = $this->_db->query(
					'SELECT
						ABS(ROUND(SUM(op.discount_subtotal), 2))
					FROM
						order_promotions AS op
					JOIN orders AS o ON
						op.order_id = o.id
					WHERE
						op.code = ?
						AND o.date_posted >= FROM_UNIXTIME(?)
						AND o.date_posted < FROM_UNIXTIME(?)
						AND o.terminal_no IN ('.$this->_siblings.')
					GROUP BY op.code',
					array(
						$discount['ID'],
						$date,
						strtotime('+1 day', $date)
					)
				);
				$discount[date('Y-m-d', $date)] = (double) $stmnt->fetchColumn();
			}

			$dates[] = date('Y-m-d', $date);
			$date = strtotime('+1 day', $date);
		}

		if (empty($discounts)) {
			fwrite($this->_fp, "[discounts]\n");
			fwrite($this->_fp, "ID, NAME, ".implode(',', $dates)."\n");
		} else {
			$this->_writeSection('discounts', $discounts);
		}
	}

	public function createDiscountDetailSection()
	{
		if (empty($this->_client)) throw new Exception ('FAIL');
		$detail = array();

		$this->_db->exec('USE vivipos_order');
		$stmnt = $this->_db->query(
			'SELECT
				CAST(CONCAT(SUBSTRING(o.sequence, 2), o.terminal_no) AS UNSIGNED INTEGER) AS sequence,
				op.code,
				op.discount_subtotal
			FROM
				order_promotions AS op
			JOIN orders AS o ON
				op.order_id = o.id
			WHERE
				o.date_posted >= FROM_UNIXTIME(:start)
				AND o.date_posted < FROM_UNIXTIME(:end)
				AND o.terminal_no IN (' . $this->_siblings . ')
			ORDER BY o.sequence',
			array(
				'start' => $this->start,
				'end' => $this->end
			)
		);

		$this->_writeSection('discount detail', $stmnt->fetchAll(PDO::FETCH_ASSOC));

		$stmnt->closeCursor();
		unset($stmnt);
	}

	public function createPaymentDetailSection()
	{
		if (empty($this->_client)) throw new Exception ('FAIL');
		$payments = array();

		$this->_db->exec('USE vivipos_order');
		$stmnt = $this->_db->query(
			'(SELECT
				CAST(CONCAT(SUBSTRING(o.sequence, 2), o.terminal_no) AS UNSIGNED INTEGER) AS sequence,
				IF(
					op.name = \'CA\' OR op.name = \'cash\',
					1,
					IF(
						op.name = \'CC\',
						4,
						IF(
							op.name = \'CK\',
							14,
							IF(
								op.name = \'PD\',
								5,
								IF(
									op.name = \'CR\',
									6,
									IF(
										op.name = \'check\',
										8,
										IF(
											op.name = \'giftcard\',
											9,
											0
										)
									)
								)
							)
						)
					)
				) AS name,
				op.amount,
				op.change,
				0 AS tips,
				IF(
					CAST(oi.product_no AS SIGNED) > 0,
					oi.product_no,
					IF(
						CAST(op.customer_card_number AS SIGNED) > 0,
						op.customer_card_number,
						o.customer_card_number
					)
				) AS customer_card_number
			FROM
				order_payments AS op
			JOIN orders AS o ON
				op.order_id = o.id
			LEFT JOIN order_items AS oi ON
				op.order_id = oi.order_id
				AND oi.tax_name = :tax_name
			WHERE
				o.date_posted >= FROM_UNIXTIME(:start)
				AND o.date_posted < FROM_UNIXTIME(:end)
				AND o.terminal_no IN (' . $this->_siblings . '))
			UNION ALL
			(SELECT
				CAST(CONCAT(SUBSTRING(FROM_UNIXTIME(op.modified, \'%Y%m%d%H%i%s\'), 2), op.terminal_no) AS UNSIGNED INTEGER) AS sequence,
				IF(
					op.name = \'CA\' OR op.name = \'cash\',
					1,
					IF(
						op.name = \'CC\',
						4,
						IF(
							op.name = \'CK\',
							14,
							IF(
								op.name = \'PD\',
								5,
								IF(
									op.name = \'CR\',
									6,
									IF(
										op.name = \'check\',
										8,
										0
									)
								)
							)
						)
					)
				) AS name,
				op.amount,
				op.change,
				0 AS tips,
				IF(op.customer_card_number = "NULL", "", op.customer_card_number) AS customer_card_number
			FROM
				order_payments AS op
			WHERE
				op.date_posted >= FROM_UNIXTIME(:start)
				AND op.date_posted < FROM_UNIXTIME(:end)
				AND op.terminal_no IN ('.$this->_siblings.')
				AND op.name IN (\'CR\', \'CA\'))
			ORDER BY sequence',
			array(
				'tax_name' => 'X',
				'start' => $this->start,
				'end' => $this->end
			)
		);

		$this->_writeSection('paymentdetail', $stmnt->fetchAll(PDO::FETCH_ASSOC));

		$stmnt->closeCursor();
		unset($stmnt);
	}

	public function createHeaderSection()
	{
		if (empty($this->_client)) throw new Exception ('FAIL');

		$date1 = $this->start;
		$date2 = strtotime("+1 day", $date1);
		$counter = 1;

		while($date2 <= $this->end){
			$dates[$counter] = $date1;
			$date1 = $date2;
			$date2 = strtotime("+1 day", $date1);
			$counter++;
		}

		///write to file
		$counter = 1;
		foreach($dates AS $key => $value){
			$showdate = date("Y-m-d H:i:s", $value);
			if($counter == 1){fwrite($this->_fp, "\"$showdate\"");}
			else{fwrite($this->_fp, ",\"$showdate\"");}
			$counter++;
		}
		fwrite($this->_fp, "\n");
	}

	public function createSalesSection()
	{
		if (empty($this->_client)) throw new Exception ('FAIL');

		$menu_groups = array();
		$totals = array();
		$dates = array();

		$date1 = $this->start;
		$date2 = strtotime("+1 day", $date1);
		$counter = 1;

		$this->_db->exec('USE vivipos_order');
		
		/*
		$this->_db->exec('
			UPDATE order_items
			SET
				cate_no = 1
			WHERE
				cate_no < 1
				AND tax_name != \'X\'
		');
		 * 
		 */

		$statement = $this->_db->prepare(
			'SELECT
				order_items.cate_no,
				order_items.cate_name,
				SUM(
					IF(
						order_items.sale_unit = \'lb\',
						order_items.current_subtotal + order_items.current_discount,
						order_items.current_price * order_items.current_qty + order_items.current_discount + order_items.current_condiment
					)
				) AS total
			FROM
				order_items
			JOIN orders ON
				orders.id = order_items.order_id
			WHERE
				orders.date_posted >= FROM_UNIXTIME(:start)
				AND orders.date_posted < FROM_UNIXTIME(:end)
				AND orders.terminal_no IN (' . $this->_siblings . ')
				AND orders.status = 1
			GROUP BY order_items.cate_no'
		);

		while($date2 <= $this->end) {
			$statement->execute(array(
				'start' => $date1,
				'end' => $date2
			));
			$result = $statement->fetchAll(PDO::FETCH_ASSOC);

			foreach($result AS $line){
				$menu_groups[$line["cate_no"]] = $line["cate_name"];
				$totals[$line["cate_no"]]["$date1"] = $line["total"];
			}

			$dates[$counter] = $date1;
			$date1 = $date2;
			$date2 = strtotime("+1 day", $date1);
			$counter++;
		}

		///write to file
		fwrite($this->_fp, "[sales]\n");
		fwrite($this->_fp, "\"ID\",\"NAME\"");
		foreach($dates AS $key => $value){
			$showdate = date("Y-m-d", $value);
			fwrite($this->_fp, ",\"$showdate\"");
		}
		fwrite($this->_fp, "\n");

		foreach($menu_groups AS $key => $value){
			fwrite($this->_fp, "$key,$value");
			foreach($dates AS $key2 => $value2){
				$showtotal = number_format($totals[$key][$value2],2,'.','');
				fwrite($this->_fp, ",$showtotal");
			}
			fwrite($this->_fp, "\n");
		}
	}

	public function createTaxesSection()
	{
		if (empty($this->_client)) throw new Exception ('FAIL');

		$taxes = array();
		$totals = array();
		$dates = array();

		$date1 = $this->start;
		$date2 = strtotime("+1 day", $date1);
		$counter = 1;

		$this->_db->exec('USE vivipos_order');

		$orderItemsStatement = $this->_db->prepare(
			'SELECT
				order_items.tax_name,
				ROUND( SUM( order_items.current_tax ), 2) AS total
			FROM
				order_items
			JOIN orders ON
				orders.id = order_items.order_id
			WHERE
				orders.date_posted >= FROM_UNIXTIME(:start)
				AND orders.date_posted < FROM_UNIXTIME(:end)
				AND order_items.current_tax != 0
				AND orders.terminal_no IN (' . $this->_siblings . ')
				AND orders.status = 1
			GROUP BY order_items.tax_name'
		);

		$promosStatement = $this->_db->prepare(
			'SELECT
				order_promotions.tax_name,
				ROUND( SUM( order_promotions.current_tax ), 2 ) AS total
			FROM
				order_promotions
			JOIN orders ON
				orders.id = order_promotions.order_id
			WHERE
				orders.date_posted >= FROM_UNIXTIME(:start)
				AND orders.date_posted < FROM_UNIXTIME(:end)
				AND orders.terminal_no IN ('.$this->_siblings.')
			GROUP BY order_promotions.tax_name'
		);

		while($date2 <= $this->end) {
			$timestamps = array(
				'start' => $date1,
				'end' => $date2
			);

			$orderItemsStatement->execute($timestamps);
			$orderItems = $orderItemsStatement->fetchAll(PDO::FETCH_KEY_PAIR);

			$promosStatement->execute($timestamps);
			$promos = $promosStatement->fetchAll(PDO::FETCH_KEY_PAIR);

			$counter2 = 1;

			foreach($orderItems AS $name => $total){
				$taxes[$name] = "Tax Rate $counter2";
				if (isset($promos[$name])) {
					$total -= $promos[$name];
				}
				$totals[$name][$date1] = $total;
				$counter2++;
			}

			$dates[$counter] = $date1;
			$date1 = $date2;
			$date2 = strtotime("+1 day", $date1);
			$counter++;
		}

		///write to file
		fwrite($this->_fp, "[taxes]\n");
		fwrite($this->_fp, "\"ID\",\"NAME\"");
		foreach($dates AS $key => $value){
			$showdate = date("Y-m-d", $value);
			fwrite($this->_fp, ",\"$showdate\"");
		}
		fwrite($this->_fp, "\n");

		foreach($taxes AS $key => $value){
			fwrite($this->_fp, "$key,$value");
			foreach($dates AS $key2 => $value2){
				$showtotal = number_format($totals[$key][$value2],2,'.','');
				fwrite($this->_fp, ",$showtotal");
			}
			fwrite($this->_fp, "\n");
		}
	}

	public function createTendersSection()
	{
		if (empty($this->_client)) throw new Exception ('FAIL');

		$tenders = array();
		$totals = array();

		$date1 = $this->start;
		$date2 = strtotime("+1 day", $date1);
		$counter = 1;

		$this->_db->exec('USE vivipos_order');

		$statement = $this->_db->prepare(
			'SELECT
				order_payments.name,
				SUM( ROUND(order_payments.amount - order_payments.change,2) ) AS total
			FROM
				order_payments
			LEFT JOIN orders ON
				orders.id = order_payments.order_id
				AND orders.status = 1
			WHERE
				order_payments.date_posted >= FROM_UNIXTIME(:start)
				AND order_payments.date_posted < FROM_UNIXTIME(:end)
				AND order_payments.terminal_no IN (' . $this->_siblings . ')
			GROUP BY order_payments.name'
		);

		$lineNamePosMap = array(
			'ca' => 1,
			'ck' => 14,
			'cc' => 4,
			'pd' => 5,
			'cr' => 6,
			'cash' => 7,
			'check' => 8,
			'giftcard' => 9,
			'coupon' => 10,
		);

		while($date2 <= $this->end){
			$statement->execute(array(
				'start' => $date1,
				'end' => $date2
			));
			$result = $statement->fetchAll(PDO::FETCH_ASSOC);

			foreach ($result AS $line) {
				/////pos_id
				$pos_id = 0;
				if ( array_key_exists(strtolower($line['name']), $lineNamePosMap) )
				{
					$pos_id = $lineNamePosMap[ strtolower($line['name']) ];
				}

				$tenders[$pos_id] = $line["name"];
				$totals[$pos_id]["$date1"] = $line["total"];

				////new payment method - Steve 12/28/11
				///not ready for multiple payments on one check
				if($pos_id == 4 || $pos_id == 7) {
					$tax_name = "X";

					if ($pos_id == 4) {
						$new_pos_id = 6;
						$new_pos_name = "CR";
					}
					elseif ($pos_id == 7) {
						$new_pos_id = 1;
						$new_pos_name = "CA";
					}
					/*
					$query = 'SELECT order_payments.name,
							SUM( ROUND( order_items.current_subtotal, 2 ) ) AS total
						FROM order_payments
						JOIN order_items ON order_items.order_id = order_payments.order_id
							AND order_items.tax_name = :tax_name
						WHERE order_payments.modified >= :date1
							AND order_payments.modified < :date2
							AND order_payments.terminal_no IN (' . $this->_siblings . ')
							AND order_payments.name = :line_name
						GROUP BY order_payments.name';
					$this->_db->prepare($query);
					$this->_db->execute(array('tax_name' => $tax_name, 'date1' => $date1, 'date2' => $date2));
					*/
					$result2 = $this->_db->query('
						SELECT
							order_payments.name,
							SUM( ROUND( order_items.current_subtotal, 2 ) ) AS total
						FROM
							order_payments
						JOIN order_items ON
							order_items.order_id = order_payments.order_id
							AND order_items.tax_name = \'' . $tax_name . '\'
						JOIN orders ON
							orders.id = order_payments.order_id
							AND orders.status = 1
						WHERE
							order_payments.date_posted >= \'' . date('Y-m-d H:i:s', $date1) . '\'
							AND order_payments.date_posted < \'' . date('Y-m-d H:i:s', $date2) . '\'
							AND order_payments.terminal_no IN (' . $this->_siblings . ')
							AND order_payments.name = \'' . $line["name"] . '\'
						GROUP BY order_payments.name');
					$result2 = $result2->fetchAll(PDO::FETCH_ASSOC);

					foreach($result2 AS $line2){
						$tenders[$new_pos_id] = $new_pos_name;
						$totals[$new_pos_id]["$date1"] += $line2["total"];

						////remove amount from cash/credit card
						$totals[$pos_id]["$date1"] -= $line2["total"];
					}
				}
				////end new payment method
			}

			$dates[$counter] = $date1;
			$date1 = $date2;
			$date2 = strtotime("+1 day", $date1);
			$counter++;
		}

		///write to file
		fwrite($this->_fp, "[tenders]\n");
		fwrite($this->_fp, "\"ID\",\"NAME\"");
		foreach($dates AS $key => $value){
			$showdate = date("Y-m-d", $value);
			fwrite($this->_fp, ",\"$showdate\"");
		}
		fwrite($this->_fp, "\n");

		foreach($tenders AS $key => $value){
			fwrite($this->_fp, "$key,$value");
			foreach($dates AS $key2 => $value2){
				$showtotal = number_format($totals[$key][$value2],2,'.','');
				fwrite($this->_fp, ",$showtotal");
			}
			fwrite($this->_fp, "\n");
		}
	}

	public function sendData()
	{
		$this->_closeFile();
		//echo file_get_contents('http://'.$this->_uri->getHost().'/receive/receivedata.php?programid='.$this->_client->client_programid.'&vivipos=1');
		$ch = curl_init('http://'.$this->_uri->getHost().'/receive/receivedata.php?programid='.$this->_client->client_programid.'&vivipos=1');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		echo curl_exec($ch);
		curl_close($ch);
	}

	public function sendEmails()
	{
		$this->_db->exec('USE vivipos_order');
		$stmnt = $this->_db->query(
			'SELECT
				o.id AS oid,
				c.id,
				CAST(CONCAT(SUBSTRING(o.sequence, 2), o.terminal_no) AS UNSIGNED INTEGER) AS sequence,
				c.cc_transaction_id,
				c.ref_number,
				c.auth_code,
				c.email,
				o.created,
				o.date_posted
			FROM
				cc_transactions AS c
			JOIN orders AS o ON
				c.cc_transaction_id = o.customer_card_number
			WHERE
				c.email <> \'NO RECEIPT\'
				AND c.is_processed = 0
				AND o.terminal_no = ?',
			array($this->_client->client_programid)
		);

		$orderItemsStatement = $this->_db->prepare(
			'SELECT
				oi.product_name,
				oi.current_price,
				oi.current_qty,
				oi.current_subtotal,
				oit.tax_name,
				oit.tax_subtotal
			FROM
				order_items AS oi
			LEFT JOIN order_item_taxes AS oit ON
				oi.id = oit.order_item_id
			WHERE oi.order_id = ?'
		);

		$taxesStatement = $this->_db->prepare("
			SELECT
				tax_name,
				tax_subtotal
			FROM
				order_item_taxes
			WHERE
				order_id = ?
				AND order_item_id = ''
		");

		$paymentStatement = $this->_db->prepare('
			SELECT
				amount
			FROM
				order_payments
			WHERE
				order_id = ?
				AND terminal_no = ?
		');

		foreach ($stmnt->fetchAll(PDO::FETCH_ASSOC) as $row) {
			$view = new Manage_Template();
			$sep = 'PHP-mixed-'.sha1(date('r'));
			$view->assign('sep', '--'.$sep);
			$view->assign('business', $this->_client->getCompanyName());
			$view->assign('order', $row);

			$orderItemsStatement->execute( array($row['oid']) );
			$taxesStatement->execute( array($row['id']) );
			$paymentStatement->execute( array($row['oid'], $this->_client->client_programid) );
			$view->assign('items', $orderItemsStatement->fetchAll(PDO::FETCH_ASSOC));
			$view->assign('taxes', $taxesStatement->fetchAll(PDO::FETCH_ASSOC));
			$view->assign('payment', $paymentStatement->fetch(PDO::FETCH_ASSOC));

			// Prepare the e-mail
			$headers  = 'MIME-Version: 1.0'."\r\n";
			$headers .= 'Content-type: multipart/mixed; boundary="'.$sep."\"\r\n";
			$headers .= 'From: Company Kitchen <no-reply@payment.companykitchen.com>'."\r\n";

			// Mail it!
			mail($row['email'], 'Company Kitchen Receipt', $view->render('email/receipt.tpl'), $headers);
			// Update the database so we don't send again.
			$this->_db->exec('UPDATE cc_transactions SET is_processed = 1 WHERE id = ?', array($row['id']));
		}

		$stmnt->closeCursor();

		// Now grab the CC reloads
		$stmnt = $this->_db->query(
			'SELECT
				ct.id,
				ct.cc_transaction_id,
				ct.ref_number,
				ct.auth_code,
				ct.email,
				op.amount,
				op.created,
				op.date_posted
			FROM
				cc_transactions AS ct
			JOIN order_payments AS op ON
				op.id = ct.cc_transaction_id
			WHERE
				ct.email <> \'NO RECEIPT\'
				AND ct.is_processed = 0
				AND op.terminal_no = ?',
			array($this->_client->client_programid)
		);

		if ($stmnt->rowCount() > 0)
		{
			foreach ($stmnt->fetchAll(PDO::FETCH_ASSOC) as $row) {
				$view = new Manage_Template();
				$sep = 'PHP-mixed-'.sha1(date('r'));
				$view->assign('sep', '--'.$sep);
				$view->assign('business', $this->_client->getCompanyName());
				$view->assign('order', $row);

				// Prepare the e-mail
				$headers  = 'MIME-Version: 1.0'."\r\n";
				$headers .= 'Content-type: multipart/mixed; boundary="'.$sep."\"\r\n";
				$headers .= 'From: Company Kitchen <no-reply@payment.companykitchen.com>'."\r\n";

				// Mail it!
				mail($row['email'], 'Company Kitchen Receipt', $view->render('email/reload.tpl'), $headers);
				// Update the database so we don't send again.
				$this->_db->exec('UPDATE cc_transactions SET is_processed = 1 WHERE id = ?', array($row['id']));
			}

			$stmnt->closeCursor();
		}
	}

	public function setClient(ClientProgramsModel $client, SiTech_Uri $uri)
	{
		$this->_client = $client;
		$this->_uri = $uri;
		$this->_db = new Manage_DB('vivipos db');
		$this->_siblings = $client->getViviposSiblings();

		// Generate the file to be processed
		if (!($this->_fp = fopen(realpath(SITECH_APP_PATH.'/files').'/'.$client->client_programid.'.txt', 'w'))) {
			echo 'FAILED';
			return(false);
		}

		$ch = curl_init('http://'.$uri->getHost().'/send/senddata.php?programid='.$client->client_programid.'&vivipos=1');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		$this->data = explode(',', curl_exec($ch));
		curl_close($ch);

		//$this->data = explode(',', file_get_contents('http://'.$uri->getHost().'/send/senddata.php?programid='.$client->client_programid.'&vivipos=1'));

		foreach ($this->data as $k => $v) {
			unset($this->data[$k]);
			list($k, $v) = explode('|', $v);
			$this->data[$k] = $v;
		}

		if ($this->data['todo'] == '2') {
			$this->start = strtotime($this->data['startdate'].' '.$this->data['day_start']);
			$this->end = strtotime("+1 day", strtotime($this->data['enddate'] .' '.$this->data['day_start']));
		}

		return(true);
	}

	protected function _closeFile()
	{
		if (!empty($this->_fp)) {
			fclose($this->_fp);
			$this->_fp = null;
		}
	}

	protected function _writeSection($header, array $section)
	{
		fwrite($this->_fp, "[$header]\n");
		if (sizeof($section) > 0) {
			fwrite($this->_fp, implode(',', array_keys($section[0]))."\n");
			foreach ($section as $line) {
				fwrite($this->_fp, implode(',', $line)."\n");
			}
		}

		echo 'Wrote '.sizeof($section).' records for "'.$header.'"<br />'."\n";
	}
}
