<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of createupgrade
 *
 * @author Eric Gach <eric@essential-elements.net>
 * @version $Id$
 */
class CreateUpgrade
{
	/**
	 *
	 * @var Manage_DB
	 */
	protected $_db;

	protected $_date;
	protected $_errors = array();
	protected $_folder;
	protected $_folders = array();
	protected $_tmpFolder = '/tmp';

	public function __construct($workgroup = null)
	{
		$this->_workgroup = $workgroup;
		$this->_db = Manage_DB::singleton();
		set_time_limit(0);
		$hazFile = false;
		foreach ($_FILES['files']['name'] as $k => $file) {
			if (!empty($file) && ($this->_errors[$k] = $this->getError($k)) !== false) {
				throw new SiTech_Exception($this->_errors[$k]);
			} elseif (!empty($file)) {
				$hazFile = true;
			}
		}

		if (!$hazFile) {
			throw new Exception('No files were uploaded successfully. Cannot create upgrade');
		}

		$this->_date = date('YmdHis');
		$this->_folder = $this->_tmpFolder.'/'.$this->_date;
		if (is_dir($this->_folder)) {
			throw new SiTech_Exception('Upgrade directory %s already exists. Aborting.', array($this->_folder));
		}

		$this->_mkdir($this->_folder.'/files');
	}

	public function __destruct()
	{
		$this->cleanup();
	}

	public function cleanup()
	{
		if (is_dir($this->_folder)) {
			$this->_rrmdir($this->_folder);
		}
	}

	/**
	 * This should be called after calling extractFiles() so that we've got some
	 * extension folders to create an archive out of for our update package.
	 */
	public function createPackage()
	{
		$this->_keepAlive();
		$file = $this->_folder.'/files/'.urlencode('/data/profile/extensions/.tar');
		$cmd = 'tar -cf '.$file.' --exclude='.$file.' --transform=\'s|^'.substr($this->_folder, 1).'/files|data/profile/extensions|\' '.$this->_folder.'/files 2>&1';
		$tar = popen($cmd, 'r');
		$tarOut = '';
		//shell_exec($cmd);

		while(!feof($tar)) $tarOut .= fread($tar, 2096);
		pclose($tar);
		$this->_keepAlive();
		
		if (!file_exists($file)) {
			throw new SiTech_Exception('Failed to create tar archive %s%star output: %s', array($file, "\n", $tarOut));
		}

		// Cleanup the old un-needed folders
		foreach ($this->_folders as $folder) {
			$this->_rrmdir($folder);
		}

		// Now that we've got the extensions in a tar archive, create everything else..
		$this->_mkdir($this->_folder.'/dbs');
		$this->_mkdir($this->_folder.'/prefs');
		file_put_contents($this->_folder.'/actions.json', json_encode(array(array(
			'type' => 'file',
			'file' => '/data/profile/extensions*',
			'export_file' => urlencode('/data/profile/extensions/.tar')
		))));

		$file = SITECH_APP_PATH.'/upgrades/'.$this->_date.'.tbz';
		$cmd = 'tar -cjf '.$file.' --transform=\'s|^'.substr($this->_folder, 1).'|.|\' '.$this->_folder.' 2>&1';
		$tar = popen($cmd, 'r');
		$tarOut = '';
		//shell_exec($cmd);

		while (!feof($tar)) $tarOut .= fread($tar, 2096);
		pclose($tar);
		$this->_keepAlive();
		
		if (!file_exists($file)) {
			throw new SiTech_Exception('Failed to create the tar archive %s', array($file, "\n", $tarOut));
		}

		// Now that the package is created, add it to the database.
		$db = Manage_DB::singleton();
		$values = array('FileName' => basename($file), 'FileSize' => filesize($file), 'CheckSum' => md5_file($file));
		if (!empty($this->_workgroup)) $values['Workgroup'] = $this->_workgroup;
		$db->insert('vivipos_upgrades', $values);
	}

	public function extractFiles()
	{
		foreach ($_FILES['files']['name'] as $k => $file) {
			if (empty($_FILES['files']['name'][$k])) continue;
			// Initalize the archive
			$xpi = new ZipArchive();
			if (($result = $xpi->open($_FILES['files']['tmp_name'][$k])) !== true) {
				throw new SiTech_Exception('Unable to open XPI. Error code: %i', array($result));
			}

			// First we grab the install.rdf so we can read what folder it goes into
			if ($xpi->extractTo($this->_folder, 'install.rdf') === false) {
				throw new SiTech_Exception('Failed to extract XPI. Unknown error.');
			}
			$this->_keepAlive();

			// Now read the rdf file and figure out what folder we're extracting to
			$rdf = simplexml_load_file($this->_folder.'/install.rdf');
			foreach ($rdf->Description as $item) {
				$children = $item->children('http://www.mozilla.org/2004/em-rdf#');
				$extDir = '/'.$children->id;
			}

			unlink($this->_folder.'/install.rdf');
			$this->_mkdir($this->_folder.'/files/'.$extDir);
			$this->_folders[] = $this->_folder.'/files/'.$extDir;

			// Extract the XPI to the folder where it would go in the extensions
			if ($xpi->extractTo($this->_folder.'/files/'.$extDir) === false) {
				throw new SiTech_Extension('Failed to extract XPI to %s/files/%s', array($this->_folder, $extDir));
			}
			$this->_keepAlive();

			// Close the XPI since we're done extracting it.
			$xpi->close();
			shell_exec('chmod a+x `find '.$this->_folder.'/files/'.$extDir.' -name \'*.sh\'`');
		}
	}

	/**
	 * Get an error associated with a file that has been uploaded. Returns false
	 * if there is no error.
	 *
	 * @param string $file Input name for the file upload
	 * @return string Returns false if no error is found
	 */
	public function getError($file)
	{
		$code = $_FILES['files']['error'][$file];

		switch ($code) {
			case UPLOAD_ERR_OK:
				return(false);

			case UPLOAD_ERR_INI_SIZE:
			case UPLOAD_ERR_FORM_SIZE:
				return('File is to big to be uploaded. Please contact the system administrator.');

			case UPLOAD_ERR_PARTIAL:
				return('The uploaded file was only partially uploaded.');

			case UPLOAD_ERR_NO_FILE:
				return('No files were uploaded.');

			case UPLOAD_ERR_NO_TMP_DIR:
			case UPLOAD_ERR_CANT_WRITE:
				return('Unable to save file to temp directory.');

			default:
				return('An unknown error has occurrd. Error ID: '.$_FILES['files']['error'][$file]);
		}
	}

	protected function _keepAlive()
	{
		$this->_db->exec('SELECT 1');
	}

	protected function _mkdir($dir)
	{
		if (@mkdir($dir, 0755, true) === false) {
			throw new SiTech_Exception('Failed to create directory %s', array($dir));
		}
	}

	protected function _rrmdir($dir)
	{
		if (is_dir($dir)) {
			$objects = scandir($dir);
			foreach ($objects as $object) {
				if ($object != "." && $object != "..") {
					if (filetype($dir."/".$object) == "dir") $this->_rrmdir($dir."/".$object); else unlink($dir."/".$object);
				}
			}
			reset($objects);
			rmdir($dir);
		}
	}
}
