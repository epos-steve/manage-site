<?php
require_once( 'inc/SoapDiscovery.class.php' );
ini_set( 'soap.wsdl_cache_enabled', 0 );

define( 'DIR_SMARTMENU_FILES', SITECH_APP_PATH . '/files/smartmenu/' );

class Smartmenu_PackageController extends Manage_Controller_Abstract {

	/**
	 * @var SoapServer
	 */
	protected $soapServer;

	protected $serviceName = 'smartMenu_Package';
	protected $serviceNamespace = 'http://www.essential-elements.net/package';

	protected $_tdb, $_mdb;

	protected function init( ) {
		parent::init( );

		$this->_tdb = new Manage_DB('treat db');
		$this->_mdb = $this->_db;
		$this->_db = $this->_tdb;
		$this->_passedPageSecurityCheck = false;

		Treat_Model_Abstract::db( $this->_tdb );
	}

	public function service( ) {
		$class = get_called_class( );

		try {
			$this->soapServer = @new SoapServer( $this->_uri->getScheme( )
												. '://'
												. $this->_uri->getHost( )
												. '/'
												. $this->_uri->getController( )
												. '/'
												. 'wsdl' );
		} catch( Exception $e ) {
			throw new SiTech_Exception( $e->getMessage( ) );
		}

		$serverObject = new Smartmenu_PackageController_SOAP;
		$serverObject->serverURI = $this->_uri;
		$this->soapServer->setObject( $serverObject );

		//$this->soapServer->setClass( $class . '_SOAP' );

		$this->soapServer->handle( );
		return false;
	}

	public function wsdl( ) {
		$class = get_called_class( );

		$disco = new SoapDiscovery( $class . '_SOAP', $this->serviceName, $this->serviceNamespace );

		header( 'Content-type: text/xml' );
		echo $disco->getWSDL( $this->_uri->getHost( )
							. '/'
							. $this->_uri->getController( )
							. '/'
							. 'service' );

		return false;
	}

	public function retrieve( ) {
		$id = intval( $_REQUEST['id'] );
		$request = Treat_Model_SmartMenu_PackageRequest::getById( $id );

		if( !$request ) {
			throw new SiTech_Exception( 'Access denied' );
		}

		try {
			switch( $request->fileType ) {
				case 'package':
					$package = Treat_Model_SmartMenu_Package::getByPackageFile( $request->packageFile );
					$filename = $request->packageFile . '.eepx';
					break;
				
				case 'font':
					$package = Treat_Model_SmartMenu_Font::getById( $request->packageFile );
					$filename = $request->packageFile . '.ttf';
					break;
				
				case 'datafile':
					$package = Treat_Model_SmartMenu_File::getByDataFile( $request->packageFile );
					$filename = $package->fileName;
					break;
			}
			
			$packageData = $package->retrieve( );

			header( 'Content-type: application/octet-stream' );
			header( 'Content-Disposition: attachment; filename="' . $filename . '"' );
			header( 'Pragma: no-cache' );
			header( 'Cache-Control: no-cache, must-revalidate' );
			header( 'Expires: -1' );
			echo $packageData;

			$request->delete( );
		} catch( Exception $e ) {
			throw new SiTech_Exception( $e->getMessage( ) );
		}

		return false;
	}

	public function upload()
	{
		$id = $this->_args[0];
		if (!($upload = Treat_Model_SmartMenu_Upload::get("MD5(CONCAT(createdTime, packageFile)) = '$id'", true))) {
			throw new Exception('Invalid upload ID');
		}
		$packageData = file_get_contents('php://input');

		try {
			if (!($client = Treat_Model_SmartMenu_Auth::get((int)$upload->createdBy, true))) {
				throw new Exception('Failed to get auth record');
			}
			$client = $client->getClient();
			
			switch( $upload->fileType ) {
				case 'package':
					Treat_Model_SmartMenu_Package::create($client, $upload->packageFile, $packageData);
					break;
				
				case 'font':
					Treat_Model_SmartMenu_Font::create( $client, $upload->packageFile, $packageData );
					break;
				
				case 'datafile':
					list( $packageId, $fileName ) = explode( ',', $upload->uploadOptions );
					Treat_Model_SmartMenu_File::create( $client, $packageId, $fileName, $upload->packageFile, $packageData );
					break;
			}
			
			$upload->delete();
			echo strlen($packageData);
		} catch( PDOException $e ) {
			$errorArray = Treat_Model_SmartMenu_Package::db()->errorInfo();
			print_r($errorArray);
			return;
		} catch(Exception $e) {
			header( 'HTTP/1.0 500 Internal Server Error', true, 500 );
			echo 'Error: ' . $e->getMessage( );
		}

		return(false);
	}
}

class Smartmenu_PackageController_SOAP {
	
	static protected $client;
	public $serverURI;

	//
	// Start new Cleaned up functions
	//
	
	/**
	 *
	 * smart menu GetPackageUploadUrl function
	 *
	 * @param string $packageGuid
	 * @throws SoapFault
	 * @return string
	 */
	public function GetPackageUploadUrl( $packageGuid ) {
		extract( (array)$packageGuid, EXTR_OVERWRITE );
		return $this->_getUploadToken( $packageGuid, 'package' );
	}
	
	
	protected function _getUploadToken( $packageGuid, $fileType, $uploadOptions = null ) {
		if( !static::$client )
			throw new SoapFault( '403', 'Access denied' );
		extract( (array)$packageGuid, EXTR_OVERWRITE );

		try {
			if (is_object($packageGuid)) $packageGuid = $packageGuid->packageGuid;
			
			if( $fileType != 'font' ) $this->ThrowExceptionOnInvalidGuid( $packageGuid );
			
			$upload = Treat_Model_SmartMenu_Upload::create($packageGuid, static::$client, $fileType, $uploadOptions);
			$url = $this->serverURI->getScheme()
							. '://'
							. $this->serverURI->getHost()
							. '/'
							. $this->serverURI->getController()
							. '/upload/'.md5($upload->createdTime.$packageGuid);
		} catch (Exception $e) {
			throw new SoapFault( '500', $e->getMessage());
		}

		return array('return' => $url);
	}
	
	/**
	 *
	 * smart menu GetDownloadUrl function
	 *
	 * @param string $packageGuid
	 * @throws SoapFault
	 * @return string
	 */
	public function GetPackageDownloadUrl( $packageGuid ) {
		if( !static::$client )
			throw new SoapFault( '403', 'Access denied' );
		extract( (array)$packageGuid, EXTR_OVERWRITE );

		try {
			if (is_object($packageGuid)) $packageGuid = $packageGuid->packageGuid;
			
			$this->ThrowExceptionOnInvalidGuid( $packageGuid );
			
			$baseURI = $this->serverURI->getScheme( )
							. '://'
							. $this->serverURI->getHost( )
							. '/'
							. $this->serverURI->getController( )
							. '/retrieve?id=';
			
			$request = Treat_Model_SmartMenu_PackageRequest::create( $packageGuid );
			$url = $baseURI . $request->id;
			
		} catch (Exception $e) {
			throw new SoapFault( '500', $e->getMessage());
		}

		return array('return' => $url);
	}
	
	/**
	 *
	 * smart menu upload function
	 *
	 * @param string $packageFile
	 * @throws SoapFault
	 * @return string
	 */
	public function getUploadToken( $packageFile ) {
		return $this->getUploadToken($packageFile);
	}

	
	private function ThrowExceptionOnInvalidGuid( $guid )
	{
		$guid = preg_replace('[[^A-Za-z0-9]]', '', $guid);
		if(strlen($guid) != 32)throw new Exception("The id provided is invalid.");
	}
	
	//
	// End new Cleaned up functions
	//
	
	
	
	/**
	 * smart menu font upload function
	 *
	 * @param string $fontFile
	 * @throws SoapFault
	 * @return string
	 */
	public function GetFontUploadUrl( $fontFile ) {
		extract( (array)$fontFile, EXTR_OVERWRITE );
		return $this->_getUploadToken( $fontFile, 'font' );
	}
	
	/**
	 * smart menu data file upload function
	 *
	 * @param string $dataFile
	 * @param string $packageId
	 * @param string $fileName
	 * @throws SoapFault
	 * @return string
	 */
	public function GetDataUploadUrl( $dataFile, $packageId, $fileName ) {
		extract( (array)$dataFile, EXTR_OVERWRITE );
		return $this->_getUploadToken( $dataFile, 'datafile', $packageId . ',' . $fileName );
	}

	/**
	 *
	 * smart menu soap authentication
	 * @param mixed $usernameOrObj
	 * @param string $password
	 * @throws SoapFault
	 * @return boolean
	 */
	static public function authenticate( $usernameOrObj, $password ) {
		if( is_object( $usernameOrObj ) ) {
			$loginKey = $usernameOrObj->username;
			$loginSecret = $usernameOrObj->password;
		} else {
			$loginKey = $usernameOrObj;
			$loginSecret = $password;
		}

		$auth = Treat_Model_SmartMenu_Auth::connect( $loginKey, $loginSecret );

		if( !$auth ) {
			throw new SoapFault( '401', 'Access denied' );
		}


		static::$client = $auth->getClient( );

		return true;
	}

	/**
	 *
	 * smart menu retrieve function
	 * @param string $packageFile
	 * @throws SoapFault
	 * @return string
	 * @SOAPreturn base64Binary
	 */
	public function retrieve( $packageFile ) {
		if( !static::$client )
			throw new SoapFault( '403', 'Access denied' );
		extract( (array)$packageFile, EXTR_OVERWRITE );

		try {
			$package = Treat_Model_SmartMenu_Package::getByPackageFile( $packageFile );
			$packageData = $package->retrieve( );

			$soapResponse = new SoapVar( $packageData, XSD_BASE64BINARY );
		} catch( Exception $e ) {
			throw new SoapFault( '500', $e->getMessage( ) );
		}

		return array( 'return' => $soapResponse );
	}
	
	/**
	 *
	 * smart menu destroy function
	 * @param string $packageFile
	 * @throws SoapFault
	 * @return boolean
	 */
	public function destroy( $packageFile ) {
		if( !static::$client )
			throw new SoapFault( '403', 'Access denied' );
		extract( (array)$packageFile, EXTR_OVERWRITE );

		try {
			Treat_Model_SmartMenu_Package::destroy( static::$client, $packageFile );
		} catch( Exception $e ) {
			throw new SoapFault( '500', $e->getMessage( ));
		}

		return array( 'return' => true );
	}

	/**
	 *
	 * smart menu link function
	 * @param string $boardGuid
	 * @param string $packageFile
	 * @param string $locationGuid
	 * @throws SoapFault
	 * @return boolean
	 */
	public function link( $boardGuid, $packageFile, $locationGuid = NULL ) {
		if( !static::$client )
			throw new SoapFault( '403', 'Access denied' );
		extract( (array)$boardGuid, EXTR_OVERWRITE );

		try {
			Treat_Model_SmartMenu_PackageLink::link( $boardGuid, $packageFile, $locationGuid );
		} catch( Exception $e ) {
			throw new SoapFault( '500', $e->getMessage( ) );
		}

		return array( 'return' => true );
	}

	/**
	 *
	 * smart menu unlink function
	 * @param string $boardGuid
	 * @param string $packageFile
	 * @throws SoapFault
	 * @return boolean
	 */
	public function unlink( $boardGuid, $packageFile ) {
		if( !static::$client )
			throw new SoapFault( '403', 'Access denied' );
		extract( (array)$boardGuid, EXTR_OVERWRITE );

		try {
			$package = Treat_Model_SmartMenu_PackageLink::getByBoardAndPackage( $boardGuid, $packageFile );
			$package->delete( );
		} catch( Exception $e ) {
			throw new SoapFault( '500', $e->getMessage( ) );
		}

		return array( 'return' => true );
	}

	/**
	 *
	 * smart menu request function
	 * @param string $boardGuid
	 * @throws SoapFault
	 * @return PackageDetails[]
	 */
	public function request( $boardGuid ) {
		if( !static::$client )
			throw new SoapFault( '403', 'Access denied' );
		extract( (array)$boardGuid, EXTR_OVERWRITE );

		try {
			$packageLinks = Treat_Model_SmartMenu_PackageLink::getByBoardGuid( $boardGuid );
			$baseURI = $this->serverURI->getScheme( )
							. '://'
							. $this->serverURI->getHost( )
							. '/'
							. $this->serverURI->getController( )
							. '/retrieve?id=';
			$soapResponse = array( );

			foreach( $packageLinks as $packageLink ) {
				$package = $packageLink->getPackage( );

				$request = Treat_Model_SmartMenu_PackageRequest::create( $package->packageFile );
				$requestURI = $baseURI . $request->id;

				$response = new PackageDetails( $package->packageFile,
												 date( 'c', strtotime( $package->modifiedTime ) ),
												 $requestURI );

				$soapResponse[] = $response;
			}
		} catch( Exception $e ) {
			throw new SoapFault( '500', $e->getMessage( ) );
		}

		return array( 'return' => $soapResponse );
	}
	
	/**
	 * smart menu request all data files function
	 * @param string $packageId
	 * @throws SoapFault
	 * @return PackageDetails[]
	 */
	public function requestDataFiles( $packageId ) {
		if( !static::$client )
			throw new SoapFault( 'Access denied', 403 );
		extract( (array)$packageId, EXTR_OVERWRITE );
		
		try {
			$package = Treat_Model_SmartMenu_Package::getByPackageFile( $packageId );
			$files = Treat_Model_SmartMenu_File::getByPackageId( $package->id );
			$baseURI = $this->serverURI->getScheme( ) . '://' . $this->serverURI->getHost( ) . '/' . $this->serverURI->getController( ) . '/retrieve?id=';
			
			$soapResponse = array( );
			
			foreach( $files as $file ) {
				$request = Treat_Model_SmartMenu_PackageRequest::create( $file->dataFile, 'datafile' );
				$requestURI = $baseURI . $request->id;
				
				$response = new PackageDetails( $file->dataFile, date( 'c', strtotime( $file->modifiedTime ) ), $requestURI, $file->fileName );
				$soapResponse[] = $response;
			}
		} catch( Exception $e ) {
			throw new SoapFault( $e->getMessage( ), 500 );
		}
		
		return array( 'return' => $soapResponse );
	}
	
	/**
	 * smart menu request font function
	 *
	 * @param string $fontFile
	 * @throws SoapFault
	 * @return PackageDetails
	 */
	public function requestFont( $fontFile ) {
		if( !static::$client )
			throw new SoapFault( 'Access denied', 403 );
		extract( (array)$fontFile, EXTR_OVERWRITE );
		
		try {
			if( !( $font = Treat_Model_SmartMenu_Font::getById( $fontFile ) ) )
				throw new Exception( 'Font file does not exist' );
			
			$request = Treat_Model_SmartMenu_PackageRequest::create( $font->fontFile, 'font' );
			$requestURI = $this->serverURI->getScheme( ) . '://' . $this->serverURI->getHost( ) . '/' . $this->serverURI->getController( ) . '/retrieve?id=' . $request->id;
			
			$soapResponse = new PackageDetails( $font->fontFile, date( 'c', strtotime( $font->createdTime ) ), $requestURI );
		} catch( Exception $e ) {
			throw new SoapFault( $e->getMessage( ), 500 );
		}
		
		return array( 'return' => $soapResponse );
	}
	
	/**
	 * smart menu request data file function
	 *
	 * @param string $dataFile
	 * @throws SoapFault
	 * @return PackageDetails
	 */
	public function requestDataFile( $dataFile ) {
		if( !static::$client )
			throw new SoapFault( 'Access denied', 403 );
		extract( (array)$dataFile, EXTR_OVERWRITE );
		
		try {
			if( !( $file = Treat_Model_SmartMenu_File::getByDataFile( $dataFile ) ) )
				throw new Exception( 'Data file does not exist' );
			
			$request = Treat_Model_SmartMenu_PackageRequest::create( $file->dataFile, 'datafile' );
			$requestURI = $this->serverURI->getScheme( ) . '://' . $this->serverURI->getHost( ) . '/' . $this->serverURI->getController( ) . '/retrieve?id=' . $request->id;
			
			$soapResponse = new PackageDetails( $file->dataFile, date( 'c', strtotime( $file->modifiedTime ) ), $requestURI );
		} catch( Exception $e ) {
			throw new SoapFault( $e->getMessage( ), 500 );
		}
		
		return array( 'return' => $soapResponse );
	}

	/**
	 *
	 * smart menu control function
	 * @param string $boardGuid
	 * @throws SoapFault
	 * @return string
	 */
	public function control( $boardGuid ) {
		if( !static::$client )
			throw new SoapFault( '403', 'Access denied' );
		extract( (array)$boardGuid, EXTR_OVERWRITE );

		try {
			$collection = Treat_Model_SmartMenu_Control::getCollectionByBoardGuid( $boardGuid );
			$xml = $collection->toXml( );
		} catch( Exception $e ) {
			throw new SoapFault( '500', $e->getMessage( ) );
		}

		return array( 'return' => $xml );
	}

	/**
	 *
	 * smart menu simpleMenu function
	 * @param int $menuId
	 * @throws SoapFault
	 * @return string
	 */
	public function simpleMenu( $menuId ) {
		if( !static::$client )
			throw new SoapFault( '403', 'Access denied' );
		extract( (array)$menuId, EXTR_OVERWRITE );

		try {
			$menu = Treat_Model_SmartMenu_Menu::getById( $menuId );
			$collection = Treat_Model_SmartMenu_MenuItem::getCollectionByMenuId( $menuId, Treat_Model_SmartMenu_MenuItem_Collection::SIMPLE_MENU );
			$xml = $collection->toXml( $menu->menuName );
		} catch( Exception $e ) {
			throw new SoapFault( '500', $e->getMessage( ) );
		}

		return array( 'return' => $xml );
	}

	/**
	 * @param string $boardGuid
	 * @throws SoapFault
	 * @return MenuDetails[]
	 */
	public function menus( $boardGuid ) {
		if( !static::$client )
			throw new SoapFault( '403', 'Access denied' );
		extract( (array)$boardGuid, EXTR_OVERWRITE );

		$menuList = array( );

		try {
			$board = Treat_Model_SmartMenu_Board::getByGuid( $boardGuid );
			$menus = Treat_Model_SmartMenu_Menu::getListByBusinessid( $board->getBusinessId( ) );
			foreach( $menus as $menu ) {
				$menuDetails = new MenuDetails( $menu['id'], $menu['menuName'] );
				$menuList[] = $menuDetails;
			}
		} catch( Exception $e ) {
			throw new SoapFault( '500', $e->getMessage( ) );
		}

		return array( 'return' => $menuList );
	}

	/**
	* @throws SoapFault
	* @return string[]
	*/
	public function locations( ) {
		if( !static::$client )
			throw new SoapFault( '403', 'Access denied' );

		try {
			$locations = Treat_Model_SmartMenu_Location::getByCompanyid( static::$client->getCompanyId( ) );
			Treat_Model_Abstract::collapse( $locations, 'locationGuid' );
		} catch( Exception $e ) {
			throw new SoapFault( '500', $e->getMessage( ) );
		}

		return array( 'return' => $locations );
	}

	/**
	 * @param string $locationGuid
	 * @throws SoapFault
	 * @return string[]
	 */
	public function boards( $locationGuid ) {
		if( !static::$client )
			throw new SoapFault( '403', 'Access denied' );
		extract( (array)$locationGuid, EXTR_OVERWRITE );

		try {
			$location = Treat_Model_SmartMenu_Location::getByGuid( $locationGuid );
			$boards = Treat_Model_SmartMenu_Board::getByBusinessid( $location->businessid );
			Treat_Model_Abstract::collapse( $boards, 'boardGuid' );
		} catch( Exception $e ) {
			throw new SoapFault( '500', $e->getMessage( ) );
		}

		return array( 'return' => $boards );
	}

	/**
	 * @throws SoapFault
	 * @return UserDetails
	 */
	public function userlogin( ) {
		if( !static::$client )
			throw new SoapFault( '403', 'Access denied' );

		try {
			$locations = Treat_Model_SmartMenu_Location::getByCompanyid( static::$client->getCompanyId( ) );

			$oLocations = array( );
			foreach( $locations as $location ) {
				$boards = Treat_Model_SmartMenu_Board::getByBusinessid( $location->businessid );

				$oBoards = array( );
				foreach( $boards as $board ) {
					$packages = Treat_Model_SmartMenu_PackageLink::getByBoardGuid( $board->boardGuid );
					$menus = Treat_Model_SmartMenu_Menu::getListByBusinessid( $board->getBusinessId( ) );
					$screens = Treat_Model_SmartMenu_Board_Screen::get('BoardId = '.$board->id);

					$oPackages = array( );
					foreach( $packages as $package ) {
						$oPackages[] = new LayoutDetails( $package->packageFile, $package->packageFile /*->packageName*/ );
					}

					$oMenus = array( );
					foreach( $menus as $menu ) {
						$oMenus[] = new MenuDetails( $menu['id'], $menu['menuName'] );
					}

					$oScreens = array();
					foreach ($screens as $screen) {
						$oScreens[] = new ScreenLayout($screen->Name, $screen->Width, $screen->Height, $screen->OffsetX, $screen->OffsetY, $screen->Primary);
					}

					$oVersions = array();

					$serverInfo = new ServerInfo($oScreens, $oVersions);
					$oBoards[] = new BoardDetails( $board->boardGuid, $board->boardName, $oPackages, $oMenus, $serverInfo);
				}
				$oLocations[] = new BusinessDetails( $location->locationGuid, $location->businessname, $oBoards );
			}

			if( static::$client->user ) {
				$fullName = static::$client->user->firstname . ' ' . static::$client->user->lastname;
				$companyName = static::$client->getCompany( )->companyname;
			} else {
				$fullName = "Smart Menu User";
				$companyName = "";
			}
			$userDetails = new UserDetails( $fullName, $companyName, $oLocations );

		} catch( Exception $e ) {
			throw new SoapFault( '500', $e->getMessage( ) );
		}

		return array( 'return' => $userDetails );
	}

	/**
	 * @param BoardDetails $boardDetails
	 * @return boolean
	 * @throws SoapFault
	 */
	public function updateServerInfo( $boardDetails )
	{
		if( !static::$client )
			throw new SoapFault( '403', 'Access denied' );
		extract((array)$boardDetails, EXTR_OVERWRITE);

		if (empty($boardDetails->serverInfo)) {
			throw new SoapFault( '500', 'No server information available' );
		}

		try {
			if (!($board = Treat_Model_SmartMenu_Board::getByGuid($boardDetails->id)))
				throw new SoapFault( '404', 'Menu board not found' );
			Treat_Model_SmartMenu_Board_Screen::remove($board->id);

			foreach ($boardDetails->serverInfo->Screens as $screen) {
				$obj = new Treat_Model_SmartMenu_Board_Screen(null, $screen);
				$obj->BoardId = $board->id;
				$obj->save();
				unset($obj);
			}

			if (is_array($boardDetails->serverInfo->Versions)) {
				foreach ($boardDetails->serverInfo->Versions as $version) {
					$obj = new Treat_Model_SmartMenu_Board_Layout(null, $version);
					$obj->BoardId = $board->id;
					$obj->save();
					unset($obj);
				}
			}
		} catch (Exception $e) {
			if ($e instanceof SoapFault) throw $e;
			throw new SoapFault( '200', $e->getMessage());
		}

		return(array('return' => true));
	}
	
	/**
	 * @param string $boardGuid
	 * @param ScreenLayout[] $screens
	 * @return boolean
	 * @throws SoapFault
	 */
	public function reportCurrentScreens( $boardGuid, $screens )
	{
		if( !static::$client )
			throw new SoapFault( '403', 'Access denied' );
		extract((array)$boardGuid, EXTR_OVERWRITE);
		
		try {
			if (!($board = Treat_Model_SmartMenu_Board::getByGuid( $boardGuid )))
				throw new SoapFault( '404', 'Menu board not found' );
			Treat_Model_SmartMenu_Board_Screen::remove($board->id);

			if(!is_array($screens->ScreenLayout))
			{
				$screens->ScreenLayout = array($screens->ScreenLayout);
			}
						
			foreach ($screens->ScreenLayout as $screen) {
				$obj = new Treat_Model_SmartMenu_Board_Screen(null, $screen);
				$obj->BoardId = $board->id;
				$obj->save();
				unset($obj);
			}
		} catch (Exception $e) {
			if ($e instanceof SoapFault) throw $e;
			throw new SoapFault( '200', $e->getMessage());
		}

		return(array('return' => true));
	}
	
	/**
	 * @param string $boardGuid
	 * @param LibraryVersion[] $libraries
	 * @return boolean
	 * @throws SoapFault
	 */
	public function reportCurrentLibraries( $boardGuid, $libraries )
	{
		if( !static::$client )
			throw new SoapFault( '403', 'Access denied' );
		extract((array)$boardGuid, EXTR_OVERWRITE);
		
		try {
			if (!($board = Treat_Model_SmartMenu_Board::getByGuid( $boardGuid )))
				throw new SoapFault( '404', 'Menu board not found' );
			Treat_Model_SmartMenu_Board_Library::remove($board->id);
			
			if(!is_array($libraries->LibraryVersion))
			{
				$libraries->LibraryVersion = array($libraries->LibraryVersion);
			}
			
			foreach ((array)$libraries->LibraryVersion as $library) {
				$obj = new Treat_Model_SmartMenu_Board_Library(null, $library);
				$obj->BoardId = $board->id;
				$obj->save();
				unset($obj);
			}
		} catch (Exception $e) {
			if ($e instanceof SoapFault) throw $e;
			throw new SoapFault( '200', $e->getMessage());
		}

		return(array('return' => true));
	}
	
	/**
	 * @param string $boardGuid
	 * @return ReservedAreaDetails[]
	 * @throws SoapFault
	 */
	public function GetReservedAreas( $boardGuid ) {
		if( !static::$client )
			throw new SoapFault( '403', 'Access denied' );
		extract( (array)$boardGuid, EXTR_OVERWRITE );
		
		try {
			
			$reservedAreas = array(
				new ReservedAreaDetails( 100, 100, 100, 100, '' )
			);
			
		} catch( Exception $e ) {
			if( $e instanceof SoapFault ) throw $e;
			throw new SoapFault( '500', $e->getMessage( ) );
		}
		
		return( array( 'return' => $reservedAreas ) );
	}
}





class PackageDetails {
	/**
	 * @var string
	 */
	public $packageFile;

	/**
	 * @var string
	 * @SOAPvar dateTime
	 */
	public $modifiedTime;

	/**
	 * @var string
	 * @SOAPvar anyURI
	 */
	public $requestUri;
	
	/**
	 * @var string
	 */
	public $details;

	function __construct( $packageFile, $modifiedTime, $requestUri, $details = '' ) {
		$this->packageFile = $packageFile;
		$this->modifiedTime = $modifiedTime;
		$this->requestUri = $requestUri;
		$this->details = $details;
	}
}

class MenuDetails {
	/**
	 * @var int
	 */
	public $menuId;

	/**
	 * @var string
	 */
	public $menuName;

	function __construct( $menuId, $menuName ) {
		$this->menuId = $menuId;
		$this->menuName = $menuName;
	}
}

class UserDetails {
	/**
	 * @var string
	 */
	public $fullname;

	/**
	 * @var string
	 */
	public $company;

	/**
	 * @var BusinessDetails[]
	 */
	public $businesses;

	function __construct( $fullname, $company, $businesses ) {
		$this->fullname = $fullname;
		$this->company = $company;
		$this->businesses = $businesses;
	}
}

class BusinessDetails {
	/**
	 * @var string
	 */
	public $id;

	/**
	 * @var string
	 */
	public $description;

	/**
	 * @var BoardDetails[]
	 */
	public $boards;

	function __construct( $id, $description, $boards ) {
		$this->id = $id;
		$this->description = $description;
		$this->boards = $boards;
	}
}

class BoardDetails {
	/**
	 * @var string
	 */
	public $id;

	/**
	 * @var string
	 */
	public $description;

	/**
	 * @var LayoutDetails[]
	 */
	public $layouts;

	/**
	 * @var MenuDetails[]
	 */
	public $menus;

	/**
	 * @var ServerInfo
	 */
	public $serverInfo;

	function __construct( $id, $description, $layouts, $menus, $serverInfo ) {
		$this->id = $id;
		$this->description = $description;
		$this->layouts = $layouts;
		$this->menus = $menus;
		$this->serverInfo = $serverInfo;
	}
}

class LayoutDetails {
	/**
	 * @var string
	 */
	public $id;

	/**
	 * @var string
	 */
	public $description;

	function __construct( $id, $description) {
		$this->id = $id;
		$this->description = $description;
	}
}

class ServerInfo {
	/**
	 * @var ScreenLayout[]
	 */
	public $Screens = array();

	/**
	 * @var LibraryVersion[]
	 */
	public $Versions = array();

	public function __construct(array $screens, array $versions)
	{
		$this->Screens = $screens;
		$this->Versions = $versions;
	}
}

class LibraryVersion {
	/**
	 * @var string
	 */
	public $AssemblyName;

	/**
	 * @var string
	 */
	public $AssemblyVersion;

	/**
	 * @var string
	 */
	public $AssemblyFileVersion;

	public function __construct($assemblyName, $assemblyVersion, $assemblyFileVersion)
	{
		$this->AssemblyFileVersion = $assemblyFileVersion;
		$this->AssemblyName = $assemblyName;
		$this->AssemblyVersion = $assemblyVersion;
	}
}

class ScreenLayout {
	/**
	 * @var int
	 */
	public $Height;

	/**
	 * @var string
	 */
	public $Name;

	/**
	 * @var int
	 */
	public $OffsetX;

	/**
	 * @var int
	 */
	public $OffsetY;

	/**
	 * @var boolean
	 */
	public $Primary;

	/**
	 * @var int
	 */
	public $Width;

	public function __construct($name, $width, $height, $offsetX = 0, $offsetY = 0, $primary = false)
	{
		$this->Height = $height;
		$this->Name = $name;
		$this->OffsetX = $offsetX;
		$this->OffsetY = $offsetY;
		$this->Primary = $primary;
		$this->Width = $width;
	}

}

class ReservedAreaDetails {
	/**
	 * @var int
	 */
	public $left;
	
	/**
	 * @var int
	 */
	public $top;
	
	/**
	 * @var int
	 */
	public $width;
	
	/**
	 * @var int
	 */
	public $height;
	
	/**
	 * @var string
	 */
	public $packageGuid;
	
	public function __construct( $left, $top, $width, $height, $packageGuid ) {
		$this->left = $left;
		$this->top = $top;
		$this->width = $width;
		$this->height = $height;
		$this->packageGuid = $packageGuid;
	}
}