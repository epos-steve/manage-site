<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of cron
 *
 * @author eric
 */
class CronController extends Manage_Controller_Abstract
{
	/**
	 * Tables to archive using the archive method here. Should be organized
	 * using database => array(table => datefield, table => datefield) format.
	 *
	 * @var array
	 */
	private static $_archiveTables = array(
		'vivipos_order' => array(
			'cashdrawer_records'    => 'modified',
			'cc_transactions'       => 'time_stamp',
			'kiosk_logs'            => 'created',
			'order_items'           => 'modified',
			'order_item_condiments' => 'modified',
			'order_objects'         => 'modified',
			'order_payments'        => 'modified',
			//'order_promotions'    => 'modified', Don't do this one yet since there is no trigger
			'order_receipts'        => 'modified',
			'orders'                => array('field' => 'modified', 'linked' => array('order_item_taxes' => array('id', 'order_id'))),
			'syncs'                 => 'modified'
		)
	);

	protected $_argMap = array(
		'viviposSend' => array(
			'bid'
		)
	);

	protected $_components = array('DetailCreation');

	protected $_models = array('ClientSchedule', 'ClientPrograms');

	public function processSync() {
		set_time_limit(900);
		
		$vivipos = new Manage_DB( 'vivipos db');
		$vivipos->exec('USE machineStatus');
		
		$logger = new \EE\Sync\Kiosk\Logger($vivipos);
		$logger->enableErrorHandler();
		
		try {
			$failedSyncs = $vivipos->query('SELECT * FROM `syncData` ORDER BY RAND() LIMIT 100')->fetchAll(\PDO::FETCH_OBJ);
			
			if (count($failedSyncs) < 1) {
				return;
			}
			
			$completedList = array();
			
			if (is_array($failedSyncs)) {
				$startTotal = microtime(true);
				$syncElapseTimes = array();
				foreach ($failedSyncs as $failedSync) {
					$startFailedSyncElapse = microtime(true);
					$processor = new \EE\Sync\Kiosk\Processor($logger);
					$processor->clientProgram($failedSync->client_programid);
					
					if ( false === $processor->decode($failedSync->data, $failedSync->id) || false === $processor->validate() ) {
						$completedList[] = $failedSync->id;
						continue;
					}
					
					try {
						if ($processor->run()) {
							$completedList[] = $failedSync->id;
						}
					} catch (\Exception $e) {
						$logger->exception($e);
					}
					
					$syncElapseTimes[] = microtime(true) - $startFailedSyncElapse;
				}
				$elapse = round(microtime(true) - $startTotal, 6);
				
				$averageElapse = 0;
				if (count($syncElapseTimes) > 0) {
					$averageElapse = round(array_sum($syncElapseTimes) / count($syncElapseTimes), 6);
				}
				
				$logger->log('DEBUG', count($syncElapseTimes)." records took $elapse seconds with average of $averageElapse seconds to complete.");
				
				if (count($completedList)) {
					$vivipos->exec('DELETE FROM `syncData` WHERE `id` IN ('.implode(',', $completedList).')');
				}
			}
		} catch (\PDOException $e) {
			$logger->exception($e);
		}
		
		var_dump('DONE!');

		return false;
	}

	/**
	 * Archive the database to minimize the amount of data we have to deal with.
	 */
	public function archive()
	{
		die('disabled');
		// Currently we back up everything older than a month
		$archiveDate = strtotime('-1 month');

		foreach (self::$_archiveTables as $database => $tables) {
			$this->_db->exec('USE '.$database);
			echo '<h3>Database '.$database.'</h3>';
			foreach ($tables as $table => $field) {
				$format = $archiveDate;
				$linkedTable = $linkedFields = null;
				$this->_db->beginTransaction();
				echo '<div>Archiving table <strong>'.$table.'</strong>...&nbsp;';

				if (is_array($field)) {
					$info = $field;
					if (isset($info['field'])) {
						if (is_array($info['field'])) {
							$field = $info['field'][0];
							$format = date($info['field'][1], $archiveDate);
						} else {
							$field = $info['field'];
						}
					}

					if (isset($info['linked'])) {
						$linkedTable = key($info['linked']);
						$linkedFields = $info['linked'][$linkedTable];
					}
				}

				// Now remove the old records from the regular table.
				try {
					if (!empty($linkedTable)) {
						$rows = $this->_db->exec('DELETE t1.*, t2.* FROM '.$table.' AS t1, '.$linkedTable.' AS t2 WHERE (t1.'.$linkedFields[0].' = t2.'.$linkedFields[1].' OR t2.'.$linkedFields[1].' IS NULL) AND t1.'.$field.' < ?', array($archiveDate));
					} else {
						$rows = $this->_db->exec('DELETE FROM '.$table.' WHERE '.$field.' < ?', array($archiveDate));
					}
				} catch (PDOException $e) {
					echo '<strong style="color: red;">Failed!</strong>';
					echo '<br /><strong>MySQL Said</strong>: '.$e->getMessage();
					echo '</div>';
					$this->_db->rollBack();
					continue;
				}

				$this->_db->commit();
				echo '<strong style="color: green;">Done</strong> ('.$rows.' records archived)';
				echo '</div>';
			}
		}
	}

	/**
	 * Schedule all the programs to send data here.
	 */
	public function schedule()
	{
		// schedule all clients that have active schedules now
		ClientScheduleModel::scheduleReady();
		return(false);
	}
	
	public function sync_customer_balance() {
		$kioskList = array(
			677, 678, 679, 1751,
		);
		$query = "
			SELECT
				GROUP_CONCAT( DISTINCT  `client_programid` SEPARATOR  ',' ) AS client_programid,
				`db_ip`,
				`db_name`,
				`db_user`,
				`db_pass`
			FROM `client_programs` 
			WHERE `businessid` > 0 AND `pos_type` = 7
			GROUP BY `db_ip` , `db_name` , `db_user` , `db_pass` 
			";
		
		$clients = ClientProgramsModel::db()->query($query)->fetchAll(\PDO::FETCH_OBJ);
		
		foreach($clients as $client) {
			if (empty($client->db_ip) || empty($client->db_name) || empty($client->db_user) || empty($client->db_pass)) {
				echo "<p>No connection info.</p>";
				continue;
			}
			
			try {
				Manage_DB_Client::$client = $client;
				$cdb = new Manage_DB_Client();
				$cdb->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

				\EE\Model\KioskSync::db($cdb);
				\EE\Model\ClientPrograms::db(ClientProgramsModel::db());
				$stmt = \EE\Model\KioskSync::db()->prepare(
						"REPLACE INTO kiosk_sync (client_programid, item_id, item_type) VALUES(?,?,?)"
					);
				
				if (strpos($client->client_programid, ',') !== false) {
					$client_programids = explode(',', $client->client_programid);
				} else {
					$client_programids = array($client->client_programid);
				}
				
				foreach($client_programids as $client_programid) {
					if (!in_array($client_programid, $kioskList)) {
						continue;
					}
					$stmt->execute(array($client_programid, 1, \EE\Model\KioskSync::TYPE_CUSTOMER_SYNC));
				}
				$cdb = null;
				echo "<p>Add customer balance sync for {$client->db_name}.</p>";
			} catch(\PDOException $e) {
				echo "<p>Could not connect.</p>";#: ".$e->getMessage()."</p>";
				#echo "<pre>".$e->getTraceAsString()."</pre>";
			}
		}
		
		return false;
	}
	
	public function syncManageTimes(){
		$this->_vdb = new Manage_DB( 'vivipos db');
		$mDateTime = date('Y-m-d H:i:s',time() - 15 * 60);
		
		$clientProgramIds = '';
		
		$this->_vdb->exec('USE machineStatus');
		
		$stmnt = $this->_vdb->query( "SELECT * FROM manageClients WHERE send_time >= '$mDateTime'" );
		$array = $stmnt->fetchAll( PDO::FETCH_ASSOC );
		
		foreach( $array AS $line ){
			$clientProgramIds .= $line["client_programid"] . ",";
		}
		
		$clientProgramIds = substr_replace($clientProgramIds ,"",-1);
		
		$this->_db->exec("UPDATE client_programs SET send_time = NOW(), receive_status = 2 WHERE client_programid IN ($clientProgramIds)");
		
		return(false);
	}
	
	public function syncManageIPs(){
		$this->_vdb = new Manage_DB( 'vivipos db');
		
		$mDateTime = date('Y-m-d H:i:s',time() - 10 * 60);
		
		$this->_vdb->exec('USE machineStatus');
		
		$stmnt = $this->_vdb->query( "SELECT * FROM manageClients WHERE send_time >= '$mDateTime'" );
		$array = $stmnt->fetchAll( PDO::FETCH_ASSOC );
		
		foreach( $array AS $line ){
			$client_programid = $line["client_programid"];
			$client_ip = $line["client_ip"];
			
			$this->_db->exec("UPDATE client_programs SET client_ip = '$client_ip' WHERE client_programid = $client_programid");
		}
		
		return(false);
	}

	public function viviposSend()
	{
		$cronFile = SITECH_APP_PATH.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'viviposSend.run';
		// Added check so if file is older than two hours, the cron will still run.
		// Changed timer to 4 hours since we're getting duplicates - Eric - 10/05/2012
		if (file_exists($cronFile) && filemtime($cronFile) > strtotime('-4 hours')) {
			error_log('Cron already running. Ending execution.', 4);
			return(false);
		}

		file_put_contents($cronFile, 1);
		register_shutdown_function(function ($file) { unlink($file); }, $cronFile);
		$clients = ClientProgramsModel::get('pos_type = 7 AND send_status = 2 AND sync_version = \'1\' GROUP BY businessid');

		foreach($clients AS $client){
			$this->_detailCreation($client);
		}

		if (empty($clients)) {
			echo 'No clients scheduled to run';
		}

		return(false);
	}
	
	public function override()
	{
		$pid = $_GET["pid"];
		$client = ClientProgramsModel::get('client_programid = ' . $pid . ' AND pos_type = 7 AND sync_version = \'1\'', true);
		$this->_detailCreation($client, true);
		
		return(false);
	}
	
	protected function _detailCreation(ClientProgramsModel $client, $force = false)
	{
		echo '<h2>'.$client->name.'</h2>'."\n";
		if ($this->DetailCreation->setClient($client, $this->_uri) && ($this->DetailCreation->data['todo'] == 2 || $force === true)) {
			
			if($force === true){
				$this->DetailCreation->start = strtotime('midnight');
				$this->DetailCreation->end = strtotime('+1 day', $this->DetailCreation->start);
			}
			
			$this->DetailCreation->createHeaderSection();
			$this->DetailCreation->createSalesSection();
			$this->DetailCreation->createTaxesSection();
			$this->DetailCreation->createTendersSection();
			$this->DetailCreation->createDiscountSection();
			$this->DetailCreation->createPaymentDetailSection();
			$this->DetailCreation->createChecksSection();
			$this->DetailCreation->createCheckDetailSection();
			$this->DetailCreation->createDiscountDetailSection();
			echo '<hr />'."\n";
			// Finalize the data and send it
			$this->DetailCreation->sendData();
		} else {
			echo 'Told to do nothing';
		}
		echo '<hr />'."\n";
		// Now get the e-mails done - this runs every time reguardless of
		// the schedule that is set
		$this->DetailCreation->sendEmails();
	}
}
