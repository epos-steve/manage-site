<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of aerispos
 *
 * @author Eric Gach <eric@essential-elements.net>
 * @version $Id$
 */
class AerisposController extends Manage_Controller_Abstract
{
	protected $_argMap = array(
		'add' => array('id'),
		'delete' => array('id'),
		'upgrade' => array('id')
	);

	protected $_layout = 'aerispos.tpl';

	public function add()
	{
		$upgrade = ViviposUpgradesModel::get((int)$this->_args['id'], true);
		if (SiTech_Filter::isPost() && !empty($_POST['update'])) {
			foreach ($_POST['update'] as $client) {
				//$this->_db->insert('vivipos_upgrade_client', array('ClientProgram' => $client, 'ViviposUpdate' => $upgrade->Id));
				$upgradeClient = new ViviposUpgradeClientModel();
				$upgradeClient->ClientProgram = $client;
				$upgradeClient->ViviposUpdate = $upgrade->Id;
				$upgradeClient->Added = date('Y-m-d H:i:s');
				$upgradeClient->save();
			}

			header('Location: '.SITECH_BASEURI.'/aerispos/upgrade/'.$this->_args['id']);
			exit;
		}

		$this->_view->assign('upgrade', $upgrade);
		$this->_view->assign('clients', ClientProgramsModel::get('pos_type = 7'));
		if ($this->_isXHR) {
			echo json_encode(array('html' => $this->_view->render('aerispos/add.tpl')));
			return(false);
		}
	}

	public function delete()
	{
		$package = ViviposUpgradesModel::get((int)$this->_args['id'], true);
		$package->delete();
		header('Location: '.SITECH_BASEURI.'/aerispos');
		exit;
	}

	public function index()
	{
		$this->_user->requireLogin();
		$this->_view->assign('upgrades', ViviposUpgradesModel::get());
	}

	/**
	 * This is to view a specific upgrade. From the view used here you should be
	 * able to see what machines have received the upgrade so far and add new
	 * machines to the list to be upgraded.
	 */
	public function upgrade()
	{
		if (empty($this->_args['id'])) {
			throw new SiTech_Exception('No upgrade specified, unable to continue');
		}

		$upgrade = ViviposUpgradesModel::get((int)$this->_args['id'], true);
		$this->_view->assign('upgrade', $upgrade);
	}

	public function upload()
	{
		if ((bool)($this->_user->permissions & MANAGE_PERMISSION_UPLOAD) === false) return(false);
		if (!empty($_FILES)) {
			try {
				$filter = new SiTech_Filter(SiTech_Filter::INPUT_POST);
				require_once(SITECH_APP_PATH.'/controllers/components/CreateUpgrade.php');
				$workgroup = ($filter->hasVar('workgroup'))? $filter->input('workgroup', SiTech_Filter::FILTER_SANITIZE_STRING, array(SiTech_Filter::FLAG_EMPTY_STRING_NULL)) : null;
				$CreateUpgrade = new CreateUpgrade($workgroup);
				$CreateUpgrade->extractFiles();
				$CreateUpgrade->createPackage();
				$CreateUpgrade->cleanup();
				header('Location: '.SITECH_BASEURI.'aerispos');
				exit;
			} catch (Exception $e) {
				$this->_view->assign('error', $e->getMessage());
				// We want to clean up our old files
				if (isset($CreateUpgrade) && is_object($CreateUpgrade)) {
					$CreateUpgrade->cleanup();
				}
			}
		}
	}
}
