<?php
/**
 * Remote controller
 * 
 * Controller for handling remote access requests
 * 
 * @author		Ryan Pessa <ryan@essential-elements.net>
 * @copyright	Copyright (c) 2013 Essential Elements, LLC (http://www.essential-elements.net)
 * @license		EE-Proprietary
 */

if( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname( __FILE__ ) . '/../../htdocs/' ) );
}

class RemoteController extends Manage_Controller_Abstract {
	protected $_client;
	
	protected $_debug = false;
	
	protected function init( ) {
		if( isset( $_GET['debug'] ) ) {
			$this->_debug = true;
		} else {
			ini_set( 'display_errors', 'false' );
		}
		
		$this->_client = ClientProgramsModel::get( (int)$this->_args[0], true );
	}
	
	protected function _connect( $protocol, array $extraParams = array() ) {
		if( !$this->_allowAccess( ) || $this->_client == null ) {
			echo 'Access Denied';
			return;
		}
		
		$name = sprintf( 'aeris%05d' , $this->_client->client_programid );
		$connId = $name . '-' . $protocol;
		$hostname = $name . '.vpn';
		
		# Connect VNC or SSH
		if ($protocol == 'vnc' ||  $protocol == 'ssh') {
			
			if( $this->_debug )
				print '<a href="' . \EE\Guacamole::generateUrl( $connId, $protocol, $hostname, $extraParams ) . '">' . $name . '</a>';
			else
				header( 'Location: ' . \EE\Guacamole::generateUrl( $connId, $protocol, $hostname, $extraParams ) );
		
		# Get the Logs files
		} else if ($protocol == 'logs') {
			
			$url = "aeris2@" . $name . ".vpn.essential-elements.net";
			$script = DOC_ROOT . '/../vendors/Treat/scripts/aeris2/getlogs.sh';
			$exec = "$script $url";
			
			header('Content-type: application/gzip');
			header("Content-Disposition: attachment; filename='$name-logs.tar.gz'");
			print shell_exec($exec);
		}
		
		return;
	}
	
	public function vnc( ) {
		$this->_connect( 'vnc' );
	}
	
	public function ssh( ) {
		$this->_connect( 'ssh', array( 'guac.username' => 'aeris2' ) );
	}
	
	public function logs( ) {
		$this->_connect( 'logs' );
	}
	
	protected function _allowAccess( ) {
		$this->_user->requireLogin( );
		
		if( $this->_user->isAdmin( ) )
			return true;
		
		return false;
	}
}

