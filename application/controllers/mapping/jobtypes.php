<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of jobtypes
 *
 * @author Eric Gach <eric@essential-elements.net>
 */
class Mapping_JobtypesController extends Manage_Controller_Abstract
{
	protected $_layout = 'general.tpl';
	
	public function index()
	{
		$client = ClientProgramsModel::get((int)$this->_args[0], true);
		$this->_view->assign('client', $client);
		$this->_view->assign('jobTypes', MappingModel::getByMappingType(12, $client));
		$this->_view->assign('btJobTypes', $client->db->query('SELECT * FROM jobtype WHERE companyid = ?', array($client->getCompanyId()))->fetchAll(PDO::FETCH_OBJ));
		$this->_display('dashboard/job_types.tpl');
	}

	public function save()
	{
		if (!empty($_POST)) {
			foreach ($_POST['jobtype'] as $jobtype => $localid) {
				$mapping = MappingModel::get((int)$jobtype, true);
				$mapping->localid = $localid;
				$mapping->save();
			}
		}

		header('Location: '.SITECH_BASEURI.'dashboard/manage/'.$this->_args[0].'#job_types');
		exit;
	}
}
