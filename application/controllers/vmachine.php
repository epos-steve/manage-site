<?php
if( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname( __FILE__ ) . '/../../htdocs/' ) );
}

class VmachineController extends Manage_Controller_Abstract {
        
		protected function init( ) {
            $pid = $_POST["pid"];
            
            $this->_client = ClientProgramsModel::get('client_programid = ' . $pid, true);
		}
		
		public function online( ){
			//mark online
			$pid = $_POST["pid"];
			if($pid > 0){
				$this->_mdb = new Manage_DB( 'database');
				$this->_mdb->query("UPDATE client_programs SET send_time = NOW(), receive_time = NOW(), receive_status = 2 WHERE client_programid = $pid");
			}
		}
		
		public function menuList(){
			$pid = $_POST["pid"];
			
			if($pid > 0){				
				$stmnt = $this->_client->db->query("SELECT miv.col_row, mip.id AS mipId, mip.new_price AS new_net, mip.price AS net, ROUND(IF(mip.new_price IS NOT NULL,mip.new_price + (mip.new_price * (mt.percent / 100)),mip.price + (mip.price * (mt.percent / 100))), 2) AS gross
						FROM menu_items_vmach miv
						JOIN menu_items_new min ON min.id = miv.menu_itemid
						JOIN menu_items_price mip ON mip.menu_item_id = miv.menu_itemid
						LEFT JOIN menu_item_tax mit ON mit.menu_itemid = miv.menu_itemid
						LEFT JOIN menu_tax mt ON mt.id = mit.tax_id
						WHERE miv.client_programid = $pid");
				$array = $stmnt->fetchAll( PDO::FETCH_ASSOC );
				
				header("Content-Type: text/plain");
				
				foreach($array AS $line){
					$slot = $line['col_row'];
					$mipId = $line['mipId'];
					$new_net = $line['new_net'];
					$net = $line['net'];
					$gross = $line['gross'];
					
					if(!is_null($new_net)){
						$net = $new_net;
						
						//update menu_items_price
						$this->_client->db->query("UPDATE menu_items_price SET price = '$new_net', new_price = NULL WHERE id = $mipId;");
					}
					
					if($gross == 0){
						$gross = $net;
					}
					
					echo $slot . chr( 9 ) . $net . chr( 9 ) . $gross . "\r\n";
				}
			}
		}
        
        protected function payrollInfo($acct, $pid){
            $this->_mdb = new Manage_DB( 'database');
            
            ///get businessid of terminal
            $stmnt = $this->_mdb->query("SELECT businessid FROM client_programs WHERE client_programid = $pid");
            $array = $stmnt->fetch( PDO::FETCH_ASSOC );
            
            $businessid = $array['businessid'];
            
            ///get businessid's associated with business group
            $stmnt = $this->_client->db->query("select businessid FROM business_group_detail 
                                            WHERE group_id = (SELECT group_id FROM business_group_detail WHERE businessid = $businessid)");
            $array = $stmnt->fetchAll( PDO::FETCH_ASSOC );

            foreach( $array AS $line ) {
                if($businessIDs == ''){
                    $businessIDs = $line['businessid'];
                }
                else{
                    $businessIDs .= ',' . $line['businessid'];
                }
            }
            
            if($businessIDs == ''){
                $businessIDs = $businessid;
            }
         
            ///customer info
            $stmnt = $this->_client->db->query("SELECT customerid AS customerID,
                                                    scancode AS cardNumber,
                                                    balance AS cardBalance,
                                                    CONCAT(first_name, ' ' , last_name) AS customerName       
                                                FROM customer WHERE businessid IN ($businessIDs) AND scancode = '$acct'");
            $customerInfo = $stmnt->fetch( PDO::FETCH_ASSOC );
            
            return $customerInfo;
        }
        
        protected function custInfo($acct){
            $stmnt = $this->_client->db->query("SELECT customerid AS customerID,
                                                    scancode AS cardNumber,
                                                    balance AS cardBalance,
                                                    CONCAT(first_name, ' ' , last_name) AS customerName       
                                                FROM customer WHERE scancode = '$acct'");
            $customerInfo = $stmnt->fetch( PDO::FETCH_ASSOC );
            
            return $customerInfo;
        }
        
        protected function locInfo($pid, $item, $payment_type){
            $this->_mdb = new Manage_DB( 'database');
            
            ///payment_types
            $payments = array(14 => 2, 5 => 6, 1 => 10, 4 => 1);
            
            $stmnt = $this->_mdb->query("SELECT businessid FROM client_programs WHERE client_programid = $pid");
            $array = $stmnt->fetch( PDO::FETCH_ASSOC );
            
            $businessid = $array['businessid'];
            
            $stmnt = $this->_client->db->query("SELECT min.pos_id, mip.cost, mt.percent
                                            FROM menu_items_vmach miv
                                            JOIN menu_items_new min ON min.id = miv.menu_itemid AND min.businessid = $businessid
                                            JOIN menu_items_price mip ON mip.menu_item_id = miv.menu_itemid
                                            LEFT JOIN menu_item_tax mit ON mit.menu_itemid = min.id
                                            LEFT JOIN menu_tax mt ON mt.id = mit.tax_id
                                            WHERE miv.client_programid = $pid AND miv.col_row = '$item'");
            $array = $stmnt->fetch( PDO::FETCH_ASSOC );
            
            $pos_id = $array['pos_id'];
            $cost = $array['cost'];
            $tax = $array['percent'];
			
			///log missing items
			if($pos_id < 1){
				file_put_contents("/tmp/vmach_data.txt", "$pid: $item\r\n", FILE_APPEND | LOCK_EX);
			}
            
            ///sales
            $stmnt = $this->_client->db->query("SELECT creditid,companyid FROM credits WHERE businessid = $businessid AND category = 15");
            $array = $stmnt->fetch( PDO::FETCH_ASSOC );
            
            $creditid = $array['creditid'];
            $companyid = $array['companyid'];
            
            ///tax
            $stmnt = $this->_client->db->query("SELECT creditid FROM credits WHERE businessid = $businessid AND category = 7");
            $array = $stmnt->fetch( PDO::FETCH_ASSOC );
            
            $taxid = $array['creditid'];
            
            ///payment
            $stmnt = $this->_client->db->query("SELECT debitid FROM debits WHERE businessid = $businessid AND category = $payments[$payment_type]");
            $array = $stmnt->fetch( PDO::FETCH_ASSOC );
            
            $debitid = $array['debitid'];
            
            $localInfo = array('businessid' => $businessid, 'companyid' => $companyid, 'pos_id' => $pos_id, 'cost' => $cost, 'creditid' => $creditid, 'taxid' => $taxid, 'debitid' => $debitid, 'tax' => $tax);
            
            return $localInfo;
        }
        
        public function balance(){
            $pid = $_POST["pid"];
            $acct = $_POST["acct"];
            
            if(!$pid || !$acct){
                $response = array('responseCode' => 'CUSTNOTFOUND', 'responseMessage' => 'customer not found');
            }
            else{
                if(substr($acct, 0 ,3) != '780'){
                    $customerInfo = VmachineController::payrollInfo($acct, $pid);
                }
                else{
                    $customerInfo = VmachineController::custInfo($acct);
                }
                
                if($customerInfo['customerID'] > 0){
                    ///response
                    $response = array('responseCode' => 'OK', 'responseMessage' => 'success', 'customerInfo' => $customerInfo);
                }
                else{
                    $response = array('responseCode' => 'CUSTNOTFOUND', 'responseMessage' => 'customer not found');
                }
            }
            
            header("Content-Type: text/plain");
            echo json_encode($response);
        }
        
        public function transaction(){
            $pid = $_POST["pid"];
            $acct = $_POST["acct"];
            $amount = $_POST["amount"];
            $tid = $_POST["tid"];
            $payment_type = $_POST["payment_type"];
            $item = $_POST["item"];
            
            if(!$pid || !$acct || !$amount || !$tid || !$payment_type || !$item){
                $response = array('responseCode' => 'CUSTNOTFOUND', 'responseMessage' => 'customer not found');
            }
            else{   
				if($acct == -1){
					////cash transactions
					$customerInfo = array('customerID' => 1, 'cardNumber' => '', 'cardBalance' => 0, 'customerName' => '');
				}
                elseif(substr($acct, 0 ,3) != '780'){
                    $customerInfo = VmachineController::payrollInfo($acct, $pid);
                }
                else{
                    $customerInfo = VmachineController::custInfo($acct);
                }
                
                if($customerInfo['customerID'] > 0){
                    $amount = round($amount,2);
                    
                    $customerid = $customerInfo['customerID'];
                    $scancode = $customerInfo['cardNumber'];
					
					//transaction time
					$year = substr($tid, 0, 2);
					$month = substr($tid, 2, 2);
					$day = substr($tid, 4, 2);
					$hour = substr($tid, 6, 2);
					$minute = substr($tid, 8, 2);
					$second = substr($tid, 10, 2);
					
					$transaction_date = "20$year-$month-$day $hour:$minute:$second";
                    
                    //get businessid
                    $localInfo = VmachineController::locInfo($pid, $item, $payment_type);
                    
                    $businessid = $localInfo['businessid'];
                    $companyid = $localInfo['companyid'];
                    $pos_id = $localInfo['pos_id'];
                    $cost = $localInfo['cost'];
                    $creditid = $localInfo['creditid'];
                    $taxid = $localInfo['taxid'];
                    $debitid = $localInfo['debitid'];
                    $tax = $localInfo['tax'];
                    
                    $today = date('Y-m-d');
                    
                    //figure tax and adjust amount
                    if($tax > 0){
                        $tax = round(($tax / 100) + 1, 4);
                        $base_amount = round($amount / $tax, 2);
                        $taxes = round($amount - $base_amount, 2);
                    }
                    else{
                        $taxes = 0;
                        $base_amount = $amount;
                    }
                    
                    //insert check, checkdetail, payment_detail, transaction_log and update balance
                    $this->_client->db->query("INSERT INTO checks (businessid,check_number,people,bill_datetime,bill_posted,total,taxes,received,account_number,terminal_group)
                                        VALUES ($businessid,$tid$pid,1,'$transaction_date',NOW(),'$amount','$taxes','$amount','$scancode','$pid');
                                        INSERT INTO checkdetail (businessid,bill_number,item_number,quantity,price,cost)
                                        VALUES ($businessid,$tid$pid,'$pos_id',1,'$base_amount','$cost');
                                        INSERT INTO payment_detail (businessid,check_number,payment_type,received,scancode)
                                        VALUES ($businessid,$tid$pid,$payment_type,'$amount','$scancode');
                                        INSERT INTO transaction_log (transactionId,clientProgramId,customerCardNum,amount) 
                                        VALUES ('$tid$pid','$pid','$scancode','$amount');
                                        UPDATE customer SET balance = ROUND(balance - $amount, 2) WHERE customerid = $customerid;
                                        INSERT INTO creditdetail (companyid,businessid,amount,date,creditid) 
                                        VALUES ($companyid,$businessid,'$base_amount','$today',$creditid) 
                                            ON DUPLICATE KEY UPDATE amount = amount + '$base_amount';
                                        INSERT INTO creditdetail (companyid,businessid,amount,date,creditid) 
                                        VALUES ($companyid,$businessid,'$taxes','$today',$taxid) 
                                            ON DUPLICATE KEY UPDATE amount = amount + '$taxes';
                                        INSERT INTO debitdetail (companyid,businessid,amount,date,debitid) 
                                        VALUES ($companyid,$businessid,'$amount','$today',$debitid) 
                                            ON DUPLICATE KEY UPDATE amount = amount + '$amount';
                                ");

                    ///response
                    $response = array('responseCode' => 'OK', 'responseMessage' => 'success', 'customerInfo' => $customerInfo);
                }
                else{
                    $response = array('responseCode' => 'CUSTNOTFOUND', 'responseMessage' => 'customer not found');
                }
            }
            
            header("Content-Type: text/plain");
            echo json_encode($response);
        }
}
?>