<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of aerispos
 *
 * @author Eric Gach <eric@essential-elements.net>
 * @version $Id$
 */
class LicenseController extends Manage_Controller_Abstract
{
	//protected $_layout = 'aerispos.tpl';
	protected $_layout = 'license.tpl';		

	public function index()
	{
		$this->_user->requireLogin();
		$this->_view->assign('terminals', LicenseModel::getTerminals());
	}

	public function sorted_terminals()
	{
		$this->_user->requireLogin();
		$this->_view->assign('terminals', LicenseModel::getTerminals($_POST['showonly']));
		return false;
	}

	public function save()
	{
		$termid = $_POST['termid'];
		$status = $_POST['status'];

		$this->_user->requireLogin();
		LicenseModel::saveTermStatus($termid, $status);
		return false;
	}
}
