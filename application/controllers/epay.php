<?php
/**
 * EPay service controller
 * @author Ryan Pessa <ryan@essential-elements.net>
 */

if( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname( __FILE__ ) . '/../../htdocs/' ) );
}

class EPayException extends Exception {
	const UNKNOWN = -1;
	const BADVALUE = 1;
	const CUSTNOTFOUND = 2;
	const BIDNOTFOUND = 3;
	const INVOP = 4;
	const NOTREGISTERED = 5;
	const EMAILREGISTERED = 6;
	
	const NSF = 10;
	
	const LANG = 100;
	
	/* This doesn't actually work the way it looks like it would - the constants are treated
	 * as unknown variables and are automatically stringified. The correct way to use would be
	 * self::CONSTANT, however, everything is working currently and 'fixing' this would require
	 * refactoring other code, both here and in AerisPOS.
	 * 
	 * Oh well.
	 */ 
	static private $exceptionStr = array(
		UNKNOWN => 'unknown error',
		BADVALUE => 'bad value',
		CUSTNOTFOUND => 'customer not found',
		BIDNOTFOUND => 'business not found',
		NSF => 'non sufficient funds',
		LANG => 'language error',
		INVOP => 'invalid operation',
		NOTREGISTERED => 'customer not registered',
		EMAILREGISTERED => 'email address already registered'
	);
	
	public $responseCode = 0;
	public $responseMessage = '';
	
	public function __construct( $responseCode, $extraMessage ) {
		$this->responseCode = $responseCode;
		$this->responseMessage = static::$exceptionStr[$responseCode] . ( $extraMessage ? ': ' . $extraMessage : '' );
	}
	
	static public function __callStatic( $method, $args ) {
		$exception = array_key_exists( $method, static::$exceptionStr ) ? $method : UNKNOWN;
		$message = ( $args && $args[0] && is_string( $args[0] ) ) ? $args[0] : null;
		
		return new EPayException( $exception, $message );
	}
	
	static public function fromException( $exception ) {
		return new EPayException( LANG, $exception->getMessage( ) );
	}
}

class EpayController extends Manage_Controller_Abstract {
	protected $_tdb;
	protected $_mdb;
	
	protected $customer = null;
	protected $transaction = null;
	
	protected $inputFilter = INPUT_POST;
	
	static protected $instance = null;
	
	/**
	 * Meta-command for EPay processing
	 * POST: string $op Process operation
	 */
	public function process( ) {
		ini_set( 'display_errors', 'false' );
		
		$op = $_REQUEST['op'];
		
		switch( $op ) {
			case 'balance':
				return $this->balance( );
			
			case 'transaction':
				return $this->transaction( );
			
			case 'register':
				return $this->register( );
			
			case 'search':
				return $this->search( );
			
			default:
				throw EPayException::INVOP( );
		}
		
		return false;
	}
	
	static protected $statusCodes = array(
		'disabled' => -1,
		'ready' => 1
	);
	
	public function status( ) {
		ini_set( 'display_errors', 'false' );
		
		echo static::$statusCodes['ready'];
		
		return false;
	}
	
	protected function setCustomer( $customerInfo ) {
		$customerInfo = (object)$customerInfo;
		$this->customer = array( );
		
		$fieldMap = array(
			// customerId
			'customerId' => 'customerId',
			'customerid' => 'customerId',
			
			// cardNumber
			'cardNumber' => 'cardNumber',
			'scancode' => 'cardNumber',
			
			// customerName
			'customerName' => 'customerName',
			'first_name' => 'firstName',
			'last_name' => 'lastName',
			
			// cardBalance
			'cardBalance' => 'cardBalance',
			'balance' => 'cardBalance'
		);
		
		foreach( $fieldMap as $field => $dest ) {
			if( isset( $customerInfo->$field ) )
				$this->customer[$dest] = $customerInfo->$field;
		}
		
		if( isset( $this->customer['firstName'] ) || isset( $this->customer['lastName'] ) ) {
			$this->customer['customerName'] = $this->customer['firstName'] . ' ' . $this->customer['lastName'];
			unset( $this->customer['firstName'] );
			unset( $this->customer['lastName'] );
		}

		if( isset( $this->customer['customerId'] ) ) {
			\EE\Model\Aeris\UserFunction::db( $this->_tdb );
			$this->customer['extraFunctions'] = \EE\Model\Aeris\UserFunction::getFunctionsForCustomer( $this->customer['customerId'] );
		}

		if( isset( $this->customer['cardNumber'] ) ) {
			\EE\Model\User\Customer\Scancode\Fingerprint::db( $this->_tdb );
			$this->customer['fingerprint'] = \EE\Model\User\Customer\Scancode\Fingerprint::getFingerprintDate( $this->customer['cardNumber'] );
		}
	}
	
	/**
	 * Get balance of a card/account
	 * POST: string $acct Account number
	 * POST: integer $pid Program ID
	 * POST: string [$mode=EP] Transaction mode
	 */
	public function balance( ) {
		//header( 'Content-Type: application/json' );
		ini_set( 'display_errors', 'false' );
		
		$acct = filter_input( $this->inputFilter, 'acct', FILTER_SANITIZE_MAGIC_QUOTES );
		$pid = filter_input( $this->inputFilter, 'pid', FILTER_SANITIZE_NUMBER_INT );
		$mode = $_REQUEST['mode'] ?: 'EP';
		
		if( empty( $acct ) )
			throw EPayException::BADVALUE( 'acct: ' . $acct );
		if( !( $pid > 0 ) )
			throw EPayException::BADVALUE( 'pid: ' . $pid );
		
		Treat_Model_User_Customer::db( $this->_tdb );
		
		if( $customer = Treat_Model_User_Customer::getUserByScancodeAndBusinessid( $acct, static::checkEP( $acct ) ? 0 : $this->_getBid( $pid ) ) ) {
			$this->setCustomer( $customer );
			$this->transaction = array(
				'terminal' => $pid,
				'mode' => $mode,
				'type' => 'balance'
			);
		} elseif( Treat_Model_User_Customer::scancodePrefix( $acct ) == Treat_Model_User_Customer::SCANCODE_SELF_REGISTERED ) {
			$this->customer = array( 'cardNumber' => $acct );
			throw EPayException::NOTREGISTERED( );
		} else {
			$this->customer = array( 'cardNumber' => $acct );
			throw EPayException::CUSTNOTFOUND( );
		}
		
		$this->output( array(
			'responseCode' => 'OK',
			'responseMessage' => 'success'
		) );
		
		return false;
	}
	
	/**
	 * Search for card/account
	 * POST: string $query Search for keyword.
	 * POST: integer $pid Program ID
	 * POST: string [$mode=EP] Transaction mode
	 */
	public function search( ) {
		//header( 'Content-Type: application/json' );
		ini_set( 'display_errors', 'false' );
		
		$query = trim(filter_input( $this->inputFilter, 'query', FILTER_SANITIZE_MAGIC_QUOTES ));
		$pid = filter_input( $this->inputFilter, 'pid', FILTER_SANITIZE_NUMBER_INT );
		$exact = (bool) $_REQUEST['exact'] ?: false;
		
		if( empty( $query ) || strlen($query) < 2 )
			throw EPayException::BADVALUE( 'query: ' . $query );
		if( !( $pid > 0 ) )
			throw EPayException::BADVALUE( 'pid: ' . $pid );
		
		Treat_Model_User_Customer::db( $this->_tdb );
		
		$customers = Treat_Model_User_Customer::searchForCustomer($query, $this->_getBid($pid), $exact);
		
		if( $customers ) {
			foreach($customers as $customer) {
				$this->customer[] = array(
					'customerId' => $customer['customerid'],
					'cardNumber' => $customer['scancode'],
					'customerName' => $customer['first_name'].' '.$customer['last_name'],
					'cardBalance' => $customer['balance'],
				);
			}
		} else {
			$this->customer = array( 'query' => $query );
			throw EPayException::CUSTNOTFOUND( );
		}
		
		$this->output( array(
			'responseCode' => 'OK',
			'responseMessage' => 'success'
		) );
		
		return false;
	}
	
	/**
	 * Update balance from a transaction
	 * POST: string $acct Account number
	 * POST: integer $pid Program ID
	 * POST: double $amount Transaction amount
	 * POST: string $tid Transaction ID
	 * POST: string [$type=sale] Transaction type
	 * POST: string [$mode=EP] Transaction mode
	 */
	public function transaction( ) {
		//header( 'Content-Type: application/json' );
		ini_set( 'display_errors', 'false' );
		
		$acct = filter_input( $this->inputFilter, 'acct', FILTER_SANITIZE_MAGIC_QUOTES );
		$pid = filter_input( $this->inputFilter, 'pid', FILTER_SANITIZE_NUMBER_INT );
		$amount = filter_input( $this->inputFilter, 'amount', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION );
		$tid = filter_input( $this->inputFilter, 'tid', FILTER_SANITIZE_MAGIC_QUOTES );
		$type = $_REQUEST['type'] ?: 'sale';
		$mode = $_REQUEST['mode'] ?: 'EP';
		
		if( empty( $acct ) )
			throw EPayException::BADVALUE( 'acct: ' . $acct );
		
		if( !( $pid > 0 ) )
			throw EPayException::BADVALUE( 'pid: ' . $pid );
		
		if( empty( $tid ) )
			throw EPayException::BADVALUE( 'tid: ' . $tid );
		
		Treat_Model_User_Customer::db( $this->_tdb );

		$bid = $this->_getBid( $pid );
		if( $customer = Treat_Model_User_Customer::getUserByScancodeAndBusinessid( $acct, static::checkEP( $acct ) ? 0 : $bid ) ) {
			$this->setCustomer( $customer );
			
			if( $type == 'sale' && $customer->balance < $amount ) {
				throw EPayException::NSF( );
			}
			
			$mult = ( $type == 'reload' ) ? 1 : -1;
			
			$customer->balance += $mult * $amount;

			if ($type == 'sale' && $customer->single_use == 1) {
				$customer->inactive = 1;
			}

			$customer->save( );
			
			$this->transaction = array(
				'terminal' => $pid,
				'mode' => $mode,
				'type' => $type,
				'id' => $tid,
				'amount' => $amount
			);
			
			$this->customer['cardBalance'] = $customer->balance;

			$query_tlog = '
				INSERT INTO transaction_log(transactionId, clientProgramId, customerCardNum, amount, refund, customerId)
				VALUES(?, ?, ?, ?, ?, ?)
			';
			$this->_tdb->exec( $query_tlog, array( $tid, $pid, $acct, $amount, (bool)($type == 'reload'), $customer->customerid ) );

			\EE\Model\User\Customer\Visit::record( $customer->customerid, $bid );
		} elseif( Treat_Model_User_Customer::scancodePrefix( $acct ) == Treat_Model_User_Customer::SCANCODE_SELF_REGISTERED ) {
			$this->customer = array( 'cardNumber' => $acct );
			throw EPayException::NOTREGISTERED( );
		} else {
			$this->setCustomer( array( 'cardNumber' => $acct ) );
			throw EPayException::CUSTNOTFOUND( );
		}
		
		$this->output( array(
			'responseCode' => 'OK',
			'responseMessage' => 'success'
		) );
		
		return false;
	}
	
	public function register( ) {
		ini_set( 'display_errors', 'false' );
		
		$acct = filter_input( $this->inputFilter, 'acct', FILTER_SANITIZE_MAGIC_QUOTES );
		$pid = filter_input( $this->inputFilter, 'pid', FILTER_SANITIZE_NUMBER_INT );
		$email = filter_input( $this->inputFilter, 'email', FILTER_SANITIZE_MAGIC_QUOTES );
		
		if( empty( $acct ) )
			throw EPayException::BADVALUE( 'acct: ' . $acct );
		
		if( !( $pid > 0 ) )
			throw EPayException::BADVALUE( 'pid: ' . $pid );
		
		if( empty( $email ) )
			throw EPayException::BADVALUE( 'email: ' . $email );
		
		Treat_Model_User_Customer::db( $this->_tdb );
		Treat_Model_Business::db( $this->_tdb );
		Treat_Model_ClientPrograms::db( $this->_mdb );
		
		if( Treat_Model_User_Customer::doesUserExist( $email ) ) {
			throw EPayException::EMAILREGISTERED( );
		}
		
		$terminal = Treat_Model_ClientPrograms::getById( $pid );
		
		if( !$terminal )
			throw EPayException::BADVALUE( 'pid does not exist: ' . $pid );
		
		$bid = $terminal->businessid;
		
		$customer = new Treat_Model_User_Customer( array(
			'businessid' => $bid,
			'username' => $email,
			'scancode' => $acct,
			'first_name' => 'Valued',
			'last_name' => 'Customer'
		) );
		$customer->save( );
		
		if( !$customer->createTemporaryPassword( ) )
			throw EPayException::UNKNOWN( 'Failed to create temporary password' );
		
		if( !$customer->emailTemporaryPassword( ) )
			throw EPayException::UNKNOWN( 'Failed to send registration email' );
		
		$this->setCustomer( $customer );
		$this->output( array(
			'responseCode' => 'OK',
			'responseMessage' => 'success'
		) );
		
		return false;
	}

	public function fingerprint( ) {
		ini_set( 'display_errors', 'false' );

		$acct = filter_input( $this->inputFilter, 'acct', FILTER_SANITIZE_MAGIC_QUOTES );
		$pid = filter_input( $this->inputFilter, 'pid', FILTER_SANITIZE_NUMBER_INT );

		if( empty( $acct ) )
			throw EPayException::BADVALUE( 'acct: ' . $acct );

		if( !( $pid > 0 ) )
			throw EPayException::BADVALUE( 'pid: ' . $pid );

		Treat_Model_User_Customer::db( $this->_tdb );
		\EE\Model\User\Customer\Scancode\Fingerprint::db( $this->_tdb );

		$terminal = Treat_Model_ClientPrograms::getById( $pid );

		if( !$terminal )
			throw EPayException::BADVALUE( 'pid does not exist: ' . $pid );

		$bid = $terminal->businessid;

		$fingerprint = \EE\Model\User\Customer\Scancode\Fingerprint::getById( $acct );
		if( !$fingerprint )
			throw EPayException::BADVALUE( 'fingerprint does not exist: ' . $acct );

		$this->output( array(
			'responseCode' => 'OK',
		    'responseMessage' => 'success',
		    'fingerprint' => $fingerprint->fpdata,
		) );

		return false;
	}

	static public function handleException( $exception ) {
		$e = $exception instanceof EPayException ? $exception : EPayException::fromException( $exception );
		static::$instance->output( $e );
		exit( );
	}
	
	protected function init( ) {
		ini_set( 'display_errors', 0 );
		
		$this->_tdb = Manage_DB_Proxy::singleton( 'treat db');
		$this->_mdb = Manage_DB_Proxy::singleton();
		
		static::$instance = $this;
		set_exception_handler( 'EPayController::handleException' );
		
		if( isset( $_GET['debug'] ) )
			$this->inputFilter = INPUT_GET;
	
		$pid = filter_input( $this->inputFilter, 'pid', FILTER_SANITIZE_NUMBER_INT );
		if (!empty($pid)) {
			$stmnt = $this->_mdb->query('SELECT db_name FROM client_programs WHERE client_programid = ?', array($pid));
			$dbname = $stmnt->fetchColumn();
			$stmnt->closeCursor();
			if (!empty($dbname)) {
				$this->_tdb->setAttribute(SiTech_DB_Proxy::ATTR_WRITEONLY, true);
				$this->_tdb->exec('USE '.$dbname);
			}
		}
	}
	
	protected $outputBase = array(
		'responseCode' => 0,
		'responseMessage' => ''
	);
	
	protected function output( $obj ) {
		$obj = (object)( $obj ?: array( ) );
		$output = get_object_vars( $obj );
		
		foreach( $this->outputBase as $key => $default ) {
			$output[$key] = property_exists( $obj, $key ) ? $obj->$key : $default;
			if( $default === null && $output[$key] === null )
				unset( $output[$key] );
		}
		
		if( $this->customer && !empty( $this->customer ) )
			$output['customerInfo'] = $this->customer;
		
		if( $this->transaction && !empty( $this->transaction ) )
			$output['transactionInfo'] = $this->transaction;
		
		echo json_encode( $output );
	}

	protected function _getBid( $pid ) {
		$bid = Manage_DB_Proxy::singleton( )->query( 'SELECT businessid FROM client_programs WHERE client_programid = ?', array( $pid ) )
									  ->fetchColumn( );
		if( empty( $bid ) )
			throw EPayException::BIDNOTFOUND( );
		return ( $bid );
	}
	
	static public function checkEP( $acct ) {
		return !(Treat_Model_User_Customer::scancodeRestricted( $acct ));
	}
	
	public function test( ) {
		Treat_Model_User_Customer::db( $this->_tdb );
		Treat_Model_Business::db( $this->_tdb );
		$customer = Treat_Model_User_Customer::getUserByEmail( 'dkived@gmail.com' );
		$result = $customer->emailTemporaryPassword( );
		
		echo '<p>success: ' . ( $result['success'] ? 'true' : 'false' ) . '</p>';
		echo $result['message'];
		
		return false;
	}
}
