<?php
if( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname( __FILE__ ) . '/../../htdocs/' ) );
}

class OfflineController extends Manage_Controller_Abstract {
        
		protected function init( ){
			$this->_mdb = new Manage_DB( 'database');
		}
		
		public function createDateRangeArray($strDateFrom,$strDateTo)
		{
			$aryRange=array();

			$iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2), substr($strDateFrom,8,2),substr($strDateFrom,0,4));
			$iDateTo=mktime(1,0,0,substr($strDateTo,5,2), substr($strDateTo,8,2),substr($strDateTo,0,4));

			if ($iDateTo>=$iDateFrom)
			{
				array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
				while ($iDateFrom<$iDateTo)
				{
					$iDateFrom+=86400; // add 24 hours
					array_push($aryRange,date('Y-m-d',$iDateFrom));
				}
			}
			return $aryRange;
		}
		
		public function changeStatus(){
			$this->_user->requireLogin();
			
			$client_programid = $_GET["pid"];
			
			if($client_programid > 0){
				$this->_mdb->query("INSERT INTO offline_check (client_programid,checkStatus,user,time_check) 
							VALUES ($client_programid,1,'{$this->_user->username}',NOW())
							ON DUPLICATE KEY UPDATE 
								checkStatus = checkStatus + 1, 
								user = '{$this->_user->username}', 
								time_check = NOW()");
			}
			
			header('Location: '.SITECH_BASEURI.'offline');
		}
		
		public function offlineGraph(){
			$this->_user->requireLogin();
			
			$cpid = $_GET["cpid"];
			
			$versions = array(1 => 'green', 2 => 'blue');
			
			$data = array();
			$dateArray = array();
			$startdate = date("Y-m-d",mktime(0, 0, 0, date("m")-4 , date("d"), date("Y")));
			$offDate = date("Y-m-d",mktime(0, 0, 0, date("m") , date("d")-14, date("Y")));
			$today = date("Y-m-d");
			$dateArray = $this->createDateRangeArray($startdate,$today);
			
			///populate with zeros
			foreach($dateArray AS $key => $value){
				$data[$value][1] = 0;
				$data[$value][2] = 0;
			}
			
			$largestAmt = 0;
			$currentAmt = 0;
			
			if($cpid > 0){
				$whereQuery = "AND cp.client_programid = $cpid";
			}
			else{
				$whereQuery = "";
			}
		
			$stmnt = $this->_mdb->query("SELECT COUNT(ol.valueid) AS amt, 
										DATE(ol.datetime_value) as kdate, 
										cp.sync_version
									FROM offline_logging ol  
									JOIN client_programs cp ON cp.client_programid = ol.client_programid
									WHERE ol.datetime_value >= '$startdate 00:00:00' 
										AND ol.new_status = 'OFFLINE' 
										$whereQuery
									GROUP BY kdate, cp.sync_version 
									ORDER BY kdate,cp.sync_version
									");
			$array = $stmnt->fetchAll( PDO::FETCH_ASSOC );
			

			foreach($array AS $line){
				$amt = $line["amt"];
				$date = $line["kdate"];
				$version = $line["sync_version"];
				
				$currentAmt += $amt;
				
				$data[$date][$version]  = $amt;
				
				if($version == 2){
					if($currentAmt > $largestAmt){
						$largestAmt = $currentAmt;
					}
					$currentAmt = 0;
				}
			}
			
			$largestAmt++;
			
			if($cpid > 0){$largestAmt *= 5;}
			
			///show graph
			echo "<table width=100% border=0 height=650><tr valign=bottom>";
			foreach($data AS $date => $value){
				$currentAmt = 0;
				$v1 = 0;
				$v2 = 0;
				
				///version update
				$stmnt = $this->_mdb->query("SELECT * FROM version_history WHERE date = '$date'");
				$array = $stmnt->fetchAll( PDO::FETCH_ASSOC );
				
				$update = "";
				foreach($array  AS $line){
					$update = $line["version"];
				}
				
				if($update != ""){
					$showUpgrade = "background-color:#CCCCCC;";
				}
				else{
					$showUpgrade = "background-color:#FFFFFF;";
				}
				
				foreach($data[$date] AS $version => $amt){
					if($cpid > 0){
						$amt *= 5;
					}
				
					if($version == 2){
						$v2 = $amt;
					}
					else{
						$v1 = $amt;
					}
					$currentAmt += $amt;
				}
				
				$left = $largestAmt - $currentAmt;

				$day = substr($date,8,2);
				$month = substr($date,5,2);
				
				if($cpid > 0){
					$showv1 = $v1 / 5;
					$showv2 = $v2 / 5;
				}
				else{
					$showv1 = $v1;
					$showv2 = $v2;
				}
				
				echo "<td style=\"height:" . $largestAmt . "px;$showUpgrade alt=\"$update\" title=\"$update\" >";
				echo "<div style=\"$showUpgrade;height:" .$left . "px;width:5px;\"><img src=\"clear.gif\" border=\"0\" height=\"1\" width=\"1\" alt=\"\" title=\"\" /></div>";
				echo "<div style=\"$showUpgrade;width:5px;\"><font size=1><a href=offlineDateDetail?date=$date border=0 style=\"text-decoration:none;\">$month/$day</a></font></div>";
				echo "<div style=\"background-color:#0000FF;height:" .$v2 . "px;width:5px;\" alt=\"v2:$showv2\" title=\"v2:$showv2\"><img src=\"clear.gif\" border=\"0\" height=\"1\" width=\"1\" /></div>";
				echo "<div style=\"background-color:#FF0000;height:" . $v1 . "px;width:5px;\" alt=\"v1:$showv1\" title=\"v1:$showv1\"><img src=\"clear.gif\" border=\"0\" height=\"1\" width=\"1\" /></div>";
				echo "</td>";
			}
			echo "</tr></table>";
			
			if($cpid > 0){
				echo "<table style=\"border:1px solid #999999;\" width=33%>
						<tr bgcolor=black>
							<td style=\"border:1px solid #999999;\"><font color=white>Offender</font></td>
							<td style=\"border:1px solid #999999;\"><font color=white>Status Change</font></td>
							<td style=\"border:1px solid #999999;\"><font color=white>Occured</font></td>
							<td style=\"border:1px solid #999999;\"><font color=white>Time</font></td>";
				///details
				$stmnt = $this->_mdb->query("SELECT *
						FROM offline_logging
						WHERE client_programid = $cpid
							AND datetime_value >= '$startdate 00:00:00'
						ORDER BY datetime_value DESC");
				$array = $stmnt->fetchAll( PDO::FETCH_ASSOC );
			
				foreach($array AS $line){
					$name = $line["name"];
					$new_status = $line["new_status"];
					$datetime_value = $line["datetime_value"];
					
					if ( $new_status == "OFFLINE" ) {
						$stmnt2 = $this->_mdb->query("SELECT TIMEDIFF('$lasttime', '$datetime_value') AS amount");
						$array2 = $stmnt2->fetchAll( PDO::FETCH_ASSOC );
						
						$amount = 0;
						foreach($array2  AS $line2){
							$amount = $line2["amount"];
						}

						//turn into minutes
						$pieces = explode(':', $amount);
						$pieces[0] *= 60;
						$pieces[1] += $pieces[0];
						$hours = floor($pieces[1] / 60);
						$minutes = fmod($pieces[1], 60);

						if ( $hours > 0 ) {
							$display_time = "$hours hrs";
							if ( $minutes > 0 ) {
								$display_time .= ", $minutes mins";
							}
						}
						else {
							$display_time = "$minutes mins";
						}
						
						////display
						echo "<tr><td style=\"border:1px solid #999999;\">$name</td><td style=\"border:1px solid #999999;\">$new_status</td><td style=\"border:1px solid #999999;\">$datetime_value</td><td style=\"border:1px solid #999999;\">$display_time</td></tr>";
					}
					else {
						$display_time = "";
					}		
					
					$lasttime = $datetime_value;
				}
			
				echo "</table><center><a href=offlineGraph>Return</a></center>";
			}
			else{
				echo "<table width=100%><tr valign=top>";
				
				////offline offenders
				$stmnt = $this->_mdb->query("select cp.client_programid, cp.name, count(ol.valueid) as offline
					FROM client_programs cp
					JOIN offline_logging ol on ol.client_programid = cp.client_programid
					WHERE ol.datetime_value >= '$offDate' AND new_status = 'OFFLINE'
					GROUP BY cp.client_programid
					HAVING offline > 5
					ORDER BY offline DESC");
				$array = $stmnt->fetchAll( PDO::FETCH_ASSOC );
				
				echo "<td width=33%><table style=\"border:1px solid #999999;\" width=100%>
						<tr bgcolor=black>
							<td style=\"border:1px solid #999999;\">
								<font color=white>ID</font>
							</td>
							<td style=\"border:1px solid #999999;\">
								<font color=white>Offender</font>
							</td>
							<td style=\"border:1px solid #999999;\">
								<font color=white>#Offline</font>
							</td>
						</tr>";
			
				foreach($array AS $line){
					$cpid = $line["client_programid"];
					$name = $line["name"];
					$offline = $line["offline"];
					
					echo "<tr>
							<td style=\"border:1px solid #999999;\">
								$cpid
							</td>
							<td style=\"border:1px solid #999999;\">
								<a href=offlineGraph?cpid=$cpid style=\"text-decoration:none;\">$name</a>
							</td>
							<td style=\"border:1px solid #999999;\">
								$offline
							</td>
						</tr>";
				}
				
				echo "</table></td>";
				
				////offline hour
				$stmnt = $this->_mdb->query("SELECT HOUR( datetime_value ) AS khour, COUNT( valueid ) AS offline
					FROM offline_logging
					WHERE datetime_value > '$offDate'
					GROUP BY khour
					ORDER BY khour");
				$array = $stmnt->fetchAll( PDO::FETCH_ASSOC );
				
				echo "<td width=33%><table style=\"border:1px solid #999999;\" width=100%>
						<tr bgcolor=black>
							<td style=\"border:1px solid #999999;\">
								<font color=white>Hour</font>
							</td>
							<td style=\"border:1px solid #999999;\">
								<font color=white>#Offline</font>
							</td>
						</tr>";
			
				foreach($array AS $line){
					$khour = $line["khour"];
					$offline = $line["offline"];
					
					echo "<tr>
							<td style=\"border:1px solid #999999;\">
								$khour:00 - $khour:59
							</td>
							<td style=\"border:1px solid #999999;\">
								$offline
							</td>
						</tr>";
				}
				
				echo "</table></td>";
				
				////current numbers
				$stmnt = $this->_mdb->query("SELECT COUNT( cp.client_programid ) AS total, cp.sync_version, cp.pos_mode
					FROM client_programs cp
					WHERE cp.client_ip NOT 
					IN (
					'72.22.219.4',  '72.22.219.5',  '74.62.165.207',  '24.123.111.146',  '72.22.209.107',  ''
					)
					AND cp.parent NOT 
					IN ( 2198, 22, 58 ) 
					GROUP BY cp.sync_version, cp.pos_mode
					ORDER BY cp.sync_version, cp.pos_mode");
				$array = $stmnt->fetchAll( PDO::FETCH_ASSOC );
				
				$clients = array();
				$modes = array(1 => 'Kiosk', 2 => 'CK Live', 3 => 'Register', 4 => 'Tablet', 5 => 'Vending Machine');
				$versions = array(1 => 1, 2 => 2);
				
				echo "<td width=33%><table style=\"border:1px solid #999999;\" width=100%>
						<tr bgcolor=black>
							<td style=\"border:1px solid #999999;\">
								<font color=white>Version</font>
							</td>
							<td style=\"border:1px solid #999999;\">
								<font color=white>POS Mode</font>
							</td>
							<td style=\"border:1px solid #999999;\">
								<font color=white>Count</font>
							</td>
						</tr>";
			
				foreach($array AS $line){
					$total = $line["total"];
					$sync_version = $line["sync_version"];
					$pos_mode = $line["pos_mode"];
					
					if($pos_mode < 1){$pos_mode = 1;}
					
					$clients[$sync_version][$pos_mode] += $total;
				}
				
				foreach($versions AS $version => $value){
					foreach($clients[$version] AS $pos_mode => $count){
						echo "<tr>
							<td style=\"border:1px solid #999999;\">
								$version
							</td>
							<td style=\"border:1px solid #999999;\">
								$modes[$pos_mode]
							</td>
							<td style=\"border:1px solid #999999;\">
								$count
							</td>
						</tr>";
					}
				}
				
				echo "</table>";
				
				echo "</td>";
				
				echo "</tr></table>";
			}
		}
		
		public function offlineDateDetail(){
			$this->_user->requireLogin();
			
			$date = $_GET["date"];
		
			$stmnt = $this->_mdb->query("SELECT COUNT(ol.valueid) AS amt, 
										cp.sync_version,
										cp.client_programid,
										cp.name,
										cp.client_ip
									FROM offline_logging ol  
									JOIN client_programs cp ON cp.client_programid = ol.client_programid
									WHERE DATE(ol.datetime_value) = '$date' 
										AND ol.new_status = 'OFFLINE' 
									GROUP BY cp.client_programid 
									ORDER BY amt DESC
									");
			$array = $stmnt->fetchAll( PDO::FETCH_ASSOC );
			
			echo "<center><b>Details for $date</b></center>";
			echo "<center><table border=0 style=\"border:1px solid #999999;\" width=60%>";
			echo "<tr bgcolor=black>
				<td style=\"border:1px solid #999999;color:white;\">#Offline</td>
				<td style=\"border:1px solid #999999;color:white;\">ID</td>
				<td style=\"border:1px solid #999999;color:white;\">Name</td>
				<td style=\"border:1px solid #999999;color:white;\">Version</td>
				<td style=\"border:1px solid #999999;color:white;\">IP</td>
				</tr>";
			
			foreach($array AS $line){
				$amt = $line["amt"];
				$version = $line["sync_version"];
				$cpid = $line["client_programid"];
				$name = $line["name"];
				$ip = $line["client_ip"];
				
				echo "<tr>
					<td style=\"border:1px solid #999999;\">$amt</td>
					<td style=\"border:1px solid #999999;\">$cpid</td>
					<td style=\"border:1px solid #999999;\"><a href=offlineGraph?cpid=$cpid style=\"text-decoration:none;\">$name</a></td>
					<td style=\"border:1px solid #999999;\">$version</td>
					<td style=\"border:1px solid #999999;\">$ip</td>
					</tr>";
			}
			echo "</table></center>";
			
			echo "<center><a href=offlineGraph>Return</a></center>";
		}
		
		public function versionStatus(){
			$stmnt = $this->_mdb->query("SELECT * FROM client_programs WHERE sync_version = 2 AND db_name != ''");
			$array = $stmnt->fetchAll( PDO::FETCH_ASSOC );
			
			$pidsGood = '';
			$pidsVersionMismatch = '';
			$pidsErrorMessage = '';

			foreach($array AS $line){
				$client_programid = $line["client_programid"];
				$client_name = $line["name"];
				$db_name = $line["db_name"];
				
				///message check
				$stmnt2 = $this->_mdb->query("SELECT mb_av.* FROM $db_name.aeris_versions mb_av 
						WHERE mb_av.client_programid = $client_programid AND mb_av.component_name LIKE '*%'");
				$array2 = $stmnt2->fetchAll( PDO::FETCH_ASSOC );
				
				$errorMessage = "";
				$resultMessage = "";
				$updateTime = "";
				
				foreach( $array2 AS $line2 ) {
					if($line2['component_name'] == "*Last Error*"){
						$errorMessage = $line2['component_version'];
					}
					elseif($line2['component_name'] == "*Last Result*"){
						$resultMessage = $line2['component_version'];
					}
					elseif($line2['component_name'] == "*Last Update*"){
						$updateTime = $line2['component_version'];
					}
				}
				
				///version check
				$stmnt2 = $this->_mdb->query("SELECT cv.*, mb_av.* FROM current_version cv
					LEFT JOIN $db_name.aeris_versions mb_av ON cv.component = mb_av.component_name 
						AND mb_av.client_programid = $client_programid");
				$array2 = $stmnt2->fetchAll( PDO::FETCH_ASSOC );
				
				$versionProblem = 0;
				$hasReported = 0;
				
				foreach( $array2 AS $line2 ) {
					if($line2["componentVersion"] != $line2["component_version"] && $line2["component_version"] != ''){
						$versionProblem = 1;
					}
					if($line2["component_version"] != ''){
						$hasReported = 1;
					}
				}
				
				if($versionProblem == 1){
					if($pidsVersionMismatch == ""){
						$pidsVersionMismatch = $client_programid;
					}
					else{
						$pidsVersionMismatch .= "," . $client_programid;
					}
				}
				elseif($errorMessage != "" || $resultMessage == "failure"){
					if($pidsErrorMessage == ""){
						$pidsErrorMessage = $client_programid;
					}
					else{
						$pidsErrorMessage .= "," . $client_programid;
					}
				}
				elseif($hasReported == 1){
					if($pidsGood == ""){
						$pidsGood = $client_programid;
					}
					else{
						$pidsGood .= "," . $client_programid;
					}
				}
			}
			
			///update versionStatus
			$this->_mdb->query("UPDATE client_programs SET versionStatus = 0 AND sync_version = 2");
			if($pidsGood != ""){
				$this->_mdb->query("UPDATE client_programs SET versionStatus = 1 WHERE client_programid IN ($pidsGood)");
			}
			if($pidsVersionMismatch != ""){
				$this->_mdb->query("UPDATE client_programs SET versionStatus = 2 WHERE client_programid IN ($pidsVersionMismatch)");
			}
			if($pidsErrorMessage != ""){
				$this->_mdb->query("UPDATE client_programs SET versionStatus = 3 WHERE client_programid IN ($pidsErrorMessage)");
			}
		}
		
		public function vpnStatus(){
			///get contents
			$url = "http://vpn.essential-elements.net?json";
			$ch = curl_init();
			$timeout = 30;
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
			$data = curl_exec($ch);
			curl_close($ch);
			
			$data = json_decode($data);
			
			$pids = "";
			
			foreach($data AS $key => $value){
				if($pids == ''){
					$pids = $data[$key][0] * 1;
				}
				else{
					$pids .= "," . $data[$key][0] * 1;
				}
			}
			
			$this->_mdb->query("UPDATE client_programs SET vpnStatus = 0");
			if($pids != ""){
				$this->_mdb->query("UPDATE client_programs SET vpnStatus = 1 WHERE client_programid IN ($pids)");
			}
			
			return true;
		}
	
		protected function index( ) {
			$this->_user->requireLogin();
			
			$versions = array('1' => 1, '2' => 2);
			$status = array('red' => 0, 'green' => 1);
			
			echo "<html>
					<head>
					<meta http-equiv=\"Refresh\" content=\"120\">
					</head>
					<body>
				";
			
			echo "<table width=100% style=\"border:2px solid black;\">";
			
			foreach($status AS $current => $statusKey){
				echo "<tr valign=top>";
				foreach($versions AS $version => $key){
					echo "<td width=50% style=\"border:2px solid $current;\">";
					OfflineController::kioskList($key,$statusKey);
					echo "</td>";
				}
				echo "</tr>";
			}
			
			echo "</table></body></html>";
		}
		
		public function kioskList($version, $status){
			$this->_user->requireLogin();
			
			if($status == 0){
				$orderBy = "online DESC";
				$showStatus = "Offline";
				$having = "HAVING online > '00:20:00'";
				$select = ", oc.checkStatus, oc.user, oc.time_check";
				$join = "LEFT JOIN offline_check oc ON oc.client_programid = cp.client_programid";
			}
			else{
				$orderBy = "versionStatus DESC, parent ASC, name ASC";
				$showStatus = "Online";
				$having = "";
				$select = "";
				$join = "";
			}
			
			$data = "<table width=100%><tr><td colspan=4><b> Version $version $showStatus</b></td></tr>";
			
			$counter = 0;
			
			$stmnt = $this->_mdb->query("SELECT cp.client_programid, cpb.name AS parent, cp.name, cp.vpnStatus, cp.versionStatus, TIMEDIFF( NOW( ) , cp.send_time ) AS online $select
				FROM client_programs cp
				JOIN client_programs cpb ON cpb.client_programid = cp.parent
				$join
				WHERE cp.sync_version =$version
				AND cp.checkin_counter =$status
				AND cp.client_ip NOT 
				IN (
				'72.22.219.4', '72.22.219.5', '74.62.165.207', '24.123.111.146', '72.22.209.107',  ''
				)
				AND (
				cp.pos_mode NOT 
				IN ( 3 ) 
				OR cp.pos_mode IS NULL
				)
				AND cp.parent NOT 
				IN ( 2198, 22, 58 ) 
				$having
				ORDER BY $orderBy");
			$array = $stmnt->fetchAll( PDO::FETCH_ASSOC );

			foreach($array AS $line){
				$client_programid = $line['client_programid'];
				$parent = $line['parent'];
				$name = $line['name'];
				$vpnStatus = $line['vpnStatus'];
				$versionStatus = $line['versionStatus'];
				$online = $line['online']; 
				
				if($online < '01:00:00' && $status == 0){
					$bgColor = "yellow";
				}
				elseif($online < '03:00:00' && $status == 0){
					$bgColor = "orange";
				}
				elseif($online < '24:00:00' && $status == 0  && strlen($online) == 8){
					$bgColor = "#C7A317";
				}
				else{
					$bgColor = "white";
				}
				
				///icons for version 2
				if($version == 2){
					if($vpnStatus == 1){
						$buttons = "<a href=\"/remote/vnc/$client_programid\" target=\"_blank\"><img src=\"icon-vnc.png\" border=\"0\" height=\"16\" width=\"16\" alt=\"Connect via VNC\" title=\"Connect via VNC\" /></a>
									<a href=\"/remote/ssh/$client_programid\" target=\"_blank\"><img src=\"icon-ssh.png\" border=\"0\" height=\"16\" width=\"16\" alt=\"Connect via SSH\" title=\"Connect via SSH\" /></a>";
					}
					else{
						$buttons = "<a href=\"/remote/vnc/$client_programid\" target=\"_blank\"><img src=\"icon-vnc-off.png\" border=\"0\" height=\"16\" width=\"16\" alt=\"Connect via VNC\" title=\"Connect via VNC\" /></a>
									<a href=\"/remote/ssh/$client_programid\" target=\"_blank\"><img src=\"icon-ssh-off.png\" border=\"0\" height=\"16\" width=\"16\" alt=\"Connect via SSH\" title=\"Connect via SSH\" /></a>";
					}
				}
				else{
					$buttons = "";
				}
				
				///version status/update error
				if($versionStatus == 1){
					$versionError = "<img src=\"statusG.png\" border=\"0\" height=\"16\" width=\"16\" alt=\"Up to Date\" title=\"Up to Date\" />";
				}
				elseif($versionStatus == 2){
					$versionError = "<img src=\"statusI.png\" border=\"0\" height=\"16\" width=\"16\" alt=\"Not on Current Version\" title=\"Not on Current Version\" />";
				}
				elseif($versionStatus == 3){
					$versionError = "<img src=\"statusB.png\" border=\"0\" height=\"16\" width=\"16\" alt=\"Upgrade Error Message\" title=\"Upgrade Error Message\" />";
				}
				elseif($version == 2){
					$versionError = "<img src=\"statusQ.png\" border=\"0\" height=\"16\" width=\"16\" alt=\"Not Reported\" title=\"Not Reported\" />";
				}
				else{
					$versionError = "";
				}
				
				///checkbox
				if($status == 0){
					$checkStatus = $line['checkStatus'];
					$checkBy = $line['user'];
					$checkTime = $line['time_check'];
					
					if($checkStatus > 0){
						$showcheck = "<a href=\"offline/changeStatus?pid=$client_programid\"><img src=\"check.png\" border=\"0\" height=\"16\" width=\"16\" alt=\"Checked $checkStatus time(s). Last by $checkBy ($checkTime)\" title=\"Checked $checkStatus time(s). Last by $checkBy ($checkTime)\" /></a>";
					}
					else{
						$showcheck = "<a href=\"offline/changeStatus?pid=$client_programid\"><img src=\"uncheck.png\" border=\"0\" height=\"16\" width=\"16\" alt=\"Not Checked\" title=\"Not Checked\" /></a>";
					}
				}
				else{
					$showcheck = "";
				}
				
				$counter++;
				
				$data .= "<tr bgcolor=$bgColor onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor='$bgColor'><td>$showcheck $client_programid</td><td>$parent</td><td>$versionError $name</td><td>$buttons $online</td></tr>";
			}	
			
			$data .= "<tr><td colspan=4><b>Total: $counter</b></td></tr></table>";
			
			echo $data;
		}
}
?>