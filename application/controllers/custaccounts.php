<?php
if( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname( __FILE__ ) . '/../../htdocs/' ) );
}

class CustaccountsController extends Manage_Controller_Abstract {
        public function gen_uuid()
        {
		return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
				// 32 bits for "time_low"
				mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

				// 16 bits for "time_mid"
				mt_rand( 0, 0xffff ),

				// 16 bits for "time_hi_and_version",
				// four most significant bits holds version number 4
				mt_rand( 0, 0x0fff ) | 0x4000,

				// 16 bits, 8 bits for "clk_seq_hi_res",
				// 8 bits for "clk_seq_low",
				// two most significant bits holds zero and one for variant DCE1.1
				mt_rand( 0, 0x3fff ) | 0x8000,

				// 48 bits for "node"cust
				mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
		);
        }
    
	public function generateCustAccounts(){
                $message = "DELETE FROM customers;\n";
            
                $stmnt = $this->_tdb->query( 'SELECT * FROM customer WHERE businessid = ?', array( $_GET["bid"] ) );
		$array = $stmnt->fetchAll( PDO::FETCH_ASSOC );
                
                foreach( $array AS $line ){
                    $newid = CustaccountsController::gen_uuid();
                    
                    $customerid = $line["customerid"];
                    $name = $line["first_name"] . " " . $line["last_name"];
                    $scancode = $line["scancode"];
                    $balance = $line["balance"];
                    
                    $message .= "INSERT INTO customers (id, customer_id, name, ck_card_number, ck_card_balance)
                                    VALUES ('$newid', '$customerid', '$name', '$scancode', '$balance');\n";
                }
                
                header("Content-Type: text/plain");
                echo $message;
	}
	
	protected function init( ) {
		$this->_tdb = new Manage_DB( 'treat db');
	}
}
?>