<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of kiosk
 *
 * @author Eric Gach <eric@essential-elements.net>
 */

if( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname( __FILE__ ) . '/../../htdocs/' ) );
}

class KioskController extends Manage_Controller_Abstract {
	protected $_argMap = array(
		'index' => array(
			'pid'
		), 'checkRestart' => array(
			'pid'
		), 'isRestarted' => array(
			'pid'
		), 'kiosk_promo_groups' => array(
			'pid'
		)
	);

	protected $_tdb;
	protected $_mdb;
	
	public function reportError( ) {
		$pid = $_POST['pid'];
		$cat = $_POST['cat'];
		$msg = $_POST['msg'];
		$ip = $_SERVER['REMOTE_ADDR'];
		
		$mailTo = 'eeteam@essential-elements.net';
		$mailSubject = 'Client Error - ' . $pid . ' - ' . $ip;
		$mailMessage = 'Client: ' . $pid . ' (' . $ip . ')' . "\r\n" . 'Category: ' . $cat . "\r\n" . 'Message: ' . $msg;
		$mailHeaders = 'From: clienterror@essential-elements.net' . "\r\n" . 'Reply-To: ' . $mailTo . "\r\n" . 'X-Mailer: PHP/' . phpversion( );
		
		mail( $mailTo, $mailSubject, $mailMessage, $mailHeaders );
	}

	public function checkRestart( ) {
		echo $this->_db
				  ->query( 'SELECT IF(send_status = 3,1,0) FROM client_programs WHERE client_programid = ?', array( $this->_args['pid'] ) )
				  ->fetchColumn( );
	}

	public function isRestarted( ) {
		$this->_db
				->query(
						'UPDATE client_programs SET send_status = 0, receive_status = 5, receive_time = ? WHERE client_programid = ? AND send_status = 3',
						array( date( 'Y-m-d H:i:s' ), $this->_args['pid'] ) );
	}

	/////offline transactions sync
	public function offline_trans( ) {
		$vdb = new Manage_DB( 'vivipos db');
		$vdb->exec( 'USE vivipos_order' );

		$stmnt = $vdb
				->query(
						"SELECT order_payments.id,order_payments.amount,order_payments.name,order_payments.terminal_no,orders.customer_card_number,orders.sequence,order_payments.customer_card_number AS scancode
														FROM order_payments
														JOIN orders ON orders.id = order_payments.order_id
														WHERE order_payments.memo2 = 'OFFLINE'
														AND order_payments.is_processed = 0
														AND
															(order_payments.name = 'CK'
															OR order_payments.name = 'PD')");
		$array = $stmnt->fetchAll( PDO::FETCH_ASSOC );

		////ck and pd transactions
		foreach( $array AS $line ) {
			$log = $this->_tdb->query('SELECT COUNT(transactionId) FROM transaction_log WHERE transactionId=? AND clientProgramId=? AND customerCardNum=?', array($line['sequence'], $line['terminal_no'], $line['customer_card_number']));
			$foundInLog = (bool)$log->fetchColumn();
			if ($foundInLog) $this->_logDuplicate($line);

			if($line["scancode"] > 0){
				$scancode = $line["scancode"];
			}
			else{
				$scancode = $line["customer_card_number"];
			}

			if( $line["name"] == "PD" ) {
				$businessid = $this->_getBid( $line["terminal_no"] );

				if (!$foundInLog) {
					$this->_tdb->exec(
								"UPDATE customer SET balance = ROUND(balance - '" . $line["amount"] . "',2) WHERE businessid = "
										. $businessid . " AND scancode = '" . $scancode . "'" );
				}

				$vdb->exec( "UPDATE order_payments SET is_processed = 1, date_posted = date_posted WHERE id = '" . $line["id"] . "'" );
			}
			else {
				if (!$foundInLog) {
					$this->_tdb->exec(
								"UPDATE customer SET balance = ROUND(balance - '" . $line["amount"] . "',2) WHERE scancode = '"
										. $scancode . "'" );
				}

				$vdb->exec( "UPDATE order_payments SET is_processed = 1, date_posted = date_posted WHERE id = '" . $line["id"] . "'" );
			}
		}

		////bill acceptor transactions
		$stmnt = $vdb
				->query(
						"SELECT order_payments.id,order_payments.amount,order_payments.customer_card_number,order_payments.order_id,order_payments.terminal_no
														FROM order_payments
														WHERE order_payments.memo2 = 'OFFLINE' AND order_payments.is_processed = 0 AND (order_payments.name = 'CA' OR order_payments.name = 'CR')" );
		$array = $stmnt->fetchAll( PDO::FETCH_ASSOC );

		foreach( $array AS $line ) {
			$log = $this->_tdb->query('SELECT COUNT(transactionId) FROM transaction_log WHERE transactionId=? AND clientProgramId=? AND customerCardNum=?', array($line['order_id'], $line['terminal_no'], $line['customer_card_number']));
			$foundInLog = (bool)$log->fetchColumn();
			if ($foundInLog) $this->_logDuplicate($line);

			if (!$foundInLog) {
				$this->_tdb->exec(
							"UPDATE customer SET balance = ROUND(balance + '" . $line["amount"] . "',2) WHERE scancode = '"
									. $line["customer_card_number"] . "'" );
			}

			$vdb->exec( "UPDATE order_payments SET is_processed = 1, date_posted = date_posted WHERE id = '" . $line["id"] . "'" );
		}

		return ( false );
	}
	
	/////online transactions sync
	public function online_trans( ) {
		$vdb = new Manage_DB( 'vivipos db');
		$vdb->exec( 'USE vivipos_order' );
		
		$counter = 0;

		$stmnt = $vdb
				->query(
						"SELECT order_payments.id,order_payments.amount,order_payments.name,order_payments.terminal_no,orders.customer_card_number,orders.sequence,order_payments.customer_card_number AS scancode
														FROM order_payments
														JOIN orders ON orders.id = order_payments.order_id
														WHERE order_payments.memo2 = 'ONLINE'
														AND order_payments.is_processed = 0
														AND
															(order_payments.name = 'CK'
															OR order_payments.name = 'PD')");
		$array = $stmnt->fetchAll( PDO::FETCH_ASSOC );

		////ck and pd transactions
		foreach( $array AS $line ) {
			$log = $this->_tdb->query('SELECT COUNT(transactionId) FROM transaction_log WHERE transactionId=? AND clientProgramId=? AND customerCardNum=?', array($line['sequence'], $line['terminal_no'], $line['customer_card_number']));
			$foundInLog = (bool)$log->fetchColumn();

			if($line["scancode"] > 0){
				$scancode = $line["scancode"];
			}
			else{
				$scancode = $line["customer_card_number"];
			}

			if( $line["name"] == "PD" ) {
				$businessid = $this->_getBid( $line["terminal_no"] );

				if (!$foundInLog) {
					/*
					$this->_tdb->exec(
								"UPDATE customer SET balance = ROUND(balance - '" . $line["amount"] . "',2) WHERE businessid = "
										. $businessid . " AND scancode = '" . $scancode . "'" );
					*/
					$counter++;
					$message .= $line["name"] . ', ' . $line["terminal_no"] . ', ' . $scancode . ', ' . $line["id"] . '<br>';
				}

				$vdb->exec( "UPDATE order_payments SET is_processed = 1, date_posted = date_posted WHERE id = '" . $line["id"] . "'" );
			}
			else {
				if (!$foundInLog) {
					/*
					$this->_tdb->exec(
								"UPDATE customer SET balance = ROUND(balance - '" . $line["amount"] . "',2) WHERE scancode = '"
										. $scancode . "'" );
					*/
					$counter++;
					$message .= $line["name"] . ', ' . $line["terminal_no"] . ', ' . $scancode . ', ' . $line["id"] . '<br>';
				}

				$vdb->exec( "UPDATE order_payments SET is_processed = 1, date_posted = date_posted WHERE id = '" . $line["id"] . "'" );
			}
		}

		////bill acceptor transactions
		$stmnt = $vdb
				->query(
						"SELECT order_payments.id,order_payments.amount,order_payments.customer_card_number,order_payments.order_id,order_payments.terminal_no
														FROM order_payments
														WHERE order_payments.memo2 = 'ONLINE' AND order_payments.is_processed = 0 AND (order_payments.name = 'CA' OR order_payments.name = 'CR')" );
		$array = $stmnt->fetchAll( PDO::FETCH_ASSOC );

		foreach( $array AS $line ) {
			$log = $this->_tdb->query('SELECT COUNT(transactionId) FROM transaction_log WHERE transactionId=? AND clientProgramId=? AND customerCardNum=?', array($line['order_id'], $line['terminal_no'], $line['customer_card_number']));
			$foundInLog = (bool)$log->fetchColumn();

			if (!$foundInLog) {
				/*
				$this->_tdb->exec(
							"UPDATE customer SET balance = ROUND(balance + '" . $line["amount"] . "',2) WHERE scancode = '"
									. $line["customer_card_number"] . "'" );
				*/
				$counter++;
				$message .= $line["name"] . ', ' . $line["terminal_no"] . ', ' . $line["customer_card_number"] . ', ' . $line["order_id"] . '<br>';
			}

			$vdb->exec( "UPDATE order_payments SET is_processed = 1 WHERE id = '" . $line["id"] . "'" );
		}
		
		if($counter != 0){
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= "From: support@companykitchen.com\r\n";
			$subject = "($counter) Online Transaction Problem";
			$email = "smartin@essentialpos.com";

			$email_msg = "<html><head></head><body>$message</body></html>";

			mail($email, $subject, $email_msg, $headers);
		}

		return ( false );
	}

	public function index( ) {
		$bid = $this->_getBid( $this->_args['pid'] );

		$stmnt = $this->_tdb
					  ->query(
						'SELECT
	m.pos_id,
	m.item_code,
	m.upc,
	p.new_price,
	m.id,
	m.name,
	m.is_button,
	p.price,
	g.pos_id AS g_pos_id
FROM
	menu_items_new as m
		JOIN kiosk_sync AS k
			ON k.item_id = m.id
			AND k.item_type = 1
		LEFT JOIN menu_items_price AS p
			ON p.menu_item_id = m.id
		LEFT JOIN menu_groups_new AS g
			ON g.id = m.group_id
WHERE
	((m.item_code IS NOT NULL AND m.item_code <> \'\')
	OR (m.upc IS NOT NULL AND m.upc <> \'\')
	OR p.new_price IS NOT NULL)
	AND m.businessid = ?
	AND k.client_programid = ?', array( $bid, $this->_args['pid'] ) );
		$array = $stmnt->fetchAll( PDO::FETCH_ASSOC );
		$this->_tdb->exec( 'DELETE FROM kiosk_sync WHERE client_programid = ? AND item_type = 1', array( $this->_args['pid'] ) );

		header( 'Content-Type: text/plain' );
		foreach( $array as $line ) {
			foreach( $line as $value ) {
				$value = str_replace( '\'', '`', $value );
			}

			$promo_groups = $this->_tdb
								 ->query(
								 'SELECT uuid FROM promo_groups JOIN promo_groups_detail ON promo_group_id = id WHERE product_id = ? AND businessid = ?',
								 array( $line['id'], $bid ) );
			$line['link_group'] = base64_encode( implode( ',', $promo_groups->fetchAll( PDO::FETCH_COLUMN ) ) );

			if( !empty( $line['new_price'] ) ) {
				$this->_tdb
						->query(
								'UPDATE menu_items_price SET price = ' . $line['new_price']
										. ', new_price = NULL WHERE menu_item_id = ' . $line['id'] . ' AND businessid = ?',
								array( $bid ) );
			}
			//$this->_tdb->exec('UPDATE menu_items_new SET pos_processed = 1 WHERE id = ? AND businessid = ?', array($line['id'], $bid));

			unset( $line['id'] );
			echo implode( ',', $line ) . "\r\n";
		}

		return ( false );
	}

	//// kiosk promotions

	public function kiosk_promotions( ) {
		$programid = $_GET['programid'];
		$businessid = $this->_getBid( $programid );

		$stmnt = $this->_tdb
					  ->query(
						'
			SELECT
				*
			FROM vPromoVivipos
			JOIN kiosk_sync
				ON kiosk_sync.item_id = vPromoVivipos.promo_id
				AND kiosk_sync.item_type = 5
				AND kiosk_sync.client_programid = ?
		', array( $programid ) );
		$array = $stmnt->fetchAll( PDO::FETCH_ASSOC );

		if( !isset( $_GET['testing'] ) )
			$this->_tdb->exec( 'DELETE FROM kiosk_sync WHERE client_programid = ? AND item_type = 5', array( $programid ) );

		foreach( $array as $key => $data ) {
			$triggerQuery = '';

			if( $data['vivipos_trigger'] == 'multi_dept' ) {
				$whereType = 'AND ( type = 31 OR type = 32 OR type = 33 ) ORDER BY type';
				$bindParams = array(
					$data['promo_id']
				);
			}
			elseif( $data['vivipos_trigger'] == 'multi_group' ) {
				$whereType = 'AND ( type = 51 OR type = 52 OR type = 53 ) ORDER BY type';
				$bindParams = array(
					$data['promo_id']
				);

				$triggerQuery = "
					SELECT
						promo_groups.uuid AS id
					FROM promo_detail
					JOIN promo_groups USING( id )
					WHERE promo_id = ?
					$whereType
				";
			}
			else {
				$whereType = 'AND type = ?';
				$bindParams = array(
					$data['promo_id'], $data['promo_target']
				);
			}

			$triggerQuery = $triggerQuery ?
										  : "
				SELECT
					promo_detail.id
				FROM promo_detail
				WHERE promo_id = ?
				$whereType
			";

			$triggerStmnt = $this->_tdb->query( $triggerQuery, $bindParams );
			$triggerData = $triggerStmnt->fetchAll( PDO::FETCH_COLUMN );

			$array[$key]['trigger_data_array'] = $triggerData;
		}

		$contentType = stristr( $_SERVER['HTTP_ACCEPT'], 'application/json' ) ? 'application/json' : 'text/plain';
		header( 'Content-Type: ' . $contentType );
		echo json_encode( $array );

		return false;
	}

	public function kiosk_promo_groups( ) {
		Treat_Model_Abstract::db( Manage_DB::singleton( ) );
		$pid = $this->_args['pid'];
		$bid = $this->_getBid( $pid );
		$stmnt = $this->_tdb
					  ->query(
						'SELECT uuid AS id,name,visible,display_order,routing FROM promo_groups JOIN kiosk_sync ON item_id = id AND item_type = 6 WHERE businessid = ? AND client_programid = ?',
						array( $bid, $pid ) );

		header( 'Content-Type: text/plain' );
		echo json_encode( $stmnt->fetchAll( PDO::FETCH_ASSOC ) );
		$stmnt->closeCursor( );
		$this->_tdb->exec( 'DELETE FROM kiosk_sync WHERE client_programid = ? AND item_type = 6', array( $pid ) );

		return ( false );
	}

	/////tax types

	public function kiosk_tax_type( ) {
		$programid = $_GET['programid'];
		///get businessid of kiosk
		$businessid = $this->_getBid( $programid );

		$stmnt = $this->_tdb
					  ->query(
						'SELECT pos_id,name,percent FROM menu_tax JOIN kiosk_sync ON item_id = id AND item_type = 3 WHERE businessid = ? AND client_programid = ?',
						array( $businessid, $programid ) );
		$array = $stmnt->fetchAll( PDO::FETCH_ASSOC );
		$this->_tdb->exec( 'DELETE FROM kiosk_sync WHERE client_programid = ? AND item_type = 3', array( $programid ) );

		header( 'Content-Type: text/plain' );
		foreach( $array AS $line ) {
			echo implode( ',', $line ), "\r\n";
		}

		return ( false );
	}

	/////menu groups

	public function kiosk_menu_groups( ) {
		$programid = $_GET['programid'];
		///get businessid of kiosk
		$businessid = $this->_getBid( $programid );

		$stmnt = $this->_tdb
					  ->query(
						'SELECT pos_id,name FROM menu_groups_new JOIN kiosk_sync ON item_id = id AND item_type = 2 WHERE businessid = ? AND client_programid = ?',
						array( $businessid, $programid ) );
		$array = $stmnt->fetchAll( PDO::FETCH_ASSOC );
		$this->_tdb->exec( 'DELETE FROM kiosk_sync WHERE client_programid = ? AND item_type = 2', array( $programid ) );

		header( 'Content-Type: text/plain' );
		foreach( $array AS $line ) {
			echo implode( ',', $line ), "\r\n";
		}

		return ( false );
	}

	/////menu groups

	public function kiosk_time( ) {

		$kiosk_time = time( );

		echo "$kiosk_time";

		return ( false );
	}

	/////menu item taxes

	public function kiosk_taxes( ) {
		$programid = $_GET["programid"];
		///get businessid of kiosk
		$businessid = $this->_getBid( $programid );

		$stmnt = $this->_tdb
					  ->query(
						'SELECT mi.pos_id AS m_pos_id,
											mt.pos_id AS t_pos_id
										FROM menu_items_new AS mi
										JOIN kiosk_sync AS k ON mi.id = k.item_id AND k.item_type = 4
										LEFT JOIN menu_item_tax AS mit ON mit.menu_itemid = mi.id
										LEFT JOIN menu_tax AS mt ON mt.id = mit.tax_id
										WHERE mi.businessid = ? AND k.client_programid = ?', array( $businessid, $programid ) );
		$array = $stmnt->fetchAll( PDO::FETCH_ASSOC );
		$this->_tdb->exec( 'DELETE FROM kiosk_sync WHERE client_programid = ? AND item_type = 4', array( $programid ) );

		header( 'Content-Type: text/plain' );
		foreach( $array AS $line ) {
			echo implode( ',', $line ), "\r\n";
		}

		return ( false );
	}

	/////kiosk customer balances

	/**
	 * Update customer card balance. Deprecated. Use EPayController (epay.php) instead.
	 * @deprecated
	 */
	public function kiosk_cust_balance( ) {
		header( "Content-Type: text/plain" );
		#ini_set( "display_errors", "false" );
		
		if( isset( $_GET['debug'] ) )
			$_POST = $_GET;

		Treat_Utility::CheckPostData(  );
		
		Treat_Model_User_Customer::db( $this->_tdb );

		$process_type = $_POST["process_type"];
		$scancode = $_POST["scancode"];
		$amount = $_POST["amount"];
		// Adding new fields
		$transaction_no = (isset($_POST['seq_num']))? $_POST['seq_num'] : ((isset($_POST['seqNum']))? $_POST['seqNum'] : null);
		$terminal_no = (isset($_POST['terminalNumber']))? $_POST['terminalNumber'] : null;
		// Make the transaction_no as it would appear in our vivipos tables
		if (!empty($transaction_no) && ($process_type == 'ckCardProcess' || $process_type == 'pdCardProcess')) {
			$transaction_no = substr($transaction_no, 2).$terminal_no;
		}

		if( $process_type == "pdTrans" ) {
			////get businessid of terminal
			$stmnt = $this->_db->query( "SELECT businessid FROM client_programs WHERE client_programid = '$terminal_no'" );
			$businessid = $stmnt->fetchColumn( );

			if( $businessid > 0 ) {

				$balance = '0.00';
				
				if( ( $customer = Treat_Model_User_Customer::getUserByScancodeAndBusinessid( $scancode, $businessid ) ) && $customer->inactive != 1 ) {
					$balance = number_format( $customer->balance, 2, '.', '' );
				}
				
				echo $balance;
			}
			else {
				echo "0.00";
			}
		}
		elseif( $process_type == "pdCardProcess" ) {
			////get businessid of terminal
			$stmnt = $this->_db->query( "SELECT businessid FROM client_programs WHERE client_programid = '$terminal_no'" );
			$businessid = $stmnt->fetchColumn( );

			if( $businessid > 0 ) {
				if( $customer = Treat_Model_User_Customer::getUserByScancodeAndBusinessid( $scancode, $businessid ) ) {
					if (!empty($transaction_no) && !empty($terminal_no)) {
						// Insert the transaction into the log. We later use this to compare
						// to offline transactions and make sure we don't double charge
						// a customer.
						$this->_tdb->exec('INSERT INTO transaction_log (transactionId, clientProgramId, customerCardNum, amount) VALUES(?, ?, ?, ?)', array($transaction_no, $terminal_no, $scancode, $amount));
					}
					
					$customer->balance -= $amount;
					$customer->save( );
				}
			}
		}
		elseif( $process_type == "pdCNFL" ) {
			////get businessid of terminal
			$stmnt = $this->_db->query( "SELECT businessid FROM client_programs WHERE client_programid = '$terminal_no'" );
			$businessid = $stmnt->fetchColumn( );

			if( $businessid > 0 && ( $customer = Treat_Model_User_Customer::getUserByScancodeAndBusinessid( $scancode, $businessid ) ) ) {
				/*$stmnt = $this->_tdb
							  ->query(
								"SELECT customerid,first_name,last_name,balance,inactive FROM customer WHERE businessid = $businessid AND scancode = '$scancode'" );
				$result = $stmnt->fetch( PDO::FETCH_ASSOC );

				$customerid = $result["customerid"];
				$first_name = $result["first_name"];
				$last_name = $result["last_name"];
				$balance = number_format( $result["balance"], 2, '.', '' );
				$inactive = $result["inactive"];

				if($inactive == 1){$balance = 0;}

				$first_name = str_replace( "'", "`", $first_name );
				$last_name = str_replace( "'", "`", $last_name );

				echo "$customerid,$first_name $last_name,$balance";*/
				
				echo $customer->customerid . ',' . $customer->first_name . ' ' . $customer->last_name . ',' . number_format( $customer->balance, 2, '.', '' );
			}
			else {
				echo "DECLINED";
			}
		}
		elseif( $process_type == "ckCardProcess" || $process_type == "ckBAT" || $process_type == "ckCR" ) {
			if( $process_type == "ckCardProcess" ) {
				$amount = $amount * -1;
			}

			$stmnt = $this->_tdb->query( "SELECT balance,first_name,last_name FROM customer WHERE scancode = '$scancode'" );
			$result = $stmnt->fetch( PDO::FETCH_ASSOC );

			$balance = number_format( $result["balance"], 2, '.', '' );
			$first_name = $result["first_name"];
			$last_name = $result["last_name"];

			echo "$balance";

			///add first name and last

			if (!empty($transaction_no) && !empty($terminal_no)) {
				// Insert the transaction into the log. We later use this to compare
				// to offline transactions and make sure we don't double charge
				// a customer.
				$this->_tdb->exec('INSERT INTO transaction_log (transactionId, clientProgramId, customerCardNum, amount) VALUES(?, ?, ?, ?)', array($transaction_no, $terminal_no, $scancode, $amount));
			}

			$this->_tdb->exec( "UPDATE customer SET balance = balance + $amount WHERE scancode = '$scancode'" );

			//echo "success";
		}
		elseif( $process_type == "ckCardLookup" ) {
			$stmnt = $this->_tdb->query( "SELECT balance,first_name,last_name FROM customer WHERE scancode = '$scancode'" );
			$result = $stmnt->fetch( PDO::FETCH_ASSOC );

			$balance = number_format( $result["balance"], 2, '.', '' );
			$first_name = $result["first_name"];
			$last_name = $result["last_name"];

			echo "$balance";

			///add first name and last
		}
		elseif( $process_type == "CNFL" || $process_type == "CNFLprocess" ) {
			$stmnt = $this->_tdb->query( "SELECT customerid,first_name,last_name,balance FROM customer WHERE scancode = '$scancode'" );
			$result = $stmnt->fetch( PDO::FETCH_ASSOC );

			$customerid = $result["customerid"];
			$first_name = $result["first_name"];
			$last_name = $result["last_name"];
			$balance = number_format( $result["balance"], 2, '.', '' );

			$first_name = str_replace( "'", "`", $first_name );
			$last_name = str_replace( "'", "`", $last_name );

			echo "$customerid,$first_name $last_name,$balance";
		}
		else {
			echo "Fail";
		}

		//debug
		$mytime = date( "Y-m-d H:i:s" );
		file_put_contents("/tmp/debug_data.txt", "$mytime,$process_type,$scancode,$amount,$terminal_no\r\n", FILE_APPEND | LOCK_EX);
		return ( false );
	}

	/////menu item taxes

	public function kiosk_customers( ) {
		$programid = $_GET["programid"];

		///get businessid of kiosk
		$businessid = $this->_getBid( $programid );

		$stmnt = $this->_tdb
					  ->query(
						"SELECT customerid,first_name,last_name,scancode FROM customer WHERE businessid = $businessid AND pos_processed = 0" );
		$array = $stmnt->fetchAll( PDO::FETCH_ASSOC );

		header( 'Content-Type: text/plain' );

		foreach( $array AS $line ) {
			echo $line["customerid"] . ",\"" . $line["first_name"] . "\",\"" . $line["last_name"] . "\"," . $line["scancode"] . "\r\n";
		}
		$this->_tdb->exec( "UPDATE customer SET pos_processed = 1 WHERE businessid = $businessid" );

		return ( false );
	}

	public function transactions( ) {
		#ini_set( "display_errors", "false" );
		$programid = $_GET["programid"];

		///get businessid of kiosk
		$businessid = $this->_getBid( $programid );

		////all approved transactions that haven't been processed
		$stmnt = $this->_tdb
					  ->query(
						"SELECT customer.scancode,customer_transactions.id,customer_transactions.subtotal FROM customer,customer_transactions WHERE customer_transactions.pos_processed = 0 AND customer_transactions.response = 'APPROVED' AND customer_transactions.customer_id = customer.customerid AND customer.businessid = $businessid" );
		$array = $stmnt->fetchAll( PDO::FETCH_ASSOC );

		header( 'Content-Type: text/plain' );

		echo "GCA_Account,GCA_Balance\r\n";

		foreach( $array AS $line ) {

			///remove 780 from beginning of scancode
			$line["scancode"] = substr( $line["scancode"], 3, 10 );

			echo "\"" . $line["scancode"] . "\"" . "," . $line["subtotal"] . "\r\n";

			///update transaction to processed
			$this->_tdb->query( 'UPDATE customer_transactions SET pos_processed = 1 WHERE id = ' . $line["id"] );
		}

		return ( false );
	}

	public function senddata( ) {
		//Treat_Utility::CheckPostData(  );
		//\EE\Utility::CheckPostData(  );

		if( !isset( $_POST["data"] ) ) {
			echo "error:no data received";
		}
		else {
			#include( 'inc/db.php' );
			#include( 'inc/class.db-proxy.php' );
			#ini_set( "display_errors", "false" );
			
			$post_date = date("Y-m-d",mktime(0, 0, 0, date("m") , date("d")-13, date("Y")));
			$post_date = "$post_date 00:00:00";
			$inv_date = date("Y-m-d");

			$data = $_POST["data"];

			$data = explode( chr( 13 ) . chr( 10 ), $data );

			$counter = 0;
			$last_mach_num = 0;
			$newQuery = "INSERT INTO inv_count_vend (machine_num,date_time,item_code,onhand,fills,waste,unit_cost,is_inv) VALUES ";
			$itemCodes = "";
			$hasInv = 0;
			
			foreach( $data AS $line => $value ) {
				$values = explode( chr( 9 ), $value );

				////current businessid
				if( $last_mach_num != $values[0] ) {
					$query = "SELECT businessid FROM machine_bus_link WHERE machine_num = '$values[0]'";
					$result = Treat_DB_ProxyOld::query( $query );

					$bid = mysql_result( $result, 0, "businessid" );
				}

				////get cost
				$query = "SELECT menu_items_price.cost FROM menu_items_price
							JOIN menu_items_new ON menu_items_new.id = menu_items_price.menu_item_id
							WHERE menu_items_new.businessid = $bid AND menu_items_new.item_code = '$values[2]'";
				$result = Treat_DB_ProxyOld::query( $query );

				if( $result ) {
					$cost = @mysql_result( $result, 0, "cost" );
				}
				else {
					$cost = 0;
				}

				if( $values[0] == -1 ) {
					//////if collected
					if( $values[1] == 1 && $values[2] == 1 ) {
						$query = "SELECT machine_bus_link.businessid,business.timezone FROM machine_bus_link
									JOIN business ON business.businessid = machine_bus_link.businessid
									WHERE machine_bus_link.machine_num = '$last_mach_num'";
						$result = Treat_DB_ProxyOld::query( $query );

						$businessid = mysql_result( $result, 0, "businessid" );
						$timezone = mysql_result( $result, 0, "timezone" );

						$mytime = date( "Y-m-d H:i:s",
								mktime( date( "H" ) + $timezone, date( "i" ), date( "s" ), date( "m" ), date( "d" ), date( "Y" ) ) );

						$query = "INSERT INTO machine_collect (businessid,machine_num,date_time) VALUES ($businessid,'$last_mach_num','$mytime')";
						$result = Treat_DB_ProxyOld::query( $query );
					}

					$counter++;
				}
				elseif( $values[4] == 1 ) {
					$query = "INSERT INTO inv_count_vend (machine_num,date_time,item_code,onhand,unit_cost,is_inv)
					VALUES ('$values[0]','$values[1]','$values[2]',$values[3],'$cost',1)
					ON DUPLICATE KEY UPDATE onhand = '$values[3]', unit_cost = '$cost', is_inv = 1";
					//$result = Treat_DB_ProxyOld::query( $query );
					
					if($values[1] >= $post_date){
						$newQuery .= "('$values[0]','$values[1]','$values[2]',$values[3],0,0,'$cost',1),";
						$itemCodes .= "'$values[2]',";
						$hasInv = 1;
						
						if( substr($values[1],0,10) < $inv_date){
							$inv_date = substr($values[1],0,10);
						}
					}
					
					$counter++;
				}
				elseif( $values[4] == 2 ) {
					$query = "INSERT INTO inv_count_vend (machine_num,date_time,item_code,fills,unit_cost,is_inv)
					VALUES ('$values[0]','$values[1]','$values[2]',$values[3],'$cost',0)
					ON DUPLICATE KEY UPDATE fills = '$values[3]', unit_cost = '$cost', is_inv = 0";
					//$result = Treat_DB_ProxyOld::query( $query );
					
					if($values[1] >= $post_date){
						$newQuery .= "('$values[0]','$values[1]','$values[2]',0,$values[3],0,'$cost',0),";
						$itemCodes .= "'$values[2]',";
						$hasInv = 1;
						
						if( substr($values[1],0,10) < $inv_date){
							$inv_date = substr($values[1],0,10);
						}
					}
					
					$counter++;
				}
				elseif( $values[4] == 3 ) {
					$query = "INSERT INTO inv_count_vend (machine_num,date_time,item_code,waste,unit_cost,is_inv)
					VALUES ('$values[0]','$values[1]','$values[2]',$values[3],'$cost',0)
					ON DUPLICATE KEY UPDATE waste = '$values[3]', unit_cost = '$cost', is_inv = 0";
					//$result = Treat_DB_ProxyOld::query( $query );
					
					if($values[1] >= $post_date){
						$newQuery .= "('$values[0]','$values[1]','$values[2]',0,0,$values[3],'$cost',0),";
						$itemCodes .= "'$values[2]',";
						$hasInv = 1;
						
						if( substr($values[1],0,10) < $inv_date){
							$inv_date = substr($values[1],0,10);
						}
					}
					
					$counter++;
				}

				////make item active
				//$query = "UPDATE menu_items_new SET active = 0 WHERE businessid = $bid AND item_code = '$values[2]'";
				//$result = Treat_DB_ProxyOld::query( $query );

				$last_mach_num = $values[0];
				if($hasInv == 1 && $counter == 1){$first_mach_num = $values[0];}
			}
			echo $counter;
			
			if($counter > 0 && $hasInv == 1){
				///new stuff
				$query = "SELECT machine_bus_link.businessid FROM machine_bus_link
										WHERE machine_bus_link.machine_num = '$first_mach_num'";
				$result = Treat_DB_ProxyOld::query( $query );

				$businessid = mysql_result( $result, 0, "businessid" );

				$newQuery = substr_replace($newQuery ,";",-1);
				$result = Treat_DB_ProxyOld::query( $newQuery );

				$itemCodes = substr_replace($itemCodes ,"",-1);
				$updateQuery = "UPDATE menu_items_new SET active = 0 WHERE businessid = $businessid AND item_code IN ($itemCodes)";
				$result = Treat_DB_ProxyOld::query( $updateQuery );
				
				///update shrink queue table for recount
				$query = "INSERT INTO dashboard_shrink_recalculate (businessid,date_posted) VALUES ($businessid,'$inv_date')
					ON DUPLICATE KEY UPDATE date_posted = IF('$inv_date' < date_posted, '$inv_date', date_posted);";
				$result = Treat_DB_ProxyOld::query($query);

				//file_put_contents("/srv/home/uploads/manage/debug.log", "$newQuery \r\n\r\n $updateQuery \r\n\r\n", FILE_APPEND);
			}
		}
		return ( false );
	}

	public function login( ) {
		if( $_POST ) {
			$user = $_POST["user"];
			$pass = $_POST["pass"];

			$stmnt = $this->_tdb->query( "SELECT * FROM login_route WHERE username = '$user' AND password = '$pass'" );
			$array = $stmnt->fetch( PDO::FETCH_ASSOC );

			if( isset( $array ) ) {
				$login_routeid = $array["login_routeid"];
			}
			else {
				$login_routeid = 0;
			}
		}

		if( $login_routeid > 0 ) {
			echo "$login_routeid";
		}
		else {
			echo "error:invalid login";
		}

		return ( false );
	}

	public function download_businessid( ) {
		#include( 'inc/db.php' );
		#include( 'inc/class.db-proxy.php' );

		$login_routeid = $_POST["routeid"];
		
		$day = date("N");

		$query = "SELECT b.businessid, 
				b.businessname, 
				IF(MAX(vms.day) > 0, 1, 0) AS scheduled, 
				b.street, 
				b.city, 
				b.state, 
				b.zip,
				IF(bld.business_latitude != 0, bld.business_latitude,0) AS latitude,
				IF(bld.business_longitude != 0, bld.business_longitude,0) AS longitude
			FROM business b
			JOIN vend_machine vm ON vm.login_routeid = $login_routeid
				AND vm.is_deleted = 0
			JOIN machine_bus_link mbl ON mbl.businessid = b.businessid 
				AND mbl.machine_num = vm.machine_num
			LEFT JOIN vend_machine_schedule vms ON vms.machineid = vm.machineid AND vms.day = $day
			LEFT JOIN business_location_data bld ON bld.businessid = b.businessid
			GROUP BY b.businessid";
		$result = DB_Proxy::query( $query );
		$num = mysql_numrows( $result );

		if( $num == 0 ) {
			echo "error:no business found";
		}
		else {
			while( $r = mysql_fetch_array( $result ) ) {
				$businessid = $r["businessid"];
				$businessname = $r["businessname"];
				$scheduled = $r["scheduled"];
				$street = $r["street"];
				$city = $r["city"];
				$state = $r["state"];
				$zip = $r["zip"];
				$latitude = $r["latitude"];
				$longitude = $r["longitude"];

				$businessname = str_replace("Kiosk - ", "", $businessname);

				echo $businessid . chr( 9 ) . $businessname . chr( 9 ) . $scheduled . chr( 9 ) . $street . chr( 9 ) . $city . chr( 9 ) . $state . chr( 9 ) . $zip . chr( 9 ) . $latitude . chr( 9 ) . $longitude .  "\r\n";
			}
		}

		return ( false );
	}

	public function download_machines( ) {
		#include( 'inc/db.php' );
		#include( 'inc/class.db-proxy.php' );

		$login_routeid = $_POST["routeid"];
		if( isset( $_POST["businessid"] ) ) {
			$businessid = $_POST["businessid"];
		}
		else {
			$businessid = 0;
		}

		if( $businessid > 0 ) {
			$subquery = "AND machine_bus_link.businessid = $businessid";
		}
		else {
			$subquery = "";
		}

		$query = "SELECT machine_bus_link.*, vend_machine.name AS mach_name FROM machine_bus_link,vend_machine
					WHERE vend_machine.login_routeid = $login_routeid
					AND vend_machine.machine_num = machine_bus_link.machine_num
					AND vend_machine.is_deleted = 0
					$subquery
					ORDER BY businessid,ordertype";
		$result = DB_Proxy::query( $query );
		$num = mysql_numrows( $result );

		if( $num == 0 ) {
			echo "error:no machines";
		}
		else {
			while( $r = mysql_fetch_array( $result ) ) {
				$machine_num = $r["machine_num"];
				$ordertype = $r["ordertype"];
				$busid = $r["businessid"];
				$mach_name = $r['mach_name'];
				$terminal_no = $r["client_programid"];

				/*
				if($ordertype == 1){$showorder = "Cold Food";}
				elseif($ordertype == 2){$showorder = "Snack";}
				elseif($ordertype == 3){$showorder = "Beverage";}
				 */

				echo $busid . chr( 9 ) . $ordertype . chr( 9 ) . $machine_num . chr( 9 ) . $mach_name . chr( 9 ) . $terminal_no
						. "\r\n";
			}
		}

		return ( false );
	}

	public function download_items( ) {
		#ini_set( "display_errors", "false" );
		#include( 'inc/db.php' );
		#include( 'inc/class.db-proxy.php' );
		#include( 'lib/class.menu_items.php' );
		#include( 'lib/class.Kiosk.php' );
		//DB_Proxy::$debug = true;
		$today = date("Y-m-d");

		////get version number
		$version_data = parse_ini_file( SITECH_APP_PATH . "/../htdocs/program/update.ini", true );

		////version check
		if( @$_POST["version"] != $version_data["EE.InventoryMobile"]["version"] ) {
			echo "error: You must update to the latest version. Use the Menu tab at the bottom of the page.";
			exit;
		}

		$login_routeid = $_POST["routeid"];
		$busid = $_POST["businessid"];
		$order_type = $_POST["ordertype"];

		///get businessid if NULL
		if( $busid < 1 ) {
			$query = "SELECT DISTINCT machine_bus_link.businessid FROM vend_machine,machine_bus_link
				WHERE vend_machine.login_routeid = $login_routeid
				AND vend_machine.machine_num = machine_bus_link.machine_num
				AND vend_machine.is_deleted = 0";
			$result = DB_Proxy::query( $query );
		}
		else {
			$query = "SELECT businessid FROM business WHERE businessid = $busid";
			$result = DB_Proxy::query( $query );
		}

		$date2 = date( "Y-m-d H:i:s" );

		header( 'Content-Type: text/plain' );

		$lastbusid = 0;
		$num = 0;
		while( $r = mysql_fetch_array( $result ) ) {
			$businessid = $r["businessid"];

			if( $order_type > 0 ) {
				$subquery = "AND ordertype = $order_type";
			}
			else {
				$subquery = "";
			}

			////create list of inventory items
			$stmnt = $this->_tdb
						  ->query(
							"SELECT id,name,item_code,upc,pos_id,ordertype,active FROM menu_items_new WHERE businessid = $businessid $subquery ORDER BY ordertype" );
			$array = $stmnt->fetchAll( PDO::FETCH_ASSOC );

			foreach( $array AS $line ) {

				if( $lastbusid != $businessid ) {
					///build machine_num array
					$machine_num = array( );

					$stmnt = $this->_tdb->query( "SELECT machine_num FROM machine_bus_link WHERE businessid = $businessid" );
					$array = $stmnt->fetchAll( PDO::FETCH_ASSOC );

					$counter = 0;
					foreach( $array AS $line ) {
						$machine_num[$counter] = $line["machine_num"];
						$counter++;
					}
				}

				if( $line["active"] == 0 ) {
					////get dates
					$dates = Kiosk::last_inv_dates( $machine_num, $line["item_code"] );
					$date = $dates[1];
					if( $date == "" ) {
						$date = $dates[0];
					}

					////last count
					$onhand2 = Kiosk::inv_onhand( $machine_num, $line["item_code"], $date );

					////fills
					$fills = Kiosk::fills( $machine_num, $line["item_code"], $date, $date2 );

					////waste
					$waste = Kiosk::waste( $machine_num, $line["item_code"], $date, $date2 );

					////get check detail
					$items_sold = menu_items::items_sold( $line["pos_id"], $businessid, $date, $date2 );

					////fills
					$prefills = Kiosk::fills( $today, $line["id"]);

					$onhand = $onhand2 + $fills - $waste - $items_sold;
				}
				else {
					$onhand = 0;
				}

				echo $businessid . chr( 9 ) . $line["ordertype"] . chr( 9 ) . $line["name"] . chr( 9 ) . $line["item_code"] . chr( 9 )
						. $line["upc"] . chr( 9 ) . $onhand . chr( 9 ) . $line["active"] . chr( 9 ) . $prefills . "\r\n";

				$lastbusid = $businessid;
				$num++;
			}
		}

		if( $num == 0 ) {
			echo "error:no items";
		}

		return ( false );
	}
	
	public function version_check(){
		////get version number
		$version_data = parse_ini_file( SITECH_APP_PATH . "/../htdocs/program/hh_update.ini", true );

		echo $version_data["EE.InventoryMobileV2"]["version"];
		
		return( false );
	}

	public function download_machine_items( ) {
		#ini_set( "display_errors", "false" );
		#include_once( 'inc/db.php' );
		#include_once( 'inc/class.db-proxy.php' );
		#include_once( 'class.menu_items.php' );
		#include_once( 'class.Kiosk.php' );
		//DB_Proxy::$debug = true;
		
		$busid = $_POST["businessid"];
		
		$districts = array(1);
		
		$query = "SELECT companyid,districtid FROM business WHERE businessid = $busid";
		$result = Treat_DB_ProxyOld::query( $query );
		
		$companyid = mysql_result($result,0,"companyid");
		$districtid = mysql_result($result,0,"districtid");

		////get version number
		$version_data = parse_ini_file( SITECH_APP_PATH . "/../htdocs/program/update.ini", true );

		$query = "SELECT businessid,companyid,districtid FROM business WHERE businessid = $busid";
		$result = Treat_DB_ProxyOld::query( $query );
		
		////version check
		/*
		if( @$_POST["version"] != $version_data["EE.InventoryMobile"]["version"] && in_array( $districtid, $districts )) {
			echo "error: You must update to the latest version. Use the Menu tab at the bottom of the page.";
			exit;
		}*/

		$date2 = date( "Y-m-d H:i:s" );
		$today = date("Y-m-d");
		
		$today_fill_date = date( "Y-m-d 00:00:00" );
		$today_last_date = date( "Y-m-d 23:59:59" );

		header( 'Content-Type: text/plain' );

		$lastbusid = 0;
		$num = 0;
		while( $r = mysql_fetch_array( $result ) ) {
			$businessid = $r["businessid"];

			////create list of inventory items
			$stmnt = $this->_tdb
						  ->query(
							"SELECT
											menu_items_new.id,
											menu_items_new.name,
											menu_items_new.item_code,
											menu_items_new.upc,
											menu_items_new.pos_id,
											menu_items_new.ordertype,
											menu_items_new.active,
											machine_bus_link.machine_num
										FROM menu_items_new
										JOIN machine_bus_link ON machine_bus_link.businessid = $businessid AND machine_bus_link.ordertype = menu_items_new.ordertype
										WHERE menu_items_new.businessid = $businessid ORDER BY machine_bus_link.machine_num" );
			$array = $stmnt->fetchAll( PDO::FETCH_ASSOC );

			foreach( $array AS $line ) {

				if( $line["active"] == 0 ) {
					///machine number
					$machine_num[0] = $line["machine_num"];

					////get dates
					$dates = Kiosk::last_inv_dates( $machine_num, $line["item_code"] );
					$date = $dates[1];
					if( $date == "" ) {
						$date = $dates[0];
					}

					////last count
					$onhand2 = Kiosk::inv_onhand( $machine_num, $line["item_code"], $date );

					////fills
					$fills = Kiosk::fills( $machine_num, $line["item_code"], $date, $date2 );
					
					////today's fills
					$today_fills = Kiosk::fills( $machine_num, $line["item_code"], $today_fill_date, $today_last_date );

					////waste
					$waste = Kiosk::waste( $machine_num, $line["item_code"], $date, $date2 );

					////get check detail
					$items_sold = menu_items::items_sold( $line["pos_id"], $businessid, $date, $date2 );
					
					$prefills = Kiosk::hh_fills($today, $line["item_code"], $businessid);
					if($prefills < 1 || $today_fills > 0){$prefills = 0;}

					$onhand = $onhand2 + $fills - $waste - $items_sold;
				}
				else {
					$onhand = 0;
					$prefills = 0;
				}
				
				////remove prepopulated fills
				//$prefills = 0;
				
				$countOrder = strtotime($date);
				if($countOrder < 1){$countOrder = 0;}

				echo $businessid . chr( 9 ) . $line["ordertype"] . chr( 9 ) . $line["name"] . chr( 9 ) . $line["item_code"] . chr( 9 )
						. $line["upc"] . chr( 9 ) . $onhand . chr( 9 ) . $line["active"] . chr( 9 ) . $line["machine_num"] . chr( 9 ) . $prefills . chr( 9 ) . $countOrder . "\r\n";
				
				$lastbusid = $businessid;
				$num++;
			}
		}

		if( $num == 0 ) {
			echo "error:no items";
		}

		return ( false );
	}

	protected function init( ) {
		$this->_tdb = new Manage_DB( 'treat db');
	}

	protected function _getBid( $pid ) {
		$bid = Manage_DB::singleton( )->query( 'SELECT businessid FROM client_programs WHERE client_programid = ?', array( $pid ) )
									  ->fetchColumn( );
		if( empty( $bid ) )
			exit;
		return ( $bid );
	}

	protected function _logDuplicate($record)
	{
		if (!($fp = fopen(SITECH_APP_PATH.'/log/duplicate_trans.log', 'a'))) {
			error_log('Failed to open duplicate transaction log');
			return;
		}

		fwrite($fp, sprintf('%s - Terminal:%d OrderPaymentId:%d Customer:%d Amount:%f%s', date('r'), $record['terminal_no'], $record['id'], $record['customer_card_number'], $record['order_total'], "\n"));
		fclose($fp);
	}
}
