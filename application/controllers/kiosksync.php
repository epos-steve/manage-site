<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

if( !function_exists( 'utf8_json_encode' ) )
	require_once( 'inc/utils.php' );

/**
 * Description of kiosksync
 *
 * @author eric
 */
class KiosksyncController extends Manage_Controller_Abstract
{
	protected $_cleanup = true;
	protected $_client;
	protected $_db;
	protected $_tdb;

	protected $_api_version = 1;

	protected $dir_kiosksync_files;
	protected $sync_type = "python";

	protected $pull_chunk_size = 700000; // Chunk size in bytes

	protected function _check_restart()
	{
		return $this->_db->query('SELECT IF(send_status = 3,1,0) FROM client_programs WHERE client_programid = ?', array($this->_args[0]))->fetchColumn();
	}

	/**
	 * Kiosk will call this to see if it should restart. If it outputs 1 the
	 * kiosk-sync script should then initate a reboot of the kiosk.
	 */
	public function checkRestart()
	{
		header('Content-Type: text/plain; charset=utf-8');
		echo $this->_check_restart();
		return false;
	}

	protected function _client_checkin()
	{	
		if(!empty($_GET["ip"])) {
			$ip = $_GET["ip"];
			$this->_vdb = new Manage_DB( 'vivipos db');
			$this->_vdb->exec('USE machineStatus');
			$query = "INSERT INTO manageClients (client_programid,client_ip) VALUES (?,?) ON DUPLICATE KEY UPDATE client_ip = ?, send_time = NOW()";
			$this->_vdb->exec($query, array($this->_args[0], $ip, $ip));
		}
	}

	public function index()
	{
		$return = array();
		foreach( static::$dataDefinition as $type ) {
			$return[static::convertNameUnderscore( $type )] = $this->{'_has' . $type}( );
		}

		echo utf8_json_encode($return);
		
		$this->_client_checkin();
		
		return false;
	}

	/**
	 * Data type definition array. To add a new type to the sync pull, add the name to this list and
	 * create the appropriate methods.
	 *
	 * Example: new type ExampleType
	 * KioskSync methods:
	 *      countExampleType( $client ) => int
	 *      getExampleType( $client, $cleanup, $api_version ) => array
	 *
	 * @var array
	 */
	protected static $dataDefinition = array(
		'ResetData',
		'PosLocation',
		'PosSettings',
		'MenuGroups',
		'ModifierGroups',       // must be before modifiers, menu_items
		'Modifiers',
		'StaticModifiers',
		'MenuItems',
		'MenuItemTaxes',
		'PromotionGroups',      // This must be before promotions
		'Promotions',
		'Combos',
		'TaxTypes',
		'RunScripts',
		'CashierUsers',
		'CustomerBalances',
		'FunctionPanel',
		'Fingerprints',
		'Forex',
		'Forecast',
		//'Images', Images doesn't get called directly, only to debug.
	);

	protected function getData( $type ) {
		if( in_array( $type, static::$dataDefinition ) ) {
			$has = '_has' . $type;
			$get = 'get' . $type;
			if( $this->{$has}( ) ) {
				return $this->{$get}( );
			}
		}

		return false;
	}

	/**
	 * Pull all sync data down to the kiosk and check the client in.
	 *
	 * Version 15:
	 *  Added support to chunk pull data.
	**/
	public function pull() {

		$binary = isset( $_GET['binary'] );
		$chunk = filter_input( INPUT_GET, 'chunk' );
		$chunk_type = filter_input( INPUT_GET, 'chunk_type' );
		$kiosk_dir = $this->dir_kiosksync_files . 'pull/';
		$file = $kiosk_dir . $this->_client->client_programid;
		$image_file = $kiosk_dir . $this->_client->client_programid . '-images.tar.gz';

		if ( $this->_api_version >= 15 ) {
			// Return the Saved Chunk File if chunk is requested.
			if ( $binary && is_numeric( $chunk ) && $chunk >= 1 ) {
				if ( $chunk_type == 'image' ) {
					$file = $image_file;
				}
				if ( !file_exists( $file ) ) {
					echo 'Invalid File: ' . $file;
					return false;
				}

				// Open the File Stream.
				$f = fopen( $file, 'r' );
				$total_size = filesize( $file );

				// If chunk is greater than 1, move the file pointer to the
				// right place on the file stream.
				if ( $chunk > 1 ) {
					$pointer = $this->pull_chunk_size * ( $chunk - 1 );
					fseek( $f, $pointer );
				} else {
					$pointer = 0;
				}

				// Figure out if there is another chunk.
				if ( ( $pointer + $this->pull_chunk_size ) < $total_size ) {
					$next_chunk = 'next_' . sprintf( "%03d", $chunk + 1 );
				} else {
					$next_chunk = 'next_000';
				}

				// Stream the chunk.
				header("Content-Type: application/octet-stream");
				echo fread( $f, $this->pull_chunk_size) . $next_chunk;
				return false;
			}
		}

		$return = array( );
		if ($this->_api_version >= 11) {
			$this->_cleanup = false;
			foreach( static::$dataDefinition as $dataItem ) {
				$return[static::convertNameUnderscore( $dataItem )] = $this->getData( $dataItem);
			}

			if ( $this->_api_version >= 15 ) {
				// Cleanup Previously Chunked File
				\EE\Model\KioskSync::clean_file( $file );
				\EE\Model\KioskSync::clean_file( $image_file );
				// Get the images and build the tar file.
				$return['images'] = \EE\Model\KioskSync::getImages( $this->_client, $this->_cleanup,
																	$this->_api_version, $kiosk_dir );
			}

			$return['check_restart'] = $this->_check_restart();
			$this->_client_checkin();
		} else {
			foreach( static::$dataDefinition as $dataItem ) {
				$return[$dataItem] = $this->getData( $dataItem );
			}
		}

		$debug = isset( $_GET['debug'] );

		if ($debug) {
			echo utf8_json_encode( $return );
		} else {

			if ( $binary && $this->_api_version >= 15 ) {
				$return = gzdeflate(utf8_json_encode( $return ));
				$out_size = strlen( $return );
				if ( $out_size > $this->pull_chunk_size ) {
					// Chunk file if it is greater than pull_chunk_size.
					file_put_contents( $file, $return );
					// Build Chunk
					$return = substr( $return, 0, $this->pull_chunk_size ) . 'next_002';
				}
				header("Content-Type: application/octet-stream");
				echo $return;
			} else if ( $binary && $this->_api_version >= 12 ) {
				header("Content-Type: application/octet-stream");
				echo gzdeflate(utf8_json_encode( $return ));
			} else {
				echo base64_encode(gzdeflate(utf8_json_encode( $return )));
			}
		}
		return false;
	}

	/**
	* Update kiosk_sync for each sync method that was run successfully on the kiosk.
	* @param json POST Data Example: { "promotions": true, "menu_items": true }
	**/
	public function pull_finish() {
		$rv = array( );

		try {
			$pull_finish_data = file_get_contents( 'php://input' );
			if( empty( $pull_finish_data ) ) {
				throw new Exception( 'Received data is null' );
			}

			$data = json_decode($pull_finish_data);

			\EE\Model\KioskSync::pull_cleanup($this->_client, $data);

			$rv['success'] = true;
		} catch( Exception $e ) {
			$rv['errno'] = 0;
			$rv['error'] = $e->getMessage( );
		}

		echo utf8_json_encode( $rv );

		return false;
	}
	
		public function push() {
			if ($this->sync_type == "php") { // Run PHP Sync
				return $this->_new_push();
			} else { // Default: Run Python Sync
				return $this->_old_push();
			}
		}

		protected function _old_push() {
		$rv = array( );

		try {
			$pushData = file_get_contents( 'php://input' );
			if( empty( $pushData ) ) {
				throw new Exception( 'Received data is null' );
			}

			$outfile = tempnam( $this->dir_kiosksync_files, $this->_client->client_programid . '-'.date('Y-m-dTHi') );
			file_put_contents( $outfile, $pushData );
			chmod($outfile, 0644);

			$rv['success'] = true;
			$rv['outfile'] = $outfile;
		} catch( Exception $e ) {
			$rv['errno'] = 0;
			$rv['error'] = $e->getMessage( );
		}

		echo utf8_json_encode( $rv );

		return false;
	}

	protected function _new_push() {
		$clientProgramID = $this->_client->client_programid;
		
                /**
		$enabledKiosks = array(
			//59, // Corporate Office.
			2208, // Test Cantaloupe Kiosk at EE.
		);
		
		if (defined('IN_PRODUCTION') && IN_PRODUCTION == true && !in_array($clientProgramID, $enabledKiosks)) {
			return $this->_old_push();
		}
		**/
                
		$failure = function($message) {
			echo utf8_json_encode( array('success' => false, 'errno' => 0, 'error' => $message) );
			return false;
		};
		
		$pushData = file_get_contents( 'php://input' );

		if( empty( $pushData ) ) {
			return $failure('Received data is null');
		}
		
		$vivipos = new Manage_DB( 'vivipos db');
		
		try {
			$vivipos->exec('USE machineStatus');
		} catch (\PDOException $e) {
			return $failure('Missing database connection.');
		}
		
		try {
			$vivipos->exec('INSERT INTO `syncData` (`client_programid`, `data`) VALUES(?, ?)', array($clientProgramID, $pushData));
			$syncDataId = $vivipos->lastInsertId();
		} catch (\PDOException $e) {
			return $failure('Could not save data.');
		}
		
		$logger = new \EE\Sync\Kiosk\Logger($vivipos);
		$logger->business($this->_client->businessid);
		$logger->kiosk($clientProgramID);
		$logger->enableErrorHandler();

		$processor = new \EE\Sync\Kiosk\Processor($logger);
		$processor->database($this->_client->db);
		$processor->business($this->_client->businessid);
		$processor->kiosk($clientProgramID);

		if (!$processor->decode($pushData) || !$processor->validate()) {
			return $failure('Invalid data format.');
		}

		try {
			if ($processor->run()) {
				if ($syncDataId > 0) {
					try {
						$vivipos->exec('DELETE FROM `syncData` WHERE id = ?', array($syncDataId));
						echo utf8_json_encode( array('success' => true) );
					} catch(\PDOException $e) {
						// Pass through.
					}
				}
			} else {
				echo utf8_json_encode( array('success' => true, 'message' => 'Processing delayed. ID: '.$syncDataId) );
			}
		} catch(\Exception $e) {
			$failure($e->getMessage());
		}
		
		$outfile = tempnam( $this->dir_kiosksync_files, $clientProgramID . '-'.date('Y-m-d\THi-') );
		file_put_contents( $outfile, $pushData );
		chmod($outfile, 0644);

		return false;
	}

	public function pushSettings() {
		\EE\Model\Aeris\Setting::db($this->_tdb);

		$businessid = $this->_client->businessid;
		$post = file_get_contents( 'php://input' );
		$settings = json_decode( $post, true );

		if( $settings && !empty( $settings ) ) {
			\EE\Model\Aeris\Setting::reportSettings( $businessid, $settings );
		}

		return false;
	}

	public function pushVersions() {
		\EE\Model\Aeris\Version::db($this->_tdb);
		$post = file_get_contents( 'php://input' );
		if( $this->_api_version >= 8 ) {
			$data = json_decode( $post, true );
			
			$type = isset( $post['type'] ) ? $post['type'] : 'update';
			
			$components = $data['packages'];
			
			if( $type == 'update' ) {
				if( isset( $data['timestamp'] ) )
					$components[] = array( 'name' => '*Last Update*', 'version' => @$data['timestamp'] );
				if( isset( $data['success'] ) )
					$components[] = array( 'name' => '*Last Result*', 'version' => @$data['success'] ? 'success' : 'failure' );
				if( isset( $data['error'] ) )
					$components[] = array( 'name' => '*Last Error*', 'version' => @$data['error'] ?: '' );
			}
			
			\EE\Model\Aeris\Version::updateVersions( $this->_client->client_programid, $components );
			
			echo utf8_json_encode(array('result' => true));
		} else {
			$components = json_decode( $post, true );
			\EE\Model\Aeris\Version::updateVersionsAeris1( $this->_client->client_programid, $components );
		}

		return false;
	}

	/**
	 * Once the kiosk has been restarted, this should be called to notify us
	 * that the kiosk has been rebooted.
	 */
	public function isRestarted()
	{
		$this->_db->query(
			'UPDATE client_programs
			SET
				send_status = 0,
				receive_status = 5,
				receive_time = ?
			WHERE
				client_programid = ?
				AND send_status = 3',
			array(
				date('Y-m-d H:i:s'),
				$this->_args[0]
			)
		);
		
		if ($this->_api_version >= 8) {
			echo utf8_json_encode(array('result' => true));
		}
		return false;
	}

	public function __call( $name, array $arguments ) {
		// call _hasTypeName() methods
		if( strlen( $name ) > 4 && strpos( $name, '_has' ) === 0 ) {
			$type = substr( $name, 4 );
			if( !in_array( $type, static::$dataDefinition ) ) {
				throw new SiTech_Exception('Method '.get_class($this).'::'.$name.'() not found (action '. $this->_action . ')', null, 404);
			}
			$method = 'count' . $type;
			return (bool)call_user_func( array( '\EE\Model\KioskSync', $method ), $this->_client );
		}

		// call getTypeName() methods
		if( strlen( $name ) > 3 && strpos( $name, 'get' ) === 0 ) {
			$type = substr( $name, 3 );
			if( !in_array( $type, static::$dataDefinition ) ) {
				throw new SiTech_Exception('Method '.get_class($this).'::'.$name.'() not found (action '. $this->_action . ')', null, 404);
			}
			$method = 'get' . $type;
			return call_user_func( array( '\EE\Model\KioskSync', $method ), $this->_client, $this->_cleanup, $this->_api_version );
		}

		// call type_name() methods
		$type = static::convertNameCamel( $name );

		if( !in_array( $type, static::$dataDefinition ) ) {
			echo '<p>' . $type . '</p><p>' . nl2br( print_r( static::$dataDefinition, true ) ) . '</p>';
			throw new SiTech_Exception('Method '.get_class($this).'::'.$name.'() not found (action '. $this->_action . ')', null, 404);
		}

		$method = 'get' . $type;
		echo utf8_json_encode( $this->{$method}( ) );
		return false;
	}

	protected static function convertNameCamel( $name ) {
		$type = ucfirst( $name );
		return preg_replace_callback( '/_([a-z])/', function( $c ) { return strtoupper( $c[1] ); }, $type );
	}

	protected static function convertNameUnderscore( $name ) {
		$type = lcfirst( $name );
		return preg_replace_callback( '/([A-Z])/', function( $c ) { return '_' . strtolower( $c[1] ); }, $type );
	}

	protected function init()
	{
		$this->_db = Manage_DB_Proxy::singleton();
		Manage_Model_Abstract::db(Manage_DB_Proxy::singleton());
		$this->_client = ClientProgramsModel::get((int)$this->_args[0], true);
		$this->_tdb = $this->_client->db;
		\EE\Model\KioskSync::db($this->_client->db);
		\EE\Model\Base::db($this->_client->db);
		\EE\Model\AbstractModel\Treat::db($this->_client->db);

		// record ip from check in
		if (isset($this->_args[0])) {
			$this->_vdb = Manage_DB::singleton('vivipos db');
			
			$client_ip = $_SERVER['REMOTE_ADDR'];
			
			$this->_vdb->exec('USE machineStatus');
			$query = "INSERT INTO manageClients (client_programid) VALUES ({$this->_args[0]}) ON DUPLICATE KEY UPDATE send_time = NOW()";
			$this->_vdb->exec($query);
		}

		if (isset($_GET['debug']))
		{
			$this->_cleanup = false;
		}

		header('Content-Type: application/json; charset=utf-8');
		$errorHandler = function ($errno, $errstr = null, $errfile = null, $errline = null, $errcontext = null) {
			if (is_numeric($errno) && defined( "IN_PRODUCTION" ) && IN_PRODUCTION)
			{
				switch($errno) {
					case E_NOTICE:
					case E_USER_NOTICE:
					case E_WARNING:
					case E_USER_WARNING:
					case E_STRICT:
					case E_DEPRECATED:
					case E_USER_DEPRECATED:
						// Ignore these errors on production.
						return;
				}
			}
			if( error_reporting( ) ) {
				header('HTTP/1.1 500 Internal Server Error');
				if (is_object($errno)) {
					$error = array('errno' => $errno->getCode(), 'error' => $errno->getMessage());
				} else {
					$error = array('errno' => $errno, 'error' => $errstr);
				}

				if( defined( 'IN_PRODUCTION' ) && !IN_PRODUCTION ) {
					$error['file'] = $errfile;
					$error['line'] = $errline;
					$error['backtrace'] = debug_backtrace( );
				}

				echo utf8_json_encode($error);
				exit;
			}
		};

		set_error_handler($errorHandler);
		set_exception_handler($errorHandler);

		if ($this->_client === false) {
			trigger_error('Failed to retrieve client from database. Aborting.', E_USER_ERROR);
		}

		if( isset( $_REQUEST['api_version'] ) )
			$this->_api_version = $_REQUEST['api_version'];

		if( isset( $_REQUEST['v'] ) )
			$this->_api_version = $_REQUEST['v'];

				// Set kiosk sync files directory
				$_dir_kiosksync_files = \EE\Config::singleton()->get( 'main', 'dir_kiosksync_files' );
				if ($_dir_kiosksync_files) {
					$this->dir_kiosksync_files = $_dir_kiosksync_files;
				} else {
					$this->dir_kiosksync_files = SITECH_APP_PATH . '/files/kiosksync/';
				}

				// Set sync Type
				$this->sync_type = \EE\Config::singleton()->get( 'main', 'push_sync_type' );
	}

	protected function _hasPosLocation() {
		return (bool) $this->_api_version >= 7 ?: \EE\Model\KioskSync::needLocation( $this->_client );
	}
	
	public function testForecast() {
		\EE\Model\KioskSync::addType( $this->_client->businessid, \EE\Model\KioskSync::TYPE_FORECAST );
	}

}
