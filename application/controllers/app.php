<?php
if( !defined( 'DOC_ROOT' ) ) {
	define( 'DOC_ROOT', realpath( dirname( __FILE__ ) . '/../../htdocs/' ) );
}

class AppController extends Manage_Controller_Abstract {
	public function login(){
		include( 'inc/db.php' );
		include( 'inc/class.db-proxy.php' );
		
		$user_email = $_POST["email"];
		$user_pass = $_POST["password"];
		
		$query = "SELECT * FROM customer WHERE username = '$user_email' AND password = '$user_pass'";
		$result = DB_Proxy::query( $query );
		
		$customerid = @mysql_result($result,0,"customerid");
		$first_name =@mysql_result($result,0,"first_name");
		$scancode = @mysql_result($result,0,"scancode");
		$balance = @mysql_result($result,0,"balance");
		$businessid = @mysql_result($result,0,"businessid");
		
		$balance = number_format($balance,2);
		
		if($customerid > 0){
			echo "$customerid\t$first_name\t$scancode\t$balance\t$businessid";
		}
		else{
			echo "0";
		}
	}
	
	public function list_promos(){
		include( 'inc/db.php' );
		include( 'inc/class.db-proxy.php' );
		
		$today = date("Y-m-d");
		
		$businessid = $_POST["businessid"];
		
		$query = "SELECT * FROM promotions WHERE businessid = $businessid AND activeFlag = 1 ORDER BY start_date";
		$result = DB_Proxy::query( $query );
		$num = mysql_num_rows($result);
		
		echo "$num\r\n";
		
		while($r = mysql_fetch_array($result)){
			$name = $r["name"];
			
			$start_date = $r["start_date"];
			$end_date = $r["end_date"];
			$start_time = $r["start_time"];
			$end_time = $r["end_time"];
			
			$mon = $r["monday"];
			$tue = $r["tuesday"];
			$wed = $r["wednesday"];
			$thu = $r["thursday"];
			$fri = $r["friday"];
			$sat = $r["saturday"];
			$sun = $r["sunday"];
			
			$duration = substr($end_time,0,2) - substr($start_time,0,2);
			
			if($start_date > $today){
				$month = substr($start_date,5,2);
				$day = substr($start_date,8,2);
				$day = $day * 1;
				
				if($month == 1){$month = "Jan";}
				elseif($month == 2){$month = "Feb";}
				elseif($month == 3){$month = "Mar";}
				elseif($month == 4){$month = "Apr";}
				elseif($month == 5){$month = "May";}
				elseif($month == 6){$month = "Jun";}
				elseif($month == 7){$month = "Jul";}
				elseif($month == 8){$month = "Aug";}
				elseif($month == 9){$month = "Sep";}
				elseif($month == 10){$month = "Oct";}
				elseif($month == 11){$month = "Nov";}
				elseif($month == 12){$month = "Dec";}
				
				$dur_msg = "*Starting on $month $day*";
			}
		
			if($duration <= 4){
				$time_msg = "Available from $start_time to $end_time";
			}
			
			echo "$name\t$dur_msg\t$time_msg\r\n";
		}
	}
	
	protected function init( ) {
		$this->_tdb = new Manage_DB( 'treat db');
	}
}
?>