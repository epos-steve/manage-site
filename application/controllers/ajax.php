<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
SiTech_Loader::loadModel('ClientPrograms');
header('Content-Type: application/json; charset=utf-8');

/**
 * Description of ajax
 *
 * @author Eric Gach <eric@essential-elements.net>
 */
class AjaxController extends Manage_Controller_Abstract
{
	protected $_argMap = array(
		'errors' => array(
			'clientId',
			'page'
		)
	);
	
	public function errors()
	{
		SiTech_Loader::loadModel('ClientErrors');
		json_encode(ClientErrorsModel::get('client_id = '.$this->_args['clientId'].' ORDER BY error_time DESC LIMIT 10 OFFSET '.(($this->_args['page'] - 1) * 10)));
		return(false);
	}

	public function getChildren()
	{
		$parent = (int)$this->_args[0];

		$clients = ClientProgramsModel::getClients($parent);
		$arrClients = array();

		foreach ($clients as $client) {
			$arrClients[] = array(
				$client->client_programid,
				$client->name,
				(string)$client->posType()->name,
				(string)$client->showSchedule(),
				(string)$client->receiveStatus()->name,
				$client->receiveTime(),
				(string)$client->timeDiff('send'),
				$client->statusImg(),
				$client->showSendStatus(),
				(bool)$client->hasChildren(),
				(bool)$client->hasErrors(),
				(bool)$client->isKiosk(),
				$client->client_ip,
				$client->sync_version,
			);
		}

		echo json_encode($arrClients);
		return(false);
	}

	public function getErrors()
	{
		$clientId = $this->_args[0];

		SiTech_Loader::loadModel('ClientErrors');
		$errors = ClientErrorsModel::get('client_id = '.$clientId.' AND isRead = 0');
		foreach ($errors as &$error) {
			$error->markRead();
			$error = array(
				$error->error_time,
				$error->message(),
				$error->trace()
			);
		}

		echo json_encode($errors);
		return(false);
	}

	public function getSchedule()
	{
		$id = $this->_args[0];

		$schedule = new Manage_Schedule_Record($id);
		$time = explode(':', $schedule->time);
		$start = explode(':', $schedule->starttime);
		$end = explode(':', $schedule->endtime);

		echo json_encode(array(
			'id' => $schedule->id,
			'isDefault' => $schedule->default,
			'day' => $schedule->day,
			'time_hour' => (int)$time[0],
			'time_min' => $time[1],
			'start_hour' => (int)$start[0],
			'start_min' => $start[1],
			'end_hour' => (int)$end[0],
			'end_min' => $end[1],
			'days' => $schedule->num_days,
			'days_back' => $schedule->days_back
		));
		return(false);
	}

	public function updateTimers()
	{
		try {
			$statuses = array();
			$stmnt = $this->_db->prepare(
			'SELECT
				cp.client_programid,
				cp.send_status,
				cp.send_time,
				TIMEDIFF(now(), cp.send_time) AS status,
				rs.name AS receive_name,
				cp.receive_time,
				COUNT(ce.id) AS errors
			FROM
				client_programs cp LEFT OUTER JOIN client_errors ce
					ON cp.client_programid = ce.client_id AND ce.isRead = 0,
				receive_status rs
			WHERE
				rs.receive_status = cp.receive_status
			GROUP BY cp.client_programid
			ORDER BY cp.client_programid');
			$stmnt->execute();
			$status = $stmnt->fetchAll(PDO::FETCH_ASSOC);

			foreach ($status as $row) {
				$client = ClientProgramsModel::get((int)$row['client_programid'], true);
				$showstatus = $client->statusImg();
				if ($row['receive_time'] == '0000-00-00 00:00:00') $row['receive_time'] = '';

				if ($row['send_status'] == 1){$showcurrent = 'gear.gif';}
				elseif ($row['send_status'] == 2){$showcurrent = 'clock.gif';}
				elseif ($row['send_status'] == 3) $showcurrent = 'restart-icon.png';
				else{$showcurrent = 'blank.gif';}

				$statuses[] = '['.$row['client_programid'].',"'.$row['status'].'", "'.$row['receive_name'].'", "'.$row['receive_time'].'", "'.$showstatus.'", "'.$showcurrent.'", "'.(($row['errors'] > 0)? 1 : 0).'"]';
				$stmnt->closeCursor();
			}

			echo '[';
			echo implode(',', $statuses);
			echo ']';
		} catch(Exception $e) {
			var_dump($e);
		}
		return(false);
	}
}
