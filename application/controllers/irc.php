<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of irc
 *
 * @author Eric Gach <eric@essential-elements.net>
 */
class IrcController extends Manage_Controller_ViViPOS
{
	protected $_argMap = array(
		'checkAvailableUpdates' => array('workgroup'),
		'downloadFile' => array('file'),
		'getAvailablePackagesList' => array('lastDownload'),
		'updatePackageStatus' => array('client', 'upgrade', 'status')
	);

	protected $_components = array('SyncHandler');

	public function checkAvailableUpdates()
	{
		$result = array('status' => 'error', 'code' => 400);
		$mid = $this->SyncHandler->getRequestMachineId();
		$packages = ViviposUpgradesModel::get('1 ORDER BY Created ASC');
		$available = array();
		$now = time();

		foreach ($packages as $package) {
			if ($package->Created <= $now && $package->hasClient($mid)) {
				if (empty($package->Workgroup) || empty($this->_args['workgroup']) || strcasecmp(chop($this->_args['workgroup']), chop($package->Workgroup)) == 0) {
					$available[] = array(
						'created' => (int)strtotime($package->Created),
						'created_machine_id' => 'EESync',
						'modules' => 'addons',
						'module_labels' => 'Addons',
						'activation' => strtotime($package->Created),
						'workgroup' => (string)$package->Workgroup,
						'description' => 'Addons package',
						'file' => basename($package->FileName),
						'checksum' => $package->CheckSum,
						'filesize' => (int)$package->FileSize,
						'reboot' => 1
					);
				}
			}
		}
		$result = array('status' => 'ok', 'code' => 200, 'response_data' => (empty($available))? false : $available);

		echo $this->SyncHandler->prepareResponse($result, 'json');
		return(false);
	}

	/**
	 * Send the file to the actual kiosk.
	 */
	public function downloadFile()
	{
		$file = ViviposUpgradesModel::get('FileName = '.$this->_db->quote($this->_args['file']), true);
        if (file_exists((string)$file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($file->FileName));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: '.$file->FileSize);
            ob_clean();
            flush();
            readfile((string)$file);
        }else {
            header("HTTP/1.0 404 Not Found");
        }
	}

	/**
	 * This should be how vivipos pulls all the updates. The json encoded string
	 * tells it which workgroup the file is for.
	 *
	 * @return bool
	 */
	public function getAvailablePackagesList()
	{
        $result = array('status' => 'error', 'code' => 400 );
		$packages = ViviposUpgradesModel::get('Created > FROM_UNIXTIME('.$this->_args['lastDownload'].') ORDER BY Created ASC');
		//$mid = $this->SyncHandler->getRequestMachineId();
		
		if ($packages !== false) {
			$result = array('status' => 'ok', 'code' => 200, 'response_data' => array());
			foreach ($packages as $package) {
				if (!$package->hasClient()) continue;
				$result['response_data'][] = array(
					'created' => (int)strtotime($package->Created),
					'created_machine_id' => 'EESync',
					'modules' => 'addons',
					'module_labels' => 'Addons',
					'activation' => strtotime($package->Created),
					'workgroup' => (string)$package->Workgroup,
					'description' => 'Addons package',
					'file' => basename($package->FileName),
					'checksum' => $package->CheckSum,
					'filesize' => (int)$package->FileSize,
					'reboot' => 1
				);
			}
		}

		echo $this->SyncHandler->prepareResponse($result, 'json');
		return(false);
	}

	/**
	 * Update the status for the package/client in question.
	 *
	 * @return bool
	 */
	public function updatePackageStatus()
	{
		$result = array('status' => 'error', 'code' => 400 );

		$package = ViviposUpgradesModel::getByFilename($this->_args['upgrade']);
		if (($client = ViviposUpgradeClientModel::get('ClientProgram = '.$this->_db->quote($this->_args['client']).' AND ViviposUpdate = '.$this->_db->quote($package->Id), true)) === false) {
			$client = new ViviposUpgradeClientModel();
			$client->ClientProgram = $this->_args['client'];
			$client->ViviposUpdate = $package->Id;
			$client->Status = 0;
		}

		$client->Received = date('Y-m-d H:i:s');
		switch ($this->_args['status']) {
			case 'downloaded':
				$client->Status = (int)$client->Status | 1;
				break;

			case 'updated':
				$client->Status = (int)$client->Status | 2;
				break;

			default:
				// Bad update status, don't do anything here.
				break(2);
		}

		if ($client->save()) {
			$result = array('status' => 'ok', 'code' => 200, 'response_data' => true);
		}

		echo $this->SyncHandler->prepareResponse($result, 'json');
		return(false);
	}
}
