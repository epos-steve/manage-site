<?php

ini_set("soap.wsdl_cache_enabled", "0");

/**
 * Description of HandheldWebserviceController
 *
 * @author Alexander Petrides <alexander@essential-elements.net>
 */
class WebserviceController extends Manage_Controller_Abstract
{
	protected $_loadComponents = array(
		'HandheldSoapClient',
		'LoginRoute',
		'Business',
		'Machine',
		'Item',
		'ItemCount',
		'Report',
		'SoftwareVersion',
		'SoftwareException',
		);

	protected $_wsdlUri;

	public function init()
	{
		foreach($this->_loadComponents as $component)
		{
			require_once('components' . DIRECTORY_SEPARATOR . 'handheld' . DIRECTORY_SEPARATOR .$component . ".php");
		}

		$this->_wsdlUri = $this->_uri->getUri(false, false);
	}

	public function handheld()
	{
		if(SiTech_Filter::isPost())
		{
			$options = array('soap_version' => SOAP_1_2, 'cache_wsdl' => WSDL_CACHE_NONE);
			$server = new SoapServer($this->_wsdlUri, $options);
			$proxyService = new SiTech_Soap_Proxy('HandheldSoapClient');
			$server->setObject($proxyService);

			try
			{
				$server->handle();
			}
			catch (Exception $e)
			{
				$server->fault('200', $e->getCode() == -999 ? $e->getMessage() : $e->getMessage());
					//'The server has encoured a fatal error and cannot complete the request.');
			}
		}
		elseif(SiTech_Filter::isGet())
		{
			$disco = new Zend_Soap_AutoDiscover();
			$disco->setComplexTypeStrategy('Zend_Soap_Wsdl_Strategy_ArrayOfTypeSequence');
			$disco->setBindingStyle(array('style' => 'document'));
			$disco->setOperationBodyStyle(array('use' => 'literal'));
			$disco->setClass('HandheldSoapClient');
			$disco->handle();
		}

		return false;
	}

}
