<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

SiTech_Loader::loadModel('ClientPrograms');
SiTech_Loader::loadModel('ClientErrors');

/**
 * Description of dashboard
 *
 * @author eric
 */
class DashboardController extends Manage_Controller_Abstract
{
	protected $_argMap = array(
		'manage' => array('pid'),
		'delete' => array('pid'),
		'data' => array('pid')
	);

	protected $_layout = 'general.tpl';

	public function add()
	{
		if (SiTech_Filter::isPost()) {
			$filter = new SiTech_Filter(SiTech_Filter::INPUT_POST);
			$client = new ClientProgramsModel();
			$client->name = $filter->input('name');
			if ($filter->input('parent') !== '0')
				$client->parent = $filter->input('parent');
			if (!$client->save()) {
				$this->_view->assign('errors', $client->errors);
			} else {
				header('Location: '.SITECH_BASEURI.'dashboard/manage/'.$client->client_programid);
				exit;
			}
		}

		$this->_view->assign('machines', ClientProgramsModel::getAll());
		if ($this->_isXHR) {
			$this->_layout = null;
		}
	}

	public function data()
	{
		if (!file_exists(SITECH_APP_PATH.'/files/'.$this->_args['pid'].'.old.txt')) throw new SiTech_Exception('Failed to load client file', 404);
		header('Cache-Control: no-cache, must-revalidate'); // HTTP/1.1
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Content-Type: text/plain');
		readfile(SITECH_APP_PATH.'/files/'.$this->_args['pid'].'.old.txt');
		return(false);
	}

	public function delete()
	{
		if (SiTech_Filter::isPost()) {
			if (($client = ClientProgramsModel::get((int)$this->_args['pid'], true)) !== false) {
				$client->delete();
				header('Location: '.SITECH_BASEURI);
				exit;
			}
		}
	}

	public function index()
	{
		$this->_user->requireLogin();
		$this->_view->assign('clients', ClientProgramsModel::getClients());
	}

	public function login()
	{
		$this->_layout = 'aerispos.tpl';
		if ($this->_user->isLoggedIn()) {
			header('Location: '.SITECH_BASEURI);
			exit;
		}
	}

	public function manage()
	{
		$this->_user->requireLogin();
		if ($this->_user->isAdmin() === false) header('Location: dashboard');

		SiTech_Loader::loadModel('Transtypes');
		SiTech_Loader::loadModel('TransMapping');
		$args = array();
		if (!empty($_POST)) {
			if (isset($_POST['start_date'])) {
				$args['start_date'] = $_POST['start_date'];
			}

			if (isset($_POST['end_date'])) {
				$args['end_date'] = $_POST['end_date'];
			}
		}

		$client = ClientProgramsModel::get((int)$this->_args['pid'], true);
		$posTypes = new Manage_PosType();
		$this->_view->assign('client', $client);

		try {
			$this->_view->assign('clients', ClientProgramsModel::getAll());
			$this->_view->assign('posTypes', $posTypes->getAll());
			if ($client->db instanceof Manage_DB || $client->db instanceof Manage_DB_Proxy) {
				$this->_view->assign('schedules', $client->getSchedules());
				$this->_view->assign('mappingTypes', MappingTypeModel::get());
				$this->_view->assign('mapping', MappingModel::getByClient($client));
				$this->_view->assign('transtypes', TranstypesModel::get());
				$this->_view->assign('transmapping', TransMappingModel::getByClient($client));
				$this->_view->assign('credits', $client->getCredits());
				$this->_view->assign('debits', $client->getDebits());
				$this->_view->assign('errors', $client->getErrors());
				$this->_view->assign('jobTypes', MappingModel::getByMappingType(12, $client));
				$this->_view->assign('btJobTypes', $client->db->query('SELECT * FROM jobtype WHERE companyid = ?', array($client->getCompanyId()))->fetchAll(PDO::FETCH_OBJ));
				$this->_view->assign('pages', ceil((ClientErrorsModel::getCount('client_id = '.$client->client_programid)) / 10));
				Treat_Model_Aeris_OperatingMode::db( new Manage_DB_Proxy( 'treat db' ) );
				$this->_view->assign( 'posModes', Treat_Model_Aeris_OperatingMode::getModes( ) );
			}
		} catch (PDOException $e) {
		}
	}

	protected function  init() {
		$this->_view->assign('user', $this->_user);
	}
}
?>
