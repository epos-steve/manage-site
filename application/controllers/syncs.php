<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of syncs
 *
 * @author Eric Gach <eric@essential-elements.net>
 */
class SyncsController extends Manage_Controller_ViViPOS
{
	/**
	 * Components to load with the controller
	 *
	 * @var array
	 */
	protected $_components = array('SyncHandler');

	/**
	 * Unused?
	 */
	protected $_pdo;

	public function pull()
	{
		//file_put_contents('/home/eric/vivipos-data/vivipos.sync-pull.txt', var_export($_REQUEST, true));
		$data = $this->SyncHandler->getServerData($_REQUEST, $this->_uri);
		//file_put_contents('/home/eric/vivipos-data/vivipos.sync-pull.txt', var_export($data, true));
		$result = array('success' => true, 'data' => $data);
		file_put_contents(realpath(SITECH_APP_PATH.'/files/').'/'.$_REQUEST['machine_id'].'.sync-pull.txt', var_export($this->SyncHandler->prepareResponse($result), true));
		echo $this->SyncHandler->prepareResponse($result);
		return(false);
	}

	public function push()
	{
		//uncomment the following line to disable the syncs. Machines will queue their data.
		//die();

		if (isset($this->_args[0])) {
			/*
			SiTech_Loader::loadModel('clientprograms');
			$client = ClientProgramsModel::get((int)$this->_args[0], true);
			$client->client_ip = $_SERVER['REMOTE_ADDR'];
			//$client->setSendStatus();
			$client->receive_status = 2;
			$client->send_time = date('Y-m-d H:i:s');
			$client->save();
			 */
			
			$client_ip = $_SERVER['REMOTE_ADDR'];
			
			$this->_db->exec('USE machineStatus');
			$query = "INSERT INTO manageClients (client_programid,client_ip) VALUES ({$this->_args[0]},'$client_ip') ON DUPLICATE KEY UPDATE client_ip = '$client_ip', send_time = NOW()";
			$this->_db->exec($query);
		}

		//kiosks sync only every 4th time
		/*
		if (!($checkin = ClientCheckinModel::get('client_programid = '.$client->client_programid, true))) {
			$checkin = new ClientCheckinModel();
			$checkin->client_programid = $client->client_programid;
			$checkin->total = 0;
			$checkin->save();
		}

		if($checkin->total != 3){
			$checkin->total += 1;
			$checkin->save();
			die();
		} else{
			$checkin->total = 0;
			$checkin->save();
		}
		 * 
		 */

		$return = array();
		$success = true;
		file_put_contents(realpath(SITECH_APP_PATH.'/files/').'/'.$client->client_programid.'.sync-push.txt', var_export($this->SyncHandler->parseRequest($_REQUEST['request_data']), true));
		$fp = fopen(realpath(SITECH_APP_PATH.'/files/').'/'.$client->client_programid.'.queries-push.txt', 'w');
		foreach ($this->SyncHandler->parseRequest($_REQUEST['request_data']) as $section => $data) {
			if (!$fp) {
				$return[$data['datasource']] = array(
					'datasource' => $data['datasource'],
					'count' => 0,
					'last_synced' => 0
				);
				continue;
			}

			try {
				if ($data['datasource'] == 'default') {
					$this->_db->exec('USE vivipos');
				} else {
					$this->_db->exec('USE vivipos_'.$data['datasource']);
				}

				foreach (explode("\n", $data['sql']) as $query) {
					if (empty($query)) continue;
					if (strstr($query, 'REPLACE INTO syncs') !== false || strstr($query, 'REPLACE INTO "order_objects"') !== false || strstr($query, 'REPLACE INTO "kiosk_logs"') !== false) continue;

					$query = str_replace(array('"', 'INSERT OR REPLACE'), array('`', 'REPLACE'), $query);
					if (preg_match('#REPLACE INTO [^\s]+ \(([^\)]*)\)#', $query, $matches)) {
						$fields = explode(',', $matches[1]);
						$fields = array_map('trim', $fields);
						$fields = '`'.implode('`,`', $fields).'`';
						$query = str_replace($matches[1], $fields, $query);
					}
					$query = preg_replace('#CC-ID-([0-9]+)#', 'CC-ID-\\1-'.$client->client_programid, $query);
					fwrite($fp, $query."\n");
					$this->_db->exec($query);
				}

				$return[$data['datasource']] = array(
					'datasource' => $data['datasource'],
					'count' => $data['count'],
					'last_synced' => $data['last_synced']
				);
			} catch (PDOException $e) {
				$success = false;
				$return[$data['datasource']] = array(
					'datasource' => $data['datasource'],
					'count' => 0,
					'last_synced' => 0
				);
				fwrite($fp, "\n\nERROR:\n".$e->getMessage());
			}
		}

		echo $this->SyncHandler->prepareResponse(array('success' => $success, 'data' => $return));
		fclose($fp);
		return(false);
	}
}
