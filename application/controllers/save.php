<?php
/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of save
 *
 * @author Eric Gach <eric@essential-elements.net>
 */
class SaveController extends Manage_Controller_Abstract
{
	protected $_models = array('ClientPrograms', 'TransMapping');

	public function transmapping()
	{
		if (empty($_POST)) {
			header('Location: '.SITECH_BASEURI.'dashboard');
			exit;
		}

		$client = ClientProgramsModel::get((int)$_POST['client_programid'], true);
		foreach ($_POST['credit'] as $k => $creditid) {
			if (!($transmap = TransMappingModel::get('client_programid = '.$client->client_programid.' AND type = '.$k, true))) {
				$transmap = new TransMappingModel();
				$transmap->type = $k;
				$transmap->client_programid = $client->client_programid;
			}

			$transmap->creditid = $creditid;
			$transmap->debitid = $_POST['debit'][$k];
			$transmap->save();
		}

		header('Location: '.SITECH_BASEURI.'dashboard/manage/'.$client->client_programid.'#client_link');
		exit;
	}
}
