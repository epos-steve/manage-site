/**
 * Created by ryan on 1/29/14.
 */

jQuery( function( ) {
	var prevrel = jQuery( '.previous-releases' ).hide();
	var prevbtn = jQuery( '<span>Previous Releases...</span>' ).button( ).click( function( ) {
		prevrel.toggle(250);
	} );
	prevbtn.insertBefore(prevrel);

	jQuery( 'article' ).addClass( 'ui-corner-all ui-state-default ui-widget release' );
	jQuery( 'article>ul' ).addClass( 'ui-corner-all ui-widget-content notes' );
} );
