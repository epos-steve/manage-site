<?php
define('EEF_APP_PATH', realpath(dirname(__FILE__)).'/../application');
set_include_path(realpath(EEF_APP_PATH.'/../lib').PATH_SEPARATOR.get_include_path());

require_once('../lib/EEF/Loader.php');
EEF_Loader::registerAutoload();
EEF_Loader::loadBootstrap();

$user = null;

if (!empty($_COOKIE['id'])) {
	$user = new Manage_Login_Record($_COOKIE['id'], $_COOKIE['hash']);
}

if (empty($user) || $user->isLoggedIn() === false) {
	echo 'Failed.';
	exit;
}

require('./db.php');
include('./function.php');

$style = "text-decoration:none";

$thisdb = mysql_connect($dbhost,$username,$password);
@mysql_select_db($database, $thisdb) or die( mysql_error($thisdb));
$pid = $_GET['pid'];
$edit = $_GET['edit'];
?>
<html>
<head>

<script LANGUAGE="JavaScript">
<!-- Web Site:  http://dynamicdrive.com -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- Begin
function disableForm(theform) {
if (document.all || document.getElementById) {
for (i = 0; i < theform.length; i++) {
var tempobj = theform.elements[i];
if (tempobj.type.toLowerCase() == "submit" || tempobj.type.toLowerCase() == "reset")
tempobj.disabled = true;
}

}
else {
alert("The form has been submitted.  But, since you're not using IE 4+ or NS 6, the submit button was not disabled on form submission.");
return false;
   }
}
//  End -->
</script>

</head>
<?php

	echo "<a href=dashboard style=$style><font color=blue>Back</font></a><p>";

	//////program details
	$query = "SELECT * FROM client_programs WHERE client_programid = '$pid'";
	$result = mysql_query($query,$thisdb);
	$num=mysql_numrows($result);
	
	$client_name=mysql_result($result,0,"name");
	$businessid=mysql_result($result,0,"businessid");
	$pos_type=mysql_result($result,0,"pos_type");
	$day_start=mysql_result($result,0,"day_start");
	$parent=mysql_result($result,0,"parent");
	$receive_status=mysql_result($result,0,"receive_status");
	$receive_time=mysql_result($result,0,"receive_time");
	$db_ip=mysql_result($result,0,"db_ip");
	$db_name=mysql_result($result,0,"db_name");
	$db_user=mysql_result($result,0,"db_user");
	$db_pass=mysql_result($result,0,"db_pass");
	
	$query2 = "SELECT * FROM receive_status WHERE receive_status = '$receive_status'";
	$result2 = mysql_query($query2,$thisdb);
		
	$receive_statusname=mysql_result($result2,0,"name");
	
	if($receive_time == "0000-00-00 00:00:00"){$receive_time="";}
	
	$query2 = "SELECT TIMEDIFF(now(),send_time) AS status FROM client_programs WHERE client_programid = '$pid'";
	$result2 = mysql_query($query2,$thisdb);
		
	$status=mysql_result($result2,0,"status");
		
	if(substr($status,0,2)>=1 || $send_time == "0000-00-00 00:00:00"){$showstatus="<img src=reddot.gif border=0 height=16 width=16>";}
	elseif(substr($status,3,2)>=15){$showstatus="<img src=yellowdot.gif border=0 height=16 width=16>";}
	else{$showstatus="<img src=greendot.gif border=0 height=16 width=16>";}
	
	echo "<table width=100% cellspacing=0 cellpadding=2 style=\"border:2px solid black;\">";
	echo "<tr bgcolor=black><td>&nbsp;<b><font color=white>($pid) $client_name</font></b> $showstatus</td><td align=right><b><font color=white>Last Action: $receive_statusname $receive_time&nbsp;</font></b></td></tr>";
	echo "<tr valign=top><td width=70%>";
	
	////existing schedules
	echo "<table cellspacing=0 style=\"border:2px solid #E8E7E7;\" width=100%><tr bgcolor=#E8E7E7><td colspan=8>&nbsp;<b>Existing Schedule(s)<b/></td></tr>";
	echo "<tr><td width=10%>&nbsp;<b>ID</td><td width=10%>&nbsp;<b>Day</td><td width=15%>&nbsp;<b>Time</td><td width=15%>&nbsp;<b>Start Time</td><td width=15%>&nbsp;<b>End Time</td><td width=15%>&nbsp;<b># of Days</td><td width=15%>&nbsp;<b># of Days Back</td><td width=5%>&nbsp;</td></tr>";
	$query2 = "SELECT * FROM client_schedule WHERE client_programid = '$pid' ORDER BY id";
	$result2 = mysql_query($query2,$thisdb);
	$num2=mysql_numrows($result2);
	
	if($num2==0){echo "<tr><td colspan=8>&nbsp;<img src=error.gif height=16 width=16> <i>No Active Schedules</i></td></tr>";}
	else{
		while($r=mysql_fetch_array($result2)){
			$id=$r["id"];
			$default=$r["default"];
			$day=$r["day"];
			$time=$r["time"];
			$starttime=$r["starttime"];
			$endtime=$r["endtime"];
			$num_days=$r["num_days"];
			$days_back=$r["days_back"];
			
			if(isset($edit) && $edit == $id){
				$color="yellow";
				$mo_color="yellow";
				$edit_day=$day;
				$pieces=explode(":",$time);
				$edit_time_hour=$pieces[0];
				$edit_time_min=$pieces[1];
				$pieces=explode(":",$starttime);
				$edit_starttime_hour=$pieces[0];
				$edit_starttime_min=$pieces[1];
				$pieces=explode(":",$endtime);
				$edit_endtime_hour=$pieces[0];
				$edit_endtime_min=$pieces[1];
				$edit_num_days=$num_days;
				$edit_days_back=$days_back;
				if($default==1){$checkdefault="CHECKED";}
				else{$checkdefault="";}
			}
			else{
				$color="white";
				$mo_color="#CCCCCC";
				$edit_day=0;
				$pieces=0;
				$edit_time_hour=0;
				$edit_time_min=0;
				$pieces=0;
				$edit_starttime_hour=0;
				$edit_starttime_min=0;
				$pieces=0;
				$edit_endtime_hour=0;
				$edit_endtime_min=0;
				$edit_num_days=0;
				$edit_days_back=0;
				$checkdefault="";
			}
			
			if($day==0){$day="Every Day";}
			elseif($day==1){$day="Mon";}
			elseif($day==2){$day="Tue";}
			elseif($day==3){$day="Wed";}
			elseif($day==4){$day="Thu";}
			elseif($day==5){$day="Fri";}
			elseif($day==6){$day="Sat";}
			elseif($day==7){$day="Sun";}
			
			if($default==1){$showdefault="<b>*</b>";}
			else{$showdefault="";}
			
			$showedit="<a href=managedetail.php?pid=$pid&edit=$id><img src=edit.gif height=16 width=16 border=0></a>";
			$showdelete="<a href=schedule.php?pid=$pid&del=1&id=$id onclick=\"return confirm('Are you sure you want to delete this schedule?');\"><img src=delete.gif height=16 width=16 border=0></a>";
			
			echo "<tr onMouseOver=this.bgColor='$mo_color' onMouseOut=this.bgColor='$color' bgcolor=$color><td>&nbsp;$showdefault$id</td><td>&nbsp;$day</td><td>&nbsp;$time</td><td>&nbsp;$starttime</td><td>&nbsp;$endtime</td><td>&nbsp;$num_days</td><td>&nbsp;$days_back</td><td align=right>$showedit $showdelete</td></tr>";
		}
	}
	echo "</table>";
	echo "</td><td width=30%>";
	//////add/edit schedule
	echo "<form action=schedule.php method=post style=\"margin:0;padding:0;display:inline;\" onSubmit='return disableForm(this);'><input type=hidden name=pid value=$pid>";
	if(isset($edit) && $edit>0){
		echo "<input type=hidden name=id value=$edit>";
		$showtitle="Edit Schedule";
	}
	else{
		$showtitle="Add Schedule";
	}
	echo "<table cellspacing=0 style=\"border:2px solid #E8E7E7;\" width=100%><tr bgcolor=#E8E7E7><td colspan=2>&nbsp;<b>$showtitle<b/></td></tr>";

	$sel1 = $sel2 = $sel3 = $sel4 = $sel5 = $sel6 = $sel7 = '';
	if(isset($edit_day) && $edit_day==1){$sel1="SELECTED";}
	elseif(isset($edit_day) && $edit_day==2){$sel2="SELECTED";}
	elseif(isset($edit_day) && $edit_day==3){$sel3="SELECTED";}
	elseif(isset($edit_day) && $edit_day==4){$sel4="SELECTED";}
	elseif(isset($edit_day) && $edit_day==5){$sel5="SELECTED";}
	elseif(isset($edit_day) && $edit_day==6){$sel6="SELECTED";}
	elseif(isset($edit_day) && $edit_day==7){$sel7="SELECTED";}
	
	echo "<tr><td align=right>Run:</td><td><select name=day><option value=0>Every Day</option><option value=1 $sel1>Mon</option><option value=2 $sel2>Tue</option><option value=3 $sel3>Wed</option><option value=4 $sel4>Thu</option><option value=5 $sel5>Fri</option><option value=6 $sel6>Sat</option><option value=7 $sel7>Sun</option></select> at <select name=time_hour>";
		for ($counter=0;$counter<=23;$counter++){
			if($edit_time_hour==$counter){$sel="SELECTED";}
			else{$sel="";}
			echo "<option value=$counter $sel>$counter</option>";
		}
		echo "</select> : <select name=time_min>";
		for ($counter=0;$counter<=55;$counter+=5){
			if(strlen($counter)<2){$counter="0$counter";}
			
			if($edit_time_min==$counter){$sel="SELECTED";}
			else{$sel="";}
			
			echo "<option value=$counter $sel>$counter</option>";
		}
	echo "</select></td></tr>";
	echo "<tr><td align=right><input type=checkbox name=default value=1 $checkdefault></td><td>Default Schedule</td></tr>";
	echo "<tr><td align=right>Start Time:</td><td><select name=start_hour>";
		for ($counter=0;$counter<=23;$counter++){
			if($edit_starttime_hour==$counter){$sel="SELECTED";}
			else{$sel="";}
			echo "<option value=$counter $sel>$counter</option>";
		}
		echo "</select> : <select name=start_min>";
		for ($counter=0;$counter<=55;$counter+=5){
			if(strlen($counter)<2){$counter="0$counter";}
			
			if($edit_starttime_min==$counter){$sel="SELECTED";}
			else{$sel="";}
			
			echo "<option value=$counter $sel>$counter</option>";
		}
	echo "</select></td></tr>";
	
	echo "<tr><td align=right>End Time:</td><td><select name=end_hour>";
		for ($counter=0;$counter<=23;$counter++){
			if($edit_endtime_hour==$counter){$sel="SELECTED";}
			else{$sel="";}
			echo "<option value=$counter $sel>$counter</option>";
		}
		echo "</select> : <select name=end_min>";
		for ($counter=0;$counter<=55;$counter+=5){
			if(strlen($counter)<2){$counter="0$counter";}
			
			if($edit_endtime_min==$counter){$sel="SELECTED";}
			else{$sel="";}
			
			echo "<option value=$counter $sel>$counter</option>";
		}
	echo "</select></td></tr>";
	echo "<tr><td align=right># of Days:</td><td><input type=text size=4 name=num_days value=$edit_num_days></td></tr>";
	echo "<tr><td align=right># of Days Back:</td><td><input type=text size=4 name=days_back value=$edit_days_back> (from today)</td></tr>";
	
	if(isset($edit) && $edit>0){$showsave="Save";}
	else{$showsave="Add";}
	
	echo "<tr><td colspan=2 align=right><input type=submit value='$showsave'></td></tr>";
	echo "</table></form>";
	/////end details
	
	echo "</td></tr>";
	
	//////////////////
	//////mapping/////
	//////////////////
	echo "<tr valign=top><td>";
	
	if($db_ip != "" && $db_name != "" && $db_user != "" && $db_pass != ""){
	
		$newdb = mysql_connect($db_ip,$db_user,$db_pass);
		@mysql_select_db($db_name,$newdb);
		
		$query = "SELECT businessname FROM business WHERE businessid = '$businessid'";
		$result = mysql_query($query,$newdb);
		
		$businessname = mysql_result($result,0,"businessname");
	
		$newfile=$pid;
		$newfile.=".old.txt";
		
		echo "<form style=\"margin:0;padding:0;display:inline;\" action=mapping.php method=post onSubmit='return disableForm(this);'><input type=hidden name=client_programid value='$pid'>";
		echo "<table cellspacing=0 style=\"border:1px solid #E8E7E7;\" width=100%><tr bgcolor=#E8E7E7><td colspan=2>&nbsp;<b>Mapping - $businessname<b/> <a target='_blank' href=receive/data/$newfile><img src='folder.gif' border='0' height='16' width='16' alt='View Data' title='View Data'></a> </td></tr>";
		
		/////credits
		$query20 = "SELECT creditid,creditname FROM credits WHERE businessid = '$businessid' ORDER BY credittype,creditname";
		$result20 = mysql_query($query20,$newdb);
		$num20 = mysql_numrows($result20);
		
		/////debits
		$query21 = "SELECT debitid,debitname FROM debits WHERE businessid = '$businessid' ORDER BY debittype,debitname";
		$result21 = mysql_query($query21,$newdb);
		$num21 = mysql_numrows($result21);
		
		$query2 = "SELECT mapping.* FROM mapping,mapping_type WHERE mapping.client_programid = '$pid' AND mapping.type = mapping_type.id ORDER BY mapping_type.order,mapping.name";
		$result2 = mysql_query($query2,$thisdb);
		$num2 = mysql_numrows($result2);
		
		if($num2==0){echo "<tr><td colspan=2 style=\"border:1px solid #E8E7E7;\">&nbsp;<i>Nothing to Display</i></td></tr>";}
		else{
			$lasttype=0;
			$lastposid=0;
			$lastmapping_table=0;
			while($r=mysql_fetch_array($result2)){
				$id = $r["id"];
				$type = $r["type"];
				$posid = $r["posid"];
				$localtype = $r["localtype"];
				$localid = $r["localid"];
				$name = $r["name"];
				
				if($lasttype!=$type){
					$query3 = "SELECT name,mapping_table FROM mapping_type WHERE id = '$type'";
					$result3 = mysql_query($query3,$thisdb);
					
					$type_name=mysql_result($result3,0,"name");
					$mapping_table=mysql_result($result3,0,"mapping_table");
					
					/*
					if($lastmapping_table!=$mapping_table){
						$query3 = "SELECT name FROM mapping_table WHERE id = '$mapping_table'";
						$result3 = mysql_query($query3,$thisdb);
					
						$mapping_name=mysql_result($result3,0,"name");
						
						echo "<tr valign=top bgcolor=orange><td style=\"border:1px solid #E8E7E7;\">&nbsp;<i><b>POS $mapping_name</b></i></td><td style=\"border:1px solid #E8E7E7;\">&nbsp;<i><b>Web $mapping_name</b></i></td></tr>";
					}*/
					
					echo "<tr valign=top bgcolor=#FFFF99><td style=\"border:1px solid #E8E7E7;\" colspan=2>&nbsp;<i><b>$type_name</b></i></td></tr>";
				}
				
				////display doubles
				if($lasttype==$type&&$lastposid==$posid){$showcolor="blue";}
				else{$showcolor="black";}
				
				echo "<tr valign=top onMouseOver=this.bgColor='#CCCCCC' onMouseOut=this.bgColor='white' bgcolor=white><td style=\"border:1px solid #E8E7E7;\">&nbsp;<font color=$showcolor>$name ($posid)</font></td><td style=\"border:1px solid #E8E7E7;\">&nbsp;<select name='map$id' tabindex=1><option value=0>(None)</option>";
				
				///credits
					$counter=0;
					while($counter<$num20){
						$creditid = mysql_result($result20,$counter,"creditid");
						$creditname = mysql_result($result20,$counter,"creditname");
					
						if($localid==$creditid && $localtype == 1){$sel="SELECTED";}
						else{$sel="";}
					
						echo "<option value=1-$creditid $sel>$creditname ($creditid)</option>";
						$counter++;
					}

				///debits
					$counter=0;
					while($counter<$num21){
						$debitid = mysql_result($result21,$counter,"debitid");
						$debitname = mysql_result($result21,$counter,"debitname");
					
						if($localid==$debitid && $localtype == 2){$sel="SELECTED";}
						else{$sel="";}
					
						echo "<option value=2-$debitid $sel>$debitname ($debitid)</option>";
						$counter++;
					}

				
				if($localid==0){$showerror="<img src=error.gif height=16 width=16>";}
				else{$showerror="";}
				
				echo "</select> $showerror <a href=editmap.php?pid=$pid&id=$id&do=1 title='Add Mapping'><img src=plus.gif alt='Add Mapping' height=16 width=16 border=0></a> <a href=editmap.php?pid=$pid&id=$id&do=2 title='Delete Mapping' onclick=\"javascript:return confirm('Are you sure?');\"><img src=delete.gif alt='Delete Mapping' height=16 width=16 border=0></a></td></tr>";
				$lasttype=$type;
				$lastposid=$posid;
			}
		}
		echo "</td></tr>";
		echo "<tr bgcolor=#E8E7E7><td colspan=2 align=right style=\"border:1px solid #E8E7E7;\"><input type=submit value='Save'></td></tr>";
		echo "</table></form>";
		
		mysql_close($newdb);
	}
		
	//////details
	echo "</td><td>";
	
		echo "<table cellspacing=0 style=\"border:2px solid #E8E7E7;\" width=100%><tr bgcolor=#E8E7E7><td colspan=2>&nbsp;<b>Client Details<b/> <form style=\"margin:0;padding:0;display:inline;\" action=saveclient.php method=post onSubmit='return disableForm(this);'><input type=hidden name=client_programid value='$pid'></td></tr>";
		echo "<tr><td align=right>Name:</td><td><input type=text name=name size=25 value='$client_name'></td></tr>";
		echo "<tr><td align=right>Parent:</td><td><select name=parent>";
		
		$query2 = "SELECT * FROM client_programs WHERE client_programid != '$pid' ORDER BY name";
		$result2 = mysql_query($query2,$thisdb);
		
		echo "<option value=0>(None)</option>";
		
		while($r=mysql_fetch_array($result2)){
			$client_pid=$r["client_programid"];
			$client_name=$r["name"];
			
			if($client_pid == $parent){$sel="SELECTED";}
			else{$sel="";}
			
			echo "<option value=$client_pid $sel>$client_name</option>";
		}
		
		echo "</select></td></tr>";
		echo "<tr><td align=right>Day End/Start:</td><td><input type=text name=day_start size=25 value='$day_start'></td></tr>";
		echo "<tr><td align=right>POS Type:</td><td><select name=pos_type>";
		
		$query2 = "SELECT * FROM pos_type ORDER BY name";
		$result2 = mysql_query($query2,$thisdb);
		
		while($r=mysql_fetch_array($result2)){
			$pos_typeid=$r["pos_type"];
			$pos_name=$r["name"];
			
			if($pos_typeid == $pos_type){$sel="SELECTED";}
			else{$sel="";}
			
			echo "<option value=$pos_typeid $sel>$pos_name</option>";
		}
		
		echo "</select></td></tr>";
		echo "<tr><td align=right>Business ID:</td><td><input type=text name=businessid size=25 value='$businessid'></td></tr>";
		echo "<tr><td align=right>DB IP:</td><td><input type=text name=db_ip size=25 value='$db_ip'></td></tr>";
		echo "<tr><td align=right>DB Name:</td><td><input type=text name=db_name size=25 value='$db_name'></td></tr>";
		echo "<tr><td align=right>DB User:</td><td><input type=text name=db_user size=25 value='$db_user'></td></tr>";
		echo "<tr><td align=right>DB Pass:</td><td><input type=text name=db_pass size=25 value='$db_pass'></td></tr>";
		echo "<tr><td align=right colspan=2><input type=submit value='Save'></form></td></tr>";
		echo "</table>";
	echo "</td></tr>";
	
	echo "</table>";
	
	echo "<p><a href=dashboard style=$style><font color=blue>Back</font></a><p>";

?>
</html>
