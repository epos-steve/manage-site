<?php
define('SITECH_APP_PATH', dirname(dirname(__FILE__)).'/application');
define('SITECH_LIB_PATH', dirname(SITECH_APP_PATH).'/lib');
define('SITECH_VENDOR_PATH', dirname(SITECH_APP_PATH).'/vendors');

set_include_path(
	SITECH_LIB_PATH
	.PATH_SEPARATOR.SITECH_VENDOR_PATH.'/SiTech/lib'
	.PATH_SEPARATOR.SITECH_VENDOR_PATH.'/Treat/lib'
	.PATH_SEPARATOR.get_include_path());

require_once('SiTech/Loader.php');
SiTech_Loader::registerAutoload();
SiTech_Loader::loadBootstrap();

$user = null;

if (!empty($_COOKIE['id'])) {
	$user = new Manage_Login_Record($_COOKIE['id'], $_COOKIE['hash']);
}

if (empty($user) || $user->isLoggedIn() === false) {
	echo 'Failed.';
	exit;
}

foreach ($_POST['credit'] as $id => $credit) {
	if (empty($credit)) continue;
	$map = MappingModel::get((int)$id, true);
	$map->tbl = 1;
	$map->localid = $credit;
	$map->save();
}

foreach ($_POST['debit'] as $id => $debit) {
	if (empty($debit)) continue;
	$map = MappingModel::get((int)$id, true);
	$map->tbl = 2;
	$map->localid = $debit;
	$map->save();
}
	
header('Location: ./dashboard/manage/' . $_POST['client_programid'] . '#client_mapping');
mysql_close();

