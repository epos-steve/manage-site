<?php

$dbconfigFile = dirname( __FILE__ ) . '/../application/config/config.ini';
$dbconfig = parse_ini_file( $dbconfigFile, true );

if( !$dbconfig ) {
	throw new Exception( "ERROR: Couldn't read database settings; check application/config/config.ini" );
} 

$dsn = $dbconfig['database']['dsn'];
$dsnValues = array( );
preg_match( '/^mysql:host=([^;]*);dbname=(.*)$/', $dsn, $dsnValues );
$dbhost = $dsnValues[1];
$database = $dsnValues[2];
$username = $dbconfig['database']['username'];
$password = $dbconfig['database']['password'];

?>
