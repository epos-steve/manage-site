<?php
define('SITECH_APP_PATH', dirname(dirname(dirname(__FILE__))).'/application');
define('SITECH_LIB_PATH', dirname(SITECH_APP_PATH).'/lib');
define('SITECH_VENDOR_PATH', dirname(SITECH_APP_PATH).'/vendors');

set_include_path(
	SITECH_LIB_PATH
	.PATH_SEPARATOR.SITECH_VENDOR_PATH.'/SiTech/lib'
	.PATH_SEPARATOR.SITECH_VENDOR_PATH.'/Treat/lib'
	.PATH_SEPARATOR.get_include_path());

require_once('SiTech/Loader.php');
SiTech_Loader::registerAutoload();
SiTech_Loader::loadBootstrap();

$now = time();
$day = date('N', $now);
$endtime = date('H:i:59', strtotime('-1 minute', $now));
$starttime = date('H:i:00', strtotime('+5 minutes', $now));

$sql = '
SELECT
	id,
	client_programid
FROM client_schedule
WHERE (
		day = 0
		OR day = ?
	)
	AND `time` > ?
	AND `time` < ?';

$db = Manage_DB::singleton();
$stmnt = $db->prepare($sql);
$stmnt->bindParam(1, $day, PDO::PARAM_INT);
$stmnt->bindParam(2, $endtime, PDO::PARAM_STR);
$stmnt->bindParam(3, $starttime, PDO::PARAM_STR);
$stmnt->execute();
$rows = $stmnt->fetchAll(PDO::FETCH_ASSOC);
$stmnt->closeCursor();

$i = 0;
foreach ($rows as $row) {
	$stmnt = $db->prepare('UPDATE client_programs SET send_status = 2, send_schedule = ? WHERE client_programid = ?');
	$stmnt->execute(array_values($row));
	$i += $stmnt->rowCount();
	$stmnt->closeCursor();
}
echo 'Set '.$i.' clients to update';