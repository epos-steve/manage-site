<?php

include('../db.php');
include('../function.php');

define('SITECH_APP_PATH', dirname(dirname(dirname(__FILE__))).'/application');
define('SITECH_LIB_PATH', dirname(SITECH_APP_PATH).'/lib');
define('SITECH_VENDOR_PATH', dirname(SITECH_APP_PATH).'/vendors');

set_include_path(
	SITECH_LIB_PATH
	.PATH_SEPARATOR.SITECH_VENDOR_PATH.'/SiTech/lib'
	.PATH_SEPARATOR.SITECH_VENDOR_PATH.'/Treat/lib'
	.PATH_SEPARATOR.get_include_path());

require_once('SiTech/Loader.php');
SiTech_Loader::registerAutoload();
SiTech_Loader::loadBootstrap();

$update_cc = 0;
$pid=$_GET["programid"];

mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( mysql_error());

$query = "SELECT * FROM client_programs WHERE client_programid = '$pid'";
$result = mysql_query($query);

$slhs = mysql_result($result, 0, "slhs_chargeAll");
$day_start = mysql_result($result,0,"day_start");
$send_status = mysql_result($result,0,"send_status");
$send_schedule = mysql_result($result,0,"send_schedule");
$pos_type = mysql_result($result,0,"pos_type");

if($send_status==0){
	$arr = array("pid" => $pid, "todo" => $send_status, "end" => "end");
}
elseif($send_status==1){
	$query = "SELECT * FROM client_programs WHERE client_programid = '$pid'";
	$result = mysql_query($query);
	
	$pos_type = mysql_result($result,0,"pos_type");
	
	$query = "SELECT * FROM pos_type WHERE pos_type = '$pos_type'";
	$result = mysql_query($query);
	
	$directory = mysql_result($result,0,"name");

	$arr = array("pid" => $pid, "todo" => $send_status, "directory" => $directory, "end" => "end");
}
elseif($send_status==2){
	$query = "SELECT * FROM client_programs WHERE client_programid = '$pid'";
	$result = mysql_query($query);

	$pos_type = mysql_result($result,0,"pos_type");
	$db_ip = mysql_result($result,0,"db_ip");
	$db_name = mysql_result($result,0,"db_name");
	$db_user = mysql_result($result,0,"db_user");
	$db_pass = mysql_result($result,0,"db_pass");
	$db_bid = mysql_result($result,0,"businessid");
	$update_cc = 1;

	$query = "SELECT * FROM client_schedule WHERE id = '$send_schedule'";
	$result = mysql_query($query);

	$num_days = 0;
	$days_back = 0;
	if (mysql_num_rows($result) > 0)
	{
		//$starttime = mysql_result($result,0,"starttime");
		//$endtime = mysql_result($result,0,"endtime");
		$num_days = mysql_result($result,0,"num_days");
		$days_back = mysql_result($result,0,"days_back");
	}

	$today = date("Y-m-d");
	for($counter=0;$counter<$days_back;$counter++){$today=prevday($today);}
	$enddate = $today;
	$startdate = $today;
	for($counter=1;$counter<$num_days;$counter++){$startdate=prevday($startdate);}

	$arr = array("pid" => $pid, "todo" => $send_status, "startdate" => $startdate, "enddate" => $enddate, "day_start" => $day_start, "end" => "end");
}
else{
	$arr = array("pid" => $pid, "todo" => $send_status, "end" => "end");
}

if ((bool)$slhs) {
	$arr['slhs'] = 1;
}

//////////////////////////////PRINT
$arr_len=count($arr);
$count=1;
foreach($arr AS $key => $value){
	if($count==$arr_len){echo "$key|$value";}
	else{echo "$key|$value,";}
	$count++;
}

/////////////////////////////////////////////////////////////////
////Catapult Kiosk & AerisPOS update cc/refund/promo totals//////
/////////////////////////////////////////////////////////////////
/*
if(($pos_type == 6 || $pos_type == 7) && $update_cc == 1){
		////credit cards
        $query = "SELECT * FROM mapping_cc_transactions WHERE client_programid = '$pid' AND type = 0";
		$result = mysql_query($query);
        $num = mysql_numrows($result);

        if($num == 1){
                $creditid = mysql_result($result,0,"creditid");
                $debitid = mysql_result($result,0,"debitid");

                ///create new connection to client db
                $tdb = new Manage_DB('treat db');

                ////get companyid
                $stmnt = $tdb->query("SELECT companyid FROM business WHERE businessid = $db_bid");
                $array = $stmnt->fetch(PDO::FETCH_ASSOC);

                $db_cid = $array["companyid"];

                ////sum of transactions for time period
                $tempdate = $arr["startdate"];
                while($tempdate <= $arr["enddate"]){
                        $date1 = "$tempdate " . $arr["day_start"];
                        $date2 = nextday($tempdate);
                        $date2 = "$date2 " . $arr["day_start"];

                        $stmnt = $tdb->query("SELECT SUM(customer_transactions.subtotal) AS total FROM customer,customer_transactions WHERE customer_transactions.response = 'APPROVED' AND customer_transactions.date_created >= '$date1' AND customer_transactions.date_created < '$date2' AND (customer_transactions.trans_type_id = 1 OR customer_transactions.trans_type_id = 2) AND customer_transactions.customer_id = customer.customerid AND customer.businessid = $db_bid");
                        $array = $stmnt->fetch(PDO::FETCH_ASSOC);

                        $total = $array["total"];

                        ////update credit
                        $tdb->query("INSERT INTO creditdetail (companyid,businessid,amount,date,creditid) VALUES ($db_cid,$db_bid,'$total','$tempdate',$creditid) ON DUPLICATE KEY UPDATE amount = '$total'");

                        ////update debit
                        $tdb->query("INSERT INTO debitdetail (companyid,businessid,amount,date,debitid) VALUES ($db_cid,$db_bid,'$total','$tempdate',$debitid) ON DUPLICATE KEY UPDATE amount = '$total'");

                        $tempdate=nextday($tempdate);
                }

                $stmnt->closeCursor();
        }

		////refunds
		$query = "SELECT * FROM mapping_cc_transactions WHERE client_programid = '$pid' AND type = 1";
		$result = mysql_query($query);
        $num = mysql_numrows($result);

        if($num == 1){
                $creditid = mysql_result($result,0,"creditid");
                $debitid = mysql_result($result,0,"debitid");

                ///create new connection to client db
                $tdb = new Manage_DB('treat db');

                ////get companyid
                $stmnt = $tdb->query("SELECT companyid FROM business WHERE businessid = $db_bid");
                $array = $stmnt->fetch(PDO::FETCH_ASSOC);

                $db_cid = $array["companyid"];

                ////sum of transactions for time period
                $tempdate = $arr["startdate"];
                while($tempdate <= $arr["enddate"]){
                        $date1 = "$tempdate " . $arr["day_start"];
                        $date2 = nextday($tempdate);
                        $date2 = "$date2 " . $arr["day_start"];

                        $stmnt = $tdb->query("SELECT SUM(customer_transactions.subtotal) AS total FROM customer,customer_transactions WHERE customer_transactions.response = 'APPROVED' AND customer_transactions.date_created >= '$date1' AND customer_transactions.date_created < '$date2' AND customer_transactions.trans_type_id = 3 AND customer_transactions.customer_id = customer.customerid AND customer.businessid = $db_bid");
                        $array = $stmnt->fetch(PDO::FETCH_ASSOC);

                        $total = $array["total"];

                        ////update credit
                        $tdb->query("INSERT INTO creditdetail (companyid,businessid,amount,date,creditid) VALUES ($db_cid,$db_bid,'$total','$tempdate',$creditid) ON DUPLICATE KEY UPDATE amount = '$total'");

                        ////update debit
                        $tdb->query("INSERT INTO debitdetail (companyid,businessid,amount,date,debitid) VALUES ($db_cid,$db_bid,'$total','$tempdate',$debitid) ON DUPLICATE KEY UPDATE amount = '$total'");

                        $tempdate=nextday($tempdate);
                }

                $stmnt->closeCursor();
        }

		/////promos
		$query = "SELECT * FROM mapping_cc_transactions WHERE client_programid = '$pid' AND type = 2";
		$result = mysql_query($query);
        $num = mysql_numrows($result);

        if($num == 1){
                $creditid = mysql_result($result,0,"creditid");
                $debitid = mysql_result($result,0,"debitid");

                ///create new connection to client db
                $tdb = new Manage_DB('treat db');

                ////get companyid
                $stmnt = $tdb->query("SELECT companyid FROM business WHERE businessid = $db_bid");
                $array = $stmnt->fetch(PDO::FETCH_ASSOC);

                $db_cid = $array["companyid"];

                ////sum of transactions for time period
                $tempdate = $arr["startdate"];
                while($tempdate <= $arr["enddate"]){
                        $date1 = "$tempdate " . $arr["day_start"];
                        $date2 = nextday($tempdate);
                        $date2 = "$date2 " . $arr["day_start"];

                        $stmnt = $tdb->query("SELECT SUM(customer_transactions.subtotal) AS total FROM customer,customer_transactions WHERE customer_transactions.response = 'APPROVED' AND customer_transactions.date_created >= '$date1' AND customer_transactions.date_created < '$date2' AND customer_transactions.trans_type_id = 4 AND customer_transactions.customer_id = customer.customerid AND customer.businessid = $db_bid");
                        $array = $stmnt->fetch(PDO::FETCH_ASSOC);

                        $total = $array["total"];

                        ////update credit
                        $tdb->query("INSERT INTO creditdetail (companyid,businessid,amount,date,creditid) VALUES ($db_cid,$db_bid,'$total','$tempdate',$creditid) ON DUPLICATE KEY UPDATE amount = '$total'");

                        ////update debit
                        $tdb->query("INSERT INTO debitdetail (companyid,businessid,amount,date,debitid) VALUES ($db_cid,$db_bid,'$total','$tempdate',$debitid) ON DUPLICATE KEY UPDATE amount = '$total'");

                        $tempdate=nextday($tempdate);
                }

                $stmnt->closeCursor();
        }

		/////kiosk cc reload
		$query = "SELECT * FROM mapping_cc_transactions WHERE client_programid = '$pid' AND type = 3";
		$result = mysql_query($query);
        $num = mysql_numrows($result);

        if($num == 1){
                $creditid = mysql_result($result,0,"creditid");
                $debitid = mysql_result($result,0,"debitid");

                ///create new connection to client db
                $tdb = new Manage_DB('treat db');

                ////get companyid
                $stmnt = $tdb->query("SELECT companyid FROM business WHERE businessid = $db_bid");
                $array = $stmnt->fetch(PDO::FETCH_ASSOC);

                $db_cid = $array["companyid"];

                ////sum of transactions for time period
                $tempdate = $arr["startdate"];
                while($tempdate <= $arr["enddate"]){
                        $stmnt = $tdb->query("SELECT amount FROM debitdetail WHERE debitid = $debitid AND date = '$tempdate'");
						$array = $stmnt->fetch(PDO::FETCH_ASSOC);

						$total = $array["amount"];

                        ////update credit
                        $tdb->query("INSERT INTO creditdetail (companyid,businessid,amount,date,creditid) VALUES ($db_cid,$db_bid,'$total','$tempdate',$creditid) ON DUPLICATE KEY UPDATE amount = '$total'");

                        $tempdate=nextday($tempdate);
                }

                $stmnt->closeCursor();
        }

		/////kiosk cash balance
		$query = "SELECT * FROM mapping_cc_transactions WHERE client_programid = '$pid' AND type = 4";
		$result = mysql_query($query);
        $num = mysql_numrows($result);

        if($num == 1){
                $creditid = mysql_result($result,0,"creditid");
                $debitid = mysql_result($result,0,"debitid");

                ///create new connection to client db
                $tdb = new Manage_DB('treat db');

                ////get companyid
                $stmnt = $tdb->query("SELECT companyid FROM business WHERE businessid = $db_bid");
                $array = $stmnt->fetch(PDO::FETCH_ASSOC);

                $db_cid = $array["companyid"];

                ////sum of transactions for time period
                $tempdate = $arr["startdate"];
                while($tempdate <= $arr["enddate"]){
                        $stmnt = $tdb->query("SELECT amount FROM debitdetail WHERE debitid = $debitid AND date = '$tempdate'");
						$array = $stmnt->fetch(PDO::FETCH_ASSOC);

						$total = $array["amount"];

                        ////update credit
                        $tdb->query("INSERT INTO creditdetail (companyid,businessid,amount,date,creditid) VALUES ($db_cid,$db_bid,'$total','$tempdate',$creditid) ON DUPLICATE KEY UPDATE amount = '$total'");

                        $tempdate=nextday($tempdate);
                }

                $stmnt->closeCursor();
        }
}*/

if (!isset($_GET['vivipos'])) {
	$query = "UPDATE client_programs SET send_time = now() WHERE client_programid = '$pid'";
	$result = mysql_query($query);
}

mysql_close();
?>
