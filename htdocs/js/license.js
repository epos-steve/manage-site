$(document).ready(function () {

	/* save terminal status */
	$('#term_table').delegate('.save_btn', 'click', function(event) {
		termid = this.id.substring(12);
		status = $('#term_status_' + termid).val();
		that = this;

		var working = jQuery.waffle.working({
			content: 'Saving...',
			autoClose: false,
			weight: 10
		});

		var posting = $.post('license/save', {termid: termid, status : status});

		posting.done(function(data) {
			jQuery.waffle.burn(working);

			jQuery.waffle.success({
				content: 'Saved',
				autoClose: true,
				weight: 10
			});

			$(that).closest('tr').removeClass('ui-state-highlight');

			/* refresh resultset */
			var view = $.post('license/sorted_terminals', {showonly: $('#show_only').val()});

			view.done(function(data) {
				$( "#lic_tbody" ).empty().append(data);
			});
		})
		.fail(function(){
			jQuery.waffle.burn(working);

			jQuery.waffle.error({
				content: 'Error saving',
				autoClose: true,
				weight: 10
			});
		});
	});

	/* filter resultset by selected status type */
	$('#show_only').change(function (event) {
		loading = jQuery.waffle.working({
			content: 'Loading...',
			autoClose: false,
			weight: 10
		});

		 var posting = $.post('license/sorted_terminals', {showonly: $(this).val()});
		
		posting.done(function(data) {
			jQuery.waffle.burn(loading);
			$( "#lic_tbody" ).empty().append(data);
		});
	});

	/* highlight a changed row */
	$('#term_table').delegate('.status_sel', 'change', function(event) {
		$(this).closest('tr').addClass('ui-state-highlight');
	});
});
