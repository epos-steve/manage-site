<?php
define('SITECH_APP_PATH', dirname(dirname(__FILE__)).'/application');
define('SITECH_LIB_PATH', dirname(SITECH_APP_PATH).'/lib');
define('SITECH_VENDOR_PATH', dirname(SITECH_APP_PATH).'/vendors');

set_include_path(
	SITECH_LIB_PATH
	.PATH_SEPARATOR.SITECH_VENDOR_PATH.'/SiTech/lib'
	.PATH_SEPARATOR.SITECH_VENDOR_PATH.'/Treat/lib'
	.PATH_SEPARATOR.get_include_path());

require_once('SiTech/Loader.php');
SiTech_Loader::registerAutoload();
SiTech_Loader::loadBootstrap();

$user = null;

if (!empty($_COOKIE['id'])) {
	$user = new Manage_Login_Record($_COOKIE['id'], $_COOKIE['hash']);
} elseif (!empty($_POST)) {
	$user = new Manage_Login_Record($_POST['username'], $_POST['password']);
}

if ((empty($user) || $user->isLoggedIn() === false) && $_GET["inv"] !== "1") {
	echo 'Failed.';
	exit;
}

include('./db.php');
include('./function.php');

mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( mysql_error());

$pid=$_GET["pid"];
$action=$_GET["action"];
$inv=(isset($_GET["inv"]))? $_GET['inv'] : null;

	if($action==1) {
		$query = "SELECT id FROM client_schedule WHERE client_programid = '$pid' AND `default` = '1'";
		$result = mysql_query($query);

		$id=mysql_result($result,0,"id");

		$query = "UPDATE client_programs SET send_status = '2', send_schedule = '$id' WHERE client_programid = '$pid'";
		$result = mysql_query($query);

                //////get inventory count
                if($inv==1){
                    $query = "UPDATE client_programs SET inv = '1' WHERE client_programid = '$pid'";
                    $result = mysql_query($query);

                    ?>
                        <html>
                            <body onload="window.close();">
                                <center><b>Inventory Count Sent</b></center>
                            </body>
                        </html>
                    <?php
                }
	} elseif($action==2) {
		$query = "UPDATE client_programs SET send_status = '0', send_schedule = '0' WHERE client_programid = '$pid'";
		$result = mysql_query($query);
	} elseif ($action == 3) {
		$query = 'UPDATE client_programs SET send_status = 3 WHERE client_programid = '.(int)$pid;
		$result = mysql_query($query);
	} else {
		$query = "UPDATE client_programs SET send_status = '1' WHERE client_programid = '$pid'";
		$result = mysql_query($query);
	}

mysql_close();
header('Location: ./dashboard#'.$pid);