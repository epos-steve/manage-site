<?php
define('SITECH_APP_PATH', dirname(dirname(__FILE__)).'/application');
require_once __DIR__ . '/../application/bootstrap.php';

$user = null;

try {
	SiTech_Controller::dispatch(new SiTech_Uri());
} catch (SiTech_Exception $e) {
	if ($e->getCode() == '404') {
		header('HTTP/1.0 404 Not Found');
	}
	echo $e->getMessage();
} catch (PDOException $e) {
	header('HTTP/1.0 500 Internal Server Error');
	echo $e->getMessage();
	echo '<pre>';
	echo $e->getTraceAsString();
	echo '</pre>';
}
