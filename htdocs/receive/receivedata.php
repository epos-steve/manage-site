<?php
set_time_limit(0);
define('SITECH_APP_PATH', dirname(dirname(dirname(__FILE__))).'/application');
define('SITECH_LIB_PATH', dirname(SITECH_APP_PATH).'/lib');
define('SITECH_VENDOR_PATH', dirname(SITECH_APP_PATH).'/vendors');

set_include_path(
	SITECH_LIB_PATH
	.PATH_SEPARATOR.SITECH_VENDOR_PATH.'/SiTech/lib'
	.PATH_SEPARATOR.SITECH_VENDOR_PATH.'/Treat/lib'
	.PATH_SEPARATOR.get_include_path());

require_once('SiTech/Loader.php');
SiTech_Loader::registerAutoload();
SiTech_Loader::loadBootstrap();
//require('Manage/ClientPrograms/Record.php');
SiTech_Loader::loadModel("clientprograms");

$client = ClientProgramsModel::get((int)$_GET['programid'],true);

try {
	if(!realpath(SITECH_APP_PATH.'/files/'.$client->client_programid.'.txt')) {
		throw new Exception('Missing client file data/'.$client->client_programid.'.txt');
	}

	echo "Success<br>";
	if (isset($_GET['vivipos'])) {
		$client->send_status = 0;
		$client->send_schedule = 0;
		$client->receive_time = date('Y-m-d H:i:s');
		$client->save();
	} else {
		$client->setSendStatus();
	}

	echo "Updating database...<br />";
	
	if ((int)$client->pos_type->is_slhs === 1) {
		require_once('Manage/Details/SLHS.php');
		$details = new Manage_Details_SLHS($client);
	} else {
		require_once('Manage/Details.php');
		$details = new Manage_Details($client);
	}
	$details->read();
	$details->doMapping();
	echo '<p>Used '.round(memory_get_usage() / 1024, 2).' KB of memory and took '.round((microtime(true) - $details->getStart()), 5).' seconds to parse the file.';
	echo '<p><strong>Total Rows Parsed: '.$details->totals['rows'].' - Skipped: '.$details->totals['skipped'].'</strong><br />Removed '.$details->totals['deleted'].' rows - Updated '.$details->totals['updated'].' rows - Inserted '.$details->totals['new'].' new rows';
	// destroy the object so our destructor gets called and the file is renamed.
	unset($details);
} catch (Exception $ex) {
	echo "Error";
	echo '<hr>'.$ex->getMessage().'<br /><pre>';
	echo $ex->getTraceAsString();
	echo '</pre>';
	$client->setSendStatus(true);
}
