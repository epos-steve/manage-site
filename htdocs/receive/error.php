<?php
if (isset($_GET['programid']) && $_GET['programid'] == '19') exit; // fuckoff.

define('SITECH_APP_PATH', dirname(dirname(dirname(__FILE__))).'/application');
define('SITECH_LIB_PATH', dirname(SITECH_APP_PATH).'/lib');
define('SITECH_VENDOR_PATH', dirname(SITECH_APP_PATH).'/vendors');

set_include_path(
	SITECH_LIB_PATH
	.PATH_SEPARATOR.SITECH_VENDOR_PATH.'/SiTech/lib'
	.PATH_SEPARATOR.SITECH_VENDOR_PATH.'/Treat/lib'
	.PATH_SEPARATOR.get_include_path());

require_once('SiTech/Loader.php');
SiTech_Loader::registerAutoload();
SiTech_Loader::loadBootstrap();

require('Manage/ClientPrograms/Record.php');

$client = new Manage_ClientPrograms_Record($_GET['programid']);

// we should just be receiving an error message from the client at this point.
echo (int)$client->logError($_POST['message'], $_POST['trace']);
