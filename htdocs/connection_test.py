#!/usr/bin/python
#VERSION 0.97 - DJS -  2015-06-22 - Need to test with internal CK before distribution and calling final 1.0.

import socket
import os
import sys
import struct, time


connections =  [

        [
            'CK Servers',
            [
             '199.91.127.120',
             '199.91.127.121',
             '199.91.127.122',
             '199.91.127.123',
             '199.91.127.124',
             '199.91.127.125',
             '199.91.127.126',
             '199.91.127.127'
            ],
            [
                [22, 'TCP'],
                [80, 'TCP'],
                [443, 'TCP'],
                [13377, 'TCP'],
                [123, 'UDP'],
                [1196, 'UDP']
            ]
        ],
        [
            'CK DR Servers',
            [ '52.10.45.229',
             '52.10.133.244',
             '52.24.123.14',
             '52.24.142.25',
             '52.24.171.109'],
                    [
                        [22, 'TCP'],
                        [80, 'TCP'],
                        [443, 'TCP'],
                        [13377, 'TCP'],
                        [123, 'UDP'],
                        [1196, 'UDP']
                    ]
        ],
        [
            'Credit Card Processing',
            ['posgateway.secureexchange.net'],
                    [
                        [443, 'TCP']
                     ]
        ],
        [
            'Digital Signage',
            ['services.brightsignnetwork.com'],
                    [
                        [80,  'TCP'],
                        [443, 'TCP'],
                        [123, 'UDP']
                    ]
        ],
        [
            'Digital Signage 2',
            ['bsnm.s3.amazonaws.com'],
                    [
                        [80,  'TCP']
                    ]
        ],
        [
            'Logmein Monitoring',
            ['logmein.com'],
                    [
                        [80,  'TCP'],
                        [443, 'TCP']

                    ]
        ]

]

class color:
    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    BLINK = '\033[5m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'

def getNTPTime(host = "pool.ntp.org"):
    port = 123
    buf = 1024
    address = (host,port)
    msg = '\x1b' + 47 * '\0'

    # reference time (in seconds since 1900-01-01 00:00:00)
    TIME1970 = 2208988800L # 1970-01-01 00:00:00

    # connect to server
    client = socket.socket( socket.AF_INET, socket.SOCK_DGRAM)
    client.settimeout(5)
    client.sendto(msg, address)
    msg, address = client.recvfrom( buf )

    t = struct.unpack( "!12I", msg )[10]
    t -= TIME1970
    return time.ctime(t).replace("  "," ")

#Clear the screen
os.system('clear')

success_total = 0
fail_total = 0

#Print Start Test Info
print "START TEST [ Please wait for END TEST ] \n\n"
print "****  \"[+]\" = SUCCESS | \"[-]\" = FAIL ****"
#check each connection group
for connection in connections:
    description = connection[0]
    hosts = connection[1]
    ports_protocols = connection[2]
    #print description of server groups
    print color.BOLD+"\n\n"+description+"\n------------------------------------------"+color.END
    #check each host in group
    for address in hosts:
        #print address / host of server to test
        print "\n"+address,
        sys.stdout.flush()
        #Check each port / protocol combination for host
        for port, comm_type in ports_protocols:
                        print "\t"+str(port)+'/'+str(comm_type),

                        #Check the protocol to see if we should connect via TCP or UDP
                        if comm_type == 'TCP':
                            s = socket.socket()
                        else:
                            s = socket.socket(socket.AF_INET,  # Internet
                            socket.SOCK_DGRAM)  # UDP
                        s.settimeout(10)
                        try:
                            result = s.connect_ex((address, port))
                        except:
                            print "FAILURE: No network connectivity."




                        #check if connection passed or failed
                        if result > 0:
                            #connection failed
                            fail_total += 1
                            print color.BOLD+color.RED+color.BLINK+"[-]"+color.END,
                            sys.stdout.flush()
                        else:
                            #connection succeeded
                            #send hello to server
                            s.send('hello')
                            success_total += 1
                            print "[+]",
                            sys.stdout.flush()
                        s.close()
#get server info
print color.BOLD+"\n\nSERVER INFO\n------------------------------------------"+color.END
print "\nSERVER: " + socket.gethostname()
print 'ADDRESS: ' + socket.gethostbyname(socket.gethostname())

#check NTP Servers for time
print color.BOLD+"\n\nTIME SERVER CHECK\n------------------------------------------"+color.END
if __name__ == "__main__":
    try:
        print 'CK Server Time: ' +getNTPTime('199.91.127.120')
    except:
        print color.BOLD+color.RED+'FAILURE: unable to get time from CK Server'+color.END
        fail_total+=1

    try:
        print 'CK DR Server Time: ' +getNTPTime('52.10.45.229')
    except:
        print color.BOLD+color.RED+'FAILURE: unable to get time from CK DR Servers'+color.END
        fail_total+=1

#output test summary
print color.BOLD+"\n\nTEST SUMMARY\n------------------------------------------\n\n"+color.END+"PASS: "+color.END+str(success_total)+" \tFAIL: "+str(fail_total)
print "\n"
print "\nEND TEST"

#if failures let the user know they should correct all failed connections
if(fail_total > 0):
    print color.BOLD+color.RED+color.BLINK+"\n--> PLEASE CORRECT ALL FAILURES <--\n\n"+color.END

