<?php
define('SITECH_APP_PATH', dirname(dirname(__FILE__)).'/application');
define('SITECH_LIB_PATH', dirname(SITECH_APP_PATH).'/lib');
define('SITECH_VENDOR_PATH', dirname(SITECH_APP_PATH).'/vendors');

set_include_path(
	SITECH_LIB_PATH
	.PATH_SEPARATOR.SITECH_VENDOR_PATH.'/SiTech/lib'
	.PATH_SEPARATOR.SITECH_VENDOR_PATH.'/Treat/lib'
	.PATH_SEPARATOR.get_include_path());

require_once('SiTech/Loader.php');
SiTech_Loader::registerAutoload();
SiTech_Loader::loadBootstrap();

$user = null;

if (!empty($_COOKIE['id'])) {
	$user = new Manage_Login_Record($_COOKIE['id'], $_COOKIE['hash']);
}

if (empty($user) || $user->isLoggedIn() === false) {
	echo 'Failed.';
	exit;
}

include('./db.php');
include('./function.php');

$style = "text-decoration:none";

mysql_connect($dbhost,$username,$password);
@mysql_select_db($database) or die( mysql_error());

$filter = new SiTech_Filter(SiTech_Filter::INPUT_REQUEST);

$pid = $filter->input('pid');
$id = $filter->input('id');
$default = $filter->input('default');
$day = $filter->input('day');
$time_hour = $filter->input('time_hour');
$time_min = $filter->input('time_min');
$start_hour = $filter->input('start_hour');
$start_min = $filter->input('start_min');
$end_hour = $filter->input('end_hour');
$end_min = $filter->input('end_min');
$num_days = $filter->input('num_days');
$days_back = $filter->input('days_back');

$del = (isset($_GET['del']))? $_GET["del"] : null;

	if($del==1){
		$id=$_GET["id"];
		$pid=$_GET["pid"];
		
		$query = "DELETE FROM client_schedule WHERE id = '$id'";
		$result = mysql_query($query);
	}
	elseif($id>0){
		$time="$time_hour:$time_min:00";
		$starttime="$start_hour:$start_min:00";
		$endtime="$end_hour:$end_min:00";
	
		$query = "UPDATE client_schedule SET day = '$day', time = '$time', starttime = '$starttime', endtime = '$endtime', num_days = '$num_days', days_back = '$days_back' WHERE id = '$id'";
		$result = mysql_query($query);
	}
	else{
		$time="$time_hour:$time_min:00";
		$starttime="$start_hour:$start_min:00";
		$endtime="$end_hour:$end_min:00";
	
		$query = "INSERT INTO client_schedule (client_programid,day,time,starttime,endtime,num_days,days_back) VALUES ('$pid','$day','$time','$starttime','$endtime','$num_days','$days_back')";
		$result = mysql_query($query);
		$id = mysql_insert_id();
	}
	
	//////only one default schedule
	if($default==1){
		$query = "UPDATE client_schedule SET `default` = '1' WHERE id = '$id'";
		$result = mysql_query($query);
		
		$query = "UPDATE client_schedule SET `default` = '0' WHERE client_programid = '$pid' AND id != '$id'";
		$result = mysql_query($query);
	}
	else{
		$query = "SELECT * FROM client_schedule WHERE client_programid = '$pid'";
		$result = mysql_query($query);
		$num = mysql_numrows($result);
		
		if($num==1){
			$query = "UPDATE client_schedule SET `default` = '1' WHERE client_programid = '$pid'";
			$result = mysql_query($query);
		}
		elseif($num>1){
			$query = "SELECT * FROM client_schedule WHERE client_programid = '$pid' AND `default` = '1' AND id != '$id'";
			$result = mysql_query($query);
			$num = mysql_numrows($result);
			
			if($num==1){}
			else{
				$query = "UPDATE client_schedule SET `default` = '0' WHERE id = '$id'";
				$result = mysql_query($query);
			
				$query = "SELECT id FROM client_schedule WHERE client_programid = '$pid' AND id != '$id' LIMIT 1";
				$result = mysql_query($query);
				
				$newid = mysql_result($result,0,"id");
			
				$query = "UPDATE client_schedule SET `default` = '1' WHERE id = '$newid'";
				$result = mysql_query($query);
			}
		}
	}

	$location = 'dashboard/manage/'.$pid.'#client_schedule';
	header('Location: ./' . $location);
	
	
mysql_close();
?>
</html>
