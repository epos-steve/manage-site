<?php
define('SITECH_APP_PATH', dirname(dirname(__FILE__)).'/application');
define('SITECH_LIB_PATH', dirname(SITECH_APP_PATH).'/lib');
define('SITECH_VENDOR_PATH', dirname(SITECH_APP_PATH).'/vendors');

set_include_path(
	SITECH_LIB_PATH
	.PATH_SEPARATOR.SITECH_VENDOR_PATH.'/SiTech/lib'
	.PATH_SEPARATOR.SITECH_VENDOR_PATH.'/Treat/lib'
	.PATH_SEPARATOR.get_include_path());

require_once('SiTech/Loader.php');
SiTech_Loader::registerAutoload();
SiTech_Loader::loadBootstrap();

$user = null;

if (!empty($_COOKIE['id'])) {
	$user = new Manage_Login_Record($_COOKIE['id'], $_COOKIE['hash']);
}

if (empty($user) || $user->isLoggedIn() === false) {
	echo 'Failed.';
	exit;
}

include('./function.php');

$style = "text-decoration:none";

$db = Manage_DB::singleton();
$client_programid=$_POST["client_programid"];
$name=$_POST["name"];
$businessid=$_POST["businessid"];
$pos_type=$_POST["pos_type"];
$parent=(empty($_POST['parent']))? '' : $_POST['parent'];
$day_start=$_POST["day_start"];
$db_name=$_POST["db_name"];
$skip_client=(empty($_POST['skip_client']))? '0' : $_POST['skip_client'];
$pos_mode = intval( $_POST['pos_mode'] );
$pos_mode = $pos_mode > 0 ? '\'' . $pos_mode . '\'' : 'NULL';
$sync_version = (isset($_POST['sync_version']))? $_POST['sync_version'] : '1';

$name=str_replace("'","`",$name);

$query = "UPDATE client_programs SET name = '$name', businessid = '$businessid', parent = '$parent', pos_type = '$pos_type', day_start = '$day_start', db_name = '$db_name', skip_client = '$skip_client', pos_mode = $pos_mode, sync_version = '$sync_version' WHERE client_programid = '$client_programid'";
$db->exec($query);
	
$location="dashboard/manage/$client_programid#client_details";
header('Location: ' . SITECH_BASEURI.$location);
