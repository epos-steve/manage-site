<?php
define('SITECH_APP_PATH', dirname(dirname(__FILE__)).'/application');
define('SITECH_LIB_PATH', dirname(SITECH_APP_PATH).'/lib');
define('SITECH_VENDOR_PATH', dirname(SITECH_APP_PATH).'/vendors');

set_include_path(
	SITECH_LIB_PATH
	.PATH_SEPARATOR.SITECH_VENDOR_PATH.'/SiTech/lib'
	.PATH_SEPARATOR.SITECH_VENDOR_PATH.'/Treat/lib'
	.PATH_SEPARATOR.get_include_path());

require_once('SiTech/Loader.php');
SiTech_Loader::registerAutoload();
SiTech_Loader::loadBootstrap();

$user = null;

if (!empty($_COOKIE['id'])) {
	$user = new Manage_Login_Record($_COOKIE['id'], $_COOKIE['hash']);
}

if (empty($user) || $user->isLoggedIn() === false) {
	echo 'Failed.';
	exit;
}

$do = $_GET['do'];
$client_programid = $_GET['pid'];
$id = $_GET['id'];
$db = Manage_DB::singleton();

if($do == 1){
	$stmnt = $db->query('SELECT type, posid, name FROM mapping WHERE id = ?', array($id));
	list($type, $posid, $name) = $stmnt->fetch(PDO::FETCH_NUM);
	
	$db->exec('INSERT INTO mapping (client_programid,type,posid,name) VALUES (?, ?, ?, ?)', array($client_programid,$type,$posid,$name));
} elseif($do == 2) {
	$db->exec('DELETE FROM mapping WHERE id = ?', array($id));
}

$location="dashboard/manage/$client_programid#client_mapping";
header('Location: ./' . $location);