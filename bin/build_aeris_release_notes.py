#!/usr/bin/env python3

import os

page_template = '''<!doctype html>
<html>
<head>
	<title>AerisPOS Release Notes</title>
	<link rel="stylesheet" type="text/css" href="/css/redmond/jquery-ui-1.8.11.custom.css" />
	<link rel="stylesheet" type="text/css" href="aerispos_release_notes.css" />
	<script language="JavaScript" src="/js/jquery-1.7.2.min.js"></script>
	<script language="JavaScript" src="/js/jquery-ui-1.8.11.custom.min.js"></script>
	<script language="JavaScript1.5" src="aerispos_release_notes.js"></script>
</head>
<body>

<header>
	<h1>AerisPOS Release Notes</h1>
</header>

<section class='current-release'>
{}
</section>

<section class='previous-releases'>
{}
</section>

</body>
</html>
'''

release_template = '''
	<article>
		<h2>{}</h2>
		<span>{}</span>
{}
	</article>
'''

notes_template = '''
		<ul>
{}
		</ul>'''

note_template = '''			<li>{}</li>'''

def build_notes():
	bindir = os.path.realpath(os.path.dirname(__file__))
	
	relfile = os.path.realpath(os.path.join(bindir, '..', 'htdocs', 'aerispos', 'aerispos_release_notes.html'))
	if not os.path.exists(relfile):
		print('Could not locate release file! (%s)' % relfile)
		raise SystemExit(1)
	
	notefile = os.path.realpath(os.path.join(bindir, '..', '..', '..', 'aeris2', 'aeris2', 'release_notes.txt'))
	if not os.path.exists(notefile):
		print('Could not locate notes file! (%s)' % notefile)
		raise SystemExit(1)
	
	releases = []
	current_release_ver = ''
	current_release = []
	
	with open(notefile, 'r') as f:
		for line in f:
			if line:
				c = line[0]
				if c == '\t':
					if not current_release_ver:
						print('Error parsing file!')
						raise SystemExit(2)
					tabs = len(line) - len(line.lstrip()) - 1
					line = line.strip()
					print('tabs: ', tabs)
					print(current_release)
					if tabs > 0:
						target = current_release
						parent = None
						for i in range((tabs - 1) * 2 + 1):
							parent = target
							target = target[-1]
						try:
							print('append to target:', target)
							target[1].append(line)
							print('after append:', target)
						except (AttributeError, IndexError):
							print('create tuple:', target)
							i = parent.index(target)
							parent[i] = (target, [line])
							print('after create:', parent[i])
					else:
						current_release.append(line)
				else:
					if current_release_ver:
						releases.append((current_release_ver, current_release))
						current_release = []
					current_release_ver = line.strip()
	
	if current_release:
		releases.append((current_release_ver, current_release))
	
	print(releases)
	
	output = []
	
	for release in sorted(releases, reverse=True):
		#notes = '\n'.join([note_template.format(note) for note in release[1]])
		notes = build_notes_list(release[1])
		relver, reldate = release[0].split(' - ')
		output.append(release_template.format(relver, reldate, notes))
	
	recent_release = output[0]
	past_releases = '\n'.join(output[1:]) if len(output) > 1 else '\tNo notes available for previous releases.'
	
	release_notes = page_template.format(recent_release, past_releases)
	print(release_notes)
	
	with open(relfile, 'w') as f:
		f.write(release_notes)

def build_notes_list(notes):
	return notes_template.format('\n'.join(build_note_list(n) for n in notes))

def build_note_list(note):
	if isinstance(note, str):
		return note_template.format(note)
	notestr = note[0] + build_notes_list(note[1])
	return note_template.format(notestr)

if __name__ == '__main__':
	build_notes()
