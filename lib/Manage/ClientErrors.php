<?php
require_once('Manage/ClientErrors/Record.php');

class ClientErrors extends Manage_Abstract
{
	public function getClientErrors($client, $unreadOnly = false, $updateReadStatus = false)
	{
		$sql = 'SELECT id, client_id, error_time, error_message, error_trace, isRead FROM client_errors WHERE client_id = ?';
		
		if ($unreadOnly) {
			$sql .= ' AND isRead = 0';
		}

		$stmnt = $this->_db->prepare($sql);
		$stmnt->execute(array($client));
		$stmnt->setFetchMode(PDO::FETCH_CLASS, 'ClientErrors_Record');
		$errors = $stmnt->fetchAll();
		if ($updateReadStatus) {
			foreach ($errors as $error) {
				$error->markRead();
			}
		}

		return($errors);
	}
}

