<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Abstract
 *
 * @author Eric Gach <eric@essential-elements.net>
 */
abstract class Manage_Model_Abstract extends SiTech_Model_Abstract
{
	protected static $db;

	/**
	 * Copies properties from object passed in
	 * parameter to new instance of this class
	 *
	 * @param object $obj
	 * @return self
	 */
	public static function copyFieldsFromObject($obj)
	{
		$props = get_object_vars($obj);
		if($props === null)return false;

		$clone = new static();
		foreach($props as $key=>$val)
		{
			$clone->{$key} = $val;
		}

		return $clone;
	}
}
