<?php
/**
 * Short description for file
 *
 * Long description for file (if any)...
 *
 * @copyright  Copyright &copy; 2012 Essential Elements, LLC (http://www.essential-elements.net)
 * @link       http://ee.docs.dev.essential-elements.net/package/PackageName
 * @license    EE-Proprietary
 * @package    Manager\Model
 */

/**
 * Part of Manage\Model
 *
 * @category Manage\Model
 */
namespace Manage\Model
{

	/**
	 * Short description for class
	 *
	 * Long description for class (if any)...
	 *
	 * @package    Manage\Model
	 * @subpackage    Manage\Model\Sentinel
	 */
	class Sentinel extends \EE\Model\Base
	{
		/**
		 * @var PDO
		 */
		protected static $db;	
	
		protected static $_dbConfigSection = 'sentinel';	
	
	}
}
