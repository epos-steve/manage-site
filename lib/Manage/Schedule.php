<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Schedule
 *
 * @author Eric
 */
class Manage_Schedule extends Manage_Abstract
{
	public function getByClient($cid)
	{
		$sql = 'SELECT * FROM client_schedule WHERE client_programid = ?';
		$stmnt = $this->_db->prepare($sql);
		$stmnt->execute(array($cid));
		$stmnt->setFetchMode(PDO::FETCH_CLASS, 'Manage_Schedule_Record');
		return($stmnt->fetchAll());
	}
}
