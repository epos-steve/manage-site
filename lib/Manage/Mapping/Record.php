<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once('Manage/Abstract/Record.php');

/**
 * Description of Record
 *
 * @author eric
 */
class Mapping_Record extends Manage_Abstract_Record
{
	protected $init = 'SELECT * FROM mapping WHERE client_programid = :id';

	public function getType()
	{
		if (!is_object($this->type)) {
			require_once('Manage/MappingType/Record.php');
			$this->type = new MappingType_Record($this->type);
		}

		return($this->type);
	}
}
