<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Template
 *
 * @author eric
 */
class Manage_Template extends SiTech_Template
{
	public function __construct()
	{
		parent::__construct(SITECH_APP_PATH.DIRECTORY_SEPARATOR.'views');
		$this->assign('baseUrl', Manage_ConfigParser::singleton()->get('main', 'base_url'));
	}
}
