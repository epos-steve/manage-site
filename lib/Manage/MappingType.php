<?php
class MappingType extends Manage_Abstract
{
	public function getByName($name)
	{
		$stmnt = $this->_db->prepare('SELECT * FROM mapping_type WHERE file_name = :name');
		$stmnt->execute(array(':name' => $name));
		require_once('Manage/MappingType/Record.php');
		$stmnt->setFetchMode(PDO::FETCH_CLASS, 'MappingType_Record');
		$return = $stmnt->fetch();
		$stmnt->closeCursor();
		return($return);
	}
}

