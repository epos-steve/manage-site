<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SLHS
 *
 * @author Eric Gach <eric@essential-elements.net>
 */
class Manage_Details_SLHS extends Manage_Details
{
	public function __construct(ClientProgramsModel $client)
	{
		parent::__construct($client);

		// Updating handlers for SLHS specific.
		$this->_handlers = array(
			// Credits
			'gross sales'   => '_dateDetail',
			'sales'         => '_dateDetail',
			'taxes'         => '_dateDetail',
			'cc tips'       => '_dateDetail',
			'paidouts'      => '_dateDetail',
			'discounts'     => '_dateDetail',
			// Debits
			'tenders'       => '_dateDetail',
			// Menu Items
			'category1'     => '_menuCat1',
			'category2'     => '_menuCat2',
			/* added to parse new groups section - Eric - 2/19/2010 */
			'menu groups'   => '_menuGroups',
			'menu items'    => '_menuItems',
			// Check Detail
			'checks'        => '_checks',
			'check detail'  => '_checkDetail',
			'paymentdetail' => '_paymentDetail',
			'cardbalances'  => '_cardBalances',
		);
	}
}
