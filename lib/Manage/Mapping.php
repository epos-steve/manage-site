<?php
class Manage_Mapping extends Manage_Abstract
{
	public function getByClient($client_id)
	{
		$stmnt = $this->_db->prepare('SELECT m.* FROM mapping AS m JOIN mapping_type AS mt WHERE m.client_programid = :id AND m.type = mt.id ORDER BY mt.order, m.name');
		$stmnt->execute(array(':id' => $client_id));
		require_once('Manage/Mapping/Record.php');
		$stmnt->setFetchMode(PDO::FETCH_CLASS, 'Mapping_Record');
		return($stmnt->fetchAll());
	}

	public function getByPosId($pos_id, $client_id, $type)
	{
		$stmnt = $this->_db->prepare('SELECT * FROM mapping WHERE client_programid = :id AND type = :type AND posid = :pos_id');
		$stmnt->execute(array(':id' => $client_id, ':type' => $type, ':pos_id' => $pos_id));
		require_once('Manage/Mapping/Record.php');
		$stmnt->setFetchMode(PDO::FETCH_CLASS, 'Mapping_Record');
		return($stmnt->fetchAll());
	}

	public function updateName($client_id, $mapping_type, $pos_id, $name)
	{
		$stmnt = $this->_db->prepare('SELECT id FROM mapping WHERE client_programid = :id AND type = :type AND posid = :pos_id LIMIT 1');
		$stmnt->execute(array(':id' => $client_id, ':type' => $mapping_type, ':pos_id' => $pos_id));
		$mapping_id = $stmnt->fetchColumn();
		$stmnt->closeCursor();
		if ($mapping_id !== false) {
			$stmnt = $this->_db->prepare('UPDATE mapping SET name = :name WHERE id = :id');
			$stmnt->execute(array(':id' => $mapping_id, ':name' => $name));
		} else {
			$stmnt = $this->_db->prepare('INSERT INTO mapping (client_programid, type, posid, name) VALUES(:client_id, :type, :posid, :name)');
			$stmnt->execute(array(':client_id' => $client_id, ':type' => $mapping_type, ':posid' => $pos_id, ':name' => $name));
		}
	}
}

