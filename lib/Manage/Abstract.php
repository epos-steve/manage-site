<?php
abstract class Manage_Abstract
{
	/**
	 *
	 * @var PDO
	 */
	protected $_db;

	/**
	 *
	 * @var Manage_Login_Record
	 */
	protected $_user;

	public function __construct()
	{
		require_once('Manage/DB.php');
		$this->_db = Manage_DB::singleton();
	}

	public function getCurrentUser()
	{
		if (empty($this->_user)) {
			if (!empty($_POST['username'])) {
				$this->_user = LoginModel::getLogin($_POST['username'], $_POST['password']);
			} elseif (!empty($_COOKIE['id'])) {
				$this->_user = LoginModel::getLogin($_COOKIE['id'], $_COOKIE['hash']);
			}

			if (empty($this->_user)) {
				$this->_user = new LoginModel();
			}
		}

		return($this->_user);
	}
}

