<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Record
 *
 * @author Eric
 */
class ReceiveStatus_Record extends Manage_Abstract_Record
{
	public function __construct($id = 0)
	{
		parent::__construct();
		if (!empty($id)) {
			$stmnt = $this->_db->prepare('SELECT * FROM receive_status WHERE receive_status = :id');
			$stmnt->execute(array(':id' => $id));
			$stmnt->setFetchMode(PDO::FETCH_INTO, $this);
			$stmnt->fetch();
		}
	}
}
