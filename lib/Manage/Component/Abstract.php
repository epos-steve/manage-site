<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Abstract
 *
 * @author Eric Gach <eric@essential-elements.net>
 */
abstract class Manage_Component_Abstract
{
	/**
	 *
	 * @var Manage_ConfigParser
	 */
	protected $_config;

	/**
	 *
	 * @var Manage_DB
	 */
	protected $_db;

	public function __construct()
	{
		$this->_config = Manage_ConfigParser::singleton();
		$this->_db = Manage_DB::singleton();
	}
}
