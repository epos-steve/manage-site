<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Recordphp
 *
 * @author Eric
 */
class Manage_PosType_Record extends Manage_Abstract_Record
{
	protected $_db;

	public function __construct($id = 0)
	{
		parent::__construct();
		if (!empty($id)) {
			$stmnt = $this->_db->prepare('SELECT * FROM pos_type WHERE pos_type = :id');
			$stmnt->execute(array(':id' => $id));
			$stmnt->setFetchMode(PDO::FETCH_INTO, $this);
			$stmnt->fetch();
		}
	}

	public function isRM()
	{
		return((substr($this->name, 0, 2) == 'RM')? true : false);
	}

	public function isCatapult()
	{
		return((strstr($this->name, 'Catapult'))? true : false);
	}

	public function isVolante()
	{
		return((strstr($this->name, 'Volante'))? true : false);
	}
}
