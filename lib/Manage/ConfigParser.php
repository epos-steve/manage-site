<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ConfigParser
 *
 * @author eric
 */
class Manage_ConfigParser extends SiTech_ConfigParser
{
	/**
	 *
	 * @var Manage_ConfigParser
	 */
	protected static $_instance;

	/**
	 * @return Manage_ConfigParser
	 */
	public static function singleton()
	{
		if (empty(self::$_instance)) {
			self::$_instance = Manage_ConfigParser::load();
			self::$_instance->read(array(SITECH_APP_PATH.'/config/config.ini'));
		}

		return(self::$_instance);
	}
}
