<?php
require_once('Manage/Abstract/Record.php');

class MappingType_Record extends Manage_Abstract_Record
{
	protected $init = 'SELECT * FROM mapping_type WHERE id = :id';

	protected $_mapping_table;

	public function __get($name)
	{
		if ($name == 'mapping_table') {
			if (!is_object($this->_mapping_table) && !empty($this->_mapping_table)) {
				require_once('Manage/MappingTable/Record.php');
				$this->_mapping_table = new MappingTable_Record($this->_mapping_table);
			}
		}

		return(parent::__get($name));
	}

	public function __isset($name)
	{
		if ($name == 'mapping_table') {
			return((empty($this->_mapping_table))? false : true);
		}

		return(parent::__isset($name));
	}
}

