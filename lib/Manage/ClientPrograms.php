<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ClientPrograms
 *
 * @author Eric
 */
class Manage_ClientPrograms extends Manage_Abstract
{
	/**
	 * Fetch an array from the database of all clients.
	 *
	 * @param int $parent Parent ID to fetch from. Default: 0 (top level)
	 */
	public function getClients($parent = 0)
	{
		$this->getCurrentUser();
		$args = array(':parent' => $parent);

		if ($this->_user->isAdmin() || $parent != 0) {
			$stmnt = $this->_db->prepare('SELECT * FROM client_programs WHERE parent = :parent ORDER BY client_programid');
		} else {
			$stmnt = $this->_db->prepare('
				SELECT
					c.*
				FROM
					client_programs AS c
					LEFT JOIN parentAccess as p
						ON p.client_id = c.client_programid
				WHERE
					c.parent = :parent
					AND p.login_id = :login
				ORDER BY client_programid');
			$args[':login'] = $this->_user->id;
		}

		//var_dump($stmnt);
		$stmnt->execute($args);
		require_once('ClientPrograms/Record.php');
		$stmnt->setFetchMode(PDO::FETCH_CLASS, 'Manage_ClientPrograms_Record');
		return($stmnt->fetchAll());
	}

	public function getAll()
	{
		$stmnt = $this->_db->query('SELECT * FROM client_programs');
		$stmnt->setFetchMode(PDO::FETCH_CLASS, 'Manage_ClientPrograms_Record');
		return($stmnt->fetchAll());
	}
}
