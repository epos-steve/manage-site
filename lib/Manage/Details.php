<?php
require_once('Manage/Abstract.php');
require_once('Manage/MappingType.php');
require_once('Manage/Mapping.php');

class Manage_Details extends Manage_Abstract
{
	protected $_args = array();

	protected $_dates;


	private $_fp;

	protected $_headers;

	protected $_handlers = array(
		// Credits
		'gross sales'   => '_dateDetail',
		'sales'         => '_dateDetail',
		'taxes'         => '_dateDetail',
		'cc tips'       => '_dateDetail',
		'paidouts'      => '_dateDetail',
		'discounts'     => '_dateDetail',
		// Debits
		'tenders'       => '_dateDetail',
		// Employee data
		'employees'     => '_employees',
		'clock ins'     => '_clockIns',
		'jobtypes'      => '_jobTypes',
		'stats'         => '_stats',
		// Menu Items
		'category1'     => '_menuCat1',
		'category2'     => '_menuCat2',
		/* added to parse new groups section - Eric - 2/19/2010 */
		'menu groups'   => '_menuGroups',
		'menu items'    => '_menuItems',
		// Check Detail
		'checks'        => '_checks',
		'check detail'  => '_checkDetail',
		'paymentdetail' => '_paymentDetail',
		'cardbalances'  => '_cardBalances',
	);

	protected $_mainSection;

	/**
	 * @var Mapping
	 */
	protected $_mapping;

	/**
	 * @var MappingType
	 */
	protected $_mappingType;

	protected $_query;

	protected $_queries = array();

	/* This is no longer needed since we're using a field in the DB now - Eric 12/03/10
	private $_skipClients = array('28', '29', '31', '32');*/

	protected $_subSection;

	private $_startTime;

	/**
	 *
	 * @var PDOStatement
	 */
	protected $_statement;

	public $totals = array(
		'deleted' => 0,
		'new'     => 0,
		'rows'    => 0,
		'skipped' => 0,
		'updated' => 0
	);

	/**
	 * Constructor - Open the file we need. If we're unable to open it, throw
	 * an exception.
	 */
	public function __construct(ClientProgramsModel $client)
	{
		parent::__construct();

		$this->_startTime = microtime(true);
		$this->_client = $client;

		if ($this->_client->canSkip()) {
			$this->_handlers = array(
				// Credits
				'gross sales'   => '_dateDetail',
				'sales'         => '_dateDetail',
				'taxes'         => '_dateDetail',
				'cc tips'       => '_dateDetail',
				'paidouts'      => '_dateDetail',
				'discounts'     => '_dateDetail',
				// Debits
				'tenders'       => '_dateDetail',
				// Check Detail
				'checks'        => '_checks',
				'check detail'  => '_checkDetail',
				'paymentdetail' => '_paymentDetail',
				'discount detail' => '_discountDetail',
				// Menu Items
				'menu groups'   => '_menuGroups',
				'menu items'    => '_menuItems',
				// Reorder
				'reorder'       => '_reOrder',
				'cardbalances'  => '_cardBalances'
			);
		}

		if (!($this->_fp = fopen(realpath(SITECH_APP_PATH.'/files/'.$client->client_programid.'.txt'), 'r'))) {
			throw new Exception('Unable to read client file.');
		}

		$this->_mapping = new Manage_Mapping();
		$this->_mappingType = new MappingType();
	}

	/**
	 * Destructor - cleanup our open file, if its open.
	 */
	public function __destruct()
	{
		if (($file = realpath(SITECH_APP_PATH.'/files/'.$this->_client->client_programid.'.sql')) !== false) {
			$start = microtime(true);
			echo '<p>'.shell_exec('mysql -h '.$this->_client->db_ip.' -u '.$this->_client->db_user.' --password='.$this->_client->db_pass.' '.$this->_client->db_name.' < '.$file);
			unlink($file);
			echo '<p>Took '.round((microtime(true) - $start), 5).' seconds to send the file to MySQL</p>';
		}

		if (!empty($this->_fp)) {
			fclose($this->_fp);
			if (!defined('DETAILS_ERROR') && is_writeable(SITECH_APP_PATH.'/files/'.$this->_client->client_programid.'.txt'))
				rename(SITECH_APP_PATH.'/files/'.$this->_client->client_programid.'.txt', SITECH_APP_PATH.'/files/'.$this->_client->client_programid.'.old.txt');
		}
	}

	public function doMapping()
	{
		if (!$this->_client->isKiosk()) return;

		$stmnt = $this->_db->query('SELECT * FROM mapping_cc_transactions WHERE client_programid = ?', array($this->_client->client_programid));
		
		$this->_client->db->setAttribute(SiTech_DB_Proxy::ATTR_WRITEONLY, true);
		
		$stmntAmount = $this->_client->db->prepare('SELECT amount FROM debitdetail WHERE debitid = ? AND date = ?');
		$icredits = $this->_client->db->prepare('
			INSERT INTO creditdetail
				(
					companyid,
					businessid,
					amount,
					date,
					creditid
				)
			VALUES (
					:cid,
					:bid,
					:total,
					:date,
					:creditid
				)
			ON DUPLICATE KEY UPDATE amount = :total
			');
		$idebits = $this->_client->db->prepare('
			INSERT INTO debitdetail
				(
					companyid,
					businessid,
					amount,
					date,
					debitid
				)
			VALUES (
					:cid,
					:bid,
					:total,
					:date,
					:debitid
				)
			ON DUPLICATE KEY UPDATE amount = :total
			');

		foreach ($stmnt->fetchAll(PDO::FETCH_ASSOC) as $mapping) {
			foreach ($this->_dates as $date) {
				switch ($mapping['type']) {
					case 0:
					case 1:
					case 2:
						if ($mapping['type'] == '0') $types = array(1,2);
						elseif ($mapping['type'] == '1') $types = array(3);
						elseif ($mapping['type'] == '2') $types = array(4);
						$sums = $this->_client->db->query(
							'SELECT
								SUM(customer_transactions.subtotal) AS total
							FROM
								customer,
								customer_transactions
							WHERE
								customer_transactions.response = \'APPROVED\'
								AND customer_transactions.date_created >= ?
								AND customer_transactions.date_created < ?
								AND customer_transactions.trans_type_id IN ('.implode(',', $types).')
								AND customer_transactions.customer_id = customer.customerid
								AND customer.businessid = ?',
							array(
								$date,
								date('Y-m-d H:i:s', strtotime('+1 day', strtotime($date))),
								$this->_client->businessid
							)
						);
						$sum = $sums->fetchColumn();
						if (empty($sum)) $sum = '0';
						$sums->closeCursor();
						unset($sums);

						////update credit
						$icredits->execute(array(
							'cid' => $this->_client->getCompanyId(),
							'bid' => $this->_client->businessid,
							'total' => $sum,
							'date' => $date,
							'creditid' => $mapping['creditid']
						));
						$icredits->closeCursor();

							////update debit
						$idebits->execute(array(
							'cid' => $this->_client->getCompanyId(),
							'bid' => $this->_client->businessid,
							'total' => $sum,
							'date' => $date,
							'debitid' => $mapping['debitid']
						));
						$idebits->closeCursor();
						break;

					case 3:
					case 4:
						$stmntAmount->execute(array(
							$mapping['debitid'],
							$date
						));
						$amount = $stmntAmount->fetchColumn();
						$stmntAmount->closeCursor();

						$icredits->execute(array(
							'cid' => $this->_client->getCompanyId(),
							'bid' => $this->_client->businessid,
							'total' => $amount,
							'date' => $date,
							'creditid' => $mapping['creditid']
						));
						$icredits->closeCursor();
						break;
				}
			}
		}
		$stmnt->closeCursor();
		$this->_client->db->setAttribute(SiTech_DB_Proxy::ATTR_WRITEONLY, false);
	}

	public function getStart()
	{
		return($this->_startTime);
	}

	/**
	 * Read the file and parse the sections. Once we're in a section, we can
	 * start passing data to the database itself.
	 */
	public function read()
	{
		// first row is our dates
		$this->_dates = fgetcsv($this->_fp);
		// Now clean the old data for these dates out.
		$start = microtime(true);
		$this->_cleanPrevious();
		echo 'Cleanup completed in '.round(microtime(true) - $start, 5).' seconds<br />'."\n";

		while (($csv = $this->_read())) {
			try {
				if (sizeof($csv) == 1 && $csv[0][0] == '[') {
					$compareChecks = false;
					if (!empty($this->_query)) {
						$compareChecks = true;
						$_args = array_chunk($this->_args, 500);
						foreach ($_args AS $args) {
							$query = $this->_query.' VALUES('.implode("),\n(", $args).')';
							$this->totals['new'] += $this->_client->db->exec($query);
						}
						unset($this->_query);
						$this->_args = array();
					}

					// Destroy
					if (!empty($this->_statement)) {
						unset($this->_statement);
					}

					if (!empty($sectionStart) && !empty($mappingType) && is_object($mappingType)) {
						if ($mappingType->file_name == 'checks' && $compareChecks) {
							$this->_compareChecksTables();
						}
						echo 'Previous section '.$mappingType->file_name.' took '.round((microtime(true) - $sectionStart), 5).' seconds to parse<br />',"\n";
					}

					$sectionStart = microtime(true);
					// We're looking at a section here, parse it!

					$mappingType = $this->_mappingType->getByName(trim($csv[0], '[]'));
					$this->_headers = $this->_read();
				} else {
					if (empty($mappingType) || !is_object($mappingType)) {
						// we should always have a mapping type - this should most
						// likely output an error
						continue;
					}

					// whee
					if (isset($this->_handlers[$mappingType->file_name])) {
						//echo 'Calling callback for '.$mappingType->file_name.'<br />';
						call_user_func_array(array($this, $this->_handlers[$mappingType->file_name]), array($mappingType, $csv));
						$this->totals['rows']++;
					} else {
						echo 'No callback found for '.$mappingType->file_name.'<br />';
					}
				}

				$this->_db->keepAlive();
			} catch (PDOException $ex) {
				if (strstr($ex->getMessage(), 'Integrity constraint violation')) {
					var_dump($csv);
				}

				define('DETAILS_ERROR', true);
				throw $ex;
			} catch (Exception $ex) {
				define('DETAILS_ERROR', true);
				throw $ex;
			}
		}

		if (!empty($this->_query)) {
			$_args = array_chunk($this->_args, 500);
			foreach ($_args AS $args) {
				$query = $this->_query.' VALUES('.implode("),\n(", $args).')';
				$this->totals['new'] += $this->_client->db->exec($query);
			}
			unset($this->_query);
			$this->_args = array();
		}

		if (!empty($sectionStart) && !empty($mappingType) && is_object($mappingType)) {
			echo 'Previous section '.$mappingType->file_name.' took '.round((microtime(true) - $sectionStart), 5).' seconds to parse<br />',"\n";
		}
	}

	/**
	 * Incoming CSV Format:
	 *	Int:		Check Number
	 *	Int:		Session Number
	 *	Int:		Table Number
	 *	Int:		Seat Number
	 *	Int:		Employee Number
	 *	Int:		Number of People
	 *	DateTime:	Bill Date/Time
	 *	Currency:	Total amount of Check
	 *	Currency:	Total taxes of Check
	 *	Currency:	Total received
	 *	Int:		Sale Type
	 */
	protected function _checks(MappingType_Record $mappingType, $csv)
	{
		/*
		 * Changes the time to the original timezone of the kiosk.
		 * 
		 * Commented out because it is implemented in MySQL as a Trigger in the
		 * checks table.
		static $_timezones = array();

		if ( ! in_array($this->_client->businessid, $_timezones)) {
			$timezone_query = $this->_client->db->query("SELECT timezone FROM business WHERE businessid = ?", array($this->_client->businessid));
			if ($timezone_query->rowCount() > 0) {
				$_timezones[$this->_client->businessid] = intval($timezone_query->fetchColumn());
				if ($_timezones[$this->_client->businessid] > -1) {
					$_timezones[$this->_client->businessid] = '+'.$_timezones[$this->_client->businessid];
				}
			}
		}
		 */

		/* Ignore 4 (For Catapult) */
		if (isset($csv[12]) && strlen($csv[12]) == 16) {
			if(substr($csv[12], 0, 1) == '4') {
				$csv[12] = substr($csv[12], 1);
			}
		}

		/* Ignore trailing 32's */
		if (isset($csv[12]) && substr($csv[12], -4) == '3232') {
			$csv[12] = substr($csv[12], 0, -4);
		}

		if (empty($this->_query)) {
			$this->_client->db->exec('
				CREATE TEMPORARY TABLE `tmp_checks` (
					`id` int(11) NOT NULL AUTO_INCREMENT,
					`businessid` int(11) NOT NULL,
					`check_number` bigint(20) UNSIGNED NOT NULL,
					`session_number` int(11) NOT NULL,
					`table_number` int(5) NOT NULL,
					`seat_number` int(5) NOT NULL,
					`emp_no` int(11) NOT NULL,
					`people` int(5) NOT NULL,
					`bill_datetime` datetime NOT NULL,
					`bill_posted` datetime NOT NULL,
					`total` double(10,2) NOT NULL,
					`taxes` double(10,2) NOT NULL,
					`received` decimal(10,2) NOT NULL,
					`sale_type` int(5) NOT NULL,
					`account_number` bigint(20) NOT NULL,
					`terminal_group` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
					`is_void` tinyint(1) NOT NULL DEFAULT \'0\',
					PRIMARY KEY (`id`),
					UNIQUE KEY `businessid` (`businessid`,`check_number`),
					KEY `account_number` (`account_number`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
			');
			$this->_query = '
				INSERT INTO tmp_checks (
					businessid,
					check_number,
					session_number,
					table_number,
					seat_number,
					emp_no,
					people,
					bill_datetime,
					bill_posted,
					total,
					taxes,
					received,
					is_void,
					sale_type,
					account_number,
					terminal_group
				)
			';
		}

		$this->_args[] = implode(',', array(
			// businessid
			$this->_client->db->quote($this->_client->businessid),
			// check_number
			$this->_client->db->quote($csv[0]),
			// session_number
			$this->_client->db->quote((int)$csv[1]),
			// table_number
			$this->_client->db->quote($csv[2]),
			// seat_number
			$this->_client->db->quote($csv[3]),
			// emp_no
			$this->_client->db->quote($csv[4]),
			// people
			$this->_client->db->quote((int)$csv[5]),
			// bill_datetime
			$this->_client->db->quote($csv[6]),
			/*
			 * Second part of the timezone change correction code.
			 * 
			 * Hasn't actually been tested.
			$this->_client->db->quote(
				DateTime::createFromFormat('Y-m-d H:i:s', $csv[6])->modify($_timezones[$this->_client->businessid] . ' hours')
			),
			 */
			// bill_posted
			$this->_client->db->quote((isset($csv[14]) ? $csv[14] : $csv[6])),
			// total
			$this->_client->db->quote($csv[7]),
			// taxes
			$this->_client->db->quote($csv[8]),
			// received
			$this->_client->db->quote($csv[9]),
			// is_void
			$this->_client->db->quote($csv[10]),
			// sale_type
			$this->_client->db->quote($csv[11]),
			// account_number
			$this->_client->db->quote((isset($csv[12])? (int)$csv[12] : 0)),
			// terminal_group
			$this->_client->db->quote((isset($csv[13]) ? $csv[13] : 0))
		));

			/*$this->_statement = $this->_client->db->prepare(
'INSERT INTO checks (
	businessid,
	check_number,
	session_number,
	table_number,
	seat_number,
	emp_no,
	people,
	bill_datetime,
	total,
	taxes,
	received,
	is_void,
	sale_type,
	account_number,
	terminal_group
)
VALUES(
	:bid,
	:chkno,
	:sessno,
	:table,
	:seat,
	:empno,
	:people,
	:date,
	:total,
	:taxes,
	:recv,
	:void,
	:sale,
	:acctno,
	:termgrp
)
ON DUPLICATE KEY UPDATE
	session_number = :sessno,
	table_number = :table,
	seat_number = :seat,
	emp_no = :empno,
	people = :people,
	bill_datetime = :date,
	total = :total,
	taxes = :taxes,
	received = :recv,
	is_void = :void,
	sale_type = :sale,
	account_number = :acctno,
	terminal_group = :termgrp');*/
		/*
		 $args = array(
			':bid'     => $this->_client->businessid,
			':chkno'   => $csv[0],
			':sessno'  => (int)$csv[1],
			':table'   => $csv[2],
			':seat'    => $csv[3],
			':empno'   => $csv[4],
			':people'  => (int)$csv[5],
			':date'    => $csv[6],
			':total'   => $csv[7],
			':taxes'   => $csv[8],
			':recv'    => $csv[9],
			':void'    => $csv[10],
			':sale'    => $csv[11],
			':acctno'  => (isset($csv[12])? (int)$csv[12] : 0),
			':termgrp' => (isset($csv[13]) ? $csv[13] : 0)
		);

		$this->_statement->exec($args);
		$rowCount = $this->_statement->rowCount();
		if ($rowCount % 2) {
			$this->totals['new']++;
		} elseif ($rowCount != 0) {
			$this->totals['updated']++;
		}
		$this->_statement->closeCursor();
		 */

		/*if ($this->_client->canSkip())	return;
		$time = date('h:i:s', strtotime($csv[6]));
		// Now add to the stats table
		$stmnt = $this->_client->db->prepare('SELECT * FROM rm_mealp WHERE businessid = ?');
		$stmnt->execute(array($this->_client->businessid));
		$rows = $stmnt->fetchAll(PDO::FETCH_ASSOC);
		$stmnt->closeCursor();
		foreach ($rows as $mealPeriod) {
			if ($mealPeriod['start_time'] <= $time && $mealPeriod['end_time'] >= $time) {
				$sql = 'UPDATE stats SET ';
				switch ((int)$mealPeriod['id']) {
					case 1:
						$sql .= 'number = number';
						break;

					case 2:
						$sql .= 'lunch = lunch';
						break;

					case 3:
						$sql .= 'dinner = dinner';
						break;

					default:
						continue(2);
				}
				$sql .= ' + '.$csv[5].' WHERE businessid = ? AND date = ?';

				$stmnt = $this->_client->db->prepare($sql);
				$stmnt->execute(array($this->_client->businessid, date('Y-m-d', strtotime($csv[6]))));
				$this->totals['updated'] += $stmnt->rowCount();
				$stmnt->closeCursor();
			}
		}*/
	}

	/**
	 * Incoming CSV Format:
	 *	Int:		Bill Number
	 *	Int:		Item Number
	 *	Int:		Quantity
	 *	Curency:	Price
	 */
	protected function _checkDetail(MappingType_Record $mappingType, $csv)
	{
		// If the query doesn't exist, create it.
		if (empty($this->_query)) {
			$this->_query = 'INSERT INTO checkdetail
												(
													businessid,
													bill_number,
													item_number,
													quantity,
													price,
													cost,
													is_void
												)';
		}

		////get cost
		$stmt = $this->_client->db->query("SELECT menu_items_price.cost FROM menu_items_price
								JOIN menu_items_new ON
									menu_items_new.id = menu_items_price.menu_item_id
									AND menu_items_new.businessid = ?
									AND menu_items_new.pos_id = ?" ,
					array($this->_client->businessid,$csv[1]));
		$cost = $stmt->fetchColumn();

		// Add the items to the array to be entered into the database
		$this->_args[] = implode(',', array(
			$this->_client->db->quote($this->_client->businessid),
			$this->_client->db->quote($csv[0]),
			$this->_client->db->quote($csv[1]),
			$this->_client->db->quote($csv[3]),
			$this->_client->db->quote($csv[4]),
			$this->_client->db->quote($cost),
			$this->_client->db->quote($csv[5])
		));
	}

	/**
	 * This should go through all of our previous data and delete the old data
	 * from previous imports. Please take special note that this only deletes
	 * certian data as some of our data from the import is updated instead
	 * created again.
	 */
	protected function _cleanPrevious()
	{
		// 11/10/10  shooting each other in a fox hole (in the dark)
		if(isset($_GET["skip_clean"]) && $_GET["skip_clean"] == 1) return;

		$maps = MappingModel::getByClient($this->_client->client_programid);
		$start = date('Y-m-d', strtotime($this->_dates[0]));
		$end = date('Y-m-d', strtotime(end($this->_dates)));
		// Credits
		$creditStmnt = $this->_client->db->prepare('DELETE FROM creditdetail WHERE creditid = ? AND date >= ? AND date <= ?');
		$creditStmnt->bindParam(2, $start, PDO::PARAM_STR);
		$creditStmnt->bindParam(3, $end, PDO::PARAM_STR);
		// Debits
		$debitStmnt = $this->_client->db->prepare('DELETE FROM debitdetail WHERE debitid = ? AND date >= ? AND date <= ?');
		$debitStmnt->bindParam(2, $start, PDO::PARAM_STR);
		$debitStmnt->bindParam(3, $end, PDO::PARAM_STR);

		foreach ($maps as $map) {
			if ($map->localid == 0) continue;
			switch ($map->tbl->id) {
				// credits
				case 1:
					$creditStmnt->bindValue(1, $map->localid, PDO::PARAM_INT);
					$creditStmnt->execute();
					$this->totals['deleted'] += $creditStmnt->rowCount();
					break;

				// debits
				case 2:
					$debitStmnt->bindValue(1, $map->localid, PDO::PARAM_INT);
					$debitStmnt->execute();
					$this->totals['deleted'] += $debitStmnt->rowCount();
					break;

				// labor
				case 3:
					break;
			}

		}

		$creditStmnt->closeCursor();
		$debitStmnt->closeCursor();

		$start = $this->_dates[0];
		$end = date('Y-m-d H:i:s', strtotime('+24 hours', strtotime(end($this->_dates))) - 1);
		/*$this->totals['deleted'] += $this->_client->db->exec('DELETE
					cd,
					p
				FROM
					checks AS c
						LEFT JOIN checkdetail AS cd
							ON c.businessid = cd.businessid
							AND c.check_number = cd.bill_number
						LEFT JOIN payment_detail AS p
							ON c.businessid = p.businessid
							AND c.check_number = p.check_number
				WHERE
					c.businessid = '.$this->_client->businessid.'
					AND c.bill_datetime >= \''.$start.'\'
					AND c.bill_datetime <= \''.$end.'\'');*/
		$stmnt = $this->_client->db->prepare('SELECT check_number FROM checks WHERE businessid = ? AND bill_posted >= ? AND bill_posted <= ?');
		$stmnt->execute(array($this->_client->businessid, $start, $end));
		$checks = $stmnt->fetchAll(PDO::FETCH_COLUMN, 0);
		$stmnt->closeCursor();

		if (!empty($checks)) {
			// Remove check/payment detail
			$this->totals['deleted'] += $this->_client->db->exec('DELETE FROM checkdetail WHERE businessid = '.$this->_client->businessid.' AND bill_number IN ('.implode(',', $checks).')');
			$this->totals['deleted'] += $this->_client->db->exec('DELETE FROM payment_detail WHERE businessid = '.$this->_client->businessid.' AND check_number IN ('.implode(',', $checks).')');
			if ($this->_client->canSkip())
				$this->totals['deleted'] += $this->_client->db->exec('DELETE FROM checkdiscounts WHERE businessid = '.$this->_client->businessid.' AND check_number IN ('.implode(',', $checks).')');
		}

		if ($this->_client->canSkip()) return;
		//Wipe the labor
		$this->totals['deleted'] = $this->_client->db->exec('DELETE FROM labor_clockin WHERE businessid = :bid AND ((clockin >= :start AND clockin <= :end) OR (clockout >= :start AND clockout <= :end))', array('bid' => $this->_client->businessid, 'start' => $start, 'end' => $end));

		/*
		foreach ($checks as $check) {
			$this->totals['deleted'] += $this->_client->db->exec('DELETE FROM checkdetail WHERE businessid = '.$this->_client->businessid.' AND bill_number = '.$check);
			///payment detail
			$this->totals['deleted'] += $this->_client->db->exec('DELETE FROM payment_detail WHERE businessid = '.$this->_client->businessid.' AND check_number = '.$check);
		}

		if ($this->_client->canSkip()) return;
		// Remove old stats
		$stmnt = $this->_client->db->prepare('DELETE FROM stats WHERE `date` >= ? AND date <= ? AND businessid = ?');
		$stmnt->execute(array(date('Y-m-d', strtotime($this->_dates[0])), date('Y-m-d', strtotime(end($this->_dates))), $this->_client->businessid));
		$stmnt->closeCursor();
		// Now add them back in with empty values
		$stmnt = $this->_client->db->prepare('INSERT INTO stats (businessid, companyid, `date`) VALUES(?, ?, ?)');
		foreach ($this->_dates as $date) {
			$stmnt->execute(array($this->_client->businessid, $this->_client->getCompanyId(), date('Y-m-d', strtotime($date))));
		}
		$stmnt->closeCursor();*/
	}

	/**
	 * Parse all the clockin/clockout times and put them in the database. If the
	 * clock in already exists, then we need to update the values.
	 *
	 * Incoming CSV Format:
	 * 	Int:		Employee
	 * 	Int:		Job Type
	 *	DateTime:	Day of Business
	 *	DateTime:	Clock-in
	 *	DateTime:	Clock-out
	 *	Currency:	Hourly Rate
	 *	Currency:	Revenues
	 *	Currency:	Real Tips
	 *	Currency:	Cash Tips
	 *	Int:		Login Ref?
	 */
	protected function _clockIns(MappingType_Record $mappingType, $csv)
	{
		// This makes the date into something comparable in our database, and
		// we have to do it in both formats... *sigh*
		$date = date('Y-m-d', strtotime($csv[2]));
		$effedDate = date('m/d/Y', strtotime($csv[2]));
		$mapping = MappingModel::get('posid = '.$csv[1].' AND client_programid = '.$this->_client->client_programid.' AND type = 12', true);
		$emp = $this->_client->db->query('SELECT loginid FROM login WHERE empl_no = ?', array($csv[0]))->fetch();
		$this->totals['updated'] += $this->_client->db->exec('UPDATE jobtypedetail SET rate = ? WHERE loginid = ? AND jobtype = ?', array($csv[5], $emp['loginid'], $mapping->localid));

		$args = array(
			//':effedDate' => $effedDate,
			':bid' => $this->_client->businessid,
			':empid' => $emp['loginid'],
			':jobid' => $mapping->localid,
			':clockin' => date('Y-m-d H:i:s', strtotime($csv[3])),
			':clockout' => date('Y-m-d H:i:s', strtotime($csv[4])),
		);

		if ($this->_countRows('SELECT COUNT(loginid) FROM labor_clockin WHERE businessid = ? AND jobtype = ? AND loginid = ? AND clockin = ? AND clockout = ?', array($this->_client->businessid, $mapping->localid, $emp['loginid'], strtotime($csv[3]), strtotime($csv[4]))) == 0) {
			/*$sql = 'UPDATE labor_clockins
					SET

					WHERE
						businessid=:bid
						AND companyid=:cid
						AND job_id=:jobid
						AND emp_id=:empid
						AND (
							date=:date
							OR date=:effedDate
						)
						AND login_record=:login';
			$stmnt = $this->_client->db->prepare($sql);
			$args[':effedDate'] = $effedDate;
			$stmnt = $this->_client->db->prepare($sql);
			$stmnt->execute($args);
			$this->totals['updated'] += $stmnt->rowCount();
		} else {*/
			$sql = 'INSERT INTO labor_clockin
					(
						loginid,
						businessid,
						clockin,
						clockout,
						jobtype
					)
					VALUES
					(
						:empid,
						:bid,
						:clockin,
						:clockout,
						:jobid
					)';
			$stmnt = $this->_client->db->prepare($sql);
			$stmnt->execute($args);
			$this->totals['new'] += $stmnt->rowCount();
		}
		$stmnt->closeCursor();
	}

	protected function _compareChecksTables()
	{
		$sql =
'UPDATE checks AS c, tmp_checks AS tc SET
	c.session_number = tc.session_number,
	c.table_number = tc.table_number,
	c.seat_number = tc.seat_number,
	c.emp_no = tc.emp_no,
	c.people = tc.people,
	c.bill_datetime = tc.bill_datetime,
	c.bill_posted = tc.bill_posted,
	c.total = tc.total,
	c.taxes = tc.taxes,
	c.received = tc.received,
	c.is_void = tc.is_void,
	c.sale_type = tc.sale_type,
	c.account_number = tc.account_number,
	c.terminal_group = tc.terminal_group
WHERE
	c.businessid = tc.businessid
	AND c.check_number = tc.check_number';
		$this->totals['updated'] += $this->_client->db->exec($sql);

		$sql =
'INSERT INTO checks (
	businessid,
	check_number,
	session_number,
	table_number,
	seat_number,
	emp_no,
	people,
	bill_datetime,
	bill_posted,
	total,
	taxes,
	received,
	is_void,
	sale_type,
	account_number,
	terminal_group
)
SELECT
	tc.businessid,
	tc.check_number,
	tc.session_number,
	tc.table_number,
	tc.seat_number,
	tc.emp_no,
	tc.people,
	tc.bill_datetime,
	tc.bill_posted,
	tc.total,
	tc.taxes,
	tc.received,
	tc.is_void,
	tc.sale_type,
	tc.account_number,
	tc.terminal_group
FROM
	tmp_checks AS tc
		LEFT JOIN checks AS c
			ON c.businessid = tc.businessid
			AND c.check_number = tc.check_number
WHERE
	c.id IS NULL';
		$this->totals['new'] += $this->_client->db->exec($sql);

		$this->totals['new'] -= $this->_client->db->exec('DELETE FROM tmp_checks');
		$this->_client->db->exec('DROP TEMPORARY TABLE tmp_checks');
	}

	protected function _countRows($sql, array $args)
	{
		$stmnt = $this->_client->db->prepare($sql);
		$stmnt->execute($args);
		$count = $stmnt->fetchColumn(0);
		$stmnt->closeCursor();
		return((int)$count);
	}

	/**
	 * All credit detail should be in the same format... so parse it here and
	 * put it where it needs to go. We'll have to add more later.
	 */
	protected function _dateDetail(MappingType_Record $mappingType, $csv)
	{
		// We don't want to process if for some reason the ID field is empty.
		if (empty($csv[0])) return;
		// In lines by date detail, the line always starts with id,name
		$this->_mapping->updateName($this->_client->client_programid, $mappingType->id, $csv[0], $csv[1]);
		// $csv[0] should always be the ID field
		$mappings = MappingModel::get('posid = '.$csv[0].' AND client_programid = '.$this->_client->client_programid.' AND type = '.$mappingType->id);
		foreach ($mappings as $mapping) {
			// Only continue if we've got a mapping
			if (!empty($mapping->localid) && !empty($mappingType->mapping_table)) {
				// Build the query itself for inserting
				$sql = 'INSERT INTO '.$mapping->tbl->detail_table.' (companyid, businessid, amount, date, '.$mapping->tbl->field.') VALUES(?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE amount = (amount + ?)';
				$stmnt = $this->_client->db->prepare($sql);
				for ($i = 2; $i < sizeof($this->_headers); $i++) {
					if (isset($csv[$i]) && (double)$csv[$i] != 0) {
						// This executes the query with each field in the query
						try {
							$args = array($this->_client->getCompanyId(), $this->_client->businessid, $csv[$i], $this->_headers[$i], $mapping->localid, $csv[$i]);
							if (!($total = $stmnt->execute($args))) {
								var_dump($stmnt->errorInfo());
							} else {
								if ($stmnt->rowCount() == 2)
									$this->totals['updated']++;
								else
									$this->totals['new']++;
							}
						} catch (PDOException $ex) {
							var_dump($ex, $args);
						}
					}
				}
			}
		}
	}

	///discount detail added 6/13/11 Steve
	protected function _discountDetail(MappingType_Record $mappingType, $csv)
	{
		// If the query doesn't exist, create it.
		if (empty($this->_query)) {
			$this->_query = 'INSERT INTO checkdiscounts
												(
													businessid,
													check_number,
													pos_id,
													amount
												)';
		}

		// Add the items to the array to be entered into the database
		$this->_args[] = implode(',', array(
			$this->_client->db->quote($this->_client->businessid),
			$this->_client->db->quote($csv[0]),
			$this->_client->db->quote($csv[1]),
			$this->_client->db->quote($csv[2])
		));
	}

	/**
	 * Just grab the employee section from the file and put it into the local
	 * database.
	 *
	 * @todo We're going to add rates to this, so we need to update those
	 *       values with this data.
	 */
	protected function _employees(MappingType_Record $mappingType, $csv)
	{
		$login = $this->_client->db->query('SELECT loginid FROM login WHERE empl_no = ? AND oo2 = ? AND companyid = ?', array($csv[0], $this->_client->businessid, $this->_client->getCompanyId()));
		if (($login = $login->fetch(PDO::FETCH_NUM)) !== false) {
			// Get the loginid from the table if the record exists
			$login = $login[0];
		}

		@list($fname,$lname) = preg_split('#\s+(?=\S+$)#', $csv[1], 2);
		if (empty($lname)) $lname = ' ';

		if ($login !== false) {
			// Update existing row
			$stmnt = $this->_client->db->prepare('UPDATE login SET firstname = :fname, lastname = :lname, oo2 = :bid WHERE empl_no = :emp_no AND companyid = :cid');
			$args = array(
				':emp_no' => $csv[0],
				':fname'  => $fname,
				':lname'  => $lname,
				':bid'    => $this->_client->businessid,
				':cid'    => $this->_client->getCompanyId()
			);
			$stmnt->execute($args);
			$this->totals['updated'] += $stmnt->rowCount();
		} else {
			// Insert new row
			$stmnt = $this->_client->db->prepare('INSERT INTO login (empl_no, oo2, companyid, firstname, lastname) VALUES(?, ?, ?, ?, ?)');
			$stmnt->execute(array($csv[0], $this->_client->businessid, $this->_client->getCompanyId(), $fname, $lname));
			$login = $this->_client->db->lastInsertId();
			$this->totals['new'] += $stmnt->rowCount();

			// Inser the jobtype
			for ($x = 3; $x <=7; $x++) {
				if (($jobType = MappingModel::getByPosId($csv[$x], 12, $this->_client)) !== false) {
					$stmnt = $this->_client->db->prepare('INSERT INTO jobtypedetail (loginid, jobtype) VALUES(?, ?)');
					$stmnt->execute(array($login, $jobType->localid));
				}
			}
		}

		$stmnt->closeCursor();
	}

	protected function _jobTypes(MappingType_Record $type, $csv)
	{
		// Update the mapping info
		$this->_mapping->updateName($this->_client->client_programid, $type->id, $csv[0], $csv[1]);

		/**
		 * This is the old way of doing job types with cattracker... in BT the
		 * job types are defined on the company level and mapped on the business
		 * level through manage.
		 *
		// If the local id isn't set, we can't map it anywhere.
		if (empty($mapping->localid)) return;

		if ($this->_countRows('SELECT COUNT(idpk) FROM rm_jobtype WHERE businessid=? AND id=? AND companyid=?', array($this->_client->businessid, $csv[0], $this->_client->getCompanyId())) > 0) {
			$stmnt = $this->_client->db->prepare('UPDATE rm_jobtype SET jobtype=? WHERE businessid=? AND id=? AND companyid=?');
			$stmnt->execute(array($csv[1], $this->_client->businessid, $csv[0], $this->_client->getCompanyId()));
			$this->totals['updated'] += $stmnt->rowCount();
		} else {
			$stmnt = $this->_client->db->prepare('INSERT INTO rm_jobtype (businessid, companyid, id, jobtype) VALUES(?, ?, ?, ?)');
			$stmnt->execute(array($this->_client->businessid, $this->_client->getCompanyId(), $csv[0], $csv[1]));
			$this->totals['new'] += $stmnt->rowCount();
		}

		$stmnt->closeCursor();
		 */
	}

	/**
	 * Please note that the menu category 1 goes to menu_type
	 */
	protected function _menuCat1(MappingType_Record $type, $csv)
	{
		if ($this->_countRows('SELECT COUNT(menu_typeid) FROM menu_type WHERE menu_posid=? AND businessid=? AND companyid=?', array($csv[0], $this->_client->businessid, $this->_client->getCompanyId())) > 0) {
			$stmnt = $this->_client->db->prepare('UPDATE menu_type SET menu_typename=? WHERE menu_posid=? AND businessid=? AND companyid=?');
			$stmnt->execute(array($csv[1], $csv[0], $this->_client->businessid, $this->_client->getCompanyId()));
			$this->totals['updated'] += $stmnt->rowCount();
		} else {
			$stmnt = $this->_client->db->prepare('INSERT INTO menu_type (menu_posid, menu_typename, businessid, companyid) VALUES(?, ?, ?, ?)');
			$stmnt->execute(array($csv[0], $csv[1], $this->_client->businessid, $this->_client->getCompanyId()));
			$this->totals['new'] += $stmnt->rowCount();
		}

		$stmnt->closeCursor();
	}

	protected function _menuCat2(MappingType_Record $type, $csv)
	{
		$query = 'SELECT COUNT(menu_group_id) FROM menu_groups WHERE pos_groupid = :pos_groupid AND businessid = :businessid AND companyid = :companyid' . (empty($csv[6]) ? '' : ' AND group_type = :group_type');
		$args = array(':pos_groupid' => $csv[0], ':businessid' => $this->_client->businessid, ':companyid' => $this->_client->getCompanyId());
		if(!empty($csv[6]))$args[':group_type'] = $csv[6];
		if ($this->_countRows($query, $args) > 0) {
			$query = 'UPDATE menu_groups SET groupname = :groupname WHERE
					pos_groupid = :pos_groupid AND businessid = :businessid AND companyid = :companyid';
			$stmnt = $this->_client->db->prepare($query);
			$args = array(
				':groupname' => $csv[1],
				':pos_groupid' => $csv[0],
				':businessid' => $this->_client->businessid,
				':companyid' => $this->_client->getCompanyId()
			);
			//if(!empty($csv[6]))$args[':group_type'] = $csv[6];
			$stmnt->execute($args);
			$this->totals['updated'] += $stmnt->rowCount();
		} else {
			$query = 'SELECT menu_typeid FROM menu_type WHERE menu_posid=? AND businessid=?';
			$stmnt = $this->_client->db->prepare($query);
			$stmnt->execute(array($csv[2], $this->_client->businessid));
			$menu_typeid = $stmnt->fetchColumn(0);
			$stmnt->closeCursor();

			$stmnt = $this->_client->db->prepare('INSERT INTO menu_groups (pos_groupid, groupname, businessid, companyid, menu_typeid)
												VALUES(:pos_groupid, :groupname, :businessid, :companyid, :menu_typeid)');
			$args = array(
				':pos_groupid' => $csv[0],
				':groupname'   => $csv[1],
				':businessid'  => $this->_client->businessid,
				':companyid'   => $this->_client->getCompanyId(),
				':menu_typeid' => $menu_typeid
			);

			$stmnt->execute($args);
			$this->totals['new'] += $stmnt->rowCount();
		}

		$stmnt->closeCursor();
	}

	protected function _menuGroups(MappingType_Record $type, $csv)
	{
		// Update the old tables first
		/*if (!$this->_client->canSkip()) {
			if (empty($csv[2])) {
				$this->_menuCat1($type, $csv);
			} else {
				$this->_menuCat2($type, $csv);
			}
		}*/

		// Update the new tables
		if (!empty($csv[2])) {
			if(!empty($csv[4]))
			{
				$stmnt = $this->_client->db->prepare('SELECT id FROM menu_groups_new WHERE businessid = ? AND pos_id = ? AND group_type = ?');
				$stmnt->execute(array($this->_client->businessid, $csv[2], $csv[4]));
			}
			else
			{
				$stmnt = $this->_client->db->prepare('SELECT id FROM menu_groups_new WHERE businessid = ? AND pos_id = ? AND group_type IS NULL');
				$stmnt->execute(array($this->_client->businessid, $csv[2]));
			}
			$parent = $stmnt->fetchColumn(0);
			$stmnt->closeCursor();

			if(!empty($csv[3]))
			{
				$stmnt = $this->_client->db->prepare('INSERT INTO menu_groups_new
												(businessid, pos_id, parent_id, name, group_type)
												VALUES(:busid, :posid, :parent, :name, :group_type)
												ON DUPLICATE KEY UPDATE
												name=:name');
				$array = array(
					':busid' => $this->_client->businessid,
					':posid' => $csv[0],
					':name' => $csv[1],
					':parent' => $parent,
					':group_type' => $csv[3]
				);
			}
			else
			{
				$stmnt = $this->_client->db->prepare('INSERT INTO menu_groups_new
												(businessid, pos_id, parent_id, name, group_type)
												VALUES(:busid, :posid, :parent, :name, null)
												ON DUPLICATE KEY UPDATE
												name=:name');
				$array = array(':busid' => $this->_client->businessid, ':posid' => $csv[0], ':name' => $csv[1],
					':parent' => $parent);
			}
			$stmnt->execute($array);
			$rowCount = $stmnt->rowCount();
			if ($rowCount % 2) {
				$this->totals['new']++;
			} elseif ($rowCount != 0) {
				$this->totals['updated']++;
			}
			$stmnt->closeCursor();
		} else {
			if(!empty($csv[3]))
			{
				$tst = $this->_countRows('SELECT COUNT(id) FROM menu_groups_new WHERE businessid = ? AND pos_id = ? AND parent_id IS NULL AND group_type = ?', array($this->_client->businessid, $csv[0], $csv[3]));
			}
			else
			{
				$tst = $this->_countRows('SELECT COUNT(id) FROM menu_groups_new WHERE businessid = ? AND pos_id = ? AND parent_id IS NULL AND group_type IS NULL', array($this->_client->businessid, $csv[0]));
			}
			if ($tst) {
				if(!empty($csv[3]))
				{
					$stmnt = $this->_client->db->prepare('UPDATE menu_groups_new SET name = ? WHERE businessid = ? AND pos_id = ? AND parent_id IS NULL AND group_type = ?');
					$stmnt->execute(array($csv[1], $this->_client->businessid, $csv[0], $csv[3]));
				}
				else
				{
					$stmnt = $this->_client->db->prepare('UPDATE menu_groups_new SET name = ? WHERE businessid = ? AND pos_id = ? AND parent_id IS NULL AND group_type IS NULL');
					$stmnt->execute(array($csv[1], $this->_client->businessid, $csv[0]));
				}
				$this->totals['updated'] += $stmnt->rowCount();
				$stmnt->closeCursor();
			} else {
				if(!empty($csv[3]))
				{
					$stmnt = $this->_client->db->prepare('INSERT INTO menu_groups_new (businessid, pos_id, parent_id, name, group_type) VALUES(?, ?, NULL, ?, ?)');
					$stmnt->execute(array($this->_client->businessid, $csv[0], $csv[1], $csv[3]));
				}
				else
				{
					$stmnt = $this->_client->db->prepare('INSERT INTO menu_groups_new (businessid, pos_id, parent_id, name, group_type) VALUES(?, ?, NULL, ?, NULL)');
					$stmnt->execute(array($this->_client->businessid, $csv[0], $csv[1]));
				}
				$this->totals['new'] += $stmnt->rowCount();
				$stmnt->closeCursor();
			}
		}
	}

	protected function _menuItems(MappingType_Record $type, $csv)
	{
		/*
		if (!$this->_client->canSkip() && !empty($csv[2])) {
			$query = 'SELECT menu_typeid, menu_group_id FROM menu_groups WHERE pos_groupid=:pos_groupid AND businessid=:businessid' . (empty($csv[7]) ? '' : ' AND group_type=:group_type');
			$stmnt = $this->_client->db->prepare($query);
			$args = array(':pos_groupid' => $csv[2], ':businessid'=> $this->_client->businessid);
			if(!empty($csv[7]))$args[':group_type'] = $csv[7];
			$stmnt->execute($args);
			if (!($row = $stmnt->fetch(PDO::FETCH_NUM))) {
				$stmnt->closeCursor();
				$stmnt = $this->_client->db->prepare('SELECT menu_typeid, menu_posid FROM menu_type WHERE menu_posid = ? AND businessid = ?');
				$stmnt->execute(array($csv[2], $this->_client->businessid));
			}

			if (!($row = $stmnt->fetch(PDO::FETCH_NUM))) { $stmnt->closeCursor(); $this->totals['skipped']++; return; } // TODO: Temporary fix, replace when we find out what to do.
			list($menu_typeid, $menu_group_id) = $row;
			$stmnt->closeCursor();

			// Do the old tables first
			if ($this->_countRows('SELECT COUNT(menu_item_id) FROM menu_items WHERE posinvid=? AND companyid=? AND businessid=?', array($csv[0], $this->_client->getCompanyId(), $this->_client->businessid)) > 0) {
				$query = 'UPDATE menu_items SET item_name=:item_name, price=:price, menu_typeid=:menu_typeid, groupid=:groupid WHERE posinvid=:posinvid AND companyid=:companyid AND businessid=:businessid';
				$stmnt = $this->_client->db->prepare($query);
				$args = array(
					':posinvid'    => $csv[0],
					':item_name'   => $csv[1],
					':price'       => $csv[3],
					':companyid'   => $this->_client->getCompanyId(),
					':businessid'  => $this->_client->businessid,
					':menu_typeid' => $menu_typeid,
					':groupid'     => $menu_group_id
				);
				$stmnt->execute($args);
				$this->totals['updated'] += $stmnt->rowCount();
			} else {
				$query = 'INSERT INTO menu_items (posinvid, item_name, price, companyid, businessid, menu_typeid, groupid)
							VALUES(:posinvid,:item_name,:price,:companyid,:businessid,:menu_typeid,:groupid)';
				$stmnt = $this->_client->db->prepare($query);
				$args = array(
					':posinvid'    => $csv[0],
					':item_name'   => $csv[1],
					':price'       => $csv[3],
					':companyid'   => $this->_client->getCompanyId(),
					':businessid'  => $this->_client->businessid,
					':menu_typeid' => $menu_typeid,
					':groupid'     => $menu_group_id
				);
				try {
					$stmnt->execute($args);
				} catch (Exception $x) {
					var_dump($args);
					throw $x;
				}
				$this->totals['new'] += $stmnt->rowCount();
			}

			$stmnt->closeCursor();
		}*/

		// Now update the new tables
		if ($this->_client->posType()->isRM()) {
			$stmnt = $this->_client->db->prepare('SELECT id FROM menu_groups_new WHERE pos_id = ? AND businessid = ? AND parent_id IS NOT NULL');
		} else {
			$stmnt = $this->_client->db->prepare('SELECT id FROM menu_groups_new WHERE pos_id = ? AND businessid = ?');
		}
		$stmnt->execute(array($csv[2], $this->_client->businessid));
		$group = ($csv[2] != '0' ? $stmnt->fetchColumn(0) : false);
		$stmnt->closeCursor();

		$parent_posid = $csv[2];
		$query = 'INSERT INTO menu_items_new (businessid, pos_id, group_id, name, onhand, item_code, upc)
			VALUES(:bid, :posid, :group, :name, :onhand, :item_code, :upc)
			ON DUPLICATE KEY UPDATE name = :name, onhand = :onhand, item_code = :item_code, upc = :upc';
		$stmnt = $this->_client->db->prepare($query);
		$args = array(
			':bid'       => $this->_client->businessid,
			':posid'     => $csv[0],
			':group'	=> intval($group),
			':name'      => $csv[1],
			':onhand'    => $csv[4],
			':item_code' => $csv[5],
			':upc'       => $csv[6]
		);
		//if($parent_posid != "0")$args[':gid'] = $group;
		$stmnt->execute($args);

		$rowCount = $stmnt->rowCount();
		if ($rowCount % 2) {
			$this->totals['new']++;
		} elseif ($rowCount != 0) {
			$this->totals['updated']++;
		}
		//$id = $this->_client->db->lastInsertId();
		$stmnt->closeCursor();

		//if (empty($id)) {
			// get the id ... ugh
			$query = 'SELECT id FROM menu_items_new WHERE businessid = :businessid AND pos_id = :pos_id';
			$stmnt = $this->_client->db->prepare($query);
			$args = array(':businessid' => $this->_client->businessid, ':pos_id' => $csv[0]);
			$stmnt->execute($args);
			$id = $stmnt->fetchColumn(0);
			$stmnt->closeCursor();
		//}

		//kiosk inventory count
		//steve 9/7/10
		if($this->_client->inv == 1){
			$query = 'INSERT INTO inv_count_new (item_number, amount, date_sent) VALUES(:item_number, :amount, now())';
			$stmnt = $this->_client->db->prepare($query);
			$args = array(
				':item_number' => $id,
				':amount'      => $csv[4]
			);
			$stmnt->execute($args);
			$stmnt->closeCursor();
		}

		// add the price
		$query = 'INSERT INTO menu_items_price (businessid, menu_item_id, pos_price_id, price) VALUES(:bid, :mid, :ppid, :price) ON DUPLICATE KEY UPDATE price = :price';
		$stmnt = $this->_client->db->prepare($query);
		$args = array(
			':bid'   => $this->_client->businessid,
			':mid'   => $id,
			':ppid'  => 1,
			':price' => $csv[3]
		);
		$stmnt->execute($args);
		$rowCount = $stmnt->rowCount();
		if ($rowCount % 2) {
			$this->totals['new']++;
		} elseif ($rowCount != 0) {
			$this->totals['updated']++;
		}
		$stmnt->closeCursor();
	}

	protected function _paymentDetail(MappingType_Record $type, $csv)
	{
		// If the query does not exist, create it
		if (empty($this->_query)) {
			$this->_query = 'INSERT INTO payment_detail
												(
													businessid,
													check_number,
													payment_type,
													received,
													base_amount,
													tip_amount,
													scancode
												)';
		}

		// Add the items to the array to be added to the database
		$this->_args[] = implode(',', array(
			$this->_client->db->quote($this->_client->businessid),
			$this->_client->db->quote($csv[0]),
			$this->_client->db->quote($csv[1]),
			$this->_client->db->quote($csv[2]),
			$this->_client->db->quote($csv[3]),
			$this->_client->db->quote($csv[4]),
			$this->_client->db->quote(((isset($csv[5]))? $csv[5] : ''))
		));
	}

	/**
	 * Read from the CSV file
	 */
	protected function _read()
	{
		if (feof($this->_fp)) {
			return(false);
		}

		return(fgetcsv($this->_fp));
	}

	protected function _reOrder(MappingType_Record $type, $csv)
	{
		$stmnt = $this->_client->db->prepare('UPDATE menu_items_new SET item_code = :item_code, upc = :upc, reorder = :reorder WHERE businessid = :bid AND pos_id = :id');
		$args = array(
			':bid'       => $this->_client->businessid,
			':id'        => $csv[0],
			':item_code' => $csv[2],
			':upc'       => $csv[1],
			':reorder'   => $csv[3]
		);
		$stmnt->execute($args);
		$stmnt->closeCursor();
	}

	protected function _cardBalances(MappingType_Record $type, $csv)
	{
		$stmnt = $this->_client->db->prepare('UPDATE customer SET balance = :balance WHERE businessid = :bid AND scancode = :scancode');
		$args = array(
			':bid'      => $this->_client->businessid,
			':scancode' => $csv[0],
			':balance'  => $csv[1]
		);
		$stmnt->execute($args);
		$stmnt->closeCursor();
	}

	/**
	 * No longer used with the skip_client field in the database
	private function _skipClient()
	{
		return(in_array($this->_client->client_programid, $this->_skipClients));
	}
	*/

	protected function _stats(MappingType_Record $type, $csv)
	{
		if ($this->_countRows('SELECT COUNT(statid) FROM stats WHERE number = ? AND businessid = ?', array($csv[0], $this->_client->businessid)) > 0) {
			$sql = 'UPDATE stats SET `date` = ?, labor = ?, labor_hours = ?, void = ?, voidcount = ? WHERE businessid = ? AND number = ?';
			$stmnt = $this->_client->db->prepare($sql);
			$stmnt->execute(array($csv[1], $csv[3], $csv[2], $csv[5], $csv[4], $this->_client->businessid, $csv[0]));
			$this->totals['updated'] += $stmnt->rowCount();
		} else {
			$sql = 'INSERT INTO stats (businessid, companyid, `date`, number, labor, labor_hour, void, voidcount) VALUES(?, ?, ?, ?, ?, ?)';
			$stmnt = $this->_client->db->prepare($sql);
			$stmnt->execute(array($this->_client->businessid, $this->_client->getCompanyId(), $csv[1], $csv[0], $csv[3], $csv[2], $csv[5], $csv[4]));
			$this->totals['new'] += $stmnt->rowCount();
		}

		$stmnt->closeCursor();
	}
}
