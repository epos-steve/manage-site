<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once('Manage/Abstract.php');

/**
 * Description of Record
 *
 * @author eric
 */
abstract class Manage_Abstract_Record extends Manage_Abstract
{
	protected $_vars = array();

	public function __construct($id = 0)
	{
		parent::__construct();

		if (!empty($id)) {
			if (empty($this->init)) {
				// bugger, can't continue
				throw new Exception(__CLASS__.'::$init is not set');
			} else {
				$stmnt = $this->_db->prepare($this->init);
				$stmnt->execute(array(':id' => $id));
				$stmnt->setFetchMode(PDO::FETCH_INTO, $this);
				$stmnt->fetch();
				$stmnt->closeCursor();
			}
		}
	}

	public function __get($name)
	{
		if (property_exists($this, '_'.$name)) {
			return($this->{'_'.$name});
		} else {
			return((isset($this->_vars[$name]))? $this->_vars[$name] : null);
		}
	}

	public function __isset($name)
	{
		if (property_exists($this, '_'.$name)) {
			return((empty($this->{'_'.$name}))? false : true);
		} else {
			return((isset($this->_vars[$name]))? true : false);
		}
	}

	public function __set($name, $value)
	{
		if (property_exists($this, '_'.$name)) {
			$this->{'_'.$name} = $value;
		} else {
			$this->_vars[$name] = $value;
		}
	}
}
