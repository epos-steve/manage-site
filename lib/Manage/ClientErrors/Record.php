<?php
require_once('Manage/Abstract/Record.php');

/**
 * Description of Record
 *
 * @author Eric
 */
class ClientErrors_Record extends Manage_Abstract_Record
{
	public function markRead()
	{
		$sql = 'UPDATE client_errors SET isRead = 1 WHERE id = ?';
		$stmnt = $this->db->prepare($sql);
		$stmnt->execute(array($this->id));
	}
}

