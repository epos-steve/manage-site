<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Record
 *
 * @author Eric
 */
class Manage_Schedule_Record extends Manage_Abstract_Record
{
	protected $init = 'SELECT * FROM client_schedule WHERE id = :id';

	public function dayToString()
	{
		switch ($this->day) {
			case 1:
				$day = 'Monday';
				break;
			case 2:
				$day = 'Tuesday';
				break;
			case 3:
				$day = 'Wednesday';
				break;
			case 4:
				$day = 'Thursday';
				break;
			case 5:
				$day = 'Friday';
				break;
			case 6:
				$day = 'Saturday';
				break;
			case 7:
				$day = 'Sunday';
				break;
			default:
				$day = 'Every Day';
				break;
		}

		return($day);
	}
}
