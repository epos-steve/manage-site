<?php
class Manage_DB extends SiTech_DB
{
	protected $_lastKeepAlive;

	static protected $_instance = array();

	public function __construct($section = 'database')
	{
		if (!is_array($section)) {
			$config = Manage_ConfigParser::singleton();
			parent::__construct($config->items($section));
		} else {
			parent::__construct($section);
		}

		$this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		// Hopefully fixes mysql has gone away error
		$this->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);
		$this->_lastKeepAlive = time();
	}

	public function keepAlive()
	{
		// Every 30 seconds
		if ((time() - $this->_lastKeepAlive) > 30) {
			$this->query('SELECT 1')->closeCursor();
			$this->_lastKeepAlive = time();
		}
	}

	public static function singleton($section = 'database')
	{
		if (empty(self::$_instance[$section])) {
			self::$_instance[$section] = new Manage_DB($section);
		}

		return(self::$_instance[$section]);
	}
}

