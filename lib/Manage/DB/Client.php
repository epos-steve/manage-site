<?php
/**
 * This class is used for the database connection for each manage client. This
 * class exists primairly to easily overwrite the database name itself so all
 * client queries make use of the proxy while still using the correct database.
 *
 * @author Eric Gach <eric@essential-elements.net>
 */
class Manage_DB_Client extends Manage_DB_Proxy
{
	/**
	 * @var ClientProgramsModel
	 */
	public static $client;

	public function __construct($config = 'treat db', array $writers = array(), array $readers = array(), $driver = 'SiTech_DB_Driver_MySQL', array $options = array())
	{
		parent::__construct($config, $writers, $readers, $driver, $options);
	}

	public static function getDsn(array $config)
	{
		if (!empty(static::$client)) {
			$config['database'] = static::$client->db_name;
		}

		return(parent::getDsn($config));
	}

	protected static function _configParameters($section = 'database')
	{
		$ret = parent::_configParameters($section);
		if (!empty(static::$client)) {
			$ret['config']['database'] = static::$client->db_name;
		}

		return($ret);
	}
}
