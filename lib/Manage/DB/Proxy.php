<?php

class Manage_DB_Proxy extends \EE\DB
{
	protected $_lastKeepAlive;

	static protected $_instance = array();

	public function __construct($config, array $writers = array(), array $readers = array(), $driver = 'SiTech_DB_Driver_MySQL', array $options = array())
	{
		if (!is_array($config)) {
			$data = static::_configParameters($config);
			parent::__construct($data['config'], $data['writers'], $data['readers'], $driver, $options);
		} else {
			parent::__construct($config, $writers, $readers, $driver, $options);
		}

		$this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		// Hopefully fixes mysql has gone away error
		$this->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);
		$this->_lastKeepAlive = time();
	}

	public function keepAlive()
	{
		// Every 30 seconds
		if ((time() - $this->_lastKeepAlive) > 30) {
			$this->_readConn->query('SELECT 1')->closeCursor();
			$this->_writeConn->query('SELECT 1')->closeCursor();
			$this->_lastKeepAlive = time();
		}
	}

	public static function singleton($section = 'database', SiTech_ConfigParser $configParser = NULL)
	{
		if ( !isset( static::$_instance[$section])) {
			$data = static::_configParameters($section);
			static::$_instance[$section] = new static($data['config'], $data['writers'], $data['readers']);
			if ($data['config']['driver'] == 'mysql')
			{
				static::$_instance[$section]->setAttribute( \PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true );
			}
		}

		return static::$_instance[$section];
	}

	protected static function _configParameters($section = 'database')
	{
		$config = Manage_ConfigParser::singleton()->items( $section );

		if ( ! array_key_exists('driver', $config) )
		{
			$config['driver'] = 'mysql';
		}

		$config['dsn'] = static::getDsn( array_merge($config, array('host' => '%s')) );

		if (
			array_key_exists( 'username', $config )
			&& !array_key_exists( 'user', $config )
		)
		{
			$config['user'] = $config['username'];
		}

		$writers = array( $config['host'] );
		if ( array_key_exists( 'ro_host', $config ) )
		{
			$readers = $config['ro_host'];
		}
		else
		{
			$readers = $writers;
		}

		return array(
			'config' => $config,
			'writers' => $writers,
			'readers' => $readers
		);
	}
	
}
