<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DetailWriter
 *
 * @author eric
 */
class Manage_DB_Statement_DetailWriter extends SiTech_DB_Statement_FileWriter
{
	public function execute(array $input_array = array())
	{
		if (substr($this->queryString, 0, 6) == 'SELECT') {
			return(parent::execute($input_array, true));
		} else {
			return(parent::execute($input_array));
		}
	}
}
