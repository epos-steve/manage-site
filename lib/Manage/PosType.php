<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PosType
 *
 * @author Eric
 */
class Manage_PosType extends Manage_Abstract
{
	public function getAll()
	{
		$stmnt = $this->_db->query('SELECT * FROM pos_type');
		$stmnt->setFetchMode(PDO::FETCH_CLASS, 'Manage_PosType_Record');
		return($stmnt->fetchAll());
	}
}
