<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ViViPOS
 *
 * @author eric
 */
class Manage_Controller_ViViPOS extends Manage_Controller_Abstract
{
	public function __construct(SiTech_Uri $uri) {
		$this->_db = Manage_DB::singleton('vivipos db');
		parent::__construct($uri);
	}
	/**
	 * This is our centeralized location, so we always need to return the same
	 * terminal name to all terminals that report to us.
	 *
	 * @return bool
	 */
	public function auth()
	{
		echo 'EESync';
		return(false);
	}

	public function index()
	{
		echo 'index';
		return(false);
	}
}

/**
 * we need to set this specifically to this class because anything using the
 * vivipos databases needs to point to another host now.
 */
//Manage_Controller_ViViPOS::db(Manage_DB::singleton('vivipos db'));
