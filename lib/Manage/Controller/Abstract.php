<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Abstract
 *
 * @author eric
 */
abstract class Manage_Controller_Abstract extends SiTech_Controller_Abstract
{
	/**
	 * Database instance holder.
	 *
	 * @var Manage_DB
	 */
	protected $_db;

	/**
	 *
	 * @var Manage_Login_Record
	 */
	protected $_user;

	/**
	 * View instance holder.
	 *
	 * @var Manage_Template
	 */
	protected $_view;

	public function __construct(SiTech_Uri $uri)
	{
		SiTech_Loader::loadModel('login');
		if (empty($this->_db)) $this->_db = Manage_DB::singleton();
		$this->_user = LoginModel::getCurrentUser();
		$this->_view = new Manage_Template();
		$this->_view->assign('user', $this->_user);
		parent::__construct($uri);
	}
}
