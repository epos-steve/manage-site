<?php

/**
 * Description of Record
 *
 * @author Eric
 */
class Manage_ClientPrograms_Record extends Manage_Abstract_Record
{
	protected $_companyId;

	/**
	 * PDO object for the client database. This needs to be accessed from the
	 * outside by $obj->db so that the connection is only made when its needed.
	 *
	 * @var PDO
	 */
	protected $_cdb;
	protected $init = 'SELECT * FROM client_programs WHERE client_programid = :id';

	public function __get($name)
	{
		if ($name == 'db') {
			if (!is_object($this->_cdb)) {
				$this->_cdb = new PDO('mysql:host='.$this->db_ip.';dbname='.$this->db_name, $this->db_user, $this->db_pass);
				$this->_cdb->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}

			return($this->_cdb);
		} else {
			return(parent::__get($name));
		}
	}

	public function getCompanyId()
	{
		if (empty($this->companyId)) {
			$stmnt = $this->_cdb->prepare('SELECT companyid FROM business WHERE businessid = ?');
			$stmnt->execute(array($this->businessid));
			$this->companyId = $stmnt->fetchColumn(0);
			$stmnt->closeCursor();
		}

		return($this->companyId);
	}

	public function getCredits()
	{
		$ret = array();

		try {
			$sql = 'SELECT creditid,creditname FROM credits WHERE businessid = '.$this->businessid.' ORDER BY credittype,creditname';
			$stmnt = $this->db->prepare($sql);
			$stmnt->execute();
			$ret = $stmnt->fetchAll(PDO::FETCH_ASSOC);
		} catch (PDOException $e) {
			if (isset($stmnt)) {
				$stmnt->closeCursor();
			}
		}

		return($ret);
	}

	public function getDebits()
	{
		$ret = array();

		try {
			$sql = 'SELECT debitid,debitname FROM debits WHERE businessid = '.$this->businessid.' ORDER BY debittype,debitname';
			$stmnt = $this->db->prepare($sql);
			$stmnt->execute();
			$ret = $stmnt->fetchAll(PDO::FETCH_ASSOC);
		} catch (PDOException $e) {
			if (isset($stmnt)) {
				$stmnt->closeCursor();
			}
		}

		return($ret);
	}

	public function getSchedules()
	{
		$schedules = new Manage_Schedule();
		return($schedules->getByClient($this->client_programid));
	}

	/**
	 * Check if the current client has any children records. If there are it will
	 * return true, otherwise false.
	 *
	 * @return bool
	 */
	public function hasChildren()
	{
		$stmnt = $this->_db->prepare('SELECT COUNT(client_programid) AS total FROM client_programs WHERE parent = :id');
		$stmnt->execute(array(':id' => $this->client_programid));
		$ret = $stmnt->fetchColumn();
		$stmnt->closeCursor();
		return((bool)$ret);
	}

	public function hasErrors()
	{
		$stmnt = $this->_db->prepare('SELECT COUNT(id) FROM client_errors WHERE client_id = ? AND isRead = 0');
		$stmnt->execute(array($this->client_programid));
		$ret = $stmnt->fetchColumn();
		$stmnt->closeCursor();
		return((bool)$ret);
	}

	public function logError($msg, $trace)
	{
		$ret = false;

		$stmnt = $this->_db->prepare('INSERT INTO client_errors (client_id, error_message, error_trace) VALUES(?, ?, ?)');
		if ($stmnt->execute(array($this->client_programid, $msg, $trace))) {
			$ret = (bool)$stmnt->rowCount();
		}

		$stmnt->closeCursor();
		return($ret);
	}

	/**
	 * Create a new object for the pos_type field. This uses the PosType_Record
	 * and creates a new object using the ID.
	 *
	 * @return PosType_Record
	 */
	public function posType()
	{
		if (!is_object($this->pos_type)) {
			require_once('Manage/PosType/Record.php');
			$this->pos_type = new Manage_PosType_Record($this->pos_type);
		}

		return($this->pos_type);
	}

	/**
	 * Create a new object from the receive_status field. This uses the
	 * ReceiveStatus_Record and creates the object from the ID stored here.
	 *
	 * @return ReceiveStatus_Record
	 */
	public function receiveStatus()
	{
		require_once('Manage/ReceiveStatus/Record.php');
		$status = new ReceiveStatus_Record($this->receive_status);
		return($status);
	}

	/**
	 * Just a helper function to format the receive_time field. If the timestamp
	 * is 0000-00-00 00:00:00 this returns an empty string.
	 *
	 * @return string
	 */
	public function receiveTime()
	{
		return($this->receive_time == '0000-00-00 00:00:00')? '' : $this->receive_time;
	}

	/**
	 * Update the table to show the client tried sending an update. Set $error
	 * to true if it failed.
	 *
	 * @param $error bool Set to true when an error occours.
	 * @return bool
	 */
	public function setSendStatus($error = false)
	{
		if ($error === true) {
			$sql = 'UPDATE client_programs SET receive_status = 3 WHERE client_programid = :id';
		} else {
			$sql = 'UPDATE client_programs SET send_status = 0, send_schedule = 0, receive_status = 2, receive_time = NOW(), inv = 0 WHERE client_programid = :id';
		}

		$stmnt = $this->_db->prepare($sql);
		$stmnt->execute(array(':id' => $this->client_programid));
		$ret = (bool)$stmnt->rowCount();
		$stmnt->closeCursor();
		return($ret);
	}

	/**
	 * Doesn't do much of anything yet... just returns some HTML for an error
	 * image if no schedule is setup yet.
	 *
	 * @return string
	 */
	public function showSchedule()
	{
		$sql = "SELECT COUNT(id) AS totalcount FROM client_schedule WHERE client_programid = '$this->client_programid'";
		$stmnt = $this->_db->query($sql);

		$totalcount = $stmnt->fetchColumn();
		$stmnt->closeCursor();

		if ($totalcount == 0) return('<img src="error.gif" height="16" width="16" border="0" alt="No Schedule Setup">');
		else return('');
	}

	/**
	 * Return HTML for an image of the status type for the current client. This
	 * is based uppon the send_status field.
	 *
	 * @return string
	 */
	public function showSendStatus()
	{
		if ($this->send_status == 2) return('<img class="current" src="clock.gif" height="16" width="16" alt="Waiting for Data" title="Waiting for Data">');
		elseif ($this->send_status == 1) return('<img class="current" src="gear.gif" height="18" width="16" alt="Waiting for Update" title="Waiting for Update">');
		else return('<img class="current" src="blank.gif" height="16" width="16">');
		//else return('<a href="update.php?pid='.$this->client_programid.'"><img src="wrench.gif" border="0" height="17" width="16" alt="Send Updates" title="Send Updates"></a>');
	}
	
	public function showSendStatus2()
	{
		if ($this->send_status == 2) return('clock.gif');
		elseif ($this->send_status == 1) return('gear.gif');
		else return('blank.gif');
	}

	/**
	 * Return the image name for the status of the last run. This is based off
	 * of the time difference retreived from the timeDiff() method.
	 *
	 * @return string
	 * @see timeDiff
	 */
	public function statusImg()
	{
		$send = $this->timeDiff('send');
		$recv = $this->timeDiff('receive');
		if (substr($send,0,2)>=1 || substr($recv,0,2)>=48 || $this->send_time == "0000-00-00 00:00:00") return('reddot.gif');
		elseif(substr($send,3,2)>=15 || substr($recv,0,2)>=24) return('yellowdot.gif');
		else return('greendot.gif');
	}

	/**
	 * Run a query against the database to find the time difference between now
	 * and the last send time from the client.
	 *
	 * @return string
	 */
	public function timeDiff($field)
	{
		$sql = 'SELECT TIMEDIFF(now(),'.$field.'_time) AS status FROM client_programs WHERE client_programid = :id';
		$stmnt = $this->_db->prepare($sql);
		$stmnt->execute(array(':id' => $this->client_programid));
		$ret = $stmnt->fetchColumn();
		$stmnt->closeCursor();
		return($ret);
	}
}
