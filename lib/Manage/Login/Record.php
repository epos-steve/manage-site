<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Record
 *
 * @author Eric Gach <eric@essential-elements.net>
 */
class Manage_Login_Record extends Manage_Abstract_Record
{
	protected $_id;
	protected $_isAdmin;
	protected $_password;
	protected $_username;

	public function __construct($user = 0, $pass = null)
	{
		parent::__construct();

		if (ctype_digit($user)) {
			$sql = 'SELECT loginid AS id, username, password, isAdmin FROM login WHERE loginid = ? AND MD5(CONCAT(username, password)) = ?';
			$stmnt = $this->_db->prepare($sql);
			$stmnt->bindValue(1, $user, PDO::PARAM_INT);
			
		} else {
			$sql = 'SELECT loginid AS id, username, password, isAdmin FROM login WHERE username = ? AND password = ?';
			$stmnt = $this->_db->prepare($sql);
			$stmnt->bindValue(1, $user, PDO::PARAM_STR);
		}

		$stmnt->bindValue(2, $pass, PDO::PARAM_STR);
		$stmnt->setFetchMode(PDO::FETCH_INTO, $this);
		$stmnt->execute();
		if ($stmnt->fetch()) {
			setcookie('id', $this->_id);
			setcookie('hash', md5($this->_username.$this->_password));
		}
	}

	public function isLoggedIn()
	{
		if (empty($this->_id)) {
			return(false);
		} else {
			return(true);
		}
	}

	public function isAdmin()
	{
		return((bool)$this->_isAdmin);
	}

	public function requireLogin()
	{
		if (!$this->isLoggedIn()) {
			header('Location: '.SITECH_BASEURI.'/dashboard/login');
			exit;
		}
	}
}
