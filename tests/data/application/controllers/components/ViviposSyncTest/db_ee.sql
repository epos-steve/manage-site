-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: writer
-- Generation Time: Jan 03, 2013 at 11:21 AM
-- Server version: 5.1.41
-- PHP Version: 5.3.10-1ubuntu3.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mburris_businesstrack`
--
CREATE DATABASE IF NOT EXISTS `mburris_businesstrack` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `mburris_businesstrack`;

DELIMITER $$
--
-- Procedures
--
DROP PROCEDURE IF EXISTS `change_tal_cost`$$
CREATE PROCEDURE `change_tal_cost`()
BEGIN

	DECLARE bid INT;
	DECLARE done INT DEFAULT FALSE;
	
	DECLARE load_menu_items_data CURSOR FOR SELECT businessid FROM jsantos_ee.menu_business_cost_updates ORDER BY businessid ASC;
	
	OPEN load_menu_items_data;
	BEGIN
		DECLARE CONTINUE handler FOR NOT FOUND SET done = TRUE;
		
		done_label: LOOP
			FETCH load_menu_items_data INTO bid;

			IF done THEN 
				LEAVE done_label;
			END IF;
			
			UPDATE menu_cost_updates SET businessid = bid;
			UPDATE menu_cost_updates m1 SET m1.menu_item_id = (SELECT m2.id FROM menu_items_new m2 WHERE (m2.businessid = m1.businessid) AND (m2.item_code = m1.item_code) AND (m2.upc = m1.upc) LIMIT 1) WHERE 1;
			UPDATE menu_items_price m1 SET m1.cost = (SELECT m2.cost FROM menu_cost_updates m2 WHERE m2.menu_item_id = m1.menu_item_id LIMIT 1) WHERE m1.businessid = bid;

			UPDATE menu_cost_updates SET businessid = NULL;
			UPDATE menu_cost_updates SET menu_item_id = NULL;
			
				
		END LOOP;
		
	END;

END$$

DROP PROCEDURE IF EXISTS `checkdetail_update_item_number`$$
CREATE PROCEDURE `checkdetail_update_item_number`(bid INT, check_number BIGINT)
BEGIN
	DECLARE businessid_value, item_number_value, pos_id_value, total_item_numbers INT DEFAULT 0;
	DECLARE itemnumber_aka_posid, pos_id_value_old, pos_id_value_new INT DEFAULT 0;
	DECLARE upc_value VARCHAR(30);

	DECLARE checkdetail_item_numbers CURSOR FOR 
    SELECT DISTINCT item_number 
    FROM checkdetail 
    WHERE (businessid = bid) AND (bill_number < check_number) 
    AND (item_number NOT IN (60, 61, 62, 223, 356662, 365378, 369152, 369528, 369565, 369597)) 
    ORDER BY item_number ASC;
	OPEN checkdetail_item_numbers;
	BEGIN
		 DECLARE EXIT HANDLER FOR SQLSTATE '02000' BEGIN END;

		 LOOP
			FETCH checkdetail_item_numbers INTO item_number_value;
            SELECT item_number_value INTO pos_id_value_old;
            SELECT COUNT(*) INTO total_item_numbers FROM checkdetail WHERE (businessid = bid) AND (bill_number < check_number) AND (item_number = item_number_value) GROUP BY item_number;
            SELECT upc INTO upc_value FROM menu_items_new_nordam_ins_main WHERE (businessid = bid) AND (pos_id = pos_id_value_old) LIMIT 1;
            SELECT pos_id INTO pos_id_value_new FROM menu_items_new WHERE (businessid = bid) AND (upc = upc_value);
            
            -- SELECT item_number_value, total_item_numbers, upc_value, pos_id_value_old, pos_id_value_new;
            UPDATE checkdetail SET item_number = pos_id_value_new WHERE (item_number = pos_id_value_old) AND (businessid = bid) AND (bill_number <= check_number);
		END LOOP;
	END;
	CLOSE checkdetail_item_numbers;
END$$

DROP PROCEDURE IF EXISTS `clear_menu`$$
CREATE PROCEDURE `clear_menu`(bid INT)
BEGIN

	DELETE FROM menu_items_new WHERE businessid = bid;
	DELETE FROM menu_items_price WHERE businessid = bid;
	DELETE FROM menu_groups_new WHERE businessid = bid;

END$$

DROP PROCEDURE IF EXISTS `create_memory_tbls`$$
CREATE PROCEDURE `create_memory_tbls`()
BEGIN
	INSERT IGNORE INTO posreports_business SELECT * FROM business ORDER BY businessid ASC;
	INSERT IGNORE INTO posreports_checkdetail SELECT * FROM checkdetail ORDER BY id ASC;
	INSERT IGNORE INTO posreports_checks SELECT * FROM checks ORDER BY id ASC;
	INSERT IGNORE INTO posreports_company SELECT * FROM company ORDER BY companyid ASC;
	INSERT IGNORE INTO posreports_district SELECT * FROM district ORDER BY districtid ASC;
	INSERT IGNORE INTO posreports_inv_count_vend SELECT * FROM inv_count_vend ORDER BY id ASC;
	INSERT IGNORE INTO posreports_machine_bus_link SELECT * FROM machine_bus_link ORDER BY id ASC;
	INSERT IGNORE INTO posreports_menu_items_new SELECT * FROM menu_items_new ORDER BY id ASC;
	INSERT IGNORE INTO posreports_menu_items_price SELECT * FROM menu_items_price ORDER BY id ASC;	
        INSERT IGNORE INTO posreports_payment_detail SELECT * FROM payment_detail ORDER BY id ASC;	


END$$

DROP PROCEDURE IF EXISTS `customer_balance_inquiry`$$
CREATE PROCEDURE `customer_balance_inquiry`(bid INT)
BEGIN
	DECLARE cust_scancode, pd_payment_type VARCHAR(30);
	DECLARE cust_balance, checks_total, calculated_balance_total, calculated_balance_1, calculated_balance_2, out_of_balance_amount DECIMAL(6,2) DEFAULT 0.00;
	DECLARE cust_customerid, cid, oob INT DEFAULT 0;
	
	DECLARE done INT DEFAULT FALSE;
	
	DECLARE scancode_balance CURSOR FOR SELECT scancode, balance FROM jsantos_ee.customer WHERE businessid = bid AND NOT IFNULL(scancode, '') = '' ORDER BY scancode ASC;
	OPEN scancode_balance;
	BEGIN
		DECLARE CONTINUE handler FOR NOT FOUND SET done = TRUE;
		SELECT companyid INTO cid FROM jsantos_ee.business WHERE businessid = bid LIMIT 1;
		done_label: LOOP
			FETCH scancode_balance INTO cust_scancode, cust_balance;
			IF done THEN 
				LEAVE done_label;
			END IF;

			SELECT customerid INTO cust_customerid FROM jsantos_ee.customer WHERE scancode = cust_scancode;

			SELECT customer_checks_total(cust_scancode) INTO calculated_balance_1;
			SELECT customer_other_total(cust_customerid) INTO calculated_balance_2;
			
			SELECT (calculated_balance_1 + calculated_balance_2) * -1 INTO calculated_balance_total;
			
			IF (cust_balance != calculated_balance_total) THEN
				SELECT 1 INTO oob;
				SELECT (cust_balance - calculated_balance_total) INTO out_of_balance_amount;
			ELSE
				SELECT 0 INTO oob;
				SELECT 0 INTO out_of_balance_amount;
			END IF;
			
			INSERT INTO jsantos_ee.customer_z_tracker (companyid, businessid, customerid, scancode, stored_balance, calculated_balance, oob_amount, out_of_balance) VALUES (cid, bid, cust_customerid, cust_scancode, cust_balance, calculated_balance_total, out_of_balance_amount, oob);

		END LOOP;
	END;
	CLOSE scancode_balance;
END$$

DROP PROCEDURE IF EXISTS `customer_checks_total`$$
CREATE PROCEDURE `customer_checks_total`(cust_scancode VARCHAR(30))
BEGIN
	DECLARE pd_payment_type VARCHAR(30);
	DECLARE checks_total, calculated_balance DECIMAL(6,2) DEFAULT 0.00;
	DECLARE cust_customerid INT DEFAULT 0;
	
	DECLARE done INT DEFAULT FALSE;
	
	DECLARE customer_checks CURSOR FOR SELECT customer.customerid, checks.total, payment_detail.payment_type FROM checks JOIN payment_detail ON payment_detail.check_number = checks.check_number AND payment_detail.businessid = checks.businessid AND payment_detail.scancode = cust_scancode JOIN customer USING (scancode) JOIN business ON business.businessid = checks.businessid WHERE checks.bill_datetime BETWEEN '2000-01-01 00:00:00' AND '2020-01-01 00:00:00' ORDER BY bill_datetime DESC;
	
	OPEN customer_checks;
	BEGIN
		DECLARE CONTINUE handler FOR NOT FOUND SET done = TRUE;
		done_label: LOOP
			FETCH customer_checks INTO cust_customerid, checks_total, pd_payment_type;
			IF done THEN 
				LEAVE done_label;
			END IF;
			
--			SELECT cust_scancode, cust_customerid, checks_total, pd_payment_type;
			
			if (pd_payment_type = 1) OR (pd_payment_type = 4) OR (pd_payment_type = 6) OR (pd_payment_type = 'Promotion') OR (pd_payment_type = 'Credit Card Transaction') OR (pd_payment_type = "Company Kitchen Refund") OR (pd_payment_type = "Transfer") OR (pd_payment_type = "CK Adjust") THEN
				SELECT (checks_total * -1) INTO checks_total;
			END IF;
			
			SELECT (calculated_balance + checks_total) INTO calculated_balance;
			
		END LOOP;	
	END;
	SELECT calculated_balance AS calculated_balance;
END$$

DROP PROCEDURE IF EXISTS `fix_inventory_variance`$$
CREATE PROCEDURE `fix_inventory_variance`(cid INT, date_value TIMESTAMP)
BEGIN
	DECLARE total_counts, is_inventoried, fills, bid INT;
	DECLARE machine VARCHAR(10);
	DECLARE itemcode_value VARCHAR(15);
	DECLARE date_time_value, date_time_minus_1_minute, date_time_plus_1_minute TIMESTAMP;
	DECLARE done INT DEFAULT FALSE;
	  
	DECLARE load_menu_items_data CURSOR FOR SELECT lmi.min_item_code, lmi.businessid FROM jsantos_ee.load_menu_items lmi WHERE lmi.companyid = cid ORDER BY lmi.businessid ASC, lmi.min_name ASC;
	OPEN load_menu_items_data;
	BEGIN
		DECLARE CONTINUE handler FOR NOT FOUND SET done = TRUE;

		done_label: LOOP
			SET done = FALSE;
			FETCH load_menu_items_data INTO itemcode_value, bid;
			IF done THEN 
				LEAVE done_label;
			END IF;

			SELECT COUNT(*) INTO total_counts FROM jsantos_ee.inv_count_vend icv JOIN jsantos_ee.machine_bus_link mbl ON icv.machine_num = mbl.machine_num AND mbl.businessid = bid WHERE (item_code = itemcode_value) AND (is_inv IN(0,1)) AND (date_time <= date_value);
			
			IF total_counts > 0 THEN

				SELECT icv.machine_num, icv.date_time, icv.fills, icv.is_inv INTO machine, date_time_value, fills, is_inventoried FROM jsantos_ee.inv_count_vend icv JOIN jsantos_ee.machine_bus_link mbl ON icv.machine_num = mbl.machine_num AND mbl.businessid = bid WHERE (item_code = itemcode_value) AND (is_inv IN(0,1)) ORDER BY date_time ASC LIMIT 1;
				
				IF is_inventoried = 0 THEN

					SELECT date_time_value - INTERVAL 1 MINUTE INTO date_time_minus_1_minute;
					INSERT INTO jsantos_ee.inv_count_vend (machine_num, date_time, item_code, onhand, is_inv) VALUES (machine, date_time_minus_1_minute, itemcode_value, 0, 1);

--					SELECT date_time_value + INTERVAL 1 MINUTE INTO date_time_plus_1_minute;
--					INSERT INTO jsantos_ee.inv_count_vend (machine_num, date_time, item_code, onhand, is_inv) VALUES (machine, date_time_plus_1_minute, itemcode_value, fills, 1);
				
				END IF;

			END IF;			
		END LOOP;
	END;
	CLOSE load_menu_items_data;			
		
END$$

DROP PROCEDURE IF EXISTS `generateCalendarTable`$$
CREATE DEFINER=`mburris_smartin`@`192.168.1.%` PROCEDURE `generateCalendarTable`(
	var_DateStart date,
	var_DateEnd date
)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
begin
	declare var_DateCurrent date;
	
	set var_DateCurrent = var_DateStart;
	
	drop table if exists calendarTable;
	create table calendarTable(
		calendarDate date primary key not null,
		weekMonday date not null,
		weekTuesday date not null,
		weekWednesday date not null,
		weekThursday date not null,
		weekFriday date not null,
		weekSaturday date not null,
		weekSunday date not null,
		isWeekend boolean,
		monthStart date not null,
		monthEnd date not null,
		yearStart date not null,
		yearEnd date not null,
		dateMonth tinyint unsigned not null,
		dateYear smallint unsigned not null
	);
	
	while var_DateCurrent < var_DateEnd do
		insert into calendarTable set
			calendarDate = var_DateCurrent,
			weekMonday = DATE_ADD( var_DateCurrent, 
				INTERVAL (  
					IF( DAYOFWEEK( var_DateCurrent ) > 2, 9, 2 )
					- DAYOFWEEK( var_DateCurrent )
				) DAY ),
			weekTuesday = DATE_ADD( var_DateCurrent,
				INTERVAL ( 
					IF( DAYOFWEEK( var_DateCurrent ) > 3, 10, 3 )
					- DAYOFWEEK( var_DateCurrent )
				) DAY ),
			weekWednesday = DATE_ADD( var_DateCurrent, 
				INTERVAL ( 
					IF( DAYOFWEEK( var_DateCurrent ) > 4, 11, 4 )
					- DAYOFWEEK( var_DateCurrent )
				) DAY ),
			weekThursday = DATE_ADD( var_DateCurrent,
				INTERVAL ( 
					IF( DAYOFWEEK( var_DateCurrent ) > 5, 12, 5 )
					- DAYOFWEEK( var_DateCurrent )
				) DAY ),
			weekFriday = DATE_ADD( var_DateCurrent, 
				INTERVAL ( 
					IF( DAYOFWEEK( var_DateCurrent ) > 6, 13, 6 )
					- DAYOFWEEK( var_DateCurrent )
				) DAY ),
			weekSaturday = DATE_ADD( var_DateCurrent,
				INTERVAL ( 
					IF( DAYOFWEEK( var_DateCurrent ) = 7, 14, 7 )
					- DAYOFWEEK( var_DateCurrent )
				) DAY ),
			weekSunday = DATE_ADD( var_DateCurrent, 
				INTERVAL ( 
					IF( DAYOFWEEK( var_DateCurrent ) > 1, 9, 1 )
					- DAYOFWEEK( var_DateCurrent )
				) DAY ),
			monthStart = DATE_FORMAT( var_DateCurrent, '%Y-%m-01' ),
			monthEnd = DATE_SUB( DATE_ADD( monthStart, INTERVAL 1 MONTH ), INTERVAL 1 DAY ),
			yearStart = DATE_FORMAT( var_DateCurrent, '%Y-01-01' ),
			yearEnd = DATE_FORMAT( var_DateCurrent, '%Y-12-31' ),
			dateMonth = MONTH( var_DateCurrent ),
			dateYear = YEAR( var_DateCurrent ),
			isWeekend = IF( DAYOFWEEK( var_DateCurrent ) = 1 OR DAYOFWEEK( var_DateCurrent ) = 7, true, false );
		set var_DateCurrent = DATE_ADD( var_DateCurrent, INTERVAL 1 DAY );
		end while;
end$$

DROP PROCEDURE IF EXISTS `get_duplicate_item_codes`$$
CREATE PROCEDURE `get_duplicate_item_codes`(cid INT)
BEGIN
DECLARE bid, bid2 INT;
DECLARE business_data CURSOR FOR 
    SELECT businessid FROM business bus WHERE bus.companyid = cid AND bus.categoryid = 4 ORDER BY bus.businessid;
  OPEN business_data;
  BEGIN
  LOOP
	FETCH business_data INTO bid;

	INSERT INTO aa_duplicate_item_codes (businessid, businessname, item_code, totals) SELECT mitn.businessid, b.businessname, mitn.item_code, COUNT(mitn.item_code) AS totals FROM  menu_items_new mitn INNER JOIN business b USING (businessid) WHERE b.businessid = bid GROUP BY item_code HAVING COUNT(mitn.item_code) > 1 ORDER BY totals DESC;

	END LOOP;
  END;
END$$

DROP PROCEDURE IF EXISTS `insert_monthly_posreports`$$
CREATE PROCEDURE `insert_monthly_posreports`(cid INT, day_month INT, insert_date DATE)
BEGIN 

	DECLARE companyid, reportid, dayofmonth INT;
	DECLARE report_name VARCHAR(50);
	DECLARE date_run_value TIMESTAMP;
	
	DECLARE monthly_pos_reports CURSOR FOR SELECT company_id, report_id, day_of_month FROM `jsantos_ee`.`pos_report_runlist` WHERE (company_id = cid) AND (how_often = 2) AND (day_of_month = day_month) GROUP BY company_id, report_id ORDER BY company_id, report_id, day_of_month;
	
	SELECT CONCAT(insert_date, ' 00:00:00') INTO date_run_value;
	
	OPEN monthly_pos_reports;
	BEGIN
		DECLARE EXIT HANDLER FOR SQLSTATE '02000' BEGIN END;
		LOOP
			FETCH monthly_pos_reports INTO companyid, reportid, dayofmonth;
				CASE reportid
					WHEN 1 THEN SELECT CONCAT(insert_date, '-', companyid, '-', 'inventory-detail-monthly') INTO report_name;
					WHEN 2 THEN SELECT CONCAT(insert_date, '-', companyid, '-', 'inventory-summary-monthly') INTO report_name;
					WHEN 3 THEN SELECT CONCAT(insert_date, '-', companyid, '-', 'inventory-sales-monthly') INTO report_name;
					WHEN 4 THEN SELECT CONCAT(insert_date, '-', companyid, '-', 'inventory-waste-monthly') INTO report_name;
					WHEN 5 THEN SELECT CONCAT(insert_date, '-', companyid, '-', 'financial-monthly') INTO report_name;
					WHEN 8 THEN SELECT CONCAT(insert_date, '-', companyid, '-', 'customer-promo-monthly') INTO report_name;
					WHEN 9 THEN SELECT CONCAT(insert_date, '-', companyid, '-', 'customer-refund-monthly') INTO report_name;
					WHEN 11 THEN SELECT CONCAT(insert_date, '-', companyid, '-', 'ck-fees-monthly') INTO report_name;
					WHEN 12 THEN SELECT CONCAT(insert_date, '-', companyid, '-', 'waste-trend') INTO report_name;
					WHEN 13 THEN SELECT CONCAT(insert_date, '-', companyid, '-', 'shrink-trend') INTO report_name;
					ELSE SELECT 'no report name' INTO report_name;
				END CASE;
				
			INSERT IGNORE INTO `jsantos_ee`.`pos_report_cache` (`report_cache_id`, `company_id`, `report_id`, `how_often`, `day_of_week`, `date_run`, `filename`, `archived`) VALUES (NULL, companyid, reportid, '2', NULL, date_run_value, report_name, 'No');
		END LOOP;	
	END;
END$$

DROP PROCEDURE IF EXISTS `insert_weekly_posreports`$$
CREATE PROCEDURE `insert_weekly_posreports`(cid INT, day_week INT, insert_date DATE)
BEGIN 

	DECLARE companyid, reportid, dayofweek INT;
	DECLARE report_name, week_day VARCHAR(50);
	DECLARE date_run_value TIMESTAMP;
	
	DECLARE weekly_pos_reports CURSOR FOR SELECT prr.company_id, prr.report_id, prr.day_of_week FROM jsantos_ee.pos_report_runlist prr WHERE (prr.company_id = cid) AND (prr.how_often = 1) AND (prr.day_of_week = day_week) GROUP BY prr.company_id, prr.report_id ORDER BY prr.company_id, prr.report_id, prr.day_of_week;
	
	SELECT CONCAT(insert_date, ' 00:00:00') INTO date_run_value;
	
	OPEN weekly_pos_reports;
	BEGIN
		DECLARE EXIT HANDLER FOR SQLSTATE '02000' BEGIN END;
		LOOP
			FETCH weekly_pos_reports INTO companyid, reportid, week_day;
				CASE reportid
					WHEN 1 THEN SELECT CONCAT(insert_date, '-', companyid, '-', 'inventory-detail-weekly') INTO report_name;
					WHEN 2 THEN SELECT CONCAT(insert_date, '-', companyid, '-', 'inventory-summary-weekly') INTO report_name;
					WHEN 3 THEN SELECT CONCAT(insert_date, '-', companyid, '-', 'inventory-sales-weekly') INTO report_name;
					WHEN 4 THEN SELECT CONCAT(insert_date, '-', companyid, '-', 'inventory-waste-weekly') INTO report_name;
					WHEN 5 THEN SELECT CONCAT(insert_date, '-', companyid, '-', 'financial-weekly') INTO report_name;
					WHEN 8 THEN SELECT CONCAT(insert_date, '-', companyid, '-', 'customer-promo-weekly') INTO report_name;
					WHEN 9 THEN SELECT CONCAT(insert_date, '-', companyid, '-', 'customer-refund-weekly') INTO report_name;
					WHEN 11 THEN SELECT CONCAT(insert_date, '-', companyid, '-', 'ck-fees-weekly') INTO report_name;
					WHEN 12 THEN SELECT CONCAT(insert_date, '-', companyid, '-', 'waste-trend') INTO report_name;
					WHEN 13 THEN SELECT CONCAT(insert_date, '-', companyid, '-', 'shrink-trend') INTO report_name;
					WHEN 17 THEN SELECT CONCAT(insert_date, '-', companyid, '-', 'financial-extras-weekly') INTO report_name;
					ELSE SELECT 'no report name' INTO report_name;
				END CASE;
				
			INSERT IGNORE INTO `jsantos_ee`.`pos_report_cache` (`report_cache_id`, `company_id`, `report_id`, `how_often`, `day_of_week`, `date_run`, `filename`, `archived`) VALUES (NULL, companyid, reportid, '1', week_day, date_run_value, report_name, 'No');
		END LOOP;	
	END;
END$$

DROP PROCEDURE IF EXISTS `trigMINClearGroupPosition`$$
CREATE DEFINER=`ryan`@`192.168.1.%` PROCEDURE `trigMINClearGroupPosition`(
	var_product_id INT,
	var_is_button BOOL
)
    MODIFIES SQL DATA
    DETERMINISTIC
BEGIN
	IF NOT var_is_button THEN
		UPDATE promo_groups_detail SET display_x = NULL, display_y = NULL, pos_color = 0 WHERE product_id = var_product_id;
	END IF;
END$$

DROP PROCEDURE IF EXISTS `trigUpdateDbMTime`$$
CREATE DEFINER=`mburris_smartin`@`192.168.1.%` PROCEDURE `trigUpdateDbMTime`(
	var_viewid int
)
    MODIFIES SQL DATA
begin
	update db_view set db_mtime = NOW() where viewid=var_viewid;
end$$

--
-- Functions
--
DROP FUNCTION IF EXISTS `customer_checks_total`$$
CREATE FUNCTION `customer_checks_total`(cust_scancode VARCHAR(30)) RETURNS decimal(6,2)
    READS SQL DATA
BEGIN
	DECLARE pd_payment_type VARCHAR(30);
	DECLARE checks_total, calculated_balance DECIMAL(6,2) DEFAULT 0.00;
	DECLARE cust_customerid INT DEFAULT 0;
	
	DECLARE done INT DEFAULT FALSE;
	
	DECLARE customer_checks CURSOR FOR SELECT customer.customerid, checks.total, payment_detail.payment_type FROM checks JOIN payment_detail ON payment_detail.check_number = checks.check_number AND payment_detail.businessid = checks.businessid AND payment_detail.scancode = cust_scancode JOIN customer USING (scancode) JOIN business ON business.businessid = checks.businessid WHERE checks.bill_datetime BETWEEN '2000-01-01 00:00:00' AND '2020-01-01 00:00:00' ORDER BY bill_datetime DESC;
	
	OPEN customer_checks;
	BEGIN
		DECLARE CONTINUE handler FOR NOT FOUND SET done = TRUE;
		done_label: LOOP
			FETCH customer_checks INTO cust_customerid, checks_total, pd_payment_type;
			IF done THEN 
				LEAVE done_label;
			END IF;
			
			IF (pd_payment_type = 1) OR (pd_payment_type = 4) OR (pd_payment_type = 6) OR (pd_payment_type = 'Promotion') OR (pd_payment_type = 'Credit Card Transaction') OR (pd_payment_type = "Company Kitchen Refund") OR (pd_payment_type = "Transfer") OR (pd_payment_type = "CK Adjust") THEN
				SELECT (checks_total * -1) INTO checks_total;
			END IF;
			
			SELECT (calculated_balance + checks_total) INTO calculated_balance;
			
		END LOOP;	
	END;
	RETURN calculated_balance;
END$$

DROP FUNCTION IF EXISTS `customer_other_total`$$
CREATE FUNCTION `customer_other_total`(cust_customerid INT) RETURNS decimal(6,2)
    READS SQL DATA
BEGIN
	DECLARE pd_payment_type VARCHAR(30);
	DECLARE checks_total, calculated_balance DECIMAL(6,2) DEFAULT 0.00;
	DECLARE done INT DEFAULT FALSE;
	
	DECLARE customer_checks CURSOR FOR SELECT ctt.label, ct.chargetotal FROM jsantos_ee.customer_transactions ct INNER JOIN customer_transaction_types ctt ON ct.trans_type_id = ctt.id WHERE (customer_id = cust_customerid) AND (ct.response LIKE 'APPROVED') ORDER BY ct.date_created DESC;
	
	OPEN customer_checks;
	BEGIN
		DECLARE CONTINUE handler FOR NOT FOUND SET done = TRUE;
		done_label: LOOP
			FETCH customer_checks INTO pd_payment_type, checks_total;
			IF done THEN 
				LEAVE done_label;
			END IF;
			
			IF (pd_payment_type = "1") OR (pd_payment_type = "4") OR (pd_payment_type = "6") OR (pd_payment_type = 'Promotion') OR (pd_payment_type = 'Credit Card Transaction') OR (pd_payment_type = "Company Kitchen Refund") OR (pd_payment_type = "Transfer") OR (pd_payment_type = "CK Adjust") THEN
				SELECT (checks_total * -1) INTO checks_total;
			END IF;
			
			SELECT (calculated_balance + checks_total) INTO calculated_balance;
			
		END LOOP;	
	END;
	RETURN calculated_balance;
	
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `SiTech_Sessions`
--

DROP TABLE IF EXISTS `SiTech_Sessions`;
CREATE TABLE IF NOT EXISTS `SiTech_Sessions` (
  `Name` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `Id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `Data` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Started` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Remember` tinyint(1) NOT NULL DEFAULT '0',
  `Strict` tinyint(1) NOT NULL DEFAULT '0',
  `RemoteAddr` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`Name`,`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `aa_duplicate_item_codes`
--

DROP TABLE IF EXISTS `aa_duplicate_item_codes`;
CREATE TABLE IF NOT EXISTS `aa_duplicate_item_codes` (
  `businessid` int(11) NOT NULL,
  `businessname` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `item_code` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `totals` int(11) NOT NULL,
  UNIQUE KEY `businessid` (`businessid`,`item_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `aa_tracey_cost`
--

DROP TABLE IF EXISTS `aa_tracey_cost`;
CREATE TABLE IF NOT EXISTS `aa_tracey_cost` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL,
  `bid` int(11) NOT NULL,
  `item_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `upc_value` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `item_code_value` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `business_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `pos_id_value` int(11) NOT NULL,
  `cost` decimal(10,3) NOT NULL,
  `price` decimal(10,3) NOT NULL,
  `units_sold` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=115144 ;

-- --------------------------------------------------------

--
-- Table structure for table `accountpend`
--

DROP TABLE IF EXISTS `accountpend`;
CREATE TABLE IF NOT EXISTS `accountpend` (
  `accountid` int(10) NOT NULL AUTO_INCREMENT,
  `accountref` int(10) NOT NULL DEFAULT '0',
  `businessid` int(10) NOT NULL DEFAULT '0',
  `companyid` int(5) NOT NULL DEFAULT '0',
  `name` varchar(30) NOT NULL DEFAULT '',
  `accountnum` varchar(10) NOT NULL DEFAULT '',
  `attn` varchar(50) NOT NULL DEFAULT '',
  `street` varchar(40) NOT NULL DEFAULT '',
  `city` varchar(25) NOT NULL DEFAULT '',
  `state` char(2) NOT NULL DEFAULT '',
  `zip` varchar(10) NOT NULL DEFAULT '',
  `phone` varchar(25) NOT NULL DEFAULT '',
  `fax` varchar(14) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `parent` int(10) NOT NULL DEFAULT '0',
  `taxid` varchar(12) NOT NULL DEFAULT '',
  `costcenter` varchar(30) NOT NULL DEFAULT '',
  `password` varchar(25) NOT NULL DEFAULT '',
  `salescode` varchar(10) NOT NULL DEFAULT '',
  `requestby` varchar(25) NOT NULL,
  `notes` text NOT NULL,
  PRIMARY KEY (`accountid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2029 ;

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
CREATE TABLE IF NOT EXISTS `accounts` (
  `accountid` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL DEFAULT '0',
  `companyid` int(5) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL,
  `accountnum` varchar(25) NOT NULL,
  `attn` varchar(50) NOT NULL DEFAULT '',
  `street` varchar(40) NOT NULL DEFAULT '',
  `city` varchar(25) NOT NULL DEFAULT '',
  `state` char(2) NOT NULL DEFAULT '',
  `zip` varchar(10) NOT NULL DEFAULT '',
  `phone` varchar(25) NOT NULL DEFAULT '',
  `fax` varchar(14) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL,
  `parent` int(10) NOT NULL DEFAULT '0',
  `taxid` varchar(12) NOT NULL DEFAULT '',
  `tax_rate` double NOT NULL,
  `costcenter` varchar(30) NOT NULL DEFAULT '',
  `create_date` date NOT NULL DEFAULT '0000-00-00',
  `salescode` varchar(10) NOT NULL DEFAULT '0',
  `menu` int(3) NOT NULL DEFAULT '-1',
  `contract_num` int(10) DEFAULT NULL,
  `assign` varchar(10) NOT NULL,
  `status` int(2) NOT NULL,
  `deliver_charge` double NOT NULL,
  `deliver_charge2` double NOT NULL,
  `allow_sub` tinyint(1) NOT NULL DEFAULT '0',
  `print_labels` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'whether to print labels or not',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `del_date` date NOT NULL,
  `b_mailing` tinyint(1) NOT NULL,
  `b_street` varchar(50) NOT NULL,
  `b_city` varchar(50) NOT NULL,
  `b_state` varchar(2) NOT NULL,
  `b_zip` varchar(10) NOT NULL,
  `export` tinyint(1) NOT NULL DEFAULT '0',
  `salesman` int(5) NOT NULL,
  `aged` tinyint(1) NOT NULL,
  `channel` int(3) NOT NULL,
  `note` text NOT NULL,
  `terms` tinyint(1) NOT NULL,
  PRIMARY KEY (`accountid`),
  KEY `businessid` (`businessid`,`companyid`,`accountnum`,`salesman`),
  KEY `accountnum` (`accountnum`),
  KEY `companyid` (`companyid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22224 ;

-- --------------------------------------------------------

--
-- Table structure for table `accounts_group`
--

DROP TABLE IF EXISTS `accounts_group`;
CREATE TABLE IF NOT EXISTS `accounts_group` (
  `groupid` int(10) NOT NULL AUTO_INCREMENT,
  `groupname` varchar(50) NOT NULL,
  PRIMARY KEY (`groupid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `accounts_groupdetail`
--

DROP TABLE IF EXISTS `accounts_groupdetail`;
CREATE TABLE IF NOT EXISTS `accounts_groupdetail` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `groupid` int(10) NOT NULL,
  `accountid` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Table structure for table `accounts_roche`
--

DROP TABLE IF EXISTS `accounts_roche`;
CREATE TABLE IF NOT EXISTS `accounts_roche` (
  `valueid` int(11) NOT NULL AUTO_INCREMENT,
  `accountid` int(11) NOT NULL DEFAULT '0',
  `cid5_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cid1_accountid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`valueid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=110 ;

-- --------------------------------------------------------

--
-- Table structure for table `acct_payable`
--

DROP TABLE IF EXISTS `acct_payable`;
CREATE TABLE IF NOT EXISTS `acct_payable` (
  `acct_payableid` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL DEFAULT '0',
  `companyid` int(5) NOT NULL DEFAULT '0',
  `acct_name` varchar(35) NOT NULL DEFAULT '',
  `accountnum` varchar(20) NOT NULL,
  `cos` tinyint(1) NOT NULL DEFAULT '0',
  `cs_type` int(2) NOT NULL DEFAULT '0',
  `daily_fixed` double NOT NULL DEFAULT '0',
  `daily_labor` double NOT NULL DEFAULT '0',
  `cs_glacct` int(10) NOT NULL DEFAULT '0',
  `cs_prcnt` float NOT NULL DEFAULT '0',
  `cs` tinyint(1) NOT NULL DEFAULT '0',
  `created` date NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `del_date` date NOT NULL,
  PRIMARY KEY (`acct_payableid`),
  KEY `accountnum` (`companyid`,`businessid`,`accountnum`),
  KEY `businessid_2` (`businessid`,`cos`,`cs_type`,`is_deleted`,`cs`,`del_date`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16520 ;

-- --------------------------------------------------------

--
-- Table structure for table `acct_status`
--

DROP TABLE IF EXISTS `acct_status`;
CREATE TABLE IF NOT EXISTS `acct_status` (
  `acct_statusid` int(1) NOT NULL,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`acct_statusid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `add_on`
--

DROP TABLE IF EXISTS `add_on`;
CREATE TABLE IF NOT EXISTS `add_on` (
  `add_onid` int(10) NOT NULL AUTO_INCREMENT,
  `accountid` int(10) NOT NULL,
  `date` date NOT NULL,
  `qty` double NOT NULL,
  `price` double NOT NULL,
  `menu_itemid` int(10) NOT NULL,
  `billed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`add_onid`),
  KEY `accountid` (`accountid`,`date`,`billed`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11567 ;

-- --------------------------------------------------------

--
-- Table structure for table `aeris_modifier`
--

DROP TABLE IF EXISTS `aeris_modifier`;
CREATE TABLE IF NOT EXISTS `aeris_modifier` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `modifierGroupId` int(10) unsigned NOT NULL,
  `name` varchar(48) NOT NULL DEFAULT '',
  `price` double NOT NULL DEFAULT '0',
  `preset` tinyint(1) NOT NULL DEFAULT '0',
  `pos_color` tinyint(4) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `modifierGroupId` (`modifierGroupId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=426 ;

-- --------------------------------------------------------

--
-- Table structure for table `aeris_modifier_group`
--

DROP TABLE IF EXISTS `aeris_modifier_group`;
CREATE TABLE IF NOT EXISTS `aeris_modifier_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `businessid` int(11) NOT NULL,
  `name` varchar(48) NOT NULL DEFAULT '',
  `type` enum('single','multiple') NOT NULL DEFAULT 'single',
  `newLine` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `businessid` (`businessid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=83 ;

-- --------------------------------------------------------

--
-- Table structure for table `aeris_operating_modes`
--

DROP TABLE IF EXISTS `aeris_operating_modes`;
CREATE TABLE IF NOT EXISTS `aeris_operating_modes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL DEFAULT '',
  `token` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `aeris_settings`
--

DROP TABLE IF EXISTS `aeris_settings`;
CREATE TABLE IF NOT EXISTS `aeris_settings` (
  `settingId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `businessid` int(10) unsigned NOT NULL,
  `registryKey` varchar(128) NOT NULL,
  `registryValue` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`settingId`),
  UNIQUE KEY `businessid` (`businessid`,`registryKey`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16107 ;

-- --------------------------------------------------------

--
-- Table structure for table `aeris_settings_type`
--

DROP TABLE IF EXISTS `aeris_settings_type`;
CREATE TABLE IF NOT EXISTS `aeris_settings_type` (
  `registryKey` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `registryType` enum('int','bool','string') COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`registryKey`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `aeris_versions`
--

DROP TABLE IF EXISTS `aeris_versions`;
CREATE TABLE IF NOT EXISTS `aeris_versions` (
  `client_programid` int(10) unsigned NOT NULL,
  `component_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `component_version` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `reported_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`client_programid`,`component_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `apinvoice`
--

DROP TABLE IF EXISTS `apinvoice`;
CREATE TABLE IF NOT EXISTS `apinvoice` (
  `apinvoiceid` int(10) NOT NULL AUTO_INCREMENT,
  `invoicenum` varchar(20) NOT NULL DEFAULT '',
  `businessid` int(10) NOT NULL DEFAULT '0',
  `companyid` int(5) NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `vendor` int(10) NOT NULL DEFAULT '0',
  `total` double NOT NULL DEFAULT '0',
  `invtotal` double NOT NULL DEFAULT '0',
  `posted` tinyint(1) NOT NULL DEFAULT '0',
  `export` tinyint(1) NOT NULL DEFAULT '0',
  `image` varchar(25) NOT NULL DEFAULT '',
  `imagetype` varchar(20) NOT NULL DEFAULT '',
  `submitdate` date NOT NULL DEFAULT '0000-00-00',
  `submitby` varchar(20) NOT NULL,
  `master_vendor_id` int(10) NOT NULL DEFAULT '0',
  `notes` text NOT NULL,
  `transfer` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-invoice,1-transfer,2-pcard,3-expense_report,4-purchase_order',
  `pc_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'pcard: 0,1. po: 1 if received',
  `interco_trans` int(10) NOT NULL DEFAULT '0',
  `accept_by` varchar(20) NOT NULL,
  `dm` varchar(15) NOT NULL,
  PRIMARY KEY (`apinvoiceid`),
  KEY `invoicenum` (`invoicenum`,`master_vendor_id`),
  KEY `vendor` (`vendor`,`date`,`transfer`),
  KEY `dm` (`dm`,`transfer`,`posted`,`pc_type`,`businessid`),
  KEY `master_vendor_id` (`master_vendor_id`,`businessid`,`date`,`transfer`),
  KEY `companyid` (`companyid`,`posted`,`transfer`),
  KEY `businessid` (`businessid`,`date`,`transfer`,`posted`),
  KEY `transfer` (`transfer`),
  KEY `posted` (`posted`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=445618 ;

-- --------------------------------------------------------

--
-- Table structure for table `apinvoice_multi_trans`
--

DROP TABLE IF EXISTS `apinvoice_multi_trans`;
CREATE TABLE IF NOT EXISTS `apinvoice_multi_trans` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `apinvoiceid` int(10) NOT NULL,
  `companyid` int(3) NOT NULL,
  `businessid` int(5) NOT NULL,
  `acct_payableid_from` int(10) NOT NULL,
  `acct_payableid` int(10) NOT NULL,
  `note` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `amount` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `apinvoiceid` (`apinvoiceid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10015 ;

-- --------------------------------------------------------

--
-- Table structure for table `apinvoicedetail`
--

DROP TABLE IF EXISTS `apinvoicedetail`;
CREATE TABLE IF NOT EXISTS `apinvoicedetail` (
  `itemid` int(10) NOT NULL AUTO_INCREMENT,
  `apinvoiceid` int(10) NOT NULL DEFAULT '0',
  `pcard` tinyint(1) NOT NULL DEFAULT '0',
  `amount` double NOT NULL DEFAULT '0',
  `tax` double NOT NULL,
  `freight` double NOT NULL,
  `apaccountid` int(10) NOT NULL DEFAULT '0',
  `item` varchar(40) NOT NULL,
  `qty` double NOT NULL,
  `unit` varchar(10) NOT NULL,
  `item_price` double NOT NULL,
  `tfr_acct` int(10) NOT NULL,
  `date` date NOT NULL,
  `categoryid` int(2) NOT NULL,
  `addon` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`itemid`),
  KEY `apinvoiceid_3` (`apinvoiceid`,`tfr_acct`),
  KEY `apinvoiceid` (`apinvoiceid`,`apaccountid`),
  KEY `apinvoiceid_2` (`apinvoiceid`,`categoryid`,`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=878266 ;

-- --------------------------------------------------------

--
-- Table structure for table `audit_budget`
--

DROP TABLE IF EXISTS `audit_budget`;
CREATE TABLE IF NOT EXISTS `audit_budget` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `businessid` (`businessid`,`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1184 ;

-- --------------------------------------------------------

--
-- Table structure for table `audit_collection`
--

DROP TABLE IF EXISTS `audit_collection`;
CREATE TABLE IF NOT EXISTS `audit_collection` (
  `audit_collectionid` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(5) NOT NULL,
  `user` varchar(25) NOT NULL,
  `date` date NOT NULL,
  `edittime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`audit_collectionid`),
  KEY `businessid` (`businessid`,`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16696 ;

-- --------------------------------------------------------

--
-- Table structure for table `audit_comm`
--

DROP TABLE IF EXISTS `audit_comm`;
CREATE TABLE IF NOT EXISTS `audit_comm` (
  `audit_comm_id` int(10) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `comment` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `businessid` int(10) NOT NULL,
  PRIMARY KEY (`audit_comm_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=446 ;

-- --------------------------------------------------------

--
-- Table structure for table `audit_estimate`
--

DROP TABLE IF EXISTS `audit_estimate`;
CREATE TABLE IF NOT EXISTS `audit_estimate` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `businessid` (`businessid`,`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=87 ;

-- --------------------------------------------------------

--
-- Table structure for table `audit_invoice`
--

DROP TABLE IF EXISTS `audit_invoice`;
CREATE TABLE IF NOT EXISTS `audit_invoice` (
  `invoice_audit_id` int(10) NOT NULL AUTO_INCREMENT,
  `invoiceid` int(10) NOT NULL,
  `type` int(1) NOT NULL,
  `email` varchar(100) NOT NULL,
  `userid` varchar(50) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`invoice_audit_id`),
  KEY `inv_audit` (`invoiceid`,`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=221984 ;

-- --------------------------------------------------------

--
-- Table structure for table `audit_labor`
--

DROP TABLE IF EXISTS `audit_labor`;
CREATE TABLE IF NOT EXISTS `audit_labor` (
  `auditid` int(10) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user` varchar(30) NOT NULL,
  `comment` varchar(60) NOT NULL,
  `businessid` int(10) NOT NULL,
  PRIMARY KEY (`auditid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=201256 ;

-- --------------------------------------------------------

--
-- Table structure for table `audit_login`
--

DROP TABLE IF EXISTS `audit_login`;
CREATE TABLE IF NOT EXISTS `audit_login` (
  `audit_loginid` int(10) NOT NULL AUTO_INCREMENT,
  `user` varchar(25) NOT NULL,
  `pass` varchar(25) NOT NULL,
  `edittime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip` varchar(25) NOT NULL,
  `host` varchar(100) NOT NULL,
  `details` text NOT NULL,
  `redirect` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`audit_loginid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11452 ;

-- --------------------------------------------------------

--
-- Table structure for table `audit_menu_items`
--

DROP TABLE IF EXISTS `audit_menu_items`;
CREATE TABLE IF NOT EXISTS `audit_menu_items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user` varchar(25) NOT NULL,
  `mytime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `myquery` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42737 ;

-- --------------------------------------------------------

--
-- Table structure for table `audit_reconcile`
--

DROP TABLE IF EXISTS `audit_reconcile`;
CREATE TABLE IF NOT EXISTS `audit_reconcile` (
  `auditid` int(10) NOT NULL AUTO_INCREMENT,
  `companyid` int(3) NOT NULL,
  `date` date NOT NULL,
  `date1` date NOT NULL,
  `date2` date NOT NULL,
  `user` varchar(25) NOT NULL,
  `acct` varchar(15) NOT NULL,
  PRIMARY KEY (`auditid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3392 ;

-- --------------------------------------------------------

--
-- Table structure for table `audit_route`
--

DROP TABLE IF EXISTS `audit_route`;
CREATE TABLE IF NOT EXISTS `audit_route` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `date1` date NOT NULL,
  `date2` date NOT NULL,
  `loginid` int(10) NOT NULL,
  `route` int(10) NOT NULL,
  `route2` int(10) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1352936 ;

-- --------------------------------------------------------

--
-- Table structure for table `audit_sales`
--

DROP TABLE IF EXISTS `audit_sales`;
CREATE TABLE IF NOT EXISTS `audit_sales` (
  `audit_sales_id` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL DEFAULT '0',
  `companyid` int(3) NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user` varchar(25) NOT NULL DEFAULT '',
  `submit` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`audit_sales_id`),
  KEY `businessid` (`businessid`,`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=496019 ;

-- --------------------------------------------------------

--
-- Table structure for table `audit_vend`
--

DROP TABLE IF EXISTS `audit_vend`;
CREATE TABLE IF NOT EXISTS `audit_vend` (
  `audit_vendid` int(10) NOT NULL AUTO_INCREMENT,
  `login_routeid` int(10) NOT NULL,
  `user` varchar(25) NOT NULL,
  `date` date NOT NULL,
  `edittime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`audit_vendid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=104777 ;

-- --------------------------------------------------------

--
-- Table structure for table `auto_login`
--

DROP TABLE IF EXISTS `auto_login`;
CREATE TABLE IF NOT EXISTS `auto_login` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `loginid` int(10) NOT NULL,
  `apinvoiceid` int(10) NOT NULL,
  `hash` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=278 ;

-- --------------------------------------------------------

--
-- Table structure for table `bboard`
--

DROP TABLE IF EXISTS `bboard`;
CREATE TABLE IF NOT EXISTS `bboard` (
  `bboardid` int(10) NOT NULL AUTO_INCREMENT,
  `subject` varchar(100) NOT NULL,
  `date` date NOT NULL DEFAULT '0000-00-00',
  `user` varchar(25) NOT NULL DEFAULT '',
  `sort` tinyint(1) NOT NULL DEFAULT '0',
  `companyid` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`bboardid`),
  KEY `date` (`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

-- --------------------------------------------------------

--
-- Table structure for table `bboarddetail`
--

DROP TABLE IF EXISTS `bboarddetail`;
CREATE TABLE IF NOT EXISTS `bboarddetail` (
  `bboarddetailid` int(10) NOT NULL AUTO_INCREMENT,
  `bboardid` int(10) NOT NULL DEFAULT '0',
  `text` text NOT NULL,
  `user` varchar(25) NOT NULL DEFAULT '',
  `date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`bboarddetailid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=52 ;

-- --------------------------------------------------------

--
-- Table structure for table `blacklist_ip`
--

DROP TABLE IF EXISTS `blacklist_ip`;
CREATE TABLE IF NOT EXISTS `blacklist_ip` (
  `ip_address` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ip_address`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `budget`
--

DROP TABLE IF EXISTS `budget`;
CREATE TABLE IF NOT EXISTS `budget` (
  `budgetid` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL,
  `companyid` int(3) NOT NULL,
  `year` int(4) NOT NULL,
  `type` int(2) NOT NULL,
  `01` double NOT NULL,
  `02` double NOT NULL,
  `03` double NOT NULL,
  `04` double NOT NULL,
  `05` double NOT NULL,
  `06` double NOT NULL,
  `07` double NOT NULL,
  `08` double NOT NULL,
  `09` double NOT NULL,
  `10` double NOT NULL,
  `11` double NOT NULL,
  `12` double NOT NULL,
  PRIMARY KEY (`budgetid`),
  KEY `businessid` (`businessid`,`year`,`type`,`companyid`),
  KEY `year` (`year`,`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3948 ;

-- --------------------------------------------------------

--
-- Table structure for table `budget_adjust`
--

DROP TABLE IF EXISTS `budget_adjust`;
CREATE TABLE IF NOT EXISTS `budget_adjust` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `businessid` int(8) NOT NULL,
  `date` date NOT NULL,
  `amount` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `businessid` (`businessid`,`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=67 ;

-- --------------------------------------------------------

--
-- Table structure for table `bulletin`
--

DROP TABLE IF EXISTS `bulletin`;
CREATE TABLE IF NOT EXISTS `bulletin` (
  `bulletinid` int(10) NOT NULL AUTO_INCREMENT,
  `user` int(10) NOT NULL DEFAULT '0',
  `message` text NOT NULL,
  `expire` date NOT NULL,
  `view` tinyint(1) NOT NULL DEFAULT '0',
  `reply` int(10) NOT NULL,
  `new` tinyint(1) NOT NULL DEFAULT '1',
  `saved` tinyint(1) NOT NULL,
  PRIMARY KEY (`bulletinid`),
  KEY `user` (`user`,`expire`),
  KEY `user_2` (`user`,`saved`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=118340 ;

-- --------------------------------------------------------

--
-- Table structure for table `business`
--

DROP TABLE IF EXISTS `business`;
CREATE TABLE IF NOT EXISTS `business` (
  `businessid` int(10) NOT NULL AUTO_INCREMENT,
  `companyid` int(3) unsigned NOT NULL DEFAULT '1',
  `businessname` varchar(50) NOT NULL DEFAULT '<font color=red>NewUnit',
  `companyname` varchar(50) NOT NULL,
  `street` varchar(50) NOT NULL DEFAULT '',
  `city` varchar(30) NOT NULL DEFAULT '',
  `state` char(2) NOT NULL DEFAULT '',
  `zip` varchar(10) NOT NULL DEFAULT '',
  `manager` int(10) NOT NULL DEFAULT '0',
  `phone` varchar(50) NOT NULL DEFAULT '',
  `paystart` date NOT NULL DEFAULT '2000-01-01',
  `paytype` tinyint(1) NOT NULL DEFAULT '0',
  `paydays` int(2) NOT NULL DEFAULT '7',
  `location` varchar(5) NOT NULL DEFAULT '',
  `unit` int(5) NOT NULL,
  `ar_unit` varchar(3) NOT NULL,
  `tax` float NOT NULL DEFAULT '0',
  `over_short_acct` varchar(20) NOT NULL,
  `inventor_acct` varchar(20) NOT NULL,
  `inventor_goal` double NOT NULL,
  `caternum` int(3) NOT NULL DEFAULT '5',
  `caterdays` tinyint(2) NOT NULL DEFAULT '2',
  `districtid` int(3) NOT NULL DEFAULT '0',
  `catering` tinyint(1) NOT NULL DEFAULT '0',
  `srvchrg` tinyint(1) NOT NULL DEFAULT '0',
  `srvchrgpcnt` float NOT NULL DEFAULT '0',
  `srvchrg_name` varchar(50) NOT NULL DEFAULT 'SERVICE CHRG',
  `weekend` tinyint(1) NOT NULL DEFAULT '0',
  `cater_min` int(3) NOT NULL,
  `cater_spread` int(2) NOT NULL,
  `cater_costcenter` int(1) NOT NULL DEFAULT '0',
  `count1` varchar(10) NOT NULL DEFAULT 'Breakfast',
  `count2` varchar(10) NOT NULL DEFAULT 'Lunch',
  `count3` varchar(10) NOT NULL DEFAULT 'Dinner',
  `multitax` tinyint(1) NOT NULL DEFAULT '0',
  `exp_sales` tinyint(1) NOT NULL DEFAULT '0',
  `exp_trans` tinyint(1) NOT NULL DEFAULT '0',
  `exp_ar` tinyint(1) NOT NULL DEFAULT '0',
  `exp_ap` tinyint(1) NOT NULL DEFAULT '0',
  `exp_inv` tinyint(1) NOT NULL DEFAULT '0',
  `exp_payroll` tinyint(1) NOT NULL DEFAULT '0',
  `ap_export_num` int(2) NOT NULL,
  `restrict_transfer_to` tinyint(1) NOT NULL DEFAULT '0',
  `skpi` tinyint(1) NOT NULL DEFAULT '1',
  `db_restrict` tinyint(1) NOT NULL DEFAULT '0',
  `benefit` float NOT NULL DEFAULT '34',
  `closed` tinyint(1) NOT NULL DEFAULT '0',
  `closed_reason` varchar(100) NOT NULL DEFAULT '',
  `inv_note` text NOT NULL,
  `operate` int(1) NOT NULL DEFAULT '5',
  `default_menu` int(3) NOT NULL DEFAULT '1',
  `cafe_menu` int(5) NOT NULL,
  `vend_menu` int(5) NOT NULL,
  `default_menu_alias` int(5) NOT NULL,
  `cafe_menu_alias` int(5) NOT NULL,
  `vend_menu_alias` int(5) NOT NULL,
  `default_only` tinyint(1) NOT NULL DEFAULT '0',
  `static_menu` tinyint(1) NOT NULL,
  `no_nutrition` tinyint(1) NOT NULL DEFAULT '0',
  `multi_cafe_menu` tinyint(1) NOT NULL DEFAULT '0',
  `allow_sub_orders` tinyint(1) NOT NULL DEFAULT '0',
  `unit_add` tinyint(1) NOT NULL DEFAULT '0',
  `transfer` int(10) NOT NULL,
  `po_num` int(5) NOT NULL DEFAULT '1001',
  `population` int(10) NOT NULL,
  `contract` tinyint(1) NOT NULL DEFAULT '0',
  `contract_num` int(10) NOT NULL DEFAULT '1',
  `show_inv_price` tinyint(1) NOT NULL DEFAULT '0',
  `show_inv_count` tinyint(1) NOT NULL DEFAULT '6',
  `show_last_count` tinyint(1) NOT NULL DEFAULT '0',
  `show_inv_par` tinyint(1) NOT NULL DEFAULT '0',
  `use_nutrition` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'uses nutrition table instead of inv_items table',
  `count_type` tinyint(1) NOT NULL DEFAULT '0',
  `vend_start` date NOT NULL,
  `vend_end` date NOT NULL,
  `interco` tinyint(1) NOT NULL DEFAULT '0',
  `categoryid` int(3) NOT NULL,
  `labor_notify_email` varchar(100) NOT NULL,
  `po_notify_email` int(10) NOT NULL COMMENT 'not an email, loginid',
  `timezone` tinyint(2) NOT NULL,
  `acct_counter` int(5) NOT NULL DEFAULT '1000',
  `cs_amounttype` tinyint(1) NOT NULL COMMENT '0None,1%,2Fixed',
  `cs_amount` double NOT NULL,
  `cs_special` double NOT NULL,
  `petty_cash` double NOT NULL,
  `budget_type` tinyint(1) NOT NULL DEFAULT '0',
  `use_mei_sales` tinyint(1) NOT NULL,
  `auto_submit_labor` tinyint(1) NOT NULL DEFAULT '0',
  `auto_submit_sales` tinyint(1) NOT NULL DEFAULT '0',
  `over_time_type` tinyint(1) NOT NULL DEFAULT '0',
  `sort` varchar(4) NOT NULL,
  `po_prefix` varchar(2) NOT NULL,
  `po_approval` int(10) NOT NULL DEFAULT '53',
  `import_pl` tinyint(1) NOT NULL,
  `setup` tinyint(4) NOT NULL DEFAULT '0',
  `kiosk_multi_inv` tinyint(1) NOT NULL DEFAULT '0',
  `business_logo` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`businessid`),
  KEY `unit` (`unit`),
  KEY `companyid` (`companyid`,`businessid`),
  KEY `districtid_2` (`districtid`,`businessid`),
  KEY `businessid` (`businessid`,`db_restrict`),
  KEY `po_notify_email` (`po_notify_email`),
  KEY `restrict_transfer_to` (`restrict_transfer_to`),
  KEY `exp_sales` (`exp_sales`),
  KEY `exp_ar` (`exp_ar`),
  KEY `companyid_2` (`companyid`,`exp_payroll`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1247 ;

--
-- Triggers `business`
--
DROP TRIGGER IF EXISTS `business_ai`;
DELIMITER //
CREATE TRIGGER `business_ai` AFTER INSERT ON `business`
 FOR EACH ROW BEGIN 
    INSERT IGNORE INTO posreports_business 
    (businessid, companyid, businessname, companyname, street, city, state, zip, manager, phone, paystart, paytype, paydays, location, unit, ar_unit, tax, over_short_acct, inventor_acct, inventor_goal, caternum, caterdays, districtid, catering, srvchrg, srvchrgpcnt, srvchrg_name, weekend, cater_min, cater_spread, cater_costcenter, count1, count2, count3, multitax, exp_sales, exp_trans, exp_ar, exp_ap, exp_inv, exp_payroll, ap_export_num, restrict_transfer_to, skpi, db_restrict, benefit, closed, closed_reason, inv_note, operate, default_menu, cafe_menu, vend_menu, default_menu_alias, cafe_menu_alias, vend_menu_alias, default_only, static_menu, no_nutrition, multi_cafe_menu, allow_sub_orders, unit_add, transfer, po_num, population, contract, contract_num, show_inv_price, show_inv_count, show_last_count, show_inv_par, use_nutrition, count_type, vend_start, vend_end, interco, categoryid, labor_notify_email, po_notify_email, timezone, acct_counter, cs_amounttype, cs_amount, cs_special, petty_cash, budget_type, use_mei_sales, auto_submit_labor, auto_submit_sales, over_time_type, sort, po_prefix, po_approval, import_pl, setup, kiosk_multi_inv, business_logo) 
    VALUES 
    (NEW.businessid, NEW.companyid, NEW.businessname, NEW.companyname, NEW.street, NEW.city, NEW.state, NEW.zip, NEW.manager, NEW.phone, NEW.paystart, NEW.paytype, NEW.paydays, NEW.location, NEW.unit, NEW.ar_unit, NEW.tax, NEW.over_short_acct, NEW.inventor_acct, NEW.inventor_goal, NEW.caternum, NEW.caterdays, NEW.districtid, NEW.catering, NEW.srvchrg, NEW.srvchrgpcnt, NEW.srvchrg_name, NEW.weekend, NEW.cater_min, NEW.cater_spread, NEW.cater_costcenter, NEW.count1, NEW.count2, NEW.count3, NEW.multitax, NEW.exp_sales, NEW.exp_trans, NEW.exp_ar, NEW.exp_ap, NEW.exp_inv, NEW.exp_payroll, NEW.ap_export_num, NEW.restrict_transfer_to, NEW.skpi, NEW.db_restrict, NEW.benefit, NEW.closed, NEW.closed_reason, NEW.inv_note, NEW.operate, NEW.default_menu, NEW.cafe_menu, NEW.vend_menu, NEW.default_menu_alias, NEW.cafe_menu_alias, NEW.vend_menu_alias, NEW.default_only, NEW.static_menu, NEW.no_nutrition, NEW.multi_cafe_menu, NEW.allow_sub_orders, NEW.unit_add, NEW.transfer, NEW.po_num, NEW.population, NEW.contract, NEW.contract_num, NEW.show_inv_price, NEW.show_inv_count, NEW.show_last_count, NEW.show_inv_par, NEW.use_nutrition, NEW.count_type, NEW.vend_start, NEW.vend_end, NEW.interco, NEW.categoryid, NEW.labor_notify_email, NEW.po_notify_email, NEW.timezone, NEW.acct_counter, NEW.cs_amounttype, NEW.cs_amount, NEW.cs_special, NEW.petty_cash, NEW.budget_type, NEW.use_mei_sales, NEW.auto_submit_labor, NEW.auto_submit_sales, NEW.over_time_type, NEW.sort, NEW.po_prefix, NEW.po_approval, NEW.import_pl, NEW.setup, NEW.kiosk_multi_inv, NEW.business_logo);
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `business_au`;
DELIMITER //
CREATE TRIGGER `business_au` AFTER UPDATE ON `business`
 FOR EACH ROW BEGIN 

	DELETE FROM posreports_business WHERE businessid = OLD.businessid;
	
    INSERT IGNORE INTO posreports_business 
    (businessid, companyid, businessname, companyname, street, city, state, zip, manager, phone, paystart, paytype, paydays, location, unit, ar_unit, tax, over_short_acct, inventor_acct, inventor_goal, caternum, caterdays, districtid, catering, srvchrg, srvchrgpcnt, srvchrg_name, weekend, cater_min, cater_spread, cater_costcenter, count1, count2, count3, multitax, exp_sales, exp_trans, exp_ar, exp_ap, exp_inv, exp_payroll, ap_export_num, restrict_transfer_to, skpi, db_restrict, benefit, closed, closed_reason, inv_note, operate, default_menu, cafe_menu, vend_menu, default_menu_alias, cafe_menu_alias, vend_menu_alias, default_only, static_menu, no_nutrition, multi_cafe_menu, allow_sub_orders, unit_add, transfer, po_num, population, contract, contract_num, show_inv_price, show_inv_count, show_last_count, show_inv_par, use_nutrition, count_type, vend_start, vend_end, interco, categoryid, labor_notify_email, po_notify_email, timezone, acct_counter, cs_amounttype, cs_amount, cs_special, petty_cash, budget_type, use_mei_sales, auto_submit_labor, auto_submit_sales, over_time_type, sort, po_prefix, po_approval, import_pl, setup, kiosk_multi_inv, business_logo) 
    VALUES 
    (OLD.businessid, NEW.companyid, NEW.businessname, NEW.companyname, NEW.street, NEW.city, NEW.state, NEW.zip, NEW.manager, NEW.phone, NEW.paystart, NEW.paytype, NEW.paydays, NEW.location, NEW.unit, NEW.ar_unit, NEW.tax, NEW.over_short_acct, NEW.inventor_acct, NEW.inventor_goal, NEW.caternum, NEW.caterdays, NEW.districtid, NEW.catering, NEW.srvchrg, NEW.srvchrgpcnt, NEW.srvchrg_name, NEW.weekend, NEW.cater_min, NEW.cater_spread, NEW.cater_costcenter, NEW.count1, NEW.count2, NEW.count3, NEW.multitax, NEW.exp_sales, NEW.exp_trans, NEW.exp_ar, NEW.exp_ap, NEW.exp_inv, NEW.exp_payroll, NEW.ap_export_num, NEW.restrict_transfer_to, NEW.skpi, NEW.db_restrict, NEW.benefit, NEW.closed, NEW.closed_reason, NEW.inv_note, NEW.operate, NEW.default_menu, NEW.cafe_menu, NEW.vend_menu, NEW.default_menu_alias, NEW.cafe_menu_alias, NEW.vend_menu_alias, NEW.default_only, NEW.static_menu, NEW.no_nutrition, NEW.multi_cafe_menu, NEW.allow_sub_orders, NEW.unit_add, NEW.transfer, NEW.po_num, NEW.population, NEW.contract, NEW.contract_num, NEW.show_inv_price, NEW.show_inv_count, NEW.show_last_count, NEW.show_inv_par, NEW.use_nutrition, NEW.count_type, NEW.vend_start, NEW.vend_end, NEW.interco, NEW.categoryid, NEW.labor_notify_email, NEW.po_notify_email, NEW.timezone, NEW.acct_counter, NEW.cs_amounttype, NEW.cs_amount, NEW.cs_special, NEW.petty_cash, NEW.budget_type, NEW.use_mei_sales, NEW.auto_submit_labor, NEW.auto_submit_sales, NEW.over_time_type, NEW.sort, NEW.po_prefix, NEW.po_approval, NEW.import_pl, NEW.setup, NEW.kiosk_multi_inv, NEW.business_logo);
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `business_ad`;
DELIMITER //
CREATE TRIGGER `business_ad` AFTER DELETE ON `business`
 FOR EACH ROW BEGIN 
   DELETE FROM posreports_business WHERE businessid = OLD.businessid LIMIT 1;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `business_category`
--

DROP TABLE IF EXISTS `business_category`;
CREATE TABLE IF NOT EXISTS `business_category` (
  `categoryid` int(3) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`categoryid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `business_gateway_data`
--

DROP TABLE IF EXISTS `business_gateway_data`;
CREATE TABLE IF NOT EXISTS `business_gateway_data` (
  `businessid` int(11) unsigned NOT NULL,
  `store_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `store_number` int(10) unsigned NOT NULL DEFAULT '0',
  `store_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `store_pem_key` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `use_business_nutrition` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `businessid` (`businessid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `business_group`
--

DROP TABLE IF EXISTS `business_group`;
CREATE TABLE IF NOT EXISTS `business_group` (
  `group_id` int(10) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `business_group_detail`
--

DROP TABLE IF EXISTS `business_group_detail`;
CREATE TABLE IF NOT EXISTS `business_group_detail` (
  `group_id` int(10) NOT NULL,
  `businessid` int(10) NOT NULL,
  PRIMARY KEY (`group_id`,`businessid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `business_nutrition`
--

DROP TABLE IF EXISTS `business_nutrition`;
CREATE TABLE IF NOT EXISTS `business_nutrition` (
  `co_nut_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL,
  `date_stamp` date NOT NULL,
  `payment_type` int(11) NOT NULL COMMENT 'currently ck-card user or non-ck-card user',
  `unique_user` int(10) NOT NULL COMMENT 'unique ck-card #s or unique bill number based on payment_type',
  PRIMARY KEY (`co_nut_id`),
  UNIQUE KEY `businessid` (`businessid`,`payment_type`,`date_stamp`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='storage for data generated for company level nutrition' AUTO_INCREMENT=89 ;

-- --------------------------------------------------------

--
-- Table structure for table `business_nutrition_xref`
--

DROP TABLE IF EXISTS `business_nutrition_xref`;
CREATE TABLE IF NOT EXISTS `business_nutrition_xref` (
  `co_nut_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'company_nutrition.co_nut_id',
  `nutrition_type_id` int(3) unsigned NOT NULL COMMENT 'nutrition_types.nutritionid',
  `value` double NOT NULL,
  PRIMARY KEY (`co_nut_id`,`nutrition_type_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='crossreference between company_nutrition and nutrition_type' AUTO_INCREMENT=89 ;

-- --------------------------------------------------------

--
-- Table structure for table `business_setting`
--

DROP TABLE IF EXISTS `business_setting`;
CREATE TABLE IF NOT EXISTS `business_setting` (
  `business_id` int(10) unsigned NOT NULL COMMENT 'matches business_id from business table',
  `page_title` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'default page title or appended to $page_title',
  `layout_template` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT 'the layout template to use',
  `policy_dir` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT 'directory for policy controller templates',
  `site_warning` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'info to output if the user is using the wrong payment site',
  `background_image` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '/assets/images/nutrition/backdrop.jpg',
  `home_page_logo` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '/menu/rc-header.jpg',
  `site_logo` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '/menu/logo.jpg',
  PRIMARY KEY (`business_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `calendarTable`
--

DROP TABLE IF EXISTS `calendarTable`;
CREATE TABLE IF NOT EXISTS `calendarTable` (
  `calendarDate` date NOT NULL,
  `weekMonday` date NOT NULL,
  `weekTuesday` date NOT NULL,
  `weekWednesday` date NOT NULL,
  `weekThursday` date NOT NULL,
  `weekFriday` date NOT NULL,
  `weekSaturday` date NOT NULL,
  `weekSunday` date NOT NULL,
  `isWeekend` tinyint(1) DEFAULT NULL,
  `monthStart` date NOT NULL,
  `monthEnd` date NOT NULL,
  `yearStart` date NOT NULL,
  `yearEnd` date NOT NULL,
  `dateMonth` tinyint(3) unsigned NOT NULL,
  `dateYear` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`calendarDate`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `calender`
--

DROP TABLE IF EXISTS `calender`;
CREATE TABLE IF NOT EXISTS `calender` (
  `number` int(10) NOT NULL AUTO_INCREMENT,
  `brief` text NOT NULL,
  `date` date NOT NULL DEFAULT '0000-00-00',
  `comment` text NOT NULL,
  `personal` varchar(10) NOT NULL DEFAULT '',
  `user` varchar(25) NOT NULL DEFAULT '',
  `cont` tinyint(1) NOT NULL DEFAULT '0',
  `companyid` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`number`),
  KEY `date` (`date`,`companyid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2471 ;

-- --------------------------------------------------------

--
-- Table structure for table `cash`
--

DROP TABLE IF EXISTS `cash`;
CREATE TABLE IF NOT EXISTS `cash` (
  `cashid` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL DEFAULT '0',
  `companyid` int(3) NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `amount` double NOT NULL DEFAULT '0',
  `type` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cashid`),
  UNIQUE KEY `businessid` (`businessid`,`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=342800 ;

-- --------------------------------------------------------

--
-- Table structure for table `caterclose`
--

DROP TABLE IF EXISTS `caterclose`;
CREATE TABLE IF NOT EXISTS `caterclose` (
  `catercloseid` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL DEFAULT '0',
  `companyid` int(3) NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`catercloseid`),
  UNIQUE KEY `businessid` (`businessid`,`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=229 ;

-- --------------------------------------------------------

--
-- Table structure for table `catering_favs`
--

DROP TABLE IF EXISTS `catering_favs`;
CREATE TABLE IF NOT EXISTS `catering_favs` (
  `customerid` int(10) NOT NULL,
  `menu_item_id` int(10) NOT NULL,
  `value` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`customerid`,`menu_item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `caternote`
--

DROP TABLE IF EXISTS `caternote`;
CREATE TABLE IF NOT EXISTS `caternote` (
  `caternoteid` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL,
  `date` date NOT NULL,
  `enddate` date NOT NULL,
  `menu_typeid` int(5) NOT NULL,
  `note` text NOT NULL,
  `recipe_station` int(3) NOT NULL,
  PRIMARY KEY (`caternoteid`),
  KEY `businessid` (`businessid`,`date`,`menu_typeid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11884 ;

-- --------------------------------------------------------

--
-- Table structure for table `caterpolicy`
--

DROP TABLE IF EXISTS `caterpolicy`;
CREATE TABLE IF NOT EXISTS `caterpolicy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `businessid` int(11) NOT NULL,
  `policy` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `businessid` (`businessid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

-- --------------------------------------------------------

--
-- Table structure for table `cc_fee`
--

DROP TABLE IF EXISTS `cc_fee`;
CREATE TABLE IF NOT EXISTS `cc_fee` (
  `businessid` int(10) NOT NULL,
  `date` date NOT NULL,
  `amount` double NOT NULL,
  PRIMARY KEY (`businessid`,`date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `checkdetail`
--

DROP TABLE IF EXISTS `checkdetail`;
CREATE TABLE IF NOT EXISTS `checkdetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `businessid` int(11) NOT NULL,
  `bill_number` bigint(20) unsigned NOT NULL,
  `item_number` int(11) NOT NULL,
  `quantity` tinyint(1) NOT NULL,
  `price` double(10,2) NOT NULL,
  `cost` double(10,2) NOT NULL,
  `is_void` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `bill_number` (`bill_number`),
  KEY `checkdetail` (`businessid`,`bill_number`,`item_number`,`is_void`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=127855200 ;

--
-- Triggers `checkdetail`
--
DROP TRIGGER IF EXISTS `checkdetail_ai`;
DELIMITER //
CREATE TRIGGER `checkdetail_ai` AFTER INSERT ON `checkdetail`
 FOR EACH ROW BEGIN 
    INSERT IGNORE INTO posreports_checkdetail 
    (id, businessid, bill_number, item_number, quantity, price, cost, is_void) 
    VALUES 
    (NEW.id, NEW.businessid, NEW.bill_number, NEW.item_number, NEW.quantity, NEW.price, NEW.cost, NEW.is_void);
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `checkdetail_au`;
DELIMITER //
CREATE TRIGGER `checkdetail_au` AFTER UPDATE ON `checkdetail`
 FOR EACH ROW BEGIN 

	DELETE FROM posreports_checkdetail WHERE id = OLD.id;
	
    INSERT IGNORE INTO posreports_checkdetail 
    (id, businessid, bill_number, item_number, quantity, price, cost, is_void) 
    VALUES 
    (OLD.id, NEW.businessid, NEW.bill_number, NEW.item_number, NEW.quantity, NEW.price, NEW.cost, NEW.is_void);
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `checkdetail_ad`;
DELIMITER //
CREATE TRIGGER `checkdetail_ad` AFTER DELETE ON `checkdetail`
 FOR EACH ROW BEGIN 
   DELETE FROM posreports_checkdetail WHERE id = OLD.id LIMIT 1;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `checkdiscounts`
--

DROP TABLE IF EXISTS `checkdiscounts`;
CREATE TABLE IF NOT EXISTS `checkdiscounts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `check_number` bigint(20) unsigned NOT NULL,
  `businessid` int(10) NOT NULL,
  `pos_id` int(5) NOT NULL,
  `amount` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `check_number` (`check_number`,`businessid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3020845 ;

-- --------------------------------------------------------

--
-- Table structure for table `checks`
--

DROP TABLE IF EXISTS `checks`;
CREATE TABLE IF NOT EXISTS `checks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `businessid` int(11) NOT NULL,
  `check_number` bigint(20) unsigned NOT NULL,
  `session_number` int(11) NOT NULL,
  `table_number` int(5) NOT NULL,
  `seat_number` int(5) NOT NULL,
  `emp_no` int(11) NOT NULL,
  `people` int(5) NOT NULL,
  `bill_datetime` datetime NOT NULL,
  `bill_posted` datetime NOT NULL,
  `total` double(10,2) NOT NULL,
  `taxes` double(10,2) NOT NULL,
  `received` decimal(10,2) NOT NULL,
  `sale_type` int(5) NOT NULL,
  `account_number` bigint(20) NOT NULL,
  `terminal_group` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `is_void` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `businessid` (`businessid`,`check_number`,`bill_datetime`,`is_void`,`total`),
  KEY `account_number` (`account_number`),
  KEY `bill_datetime` (`bill_datetime`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18121807 ;

--
-- Triggers `checks`
--
DROP TRIGGER IF EXISTS `checks_ai`;
DELIMITER //
CREATE TRIGGER `checks_ai` AFTER INSERT ON `checks`
 FOR EACH ROW BEGIN 
    INSERT IGNORE INTO posreports_checks 
    (id, businessid, check_number, session_number, table_number, seat_number, emp_no, people, bill_datetime, bill_posted, total, taxes, received, sale_type, account_number, terminal_group, is_void) 
    VALUES 
    (NEW.id, NEW.businessid, NEW.check_number, NEW.session_number, NEW.table_number, NEW.seat_number, NEW.emp_no, NEW.people, NEW.bill_datetime, NEW.bill_posted, NEW.total, NEW.taxes, NEW.received, NEW.sale_type, NEW.account_number, NEW.terminal_group, NEW.is_void);
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `checks_au`;
DELIMITER //
CREATE TRIGGER `checks_au` AFTER UPDATE ON `checks`
 FOR EACH ROW BEGIN 
	
	DELETE FROM posreports_checks WHERE id = OLD.id;
	
    INSERT IGNORE INTO posreports_checks 
    (id, businessid, check_number, session_number, table_number, seat_number, emp_no, people, bill_datetime, bill_posted, total, taxes, received, sale_type, account_number, terminal_group, is_void) 
    VALUES 
    (OLD.id, NEW.businessid, NEW.check_number, NEW.session_number, NEW.table_number, NEW.seat_number, NEW.emp_no, NEW.people, NEW.bill_datetime, NEW.bill_posted, NEW.total, NEW.taxes, NEW.received, NEW.sale_type, NEW.account_number, NEW.terminal_group, NEW.is_void);
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `checks_ad`;
DELIMITER //
CREATE TRIGGER `checks_ad` AFTER DELETE ON `checks`
 FOR EACH ROW BEGIN 
   DELETE FROM posreports_checks WHERE id = OLD.id LIMIT 1;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `com_kpi`
--

DROP TABLE IF EXISTS `com_kpi`;
CREATE TABLE IF NOT EXISTS `com_kpi` (
  `com_kpi_id` int(19) NOT NULL AUTO_INCREMENT,
  `companyid` int(3) NOT NULL,
  `sales` double NOT NULL,
  `cos` double NOT NULL,
  `labor` double NOT NULL,
  `toe` double NOT NULL,
  `pl` double NOT NULL,
  `date` date NOT NULL,
  `sales_bgt` double NOT NULL,
  `cos_bgt` double NOT NULL,
  `lbr_bgt` double NOT NULL,
  `toe_bgt` double NOT NULL,
  `pl_bgt` double NOT NULL,
  PRIMARY KEY (`com_kpi_id`),
  KEY `date` (`date`,`companyid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=98 ;

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
CREATE TABLE IF NOT EXISTS `company` (
  `companyid` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `company_type` int(3) NOT NULL,
  `companyname` varchar(50) NOT NULL DEFAULT '',
  `reference` varchar(40) NOT NULL,
  `street` varchar(40) NOT NULL,
  `city` varchar(30) NOT NULL DEFAULT '',
  `state` char(2) NOT NULL DEFAULT '',
  `zip` varchar(10) NOT NULL DEFAULT '',
  `phone` varchar(25) NOT NULL DEFAULT '',
  `code` varchar(5) NOT NULL,
  `exportby` varchar(30) NOT NULL DEFAULT '',
  `exportdate` date NOT NULL DEFAULT '0000-00-00',
  `last_export` varchar(2) NOT NULL,
  `import_num` int(2) NOT NULL DEFAULT '0',
  `import_num2` int(2) NOT NULL DEFAULT '0',
  `reply` int(10) NOT NULL,
  `rec_accesscode` varchar(20) NOT NULL,
  `exporting` tinyint(1) NOT NULL DEFAULT '0',
  `week_start` varchar(10) NOT NULL DEFAULT 'Friday',
  `week_end` varchar(10) NOT NULL DEFAULT 'Thursday',
  `payroll_date` date NOT NULL,
  `ar_update` date NOT NULL,
  `base_pay` double NOT NULL DEFAULT '0',
  `holiday_pay` double NOT NULL DEFAULT '0',
  `invoice_name` varchar(50) NOT NULL DEFAULT 'Treat America Dining',
  `invoice_remit` varchar(50) NOT NULL DEFAULT 'Treat America Food Services',
  `logo` varchar(50) NOT NULL,
  `backdrop` varchar(50) NOT NULL,
  `business_hierarchy` int(11) DEFAULT NULL,
  `ul_budget` tinyint(1) NOT NULL DEFAULT '0',
  `dl_budget` tinyint(1) NOT NULL DEFAULT '0',
  `ul_estimate` tinyint(1) NOT NULL DEFAULT '0',
  `dl_estimate` tinyint(1) NOT NULL DEFAULT '0',
  `budget_year` int(5) NOT NULL,
  `estimate_year` int(5) NOT NULL,
  `master_vendor_id` int(10) NOT NULL,
  `company_code` int(10) NOT NULL,
  `client_program_id` int(10) NOT NULL,
  PRIMARY KEY (`companyid`),
  KEY `companyid` (`companyid`,`company_type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=72 ;

--
-- Triggers `company`
--
DROP TRIGGER IF EXISTS `company_ai`;
DELIMITER //
CREATE TRIGGER `company_ai` AFTER INSERT ON `company`
 FOR EACH ROW BEGIN 
    INSERT IGNORE INTO posreports_company 
    (companyid, company_type, companyname, reference, street, city, state, zip, phone, code, exportby, exportdate, last_export, import_num, import_num2, reply, rec_accesscode, exporting, week_start, week_end, payroll_date, ar_update, base_pay, holiday_pay, invoice_name, invoice_remit, logo, backdrop, business_hierarchy, ul_budget, dl_budget, ul_estimate, dl_estimate, master_vendor_id, company_code) 
    VALUES 
    (NEW.companyid, NEW.company_type, NEW.companyname, NEW.reference, NEW.street, NEW.city, NEW.state, NEW.zip, NEW.phone, NEW.code, NEW.exportby, NEW.exportdate, NEW.last_export, NEW.import_num, NEW.import_num2, NEW.reply, NEW.rec_accesscode, NEW.exporting, NEW.week_start, NEW.week_end, NEW.payroll_date, NEW.ar_update, NEW.base_pay, NEW.holiday_pay, NEW.invoice_name, NEW.invoice_remit, NEW.logo, NEW.backdrop, NEW.business_hierarchy, NEW.ul_budget, NEW.dl_budget, NEW.ul_estimate, NEW.dl_estimate, NEW.master_vendor_id, NEW.company_code);
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `company_au`;
DELIMITER //
CREATE TRIGGER `company_au` AFTER UPDATE ON `company`
 FOR EACH ROW BEGIN 
	
	DELETE FROM posreports_company WHERE companyid = OLD.companyid;
	
    INSERT IGNORE INTO posreports_company 
    (companyid, company_type, companyname, reference, street, city, state, zip, phone, code, exportby, exportdate, last_export, import_num, import_num2, reply, rec_accesscode, exporting, week_start, week_end, payroll_date, ar_update, base_pay, holiday_pay, invoice_name, invoice_remit, logo, backdrop, business_hierarchy, ul_budget, dl_budget, ul_estimate, dl_estimate, master_vendor_id, company_code) 
    VALUES 
    (OLD.companyid, NEW.company_type, NEW.companyname, NEW.reference, NEW.street, NEW.city, NEW.state, NEW.zip, NEW.phone, NEW.code, NEW.exportby, NEW.exportdate, NEW.last_export, NEW.import_num, NEW.import_num2, NEW.reply, NEW.rec_accesscode, NEW.exporting, NEW.week_start, NEW.week_end, NEW.payroll_date, NEW.ar_update, NEW.base_pay, NEW.holiday_pay, NEW.invoice_name, NEW.invoice_remit, NEW.logo, NEW.backdrop, NEW.business_hierarchy, NEW.ul_budget, NEW.dl_budget, NEW.ul_estimate, NEW.dl_estimate, NEW.master_vendor_id, NEW.company_code);
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `company_ad`;
DELIMITER //
CREATE TRIGGER `company_ad` AFTER DELETE ON `company`
 FOR EACH ROW BEGIN 
   DELETE FROM posreports_company WHERE companyid = OLD.companyid LIMIT 1;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `company_nutrition`
--

DROP TABLE IF EXISTS `company_nutrition`;
CREATE TABLE IF NOT EXISTS `company_nutrition` (
  `co_nut_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL,
  `date_stamp` date NOT NULL,
  `payment_type` int(11) NOT NULL COMMENT 'currently ck-card user or non-ck-card user',
  `unique_user` int(10) NOT NULL COMMENT 'unique ck-card #s or unique bill number based on payment_type',
  PRIMARY KEY (`co_nut_id`),
  UNIQUE KEY `businessid` (`businessid`,`payment_type`,`date_stamp`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='storage for data generated for company level nutrition' AUTO_INCREMENT=79 ;

-- --------------------------------------------------------

--
-- Table structure for table `company_nutrition_xref`
--

DROP TABLE IF EXISTS `company_nutrition_xref`;
CREATE TABLE IF NOT EXISTS `company_nutrition_xref` (
  `co_nut_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'company_nutrition.co_nut_id',
  `nutrition_type_id` int(3) unsigned NOT NULL COMMENT 'nutrition_types.nutritionid',
  `value` double NOT NULL,
  PRIMARY KEY (`co_nut_id`,`nutrition_type_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='crossreference between company_nutrition and nutrition_type' AUTO_INCREMENT=79 ;

-- --------------------------------------------------------

--
-- Table structure for table `company_type`
--

DROP TABLE IF EXISTS `company_type`;
CREATE TABLE IF NOT EXISTS `company_type` (
  `typeid` int(3) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`typeid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `contract_temp`
--

DROP TABLE IF EXISTS `contract_temp`;
CREATE TABLE IF NOT EXISTS `contract_temp` (
  `tempid` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL,
  `menu_itemid` int(10) NOT NULL,
  `item_name` varchar(50) NOT NULL,
  `total` double NOT NULL,
  `order_units` varchar(20) NOT NULL,
  PRIMARY KEY (`tempid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=420037 ;

-- --------------------------------------------------------

--
-- Table structure for table `contract_temp2`
--

DROP TABLE IF EXISTS `contract_temp2`;
CREATE TABLE IF NOT EXISTS `contract_temp2` (
  `tempid` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL,
  `menu_itemid` int(10) NOT NULL,
  `accountid` int(10) NOT NULL,
  `menu_itemname` varchar(40) NOT NULL,
  `accountname` varchar(50) NOT NULL,
  `total` double NOT NULL,
  `units` varchar(20) NOT NULL,
  `driver` varchar(20) NOT NULL,
  PRIMARY KEY (`tempid`),
  KEY `businessid` (`businessid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1118294 ;

-- --------------------------------------------------------

--
-- Table structure for table `controlsheet`
--

DROP TABLE IF EXISTS `controlsheet`;
CREATE TABLE IF NOT EXISTS `controlsheet` (
  `csid` int(10) NOT NULL AUTO_INCREMENT,
  `companyid` int(2) NOT NULL DEFAULT '0',
  `businessid` int(10) NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `acct_payableid` int(10) NOT NULL DEFAULT '0',
  `amount1` double NOT NULL DEFAULT '0',
  `amount2` double NOT NULL DEFAULT '0',
  `amount3` double NOT NULL DEFAULT '0',
  `amount4` double NOT NULL DEFAULT '0',
  `amount5` double NOT NULL DEFAULT '0',
  `amount6` double NOT NULL DEFAULT '0',
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `amt1_comment` varchar(100) NOT NULL,
  `amt2_comment` varchar(100) NOT NULL,
  `amt3_comment` varchar(100) NOT NULL,
  `amt4_comment` varchar(100) NOT NULL,
  `amt5_comment` varchar(100) NOT NULL,
  `amt6_comment` varchar(100) NOT NULL,
  PRIMARY KEY (`csid`),
  KEY `businessid` (`businessid`,`date`,`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6411 ;

-- --------------------------------------------------------

--
-- Table structure for table `conversion`
--

DROP TABLE IF EXISTS `conversion`;
CREATE TABLE IF NOT EXISTS `conversion` (
  `conversionid` int(3) NOT NULL AUTO_INCREMENT,
  `sizeid` int(3) NOT NULL,
  `sizeidto` int(3) NOT NULL,
  `convert` float NOT NULL,
  PRIMARY KEY (`conversionid`),
  KEY `sizeid` (`sizeid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=48 ;

-- --------------------------------------------------------

--
-- Table structure for table `cost_history`
--

DROP TABLE IF EXISTS `cost_history`;
CREATE TABLE IF NOT EXISTS `cost_history` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `inv_itemid` int(10) NOT NULL,
  `date` date NOT NULL,
  `cost` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `inv_itemid` (`inv_itemid`,`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=876527 ;

-- --------------------------------------------------------

--
-- Table structure for table `creditdetail`
--

DROP TABLE IF EXISTS `creditdetail`;
CREATE TABLE IF NOT EXISTS `creditdetail` (
  `creditdetailid` int(10) NOT NULL AUTO_INCREMENT,
  `companyid` int(3) NOT NULL DEFAULT '0',
  `businessid` int(10) NOT NULL DEFAULT '0',
  `amount` double NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `creditid` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`creditdetailid`),
  UNIQUE KEY `creditid` (`creditid`,`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14668042 ;

-- --------------------------------------------------------

--
-- Table structure for table `credits`
--

DROP TABLE IF EXISTS `credits`;
CREATE TABLE IF NOT EXISTS `credits` (
  `creditid` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL DEFAULT '0',
  `companyid` int(3) NOT NULL DEFAULT '0',
  `creditname` varchar(30) NOT NULL DEFAULT '',
  `credittype` int(10) NOT NULL DEFAULT '0',
  `account` varchar(20) NOT NULL,
  `invoicesales` tinyint(1) NOT NULL DEFAULT '0',
  `invoicesales_notax` tinyint(1) NOT NULL DEFAULT '0',
  `invoicetax` tinyint(1) NOT NULL DEFAULT '0',
  `servchrg` tinyint(1) NOT NULL DEFAULT '0',
  `salescode` varchar(10) NOT NULL DEFAULT '0',
  `heldcheck` tinyint(1) NOT NULL DEFAULT '0',
  `tax_prcnt` float NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `del_date` date NOT NULL,
  `parent` int(10) NOT NULL,
  `category` int(5) NOT NULL,
  PRIMARY KEY (`creditid`),
  KEY `businessid_2` (`businessid`,`is_deleted`),
  KEY `businessid` (`businessid`,`credittype`,`is_deleted`,`del_date`,`parent`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9447 ;

-- --------------------------------------------------------

--
-- Table structure for table `credittype`
--

DROP TABLE IF EXISTS `credittype`;
CREATE TABLE IF NOT EXISTS `credittype` (
  `credittypeid` int(10) NOT NULL AUTO_INCREMENT,
  `credittypename` varchar(25) NOT NULL DEFAULT '',
  `companyid` int(3) NOT NULL DEFAULT '0',
  `ordered` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`credittypeid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `cs_memo`
--

DROP TABLE IF EXISTS `cs_memo`;
CREATE TABLE IF NOT EXISTS `cs_memo` (
  `cs_id` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL,
  `year` int(4) NOT NULL,
  `month` int(2) NOT NULL,
  `week` tinyint(1) NOT NULL,
  `memo` text NOT NULL,
  `user` varchar(30) NOT NULL,
  PRIMARY KEY (`cs_id`),
  KEY `year` (`year`,`month`,`week`,`businessid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9900 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
CREATE TABLE IF NOT EXISTS `customer` (
  `customerid` int(10) NOT NULL AUTO_INCREMENT,
  `pos_processed` tinyint(1) NOT NULL DEFAULT '0',
  `accountid` int(10) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `username` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_new` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `businessid` int(10) NOT NULL DEFAULT '0',
  `scancode` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `first_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `last_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `birthday` date DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL COMMENT '1 = male, 2 = female',
  `temp_password` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `temp_used` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `balance` decimal(10,2) NOT NULL,
  `customid` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `inactive` tinyint(4) NOT NULL DEFAULT '0',
  `notify_via_email` tinyint(1) DEFAULT NULL,
  `notify_via_sms` tinyint(1) DEFAULT NULL,
  `notify_sms_number` varchar(14) COLLATE utf8_unicode_ci NOT NULL,
  `single_use` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`customerid`),
  UNIQUE KEY `username` (`username`),
  KEY `username_2` (`username`,`password`),
  KEY `businessid` (`businessid`),
  KEY `scancode` (`scancode`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=183686 ;

--
-- Triggers `customer`
--
DROP TRIGGER IF EXISTS `customer_bi`;
DELIMITER //
CREATE TRIGGER `customer_bi` BEFORE INSERT ON `customer`
 FOR EACH ROW BEGIN 
	IF (NEW.businessid = 750) THEN 
		SET NEW.inactive = 0; 
	END IF;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_costcenter`
--

DROP TABLE IF EXISTS `customer_costcenter`;
CREATE TABLE IF NOT EXISTS `customer_costcenter` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL,
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `number` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=29 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_limits`
--

DROP TABLE IF EXISTS `customer_limits`;
CREATE TABLE IF NOT EXISTS `customer_limits` (
  `limitid` int(10) NOT NULL AUTO_INCREMENT,
  `customerid` int(10) NOT NULL,
  `nutrition_type_id` int(2) unsigned NOT NULL COMMENT 'matches the nutrition_types table id',
  `display` tinyint(1) NOT NULL COMMENT 'display in the week listing',
  `value` double DEFAULT NULL COMMENT 'exceeded limit will show a warning',
  PRIMARY KEY (`limitid`),
  UNIQUE KEY `customerid` (`customerid`,`nutrition_type_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6692 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_lozier`
--

DROP TABLE IF EXISTS `customer_lozier`;
CREATE TABLE IF NOT EXISTS `customer_lozier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `pswd` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `card_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_lozier2`
--

DROP TABLE IF EXISTS `customer_lozier2`;
CREATE TABLE IF NOT EXISTS `customer_lozier2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `card_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `customerid_value` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=86 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_nutrition`
--

DROP TABLE IF EXISTS `customer_nutrition`;
CREATE TABLE IF NOT EXISTS `customer_nutrition` (
  `customerid` int(10) NOT NULL,
  `weeknum` int(1) NOT NULL DEFAULT '5',
  `calories` tinyint(1) NOT NULL,
  `cal_from_fat` tinyint(1) NOT NULL,
  `total_fat` tinyint(1) NOT NULL,
  `sat_fat` tinyint(1) NOT NULL,
  `cholesterol` tinyint(1) NOT NULL,
  `sodium` tinyint(1) NOT NULL,
  `potassium` tinyint(1) NOT NULL,
  `carbs` tinyint(1) NOT NULL,
  `fiber` tinyint(1) NOT NULL,
  `sugar` tinyint(1) NOT NULL,
  `protein` tinyint(1) NOT NULL,
  `vit_A` tinyint(1) NOT NULL,
  `vit_C` tinyint(1) NOT NULL,
  `thiamine` tinyint(1) NOT NULL,
  `iron` tinyint(1) NOT NULL,
  `treat_score` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`customerid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customer_scancodes`
--

DROP TABLE IF EXISTS `customer_scancodes`;
CREATE TABLE IF NOT EXISTS `customer_scancodes` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL,
  `scan_start` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `scan_end` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `value` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `scan_start` (`scan_start`,`scan_end`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1419 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_transaction_types`
--

DROP TABLE IF EXISTS `customer_transaction_types`;
CREATE TABLE IF NOT EXISTS `customer_transaction_types` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_transactions`
--

DROP TABLE IF EXISTS `customer_transactions`;
CREATE TABLE IF NOT EXISTS `customer_transactions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `oid` varchar(35) CHARACTER SET utf8 NOT NULL COMMENT 'The order id that was sent to or received from linkpoint',
  `trans_type_id` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'matches id field in customer _transaction _types table',
  `customer_id` int(11) unsigned NOT NULL,
  `pos_processed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `subtotal` float NOT NULL,
  `chargetotal` float NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `authcode` varchar(15) CHARACTER SET utf8 NOT NULL,
  `response` varchar(15) CHARACTER SET utf8 NOT NULL,
  `authresponse` text CHARACTER SET utf8 NOT NULL,
  `response_messages` text CHARACTER SET utf8 NOT NULL,
  `response_tdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `response_avs` text CHARACTER SET utf8 NOT NULL,
  `response_token` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `customer_id_2` (`customer_id`,`date_created`),
  KEY `customer_id` (`customer_id`,`pos_processed`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=275552 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_transactions_debug`
--

DROP TABLE IF EXISTS `customer_transactions_debug`;
CREATE TABLE IF NOT EXISTS `customer_transactions_debug` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned NOT NULL,
  `transaction_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `order_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `response` text COLLATE utf8_unicode_ci NOT NULL,
  `response_xml` text COLLATE utf8_unicode_ci NOT NULL,
  `request` text COLLATE utf8_unicode_ci NOT NULL,
  `request_xml` text COLLATE utf8_unicode_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_z_tracker`
--

DROP TABLE IF EXISTS `customer_z_tracker`;
CREATE TABLE IF NOT EXISTS `customer_z_tracker` (
  `companyid` int(11) NOT NULL,
  `businessid` int(11) NOT NULL,
  `customerid` int(11) NOT NULL,
  `scancode` varchar(20) NOT NULL,
  `stored_balance` decimal(6,2) NOT NULL,
  `calculated_balance` decimal(6,2) NOT NULL,
  `oob_amount` decimal(6,2) NOT NULL,
  `out_of_balance` tinyint(4) NOT NULL,
  KEY `customerid` (`customerid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dashboard`
--

DROP TABLE IF EXISTS `dashboard`;
CREATE TABLE IF NOT EXISTS `dashboard` (
  `dashid` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(5) NOT NULL,
  `date` date NOT NULL,
  `sales` double NOT NULL,
  `sales_bgt` double NOT NULL,
  `labor` double NOT NULL,
  `labor_bgt` double NOT NULL,
  `cos` double NOT NULL,
  `cos_bgt` double NOT NULL,
  `toe` double NOT NULL,
  `toe_bgt` double NOT NULL,
  `pl` double NOT NULL,
  `pl_bgt` double NOT NULL,
  `customers` int(5) NOT NULL,
  `labor_hours` double NOT NULL,
  `waste` double DEFAULT NULL,
  `shrink` double DEFAULT NULL,
  `last_inv_date` datetime DEFAULT NULL,
  PRIMARY KEY (`dashid`),
  UNIQUE KEY `businessid` (`businessid`,`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3292983 ;

-- --------------------------------------------------------

--
-- Table structure for table `dashboard_aging`
--

DROP TABLE IF EXISTS `dashboard_aging`;
CREATE TABLE IF NOT EXISTS `dashboard_aging` (
  `businessid` int(5) NOT NULL,
  `zero` double NOT NULL,
  `thirty` double NOT NULL,
  `sixty` double NOT NULL,
  `ninety` double NOT NULL,
  PRIMARY KEY (`businessid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dashboard_aging_salesmen`
--

DROP TABLE IF EXISTS `dashboard_aging_salesmen`;
CREATE TABLE IF NOT EXISTS `dashboard_aging_salesmen` (
  `salesmanid` int(5) NOT NULL,
  `zero` double NOT NULL,
  `thirty` double NOT NULL,
  `sixty` double NOT NULL,
  `ninety` double NOT NULL,
  PRIMARY KEY (`salesmanid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dashboard_files`
--

DROP TABLE IF EXISTS `dashboard_files`;
CREATE TABLE IF NOT EXISTS `dashboard_files` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `businessid` int(5) NOT NULL,
  `file_name` varchar(100) NOT NULL,
  `display_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=184 ;

-- --------------------------------------------------------

--
-- Table structure for table `dashboard_machine`
--

DROP TABLE IF EXISTS `dashboard_machine`;
CREATE TABLE IF NOT EXISTS `dashboard_machine` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `machineid` varchar(10) NOT NULL,
  `businessid` int(8) NOT NULL,
  `login_routeid` int(10) NOT NULL,
  `accountid` int(10) NOT NULL,
  `collects` double NOT NULL,
  `stops` int(2) NOT NULL,
  `service` int(2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `machineid` (`machineid`,`businessid`,`date`),
  KEY `login_routeid` (`date`,`login_routeid`),
  KEY `businessid` (`businessid`,`date`),
  KEY `accountid` (`accountid`,`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1252742 ;

-- --------------------------------------------------------

--
-- Table structure for table `dashboard_route`
--

DROP TABLE IF EXISTS `dashboard_route`;
CREATE TABLE IF NOT EXISTS `dashboard_route` (
  `dashid` int(10) NOT NULL AUTO_INCREMENT,
  `login_routeid` int(5) NOT NULL,
  `date` date NOT NULL,
  `collection` double NOT NULL,
  `stops` double NOT NULL,
  `service` int(5) NOT NULL,
  PRIMARY KEY (`dashid`),
  KEY `date` (`date`,`login_routeid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=47110 ;

-- --------------------------------------------------------

--
-- Table structure for table `dashboard_update`
--

DROP TABLE IF EXISTS `dashboard_update`;
CREATE TABLE IF NOT EXISTS `dashboard_update` (
  `overview` varchar(25) DEFAULT NULL,
  `customer` varchar(25) DEFAULT NULL,
  `routes` varchar(25) DEFAULT NULL,
  `route_drivers` varchar(25) DEFAULT NULL,
  `vehicles` varchar(25) DEFAULT NULL,
  `service` varchar(25) DEFAULT NULL,
  `sales` varchar(25) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `db_cache`
--

DROP TABLE IF EXISTS `db_cache`;
CREATE TABLE IF NOT EXISTS `db_cache` (
  `cacheId` int(10) unsigned NOT NULL,
  `viewid` int(10) unsigned NOT NULL,
  `dataXml` longtext,
  `cacheDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`cacheId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `db_custom`
--

DROP TABLE IF EXISTS `db_custom`;
CREATE TABLE IF NOT EXISTS `db_custom` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `viewid` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `custom_field` varchar(50) NOT NULL,
  `future_dates` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `viewid` (`viewid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=251 ;

--
-- Triggers `db_custom`
--
DROP TRIGGER IF EXISTS `mtime_insert_custom`;
DELIMITER //
CREATE TRIGGER `mtime_insert_custom` AFTER INSERT ON `db_custom`
 FOR EACH ROW call trigUpdateDbMTime( NEW.viewid )
//
DELIMITER ;
DROP TRIGGER IF EXISTS `mtime_update_custom`;
DELIMITER //
CREATE TRIGGER `mtime_update_custom` AFTER UPDATE ON `db_custom`
 FOR EACH ROW call trigUpdateDbMTime( NEW.viewid )
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `db_group`
--

DROP TABLE IF EXISTS `db_group`;
CREATE TABLE IF NOT EXISTS `db_group` (
  `groupid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `groupname` varchar(30) NOT NULL DEFAULT '',
  `groupowner` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`groupid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

-- --------------------------------------------------------

--
-- Table structure for table `db_group_graphs`
--

DROP TABLE IF EXISTS `db_group_graphs`;
CREATE TABLE IF NOT EXISTS `db_group_graphs` (
  `viewid` int(10) unsigned NOT NULL,
  `groupid` int(10) unsigned NOT NULL,
  `dbSide` smallint(6) DEFAULT '1',
  `dbHeight` int(11) DEFAULT '250',
  `dbOrder` int(11) DEFAULT NULL,
  PRIMARY KEY (`viewid`,`groupid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `db_group_members`
--

DROP TABLE IF EXISTS `db_group_members`;
CREATE TABLE IF NOT EXISTS `db_group_members` (
  `groupid` int(10) unsigned NOT NULL,
  `loginid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`groupid`,`loginid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `db_public`
--

DROP TABLE IF EXISTS `db_public`;
CREATE TABLE IF NOT EXISTS `db_public` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupname` varchar(30) NOT NULL,
  `dbDefault` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

-- --------------------------------------------------------

--
-- Table structure for table `db_trendlines`
--

DROP TABLE IF EXISTS `db_trendlines`;
CREATE TABLE IF NOT EXISTS `db_trendlines` (
  `trendid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `viewid` int(10) unsigned NOT NULL,
  `lineStart` double DEFAULT NULL,
  `lineEnd` double DEFAULT NULL,
  `trendCaption` varchar(128) DEFAULT '',
  `lineColor` char(6) DEFAULT 'ff0000',
  `lineWidth` int(10) unsigned NOT NULL DEFAULT '1',
  `linePosition` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`trendid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Triggers `db_trendlines`
--
DROP TRIGGER IF EXISTS `mtime_insert_trendlines`;
DELIMITER //
CREATE TRIGGER `mtime_insert_trendlines` AFTER INSERT ON `db_trendlines`
 FOR EACH ROW call trigUpdateDbMTime( NEW.viewid )
//
DELIMITER ;
DROP TRIGGER IF EXISTS `mtime_update_trendlines`;
DELIMITER //
CREATE TRIGGER `mtime_update_trendlines` AFTER UPDATE ON `db_trendlines`
 FOR EACH ROW call trigUpdateDbMTime( NEW.viewid )
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `db_view`
--

DROP TABLE IF EXISTS `db_view`;
CREATE TABLE IF NOT EXISTS `db_view` (
  `viewid` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `loginid` int(10) NOT NULL,
  `graph_type` int(3) NOT NULL,
  `size` tinyint(1) NOT NULL DEFAULT '1',
  `db_height` smallint(6) DEFAULT '250',
  `xaxis` tinyint(4) DEFAULT '0',
  `db_order` int(2) NOT NULL,
  `period` tinyint(1) NOT NULL,
  `period_num` tinyint(1) NOT NULL,
  `period_group` tinyint(1) NOT NULL,
  `current` tinyint(1) NOT NULL,
  `week_ends` varchar(8) NOT NULL DEFAULT 'Thursday',
  `db_mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`viewid`),
  KEY `loginid` (`loginid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=341 ;

-- --------------------------------------------------------

--
-- Table structure for table `db_view_order`
--

DROP TABLE IF EXISTS `db_view_order`;
CREATE TABLE IF NOT EXISTS `db_view_order` (
  `viewid` int(10) unsigned NOT NULL,
  `loginid` int(10) unsigned NOT NULL,
  `dbOrder` int(11) DEFAULT NULL,
  `dbSide` int(11) DEFAULT NULL,
  `dbHeight` int(11) DEFAULT NULL,
  PRIMARY KEY (`viewid`,`loginid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `db_what`
--

DROP TABLE IF EXISTS `db_what`;
CREATE TABLE IF NOT EXISTS `db_what` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `viewid` int(10) NOT NULL,
  `type` int(2) NOT NULL,
  `what` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `viewid` (`viewid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=729 ;

--
-- Triggers `db_what`
--
DROP TRIGGER IF EXISTS `mtime_insert_what`;
DELIMITER //
CREATE TRIGGER `mtime_insert_what` AFTER INSERT ON `db_what`
 FOR EACH ROW call trigUpdateDbMTime( NEW.viewid )
//
DELIMITER ;
DROP TRIGGER IF EXISTS `mtime_update_what`;
DELIMITER //
CREATE TRIGGER `mtime_update_what` AFTER UPDATE ON `db_what`
 FOR EACH ROW call trigUpdateDbMTime( NEW.viewid )
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `db_who`
--

DROP TABLE IF EXISTS `db_who`;
CREATE TABLE IF NOT EXISTS `db_who` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `viewid` int(10) NOT NULL,
  `type` int(2) NOT NULL,
  `viewtype` int(3) NOT NULL,
  `viewbus` int(10) NOT NULL,
  `viewbus2` int(5) NOT NULL,
  `expand` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `viewid` (`viewid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1087 ;

--
-- Triggers `db_who`
--
DROP TRIGGER IF EXISTS `mtime_insert_who`;
DELIMITER //
CREATE TRIGGER `mtime_insert_who` AFTER INSERT ON `db_who`
 FOR EACH ROW call trigUpdateDbMTime( NEW.viewid )
//
DELIMITER ;
DROP TRIGGER IF EXISTS `mtime_update_who`;
DELIMITER //
CREATE TRIGGER `mtime_update_who` AFTER UPDATE ON `db_who`
 FOR EACH ROW call trigUpdateDbMTime( NEW.viewid )
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `debitdetail`
--

DROP TABLE IF EXISTS `debitdetail`;
CREATE TABLE IF NOT EXISTS `debitdetail` (
  `debitdetailid` int(10) NOT NULL AUTO_INCREMENT,
  `companyid` int(3) NOT NULL DEFAULT '0',
  `businessid` int(10) NOT NULL DEFAULT '0',
  `amount` double NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `debitid` int(10) NOT NULL DEFAULT '0',
  `bank_rec` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`debitdetailid`),
  UNIQUE KEY `debitid` (`debitid`,`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10222570 ;

-- --------------------------------------------------------

--
-- Table structure for table `debits`
--

DROP TABLE IF EXISTS `debits`;
CREATE TABLE IF NOT EXISTS `debits` (
  `debitid` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL DEFAULT '0',
  `companyid` int(3) NOT NULL DEFAULT '0',
  `debitname` varchar(50) NOT NULL DEFAULT '',
  `debittype` int(10) NOT NULL DEFAULT '0',
  `account` varchar(20) NOT NULL,
  `inv_tender` tinyint(1) NOT NULL DEFAULT '0',
  `heldcheck` tinyint(1) NOT NULL DEFAULT '0',
  `category` int(5) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `del_date` date NOT NULL,
  PRIMARY KEY (`debitid`),
  KEY `busid` (`businessid`,`debittype`,`account`),
  KEY `businessid` (`businessid`,`is_deleted`),
  KEY `businessid_2` (`businessid`,`account`),
  KEY `businessid_3` (`businessid`,`category`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8233 ;

-- --------------------------------------------------------

--
-- Table structure for table `debittype`
--

DROP TABLE IF EXISTS `debittype`;
CREATE TABLE IF NOT EXISTS `debittype` (
  `debittypeid` int(10) NOT NULL AUTO_INCREMENT,
  `debittypename` varchar(25) NOT NULL DEFAULT '',
  `companyid` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`debittypeid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `deposit`
--

DROP TABLE IF EXISTS `deposit`;
CREATE TABLE IF NOT EXISTS `deposit` (
  `depositid` int(10) NOT NULL AUTO_INCREMENT,
  `companyid` int(3) NOT NULL,
  `date` date NOT NULL,
  `user` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `export` tinyint(1) NOT NULL,
  `account` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`depositid`),
  KEY `companyid` (`companyid`,`export`,`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=37 ;

-- --------------------------------------------------------

--
-- Table structure for table `depositdetail`
--

DROP TABLE IF EXISTS `depositdetail`;
CREATE TABLE IF NOT EXISTS `depositdetail` (
  `detailid` int(10) NOT NULL AUTO_INCREMENT,
  `depositid` int(10) NOT NULL,
  `checknum` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `total` double NOT NULL,
  PRIMARY KEY (`detailid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=96 ;

-- --------------------------------------------------------

--
-- Table structure for table `depositinvoice`
--

DROP TABLE IF EXISTS `depositinvoice`;
CREATE TABLE IF NOT EXISTS `depositinvoice` (
  `dep_inv_id` int(100) NOT NULL AUTO_INCREMENT,
  `detailid` int(10) NOT NULL,
  `invoiceid` int(10) NOT NULL,
  `type` tinyint(1) NOT NULL,
  PRIMARY KEY (`dep_inv_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=164 ;

-- --------------------------------------------------------

--
-- Table structure for table `dist_kpi`
--

DROP TABLE IF EXISTS `dist_kpi`;
CREATE TABLE IF NOT EXISTS `dist_kpi` (
  `dist_kpi_id` int(10) NOT NULL AUTO_INCREMENT,
  `companyid` int(3) NOT NULL,
  `districtid` int(3) NOT NULL,
  `sales` double NOT NULL,
  `cos` double NOT NULL,
  `labor` double NOT NULL,
  `toe` double NOT NULL,
  `pl` double NOT NULL,
  `date` date NOT NULL,
  `sales_bgt` double NOT NULL,
  `cos_bgt` double NOT NULL,
  `lbr_bgt` double NOT NULL,
  `toe_bgt` double NOT NULL,
  `pl_bgt` double NOT NULL,
  PRIMARY KEY (`dist_kpi_id`),
  KEY `companyid` (`companyid`,`districtid`,`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=900 ;

-- --------------------------------------------------------

--
-- Table structure for table `district`
--

DROP TABLE IF EXISTS `district`;
CREATE TABLE IF NOT EXISTS `district` (
  `districtid` int(3) NOT NULL AUTO_INCREMENT,
  `companyid` int(3) NOT NULL,
  `divisionid` int(3) NOT NULL,
  `districtname` varchar(30) NOT NULL DEFAULT '',
  `arrequest` int(5) NOT NULL DEFAULT '0',
  `aprequest` int(5) NOT NULL DEFAULT '0',
  `labor_request` varchar(100) NOT NULL,
  `dm` int(10) NOT NULL DEFAULT '0',
  `submit` tinyint(1) NOT NULL DEFAULT '0',
  `email_submit` varchar(200) NOT NULL,
  `labor_submit` tinyint(1) NOT NULL DEFAULT '0',
  `labor_email` varchar(100) NOT NULL,
  `bad_ar` tinyint(1) NOT NULL,
  `bad_ar_email` varchar(100) NOT NULL,
  `email_orders` varchar(255) NOT NULL,
  `production` int(5) NOT NULL,
  `deactivate_items` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`districtid`),
  KEY `districtid` (`districtid`,`companyid`,`divisionid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=270 ;

--
-- Triggers `district`
--
DROP TRIGGER IF EXISTS `district_ai`;
DELIMITER //
CREATE TRIGGER `district_ai` AFTER INSERT ON `district`
 FOR EACH ROW BEGIN 
    INSERT IGNORE INTO posreports_district 
    (districtid, companyid, divisionid, districtname, arrequest, aprequest, labor_request, dm, submit, email_submit, labor_submit, labor_email, bad_ar, bad_ar_email, email_orders, production, deactivate_items) 
    VALUES 
    (NEW.districtid, NEW.companyid, NEW.divisionid, NEW.districtname, NEW.arrequest, NEW.aprequest, NEW.labor_request, NEW.dm, NEW.submit, NEW.email_submit, NEW.labor_submit, NEW.labor_email, NEW.bad_ar, NEW.bad_ar_email, NEW.email_orders, NEW.production, NEW.deactivate_items);
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `district_au`;
DELIMITER //
CREATE TRIGGER `district_au` AFTER UPDATE ON `district`
 FOR EACH ROW BEGIN 
	
	DELETE FROM posreports_district WHERE districtid = OLD.districtid;
	
    INSERT IGNORE INTO posreports_district 
    (districtid, companyid, divisionid, districtname, arrequest, aprequest, labor_request, dm, submit, email_submit, labor_submit, labor_email, bad_ar, bad_ar_email, email_orders, production, deactivate_items) 
    VALUES 
    (OLD.districtid, NEW.companyid, NEW.divisionid, NEW.districtname, NEW.arrequest, NEW.aprequest, NEW.labor_request, NEW.dm, NEW.submit, NEW.email_submit, NEW.labor_submit, NEW.labor_email, NEW.bad_ar, NEW.bad_ar_email, NEW.email_orders, NEW.production, NEW.deactivate_items);
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `district_ad`;
DELIMITER //
CREATE TRIGGER `district_ad` AFTER DELETE ON `district`
 FOR EACH ROW BEGIN 
   DELETE FROM posreports_district WHERE districtid = OLD.districtid LIMIT 1;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `division`
--

DROP TABLE IF EXISTS `division`;
CREATE TABLE IF NOT EXISTS `division` (
  `divisionid` int(3) NOT NULL AUTO_INCREMENT,
  `division_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `companyid` int(3) NOT NULL,
  PRIMARY KEY (`divisionid`),
  KEY `divisionid` (`divisionid`,`companyid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=72 ;

-- --------------------------------------------------------

--
-- Table structure for table `est_accountowner`
--

DROP TABLE IF EXISTS `est_accountowner`;
CREATE TABLE IF NOT EXISTS `est_accountowner` (
  `accountowner` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `type` tinyint(1) NOT NULL COMMENT '0 -vend, 1 -food',
  `location` int(3) NOT NULL,
  `display_name` varchar(50) NOT NULL,
  `link` int(5) NOT NULL,
  `disable_pipeline` tinyint(1) NOT NULL DEFAULT '0',
  `loginid` int(10) NOT NULL,
  `estid` varchar(50) NOT NULL,
  PRIMARY KEY (`accountowner`),
  KEY `accountowner` (`accountowner`,`type`),
  KEY `estid` (`estid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9493 ;

-- --------------------------------------------------------

--
-- Table structure for table `est_accountowner_budget`
--

DROP TABLE IF EXISTS `est_accountowner_budget`;
CREATE TABLE IF NOT EXISTS `est_accountowner_budget` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `accountowner` int(10) NOT NULL,
  `date` date NOT NULL,
  `gained` double NOT NULL,
  `lost` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `accountowner` (`accountowner`,`date`,`gained`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2118 ;

-- --------------------------------------------------------

--
-- Table structure for table `est_accountowner_type`
--

DROP TABLE IF EXISTS `est_accountowner_type`;
CREATE TABLE IF NOT EXISTS `est_accountowner_type` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `est_accounts`
--

DROP TABLE IF EXISTS `est_accounts`;
CREATE TABLE IF NOT EXISTS `est_accounts` (
  `systemid` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `notes` text NOT NULL,
  `address` varchar(100) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(30) NOT NULL,
  `zip` varchar(10) NOT NULL,
  `accountowner` int(5) NOT NULL,
  `status` int(2) NOT NULL,
  `employees` int(10) NOT NULL,
  `servicetype` int(3) NOT NULL,
  `customertype` int(3) NOT NULL,
  `channel` int(3) NOT NULL,
  `region` int(3) NOT NULL,
  `yrlyrevenue` double NOT NULL,
  `cafeannualrevenue` double NOT NULL,
  `ocsrevenue` double NOT NULL,
  `addeddate` date NOT NULL,
  `updateddate` date NOT NULL,
  `datelost` date NOT NULL,
  `redlistdate` date NOT NULL,
  `contract` varchar(30) NOT NULL,
  `contractinfofoodserv` text NOT NULL,
  `contractvend` varchar(30) NOT NULL,
  `contractinfovending` text NOT NULL,
  `contexpires` date NOT NULL,
  PRIMARY KEY (`systemid`),
  KEY `accountowner` (`accountowner`,`customertype`),
  KEY `customertype` (`customertype`),
  KEY `status` (`status`),
  KEY `contractvend` (`contractvend`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `est_accountstatus`
--

DROP TABLE IF EXISTS `est_accountstatus`;
CREATE TABLE IF NOT EXISTS `est_accountstatus` (
  `status` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  PRIMARY KEY (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4849 ;

-- --------------------------------------------------------

--
-- Table structure for table `est_channel`
--

DROP TABLE IF EXISTS `est_channel`;
CREATE TABLE IF NOT EXISTS `est_channel` (
  `channel` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  PRIMARY KEY (`channel`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4831 ;

-- --------------------------------------------------------

--
-- Table structure for table `est_customertype`
--

DROP TABLE IF EXISTS `est_customertype`;
CREATE TABLE IF NOT EXISTS `est_customertype` (
  `customertype` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`customertype`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6255 ;

-- --------------------------------------------------------

--
-- Table structure for table `est_locations`
--

DROP TABLE IF EXISTS `est_locations`;
CREATE TABLE IF NOT EXISTS `est_locations` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `location` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `est_op_status`
--

DROP TABLE IF EXISTS `est_op_status`;
CREATE TABLE IF NOT EXISTS `est_op_status` (
  `status` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  PRIMARY KEY (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2706 ;

-- --------------------------------------------------------

--
-- Table structure for table `est_opportunity`
--

DROP TABLE IF EXISTS `est_opportunity`;
CREATE TABLE IF NOT EXISTS `est_opportunity` (
  `systemid` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `accountowner` int(5) NOT NULL,
  `customer` varchar(50) NOT NULL,
  `unit` varchar(50) NOT NULL,
  `contact` varchar(50) NOT NULL,
  `income1` double NOT NULL,
  `status` int(3) NOT NULL,
  `probability` double NOT NULL,
  `expecteddate` date NOT NULL,
  `note` text NOT NULL,
  `currentprovider` varchar(50) NOT NULL,
  `initialcontactdate` date NOT NULL,
  `incometype` varchar(25) NOT NULL,
  `rfpdate` date NOT NULL,
  `contractexpiredate` date NOT NULL,
  `install_date` date NOT NULL,
  `subsidyamount` double NOT NULL,
  `addeddate` date NOT NULL,
  `updateddate` date NOT NULL,
  `lost_reason` varchar(30) NOT NULL,
  `other_lost_reason` text NOT NULL,
  `_customer` int(10) NOT NULL,
  `accountnumber` varchar(25) NOT NULL,
  PRIMARY KEY (`systemid`),
  KEY `expecteddate` (`expecteddate`,`accountowner`,`status`),
  KEY `accountowner` (`accountowner`,`status`),
  KEY `_customer` (`_customer`),
  KEY `accountnumber` (`accountnumber`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `est_redlist`
--

DROP TABLE IF EXISTS `est_redlist`;
CREATE TABLE IF NOT EXISTS `est_redlist` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` tinyint(3) NOT NULL,
  `date` date NOT NULL,
  `amount` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`,`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=403 ;

-- --------------------------------------------------------

--
-- Table structure for table `est_region`
--

DROP TABLE IF EXISTS `est_region`;
CREATE TABLE IF NOT EXISTS `est_region` (
  `region` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  PRIMARY KEY (`region`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `est_servicetype`
--

DROP TABLE IF EXISTS `est_servicetype`;
CREATE TABLE IF NOT EXISTS `est_servicetype` (
  `servicetype` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`servicetype`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

-- --------------------------------------------------------

--
-- Table structure for table `expense_report_category`
--

DROP TABLE IF EXISTS `expense_report_category`;
CREATE TABLE IF NOT EXISTS `expense_report_category` (
  `categoryid` int(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `multiplier` double NOT NULL,
  `orderid` int(2) NOT NULL,
  PRIMARY KEY (`categoryid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Table structure for table `expense_report_link`
--

DROP TABLE IF EXISTS `expense_report_link`;
CREATE TABLE IF NOT EXISTS `expense_report_link` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `companyid` int(3) NOT NULL,
  `account` int(15) NOT NULL,
  `categoryid` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `export_history`
--

DROP TABLE IF EXISTS `export_history`;
CREATE TABLE IF NOT EXISTS `export_history` (
  `historyid` int(10) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NOT NULL,
  `gl_id` int(10) NOT NULL,
  `date` date NOT NULL,
  `amount` double NOT NULL,
  PRIMARY KEY (`historyid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15051 ;

-- --------------------------------------------------------

--
-- Table structure for table `impress`
--

DROP TABLE IF EXISTS `impress`;
CREATE TABLE IF NOT EXISTS `impress` (
  `businessid` int(5) NOT NULL,
  `date` date NOT NULL,
  `petty_cash` double NOT NULL,
  `voucher` double NOT NULL,
  `unreimbursed` double NOT NULL,
  `change_fund` double NOT NULL,
  `cash_in_trans` double NOT NULL,
  `reg_bank` double NOT NULL,
  `reg_bank2` double NOT NULL,
  `other` double NOT NULL,
  `funds_adv` double NOT NULL,
  `user` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `businessid` (`businessid`,`date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `inv_area`
--

DROP TABLE IF EXISTS `inv_area`;
CREATE TABLE IF NOT EXISTS `inv_area` (
  `inv_areaid` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL,
  `companyid` int(3) NOT NULL,
  `inv_areaname` varchar(40) NOT NULL,
  PRIMARY KEY (`inv_areaid`),
  KEY `businessid` (`businessid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3335 ;

-- --------------------------------------------------------

--
-- Table structure for table `inv_areadetail`
--

DROP TABLE IF EXISTS `inv_areadetail`;
CREATE TABLE IF NOT EXISTS `inv_areadetail` (
  `inv_areadetailid` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL,
  `areaid` int(10) NOT NULL,
  `inv_itemid` int(10) NOT NULL,
  `orderid` int(10) NOT NULL,
  `custom_order` int(3) NOT NULL,
  `location` int(4) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`inv_areadetailid`),
  KEY `inv_itemid` (`inv_itemid`),
  KEY `businessid` (`areaid`,`active`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=199440 ;

-- --------------------------------------------------------

--
-- Table structure for table `inv_count`
--

DROP TABLE IF EXISTS `inv_count`;
CREATE TABLE IF NOT EXISTS `inv_count` (
  `inv_countid` int(10) NOT NULL AUTO_INCREMENT,
  `inv_itemid` int(10) NOT NULL,
  `amount` double NOT NULL,
  `price` double NOT NULL,
  `date` date NOT NULL,
  `apaccountid` int(10) NOT NULL,
  `inv_areadetailid` int(10) NOT NULL,
  PRIMARY KEY (`inv_countid`),
  KEY `date` (`date`,`apaccountid`),
  KEY `inv_areadetailid` (`inv_areadetailid`,`date`),
  KEY `inv_itemid` (`inv_itemid`,`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8676525 ;

-- --------------------------------------------------------

--
-- Table structure for table `inv_count_vend`
--

DROP TABLE IF EXISTS `inv_count_vend`;
CREATE TABLE IF NOT EXISTS `inv_count_vend` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `machine_num` varchar(10) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `item_code` varchar(15) NOT NULL,
  `onhand` double NOT NULL,
  `unit_cost` double NOT NULL,
  `pulled` tinyint(1) NOT NULL,
  `fills` double NOT NULL,
  `waste` double NOT NULL,
  `is_inv` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `machine_num` (`machine_num`,`date_time`,`item_code`,`is_inv`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5483161 ;

--
-- Triggers `inv_count_vend`
--
DROP TRIGGER IF EXISTS `inv_count_vend_ai`;
DELIMITER //
CREATE TRIGGER `inv_count_vend_ai` AFTER INSERT ON `inv_count_vend`
 FOR EACH ROW BEGIN 
    INSERT IGNORE INTO posreports_inv_count_vend 
    (id, machine_num, date_time, item_code, onhand, unit_cost, pulled, fills, waste, is_inv) 
    VALUES 
    (NEW.id, NEW.machine_num, NEW.date_time, NEW.item_code, NEW.onhand, NEW.unit_cost, NEW.pulled, NEW.fills, NEW.waste, NEW.is_inv);
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `inv_count_vend_au`;
DELIMITER //
CREATE TRIGGER `inv_count_vend_au` AFTER UPDATE ON `inv_count_vend`
 FOR EACH ROW BEGIN 
	
	DELETE FROM posreports_inv_count_vend WHERE id = OLD.id;
	
    INSERT IGNORE INTO posreports_inv_count_vend 
    (id, machine_num, date_time, item_code, onhand, unit_cost, pulled, fills, waste, is_inv) 
    VALUES 
    (OLD.id, NEW.machine_num, NEW.date_time, NEW.item_code, NEW.onhand, NEW.unit_cost, NEW.pulled, NEW.fills, NEW.waste, NEW.is_inv);
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `inv_count_vend_ad`;
DELIMITER //
CREATE TRIGGER `inv_count_vend_ad` AFTER DELETE ON `inv_count_vend`
 FOR EACH ROW BEGIN 
   DELETE FROM posreports_inv_count_vend WHERE id = OLD.id LIMIT 1;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `inv_items`
--

DROP TABLE IF EXISTS `inv_items`;
CREATE TABLE IF NOT EXISTS `inv_items` (
  `inv_itemid` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL,
  `companyid` int(3) NOT NULL,
  `item_name` varchar(50) NOT NULL,
  `item_code` char(10) NOT NULL,
  `units` varchar(10) NOT NULL,
  `price` double NOT NULL,
  `apaccountid` int(10) NOT NULL,
  `inv_areaid` int(10) NOT NULL,
  `vendor` int(10) NOT NULL,
  `price_date` date NOT NULL,
  `rec_num` float NOT NULL,
  `rec_size` int(3) NOT NULL,
  `rec_num2` float NOT NULL,
  `rec_size2` int(5) NOT NULL,
  `order_size` int(3) NOT NULL,
  `each_size` double NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `pack_size` varchar(15) NOT NULL,
  `pack_qty` float NOT NULL DEFAULT '1',
  `v_update` tinyint(1) NOT NULL DEFAULT '0',
  `par_level` double NOT NULL COMMENT 'based on units',
  `batch` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`inv_itemid`),
  KEY `businessid_2` (`businessid`),
  KEY `businessid` (`businessid`,`active`),
  KEY `item_code` (`item_code`),
  KEY `rec_num` (`rec_num`),
  KEY `batch` (`batch`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22225348 ;

-- --------------------------------------------------------

--
-- Table structure for table `inventor`
--

DROP TABLE IF EXISTS `inventor`;
CREATE TABLE IF NOT EXISTS `inventor` (
  `inventorid` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL DEFAULT '0',
  `companyid` int(3) NOT NULL DEFAULT '0',
  `acct_payableid` int(10) NOT NULL DEFAULT '0',
  `amount` double NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `posted` int(1) NOT NULL DEFAULT '0',
  `export` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`inventorid`),
  KEY `businessid` (`businessid`,`acct_payableid`,`date`),
  KEY `businessid_2` (`businessid`,`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=55641 ;

-- --------------------------------------------------------

--
-- Table structure for table `inventor2`
--

DROP TABLE IF EXISTS `inventor2`;
CREATE TABLE IF NOT EXISTS `inventor2` (
  `inventor2id` int(10) NOT NULL AUTO_INCREMENT,
  `acct_payableid` int(10) NOT NULL,
  `amount` double NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`inventor2id`),
  KEY `acct_payableid` (`acct_payableid`,`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=284405 ;

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
CREATE TABLE IF NOT EXISTS `invoice` (
  `invoiceid` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL DEFAULT '0',
  `companyid` int(5) NOT NULL DEFAULT '0',
  `accountid` int(10) NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `type` int(2) NOT NULL DEFAULT '0',
  `status` int(2) NOT NULL DEFAULT '0',
  `taxable` char(3) NOT NULL DEFAULT '',
  `tax` double NOT NULL DEFAULT '0',
  `total` double NOT NULL DEFAULT '0',
  `createdate` date NOT NULL DEFAULT '0000-00-00',
  `createby` varchar(25) NOT NULL DEFAULT '',
  `editdate` date NOT NULL DEFAULT '0000-00-00',
  `editby` varchar(25) NOT NULL DEFAULT '',
  `taxtotal` double NOT NULL DEFAULT '0',
  `event_date` date NOT NULL DEFAULT '0000-00-00',
  `event_time` varchar(8) NOT NULL DEFAULT '',
  `buildingnum` varchar(8) NOT NULL DEFAULT '',
  `floornum` varchar(10) NOT NULL DEFAULT '',
  `people` varchar(5) NOT NULL DEFAULT '',
  `set_up_time` varchar(8) NOT NULL DEFAULT '',
  `pick_up_time` varchar(8) NOT NULL DEFAULT '',
  `costcenter` varchar(50) NOT NULL,
  `consolidated_invoice` int(10) NOT NULL DEFAULT '0',
  `consolidate` tinyint(1) NOT NULL DEFAULT '0',
  `service` double NOT NULL DEFAULT '0',
  `servamt` double NOT NULL DEFAULT '0',
  `notes` text NOT NULL,
  `posted` tinyint(1) NOT NULL DEFAULT '0',
  `reserveid` int(10) NOT NULL DEFAULT '0',
  `flatsrvchrg` char(3) NOT NULL DEFAULT 'off',
  `reference` varchar(100) NOT NULL,
  `export` tinyint(1) NOT NULL DEFAULT '0',
  `salescode` varchar(10) NOT NULL DEFAULT '0',
  `contact` varchar(25) NOT NULL DEFAULT '',
  `contact_phone` varchar(25) NOT NULL DEFAULT '',
  `ponum` varchar(40) NOT NULL DEFAULT '',
  `refnum` int(10) NOT NULL DEFAULT '0',
  `cleared` tinyint(1) NOT NULL DEFAULT '0',
  `outstanding` double NOT NULL,
  `parent` int(10) NOT NULL,
  `route` varchar(10) NOT NULL,
  `invoice_num` varchar(15) NOT NULL,
  PRIMARY KEY (`invoiceid`),
  KEY `busid` (`businessid`,`taxable`,`date`,`status`,`type`,`salescode`,`companyid`,`accountid`),
  KEY `reserveid` (`reserveid`),
  KEY `accountid` (`accountid`,`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=329178 ;

-- --------------------------------------------------------

--
-- Table structure for table `invoice2`
--

DROP TABLE IF EXISTS `invoice2`;
CREATE TABLE IF NOT EXISTS `invoice2` (
  `invoiceid2` int(10) NOT NULL AUTO_INCREMENT,
  `invoice_num` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `reference` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `businessid` int(5) NOT NULL,
  `companyid` int(3) NOT NULL,
  `cust_num` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(1) NOT NULL,
  `status` int(1) NOT NULL,
  `date` date NOT NULL,
  `total` double NOT NULL,
  `cust_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `cleared` tinyint(1) NOT NULL DEFAULT '0',
  `outstanding` double NOT NULL,
  PRIMARY KEY (`invoiceid2`),
  KEY `businessid` (`businessid`,`companyid`,`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=450275 ;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_custom_fields`
--

DROP TABLE IF EXISTS `invoice_custom_fields`;
CREATE TABLE IF NOT EXISTS `invoice_custom_fields` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL,
  `field_name` varchar(100) NOT NULL,
  `type` int(2) NOT NULL COMMENT '0=text,2=select',
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `customer` tinyint(1) NOT NULL DEFAULT '0',
  `production` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `businessid` (`businessid`,`active`,`customer`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=160 ;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_custom_fields_select`
--

DROP TABLE IF EXISTS `invoice_custom_fields_select`;
CREATE TABLE IF NOT EXISTS `invoice_custom_fields_select` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `customid` int(10) NOT NULL,
  `value` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `customid` (`customid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=253219 ;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_custom_values`
--

DROP TABLE IF EXISTS `invoice_custom_values`;
CREATE TABLE IF NOT EXISTS `invoice_custom_values` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `invoiceid` int(10) NOT NULL,
  `reserveid` int(10) NOT NULL,
  `customid` int(10) NOT NULL,
  `value` varchar(5000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `reserveid` (`reserveid`,`customid`),
  KEY `invoiceid` (`invoiceid`,`customid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1596231 ;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_setting`
--

DROP TABLE IF EXISTS `invoice_setting`;
CREATE TABLE IF NOT EXISTS `invoice_setting` (
  `loginid` int(10) NOT NULL,
  `reference` tinyint(1) NOT NULL,
  `taxed` tinyint(1) NOT NULL,
  `contact` tinyint(1) NOT NULL,
  `contact_phone` tinyint(1) NOT NULL,
  `service` tinyint(1) NOT NULL,
  `serv_type` tinyint(1) NOT NULL,
  `building` tinyint(1) NOT NULL,
  `floor` tinyint(1) NOT NULL,
  `costcenter` tinyint(1) NOT NULL,
  `po` tinyint(1) NOT NULL,
  PRIMARY KEY (`loginid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoicedetail`
--

DROP TABLE IF EXISTS `invoicedetail`;
CREATE TABLE IF NOT EXISTS `invoicedetail` (
  `itemid` int(10) NOT NULL AUTO_INCREMENT,
  `invoiceid` int(10) NOT NULL DEFAULT '0',
  `qty` float NOT NULL DEFAULT '0',
  `item` varchar(255) NOT NULL,
  `price` double NOT NULL DEFAULT '0',
  `taxed` char(3) NOT NULL DEFAULT 'on',
  `reserveid` int(10) NOT NULL DEFAULT '0',
  `menu_item_id` int(10) NOT NULL DEFAULT '0',
  `detail` text NOT NULL,
  `taxpercent` float NOT NULL DEFAULT '0',
  `sales_acct` int(10) NOT NULL,
  `tax_acct` int(10) NOT NULL,
  `item_code` varchar(15) NOT NULL,
  PRIMARY KEY (`itemid`),
  KEY `invoiceindex` (`invoiceid`),
  KEY `reserveid` (`reserveid`),
  KEY `menu_item_id` (`menu_item_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1131438 ;

-- --------------------------------------------------------

--
-- Table structure for table `invstatus`
--

DROP TABLE IF EXISTS `invstatus`;
CREATE TABLE IF NOT EXISTS `invstatus` (
  `statusid` int(5) NOT NULL AUTO_INCREMENT,
  `statusname` varchar(25) NOT NULL DEFAULT '',
  PRIMARY KEY (`statusid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `invtender`
--

DROP TABLE IF EXISTS `invtender`;
CREATE TABLE IF NOT EXISTS `invtender` (
  `tenderid` int(10) NOT NULL AUTO_INCREMENT,
  `tendertype` int(5) NOT NULL DEFAULT '0',
  `invoiceid` int(10) NOT NULL DEFAULT '0',
  `amount` double NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `businessid` int(10) NOT NULL DEFAULT '0',
  `export` tinyint(1) NOT NULL DEFAULT '0',
  `sameday` tinyint(1) NOT NULL DEFAULT '0',
  `checknum` varchar(15) NOT NULL,
  PRIMARY KEY (`tenderid`),
  KEY `invoiceid` (`invoiceid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8783 ;

-- --------------------------------------------------------

--
-- Table structure for table `invtype`
--

DROP TABLE IF EXISTS `invtype`;
CREATE TABLE IF NOT EXISTS `invtype` (
  `typeid` int(5) NOT NULL AUTO_INCREMENT,
  `typename` varchar(25) NOT NULL DEFAULT '',
  `use_accounts` char(3) NOT NULL DEFAULT 'off',
  `use_supplier` char(3) NOT NULL DEFAULT 'off',
  `sales` char(3) NOT NULL DEFAULT 'off',
  PRIMARY KEY (`typeid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `jobtype`
--

DROP TABLE IF EXISTS `jobtype`;
CREATE TABLE IF NOT EXISTS `jobtype` (
  `jobtypeid` int(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `hourly` tinyint(1) NOT NULL DEFAULT '0',
  `tips` tinyint(1) NOT NULL DEFAULT '0',
  `commission` tinyint(1) NOT NULL DEFAULT '0',
  `companyid` int(3) NOT NULL DEFAULT '0',
  `pto_override` double NOT NULL,
  PRIMARY KEY (`jobtypeid`),
  KEY `hourly` (`hourly`,`companyid`),
  KEY `commission` (`commission`,`companyid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=86 ;

-- --------------------------------------------------------

--
-- Table structure for table `jobtypedetail`
--

DROP TABLE IF EXISTS `jobtypedetail`;
CREATE TABLE IF NOT EXISTS `jobtypedetail` (
  `jobid` int(10) NOT NULL AUTO_INCREMENT,
  `loginid` int(10) NOT NULL DEFAULT '0',
  `jobtype` int(3) NOT NULL DEFAULT '0',
  `rate` double NOT NULL DEFAULT '0',
  `rateCode` char(1) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `del_date` date NOT NULL,
  PRIMARY KEY (`jobid`),
  KEY `loginid` (`loginid`,`is_deleted`,`del_date`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7259 ;

-- --------------------------------------------------------

--
-- Table structure for table `kiosk_order_sched`
--

DROP TABLE IF EXISTS `kiosk_order_sched`;
CREATE TABLE IF NOT EXISTS `kiosk_order_sched` (
  `districtid` int(10) NOT NULL,
  `ordertype` int(5) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `datetime` varchar(5) NOT NULL,
  `weekend` tinyint(1) NOT NULL,
  `email` varchar(250) NOT NULL,
  `lastrun` tinyint(1) NOT NULL,
  `route_total` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `dsitrictid` (`districtid`,`ordertype`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kiosk_order_type`
--

DROP TABLE IF EXISTS `kiosk_order_type`;
CREATE TABLE IF NOT EXISTS `kiosk_order_type` (
  `id` int(3) NOT NULL,
  `ordertype` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kiosk_scripts`
--

DROP TABLE IF EXISTS `kiosk_scripts`;
CREATE TABLE IF NOT EXISTS `kiosk_scripts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fileName` varchar(32) NOT NULL DEFAULT '',
  `script` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=955 ;

-- --------------------------------------------------------

--
-- Table structure for table `kiosk_sync`
--

DROP TABLE IF EXISTS `kiosk_sync`;
CREATE TABLE IF NOT EXISTS `kiosk_sync` (
  `client_programid` int(11) unsigned NOT NULL,
  `item_id` int(11) unsigned NOT NULL,
  `item_type` int(1) unsigned NOT NULL DEFAULT '1' COMMENT '1=items,2=groups,3=tax,4=itemtax,5=promo,6=promo_groups',
  UNIQUE KEY `client_programid` (`client_programid`,`item_id`,`item_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `labor`
--

DROP TABLE IF EXISTS `labor`;
CREATE TABLE IF NOT EXISTS `labor` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `loginid` int(10) NOT NULL DEFAULT '0',
  `businessid` int(10) NOT NULL DEFAULT '0',
  `companyid` int(3) NOT NULL DEFAULT '0',
  `hours` float NOT NULL DEFAULT '0',
  `tips` float NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `rate` float NOT NULL DEFAULT '0',
  `coded` int(3) NOT NULL,
  `jobtypeid` int(5) NOT NULL,
  `tempid` int(8) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `loginid` (`loginid`,`businessid`,`date`,`jobtypeid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=917532 ;

-- --------------------------------------------------------

--
-- Table structure for table `labor_alerts`
--

DROP TABLE IF EXISTS `labor_alerts`;
CREATE TABLE IF NOT EXISTS `labor_alerts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `loginid` int(10) NOT NULL,
  `businessid` int(10) NOT NULL,
  `hours` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `loginid` (`loginid`,`businessid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=36 ;

-- --------------------------------------------------------

--
-- Table structure for table `labor_clockin`
--

DROP TABLE IF EXISTS `labor_clockin`;
CREATE TABLE IF NOT EXISTS `labor_clockin` (
  `clockinid` int(10) NOT NULL AUTO_INCREMENT,
  `loginid` int(10) NOT NULL,
  `businessid` int(10) NOT NULL,
  `clockin` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `clockout` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `jobtype` int(10) NOT NULL,
  `terminal_id` int(10) NOT NULL,
  `editby` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `edittime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`clockinid`),
  KEY `loginid` (`loginid`,`businessid`,`clockin`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=547856 ;

-- --------------------------------------------------------

--
-- Table structure for table `labor_clockin_options`
--

DROP TABLE IF EXISTS `labor_clockin_options`;
CREATE TABLE IF NOT EXISTS `labor_clockin_options` (
  `loginid` int(10) NOT NULL,
  `lunch_sun` tinyint(1) NOT NULL,
  `lunch_sun_time` float NOT NULL,
  `lunch_mon` tinyint(4) NOT NULL,
  `lunch_mon_time` float NOT NULL,
  `lunch_tue` tinyint(4) NOT NULL,
  `lunch_tue_time` float NOT NULL,
  `lunch_wed` tinyint(4) NOT NULL,
  `lunch_wed_time` float NOT NULL,
  `lunch_thu` tinyint(4) NOT NULL,
  `lunch_thu_time` float NOT NULL,
  `lunch_fri` tinyint(4) NOT NULL,
  `lunch_fri_time` float NOT NULL,
  `lunch_sat` tinyint(4) NOT NULL,
  `lunch_sat_time` float NOT NULL,
  `clockout` tinyint(1) NOT NULL,
  `clockout_time` varchar(6) COLLATE utf8_unicode_ci NOT NULL DEFAULT '12:00a',
  `auto_punch` tinyint(1) NOT NULL DEFAULT '0',
  `auto_punch_hours` float NOT NULL,
  PRIMARY KEY (`loginid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `labor_comm`
--

DROP TABLE IF EXISTS `labor_comm`;
CREATE TABLE IF NOT EXISTS `labor_comm` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `temp_commid` int(10) NOT NULL,
  `coded` int(5) NOT NULL,
  `amount` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `temp_commid_2` (`temp_commid`,`coded`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=181161 ;

-- --------------------------------------------------------

--
-- Table structure for table `labor_group_detail`
--

DROP TABLE IF EXISTS `labor_group_detail`;
CREATE TABLE IF NOT EXISTS `labor_group_detail` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `labor_group_id` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `loginid` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `labor_group_id` (`labor_group_id`,`loginid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=587 ;

-- --------------------------------------------------------

--
-- Table structure for table `labor_group_security`
--

DROP TABLE IF EXISTS `labor_group_security`;
CREATE TABLE IF NOT EXISTS `labor_group_security` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `labor_group_id` int(5) NOT NULL,
  `loginid` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `labor_group_id` (`labor_group_id`,`loginid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=90 ;

-- --------------------------------------------------------

--
-- Table structure for table `labor_groups`
--

DROP TABLE IF EXISTS `labor_groups`;
CREATE TABLE IF NOT EXISTS `labor_groups` (
  `labor_group_id` int(5) NOT NULL AUTO_INCREMENT,
  `labor_group_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `businessid` int(5) NOT NULL,
  PRIMARY KEY (`labor_group_id`),
  KEY `businessid` (`businessid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=54 ;

-- --------------------------------------------------------

--
-- Table structure for table `labor_move`
--

DROP TABLE IF EXISTS `labor_move`;
CREATE TABLE IF NOT EXISTS `labor_move` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `loginid` int(8) NOT NULL,
  `to_business` int(8) NOT NULL,
  `move_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `loginid` (`loginid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1762 ;

-- --------------------------------------------------------

--
-- Table structure for table `labor_notify`
--

DROP TABLE IF EXISTS `labor_notify`;
CREATE TABLE IF NOT EXISTS `labor_notify` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(5) NOT NULL,
  `dept_id` int(5) NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `businessid` (`businessid`,`dept_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=26 ;

-- --------------------------------------------------------

--
-- Table structure for table `labor_route_link`
--

DROP TABLE IF EXISTS `labor_route_link`;
CREATE TABLE IF NOT EXISTS `labor_route_link` (
  `laborid` int(10) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `login_routeid` int(10) NOT NULL,
  `loginid` int(10) NOT NULL,
  `freevend` double NOT NULL,
  `noncash` double NOT NULL,
  `kiosk` double NOT NULL,
  `csv` double NOT NULL,
  `weekend_pay` double NOT NULL,
  `pay_days` int(2) NOT NULL,
  `nopay_days` int(2) NOT NULL,
  `pto` double NOT NULL,
  `commissionid` int(5) NOT NULL,
  `sourceid` int(2) NOT NULL,
  PRIMARY KEY (`laborid`),
  UNIQUE KEY `date` (`date`,`login_routeid`,`loginid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=42159 ;

-- --------------------------------------------------------

--
-- Table structure for table `labor_temp`
--

DROP TABLE IF EXISTS `labor_temp`;
CREATE TABLE IF NOT EXISTS `labor_temp` (
  `tempid` int(10) NOT NULL AUTO_INCREMENT,
  `loginid` int(10) NOT NULL,
  `jobtypeid` int(5) NOT NULL,
  `businessid` int(10) NOT NULL,
  `companyid` int(3) NOT NULL,
  `date` date NOT NULL,
  `rate` double NOT NULL,
  PRIMARY KEY (`tempid`),
  UNIQUE KEY `loginid` (`loginid`,`jobtypeid`,`businessid`,`date`),
  KEY `businessid` (`businessid`,`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10692 ;

-- --------------------------------------------------------

--
-- Table structure for table `labor_temp_comm`
--

DROP TABLE IF EXISTS `labor_temp_comm`;
CREATE TABLE IF NOT EXISTS `labor_temp_comm` (
  `tempid` int(10) NOT NULL AUTO_INCREMENT,
  `loginid` int(10) NOT NULL,
  `businessid` int(10) NOT NULL,
  `jobtypeid` int(5) NOT NULL,
  `date` date NOT NULL,
  `rate` double NOT NULL,
  PRIMARY KEY (`tempid`),
  KEY `businessid` (`loginid`,`businessid`,`jobtypeid`,`date`),
  KEY `jobtypeid` (`jobtypeid`,`businessid`,`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27205 ;

-- --------------------------------------------------------

--
-- Table structure for table `labor_terminals`
--

DROP TABLE IF EXISTS `labor_terminals`;
CREATE TABLE IF NOT EXISTS `labor_terminals` (
  `terminalid` int(10) NOT NULL AUTO_INCREMENT,
  `bid` int(10) NOT NULL,
  `ip` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `loginid` int(10) NOT NULL,
  PRIMARY KEY (`terminalid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=88 ;

-- --------------------------------------------------------

--
-- Table structure for table `labor_tips`
--

DROP TABLE IF EXISTS `labor_tips`;
CREATE TABLE IF NOT EXISTS `labor_tips` (
  `tipid` int(10) NOT NULL AUTO_INCREMENT,
  `loginid` int(10) NOT NULL,
  `businessid` int(5) NOT NULL,
  `jobtypeid` int(10) NOT NULL,
  `date` date NOT NULL,
  `cash_receipts` double NOT NULL,
  `charge_receipts` double NOT NULL,
  `cash_tips` double NOT NULL,
  `charge_tips` double NOT NULL,
  `cash_out` double NOT NULL,
  `charge_out` double NOT NULL,
  `tempid` int(10) NOT NULL,
  PRIMARY KEY (`tipid`),
  KEY `loginid` (`loginid`,`businessid`,`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=73983 ;

-- --------------------------------------------------------

--
-- Table structure for table `load_kiosk_export`
--

DROP TABLE IF EXISTS `load_kiosk_export`;
CREATE TABLE IF NOT EXISTS `load_kiosk_export` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) DEFAULT '0',
  `date1` date DEFAULT NULL,
  `date2` date DEFAULT NULL,
  `creditid` int(11) DEFAULT NULL,
  `mitn_id` int(11) DEFAULT NULL,
  `item_code` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pos_id` int(11) DEFAULT '0',
  `max_value` decimal(10,2) DEFAULT '0.00',
  `businessid` int(11) DEFAULT '0',
  `machine_num` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `onhand` decimal(10,2) DEFAULT '0.00',
  `fills` decimal(10,2) DEFAULT '0.00',
  `waste` decimal(10,2) DEFAULT '0.00',
  `count_rec` int(11) DEFAULT '0',
  `price` decimal(10,2) DEFAULT '0.00',
  `cost` decimal(10,2) DEFAULT '0.00',
  `sales` decimal(10,2) DEFAULT '0.00',
  `items_sold` decimal(8,2) DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `load_menu_items`
--

DROP TABLE IF EXISTS `load_menu_items`;
CREATE TABLE IF NOT EXISTS `load_menu_items` (
  `companyid` int(11) NOT NULL,
  `businessid` int(11) NOT NULL,
  `min_item_code` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `min_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mgn_group_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `min_active` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `businessid` (`businessid`,`min_item_code`,`min_name`),
  KEY `companyid` (`companyid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `log_actions`
--

DROP TABLE IF EXISTS `log_actions`;
CREATE TABLE IF NOT EXISTS `log_actions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `at` datetime NOT NULL,
  `business_id` int(10) unsigned DEFAULT NULL,
  `log_location_id` int(10) unsigned NOT NULL,
  `account_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `changes` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `business_id` (`business_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=112 ;

-- --------------------------------------------------------

--
-- Table structure for table `log_locations`
--

DROP TABLE IF EXISTS `log_locations`;
CREATE TABLE IF NOT EXISTS `log_locations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url_path` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `last_accessed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url_path` (`url_path`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

DROP TABLE IF EXISTS `login`;
CREATE TABLE IF NOT EXISTS `login` (
  `loginid` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL DEFAULT '',
  `password` varchar(30) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `companyid` int(3) NOT NULL DEFAULT '0',
  `businessid` int(10) NOT NULL DEFAULT '0',
  `payroll` tinyint(1) NOT NULL DEFAULT '0',
  `busid2` int(10) NOT NULL DEFAULT '0',
  `busid3` int(10) NOT NULL DEFAULT '0',
  `busid4` int(10) NOT NULL DEFAULT '0',
  `busid5` int(10) NOT NULL DEFAULT '0',
  `busid6` int(10) NOT NULL DEFAULT '0',
  `busid7` int(10) NOT NULL DEFAULT '0',
  `busid8` int(10) NOT NULL DEFAULT '0',
  `busid9` int(10) NOT NULL DEFAULT '0',
  `busid10` int(10) NOT NULL DEFAULT '0',
  `security_level` int(1) NOT NULL DEFAULT '0',
  `security` int(5) NOT NULL,
  `textnum` varchar(40) NOT NULL DEFAULT '',
  `firstname` varchar(25) NOT NULL DEFAULT '',
  `lastname` varchar(25) NOT NULL DEFAULT '',
  `adddate` date NOT NULL DEFAULT '0000-00-00',
  `empl_no` varchar(15) NOT NULL DEFAULT '',
  `mylinks` tinyint(1) NOT NULL DEFAULT '0',
  `myap` tinyint(1) NOT NULL DEFAULT '0',
  `myar` tinyint(1) NOT NULL DEFAULT '0',
  `email_order` tinyint(1) NOT NULL DEFAULT '0',
  `lastpass` varchar(50) NOT NULL,
  `passexpire` date NOT NULL,
  `pr2` tinyint(1) NOT NULL DEFAULT '0',
  `pr3` tinyint(1) NOT NULL DEFAULT '0',
  `pr4` tinyint(1) NOT NULL DEFAULT '0',
  `pr5` tinyint(1) NOT NULL DEFAULT '0',
  `pr6` tinyint(1) NOT NULL DEFAULT '0',
  `pr7` tinyint(1) NOT NULL DEFAULT '0',
  `pr8` tinyint(1) NOT NULL DEFAULT '0',
  `pr9` tinyint(1) NOT NULL DEFAULT '0',
  `pr10` tinyint(1) NOT NULL DEFAULT '0',
  `temp_expire` date NOT NULL,
  `order_only` tinyint(1) NOT NULL DEFAULT '0',
  `oo2` int(10) NOT NULL DEFAULT '0',
  `oo3` varchar(10) NOT NULL DEFAULT '0',
  `oo4` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'dashboard_type',
  `oo5` int(10) NOT NULL DEFAULT '1' COMMENT 'dashboard_view',
  `oo6` int(5) NOT NULL DEFAULT '1' COMMENT 'viewbus',
  `viewsub` int(2) NOT NULL DEFAULT '10',
  `oo7` tinyint(1) NOT NULL DEFAULT '0',
  `oo8` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'TA PM',
  `oo9` tinyint(4) NOT NULL DEFAULT '0',
  `oo10` tinyint(4) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `del_date` date NOT NULL,
  `move_date` date NOT NULL,
  `login_pendid` int(8) NOT NULL,
  `dept_id` int(3) NOT NULL,
  `receive_kpi` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-All,1-1st BID,2-None',
  `email_expense_report` tinyint(1) NOT NULL,
  `hide_expense_reports` tinyint(1) NOT NULL,
  PRIMARY KEY (`loginid`),
  KEY `security` (`security`,`security_level`,`is_deleted`),
  KEY `username` (`username`,`password`),
  KEY `oo2` (`oo2`,`security_level`,`is_deleted`),
  KEY `is_deleted` (`is_deleted`),
  KEY `security_level` (`security_level`),
  KEY `companyid` (`companyid`,`security_level`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7075 ;

-- --------------------------------------------------------

--
-- Table structure for table `login_all`
--

DROP TABLE IF EXISTS `login_all`;
CREATE TABLE IF NOT EXISTS `login_all` (
  `loginid` int(5) NOT NULL,
  `login_all` tinyint(1) NOT NULL,
  PRIMARY KEY (`loginid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `login_cards`
--

DROP TABLE IF EXISTS `login_cards`;
CREATE TABLE IF NOT EXISTS `login_cards` (
  `card_num` int(12) NOT NULL,
  `loginid` int(10) NOT NULL,
  PRIMARY KEY (`card_num`),
  KEY `loginid` (`loginid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `login_cashier`
--

DROP TABLE IF EXISTS `login_cashier`;
CREATE TABLE IF NOT EXISTS `login_cashier` (
  `loginid` int(10) unsigned NOT NULL,
  `passcode` varchar(20) NOT NULL DEFAULT '12345',
  PRIMARY KEY (`loginid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `login_cashier_link`
--

DROP TABLE IF EXISTS `login_cashier_link`;
CREATE TABLE IF NOT EXISTS `login_cashier_link` (
  `loginid` int(10) unsigned NOT NULL,
  `businessid` int(10) unsigned NOT NULL,
  `groupName` varchar(20) NOT NULL DEFAULT 'Cashier',
  PRIMARY KEY (`loginid`,`businessid`),
  KEY `loginid_index` (`loginid`),
  KEY `businessid_index` (`businessid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `login_comp`
--

DROP TABLE IF EXISTS `login_comp`;
CREATE TABLE IF NOT EXISTS `login_comp` (
  `loginid` int(5) NOT NULL,
  `companyid` int(3) NOT NULL,
  UNIQUE KEY `loginid` (`loginid`,`companyid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `login_company`
--

DROP TABLE IF EXISTS `login_company`;
CREATE TABLE IF NOT EXISTS `login_company` (
  `login_co_id` int(10) NOT NULL AUTO_INCREMENT,
  `loginid` int(10) NOT NULL,
  `companyid` int(3) NOT NULL,
  PRIMARY KEY (`login_co_id`),
  UNIQUE KEY `loginid` (`loginid`,`companyid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2534 ;

-- --------------------------------------------------------

--
-- Table structure for table `login_company_type`
--

DROP TABLE IF EXISTS `login_company_type`;
CREATE TABLE IF NOT EXISTS `login_company_type` (
  `loginid` int(5) NOT NULL,
  `company_type` int(2) NOT NULL,
  UNIQUE KEY `loginid` (`loginid`,`company_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `login_district`
--

DROP TABLE IF EXISTS `login_district`;
CREATE TABLE IF NOT EXISTS `login_district` (
  `login_dist_id` int(10) NOT NULL AUTO_INCREMENT,
  `loginid` int(10) NOT NULL,
  `districtid` int(3) NOT NULL,
  `kpi` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`login_dist_id`),
  KEY `loginid` (`loginid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3652 ;

-- --------------------------------------------------------

--
-- Table structure for table `login_division`
--

DROP TABLE IF EXISTS `login_division`;
CREATE TABLE IF NOT EXISTS `login_division` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `loginid` int(10) NOT NULL,
  `divisionid` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=763 ;

-- --------------------------------------------------------

--
-- Table structure for table `login_extended`
--

DROP TABLE IF EXISTS `login_extended`;
CREATE TABLE IF NOT EXISTS `login_extended` (
  `loginid` int(10) NOT NULL,
  `title` varchar(50) NOT NULL,
  `street` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(2) NOT NULL,
  `zip` varchar(12) NOT NULL,
  `phone` varchar(40) NOT NULL,
  `fax` varchar(40) NOT NULL,
  PRIMARY KEY (`loginid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `login_pend`
--

DROP TABLE IF EXISTS `login_pend`;
CREATE TABLE IF NOT EXISTS `login_pend` (
  `login_pendid` int(10) NOT NULL AUTO_INCREMENT,
  `companyid` int(3) NOT NULL,
  `businessid` int(10) NOT NULL,
  `firstname` varchar(40) NOT NULL,
  `lastname` varchar(40) NOT NULL,
  `jobtype` int(5) NOT NULL,
  `rate` double NOT NULL,
  PRIMARY KEY (`login_pendid`),
  KEY `companyid` (`companyid`),
  KEY `businessid` (`businessid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2064 ;

-- --------------------------------------------------------

--
-- Table structure for table `login_route`
--

DROP TABLE IF EXISTS `login_route`;
CREATE TABLE IF NOT EXISTS `login_route` (
  `login_routeid` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(10) NOT NULL,
  `password` varchar(10) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `route` varchar(10) NOT NULL,
  `lastroute` varchar(10) NOT NULL,
  `truck` varchar(5) NOT NULL,
  `businessid` int(5) NOT NULL,
  `companyid` int(3) NOT NULL,
  `locationid` int(5) NOT NULL,
  `drv_update` varchar(30) NOT NULL,
  `corp_update` varchar(30) NOT NULL,
  `sec_level` int(1) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `del_date` date NOT NULL,
  `commissionid` int(5) NOT NULL,
  `sourceid` int(2) NOT NULL,
  `supervisor` int(4) NOT NULL,
  `order_by_machine` tinyint(1) NOT NULL DEFAULT '0',
  `cstore` tinyint(1) NOT NULL,
  PRIMARY KEY (`login_routeid`),
  UNIQUE KEY `username` (`username`),
  KEY `id1` (`username`,`password`),
  KEY `locationid` (`locationid`,`active`,`is_deleted`),
  KEY `supervisor` (`supervisor`,`route`,`login_routeid`),
  KEY `businessid` (`businessid`),
  KEY `route` (`route`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=679 ;

-- --------------------------------------------------------

--
-- Table structure for table `machine_bus_link`
--

DROP TABLE IF EXISTS `machine_bus_link`;
CREATE TABLE IF NOT EXISTS `machine_bus_link` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL,
  `machine_num` varchar(30) NOT NULL,
  `ordertype` int(3) NOT NULL,
  `creditid` int(10) NOT NULL DEFAULT '0',
  `client_programid` int(10) NOT NULL DEFAULT '0',
  `leadtime` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `businessid` (`businessid`,`machine_num`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1961 ;

--
-- Triggers `machine_bus_link`
--
DROP TRIGGER IF EXISTS `machine_bus_link_ai`;
DELIMITER //
CREATE TRIGGER `machine_bus_link_ai` AFTER INSERT ON `machine_bus_link`
 FOR EACH ROW BEGIN 
    INSERT IGNORE INTO posreports_machine_bus_link 
    (id, businessid, machine_num, ordertype, creditid, client_programid, leadtime) 
    VALUES 
    (NEW.id, NEW.businessid, NEW.machine_num, NEW.ordertype, NEW.creditid, NEW.client_programid, NEW.leadtime);
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `machine_bus_link_au`;
DELIMITER //
CREATE TRIGGER `machine_bus_link_au` AFTER UPDATE ON `machine_bus_link`
 FOR EACH ROW BEGIN 
	
	DELETE FROM posreports_machine_bus_link WHERE id = OLD.id;
	
    INSERT IGNORE INTO posreports_machine_bus_link 
    (id, businessid, machine_num, ordertype, creditid, client_programid, leadtime) 
    VALUES 
    (OLD.id, NEW.businessid, NEW.machine_num, NEW.ordertype, NEW.creditid, NEW.client_programid, NEW.leadtime);
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `machine_bus_link_ad`;
DELIMITER //
CREATE TRIGGER `machine_bus_link_ad` AFTER DELETE ON `machine_bus_link`
 FOR EACH ROW BEGIN 
   DELETE FROM posreports_machine_bus_link WHERE id = OLD.id LIMIT 1;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `machine_collect`
--

DROP TABLE IF EXISTS `machine_collect`;
CREATE TABLE IF NOT EXISTS `machine_collect` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL,
  `machine_num` varchar(25) NOT NULL,
  `date_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `businessid` (`businessid`,`date_time`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32059 ;

-- --------------------------------------------------------

--
-- Table structure for table `machine_order`
--

DROP TABLE IF EXISTS `machine_order`;
CREATE TABLE IF NOT EXISTS `machine_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `route` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `expected_delivery` date NOT NULL,
  `login_routeid` int(10) unsigned NOT NULL,
  `machineid` int(10) unsigned NOT NULL,
  `accountid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `machineid` (`machineid`,`expected_delivery`),
  KEY `route` (`route`,`expected_delivery`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=136652 ;

-- --------------------------------------------------------

--
-- Table structure for table `machine_orderdetail`
--

DROP TABLE IF EXISTS `machine_orderdetail`;
CREATE TABLE IF NOT EXISTS `machine_orderdetail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderid` int(10) unsigned NOT NULL,
  `menu_items_new_id` int(11) unsigned NOT NULL,
  `item_code` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `qty` double NOT NULL,
  `new_item_code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `new_qty` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `orderid` (`orderid`,`item_code`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2409479 ;

-- --------------------------------------------------------

--
-- Table structure for table `manufacturer`
--

DROP TABLE IF EXISTS `manufacturer`;
CREATE TABLE IF NOT EXISTS `manufacturer` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=474 ;

-- --------------------------------------------------------

--
-- Table structure for table `mei_product`
--

DROP TABLE IF EXISTS `mei_product`;
CREATE TABLE IF NOT EXISTS `mei_product` (
  `item_code` varchar(15) NOT NULL,
  `item_name` varchar(50) NOT NULL,
  `upc` varchar(30) NOT NULL,
  `cost` double NOT NULL,
  PRIMARY KEY (`item_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu_alt_price`
--

DROP TABLE IF EXISTS `menu_alt_price`;
CREATE TABLE IF NOT EXISTS `menu_alt_price` (
  `priceid` int(10) NOT NULL AUTO_INCREMENT,
  `menu_itemid` int(10) NOT NULL,
  `price` double NOT NULL,
  `comment` varchar(40) NOT NULL,
  PRIMARY KEY (`priceid`),
  KEY `menu_itemid` (`menu_itemid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3498 ;

-- --------------------------------------------------------

--
-- Table structure for table `menu_daypart`
--

DROP TABLE IF EXISTS `menu_daypart`;
CREATE TABLE IF NOT EXISTS `menu_daypart` (
  `menu_daypartid` int(10) NOT NULL AUTO_INCREMENT,
  `menu_daypartname` varchar(30) NOT NULL,
  `menu_typeid` int(10) NOT NULL,
  `orderid` int(2) NOT NULL,
  `deliver_charge` tinyint(1) NOT NULL DEFAULT '0',
  `breakfast` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`menu_daypartid`),
  KEY `deliver_charge` (`deliver_charge`),
  KEY `menu_typeid` (`menu_typeid`),
  KEY `breakfast` (`breakfast`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=158 ;

-- --------------------------------------------------------

--
-- Table structure for table `menu_daypartdetail`
--

DROP TABLE IF EXISTS `menu_daypartdetail`;
CREATE TABLE IF NOT EXISTS `menu_daypartdetail` (
  `detailid` int(10) NOT NULL AUTO_INCREMENT,
  `accountid` int(10) NOT NULL,
  `daypartid` int(5) NOT NULL,
  PRIMARY KEY (`detailid`),
  KEY `detailid` (`daypartid`,`accountid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1817 ;

-- --------------------------------------------------------

--
-- Table structure for table `menu_groups`
--

DROP TABLE IF EXISTS `menu_groups`;
CREATE TABLE IF NOT EXISTS `menu_groups` (
  `menu_group_id` int(5) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL DEFAULT '0',
  `companyid` int(3) NOT NULL DEFAULT '0',
  `groupname` varchar(50) NOT NULL,
  `image` varchar(30) NOT NULL DEFAULT '',
  `img_width` int(3) NOT NULL DEFAULT '0',
  `img_height` int(3) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `parent` int(10) NOT NULL DEFAULT '0',
  `menu_typeid` int(3) NOT NULL DEFAULT '1',
  `creditid` int(10) NOT NULL,
  `orderid` int(2) NOT NULL,
  PRIMARY KEY (`menu_group_id`),
  KEY `menu_typeid` (`menu_typeid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=245 ;

-- --------------------------------------------------------

--
-- Table structure for table `menu_groups_active`
--

DROP TABLE IF EXISTS `menu_groups_active`;
CREATE TABLE IF NOT EXISTS `menu_groups_active` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL,
  `menu_groupid` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `businessid` (`businessid`,`menu_groupid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2051 ;

-- --------------------------------------------------------

--
-- Table structure for table `menu_groups_new`
--

DROP TABLE IF EXISTS `menu_groups_new`;
CREATE TABLE IF NOT EXISTS `menu_groups_new` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `businessid` int(11) NOT NULL,
  `pos_processed` tinyint(1) NOT NULL DEFAULT '0',
  `pos_id` int(11) unsigned NOT NULL,
  `parent_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `group_type` tinyint(5) DEFAULT NULL,
  `menu_bid` int(5) NOT NULL,
  `menu_typeid` int(5) NOT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `pos_color` smallint(6) NOT NULL DEFAULT '0',
  `display_order` int(11) NOT NULL DEFAULT '99999',
  PRIMARY KEY (`id`),
  UNIQUE KEY `businessid` (`businessid`,`parent_id`,`pos_id`),
  KEY `parent_id2` (`parent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7443 ;

-- --------------------------------------------------------

--
-- Table structure for table `menu_item_tax`
--

DROP TABLE IF EXISTS `menu_item_tax`;
CREATE TABLE IF NOT EXISTS `menu_item_tax` (
  `menu_itemid` int(10) NOT NULL,
  `tax_id` int(10) NOT NULL,
  UNIQUE KEY `menu_itemid` (`menu_itemid`,`tax_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
CREATE TABLE IF NOT EXISTS `menu_items` (
  `menu_item_id` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL DEFAULT '0',
  `companyid` int(3) NOT NULL DEFAULT '0',
  `item_name` varchar(45) NOT NULL DEFAULT '',
  `upc` varchar(20) NOT NULL,
  `price` float NOT NULL DEFAULT '0',
  `retail` double NOT NULL,
  `parent` int(10) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `groupid` int(5) NOT NULL DEFAULT '0',
  `unit` varchar(15) NOT NULL DEFAULT '',
  `menu_typeid` int(3) NOT NULL DEFAULT '1',
  `true_unit` int(3) NOT NULL,
  `serving_size` double NOT NULL,
  `serving_size2` varchar(20) NOT NULL,
  `order_unit` int(3) NOT NULL,
  `su_in_ou` double NOT NULL,
  `label` tinyint(1) NOT NULL DEFAULT '0',
  `order_min` int(2) NOT NULL,
  `item_code` varchar(15) NOT NULL,
  `sales_acct` int(10) NOT NULL,
  `tax_acct` int(10) NOT NULL,
  `nontax_acct` int(10) NOT NULL,
  `recipe` text NOT NULL,
  `recipe_active` tinyint(1) NOT NULL DEFAULT '0',
  `recipe_active_cust` tinyint(1) NOT NULL DEFAULT '0',
  `recipe_station` int(5) NOT NULL,
  `batch_recipe` tinyint(1) NOT NULL DEFAULT '0',
  `contract_option` tinyint(1) NOT NULL DEFAULT '0',
  `heart_healthy` tinyint(1) NOT NULL DEFAULT '0',
  `image` varchar(20) NOT NULL,
  `keeper` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`menu_item_id`),
  KEY `recipe_active` (`recipe_active`),
  KEY `businessid_2` (`businessid`,`groupid`,`deleted`),
  KEY `menu_item_id` (`menu_item_id`,`item_code`,`businessid`),
  KEY `businessid` (`businessid`,`menu_typeid`,`item_code`),
  KEY `menu_typeid` (`menu_typeid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=191333 ;

-- --------------------------------------------------------

--
-- Table structure for table `menu_items_category`
--

DROP TABLE IF EXISTS `menu_items_category`;
CREATE TABLE IF NOT EXISTS `menu_items_category` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=95 ;

-- --------------------------------------------------------

--
-- Table structure for table `menu_items_company`
--

DROP TABLE IF EXISTS `menu_items_company`;
CREATE TABLE IF NOT EXISTS `menu_items_company` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(3) unsigned NOT NULL,
  `item_code` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `company_id` (`company_id`,`item_code`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='menu items per company id' AUTO_INCREMENT=2055 ;

-- --------------------------------------------------------

--
-- Table structure for table `menu_items_master`
--

DROP TABLE IF EXISTS `menu_items_master`;
CREATE TABLE IF NOT EXISTS `menu_items_master` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `UPC` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `Name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Manufacturer` int(10) unsigned DEFAULT NULL,
  `Category` int(10) unsigned DEFAULT NULL,
  `isCore` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL,
  `sold` int(10) NOT NULL,
  `ordertype` int(5) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `UPC_UNIQUE` (`UPC`),
  KEY `Manufacturer` (`Manufacturer`),
  KEY `Category` (`Category`),
  KEY `UPC` (`UPC`,`ordertype`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10494 ;

-- --------------------------------------------------------

--
-- Table structure for table `menu_items_new`
--

DROP TABLE IF EXISTS `menu_items_new`;
CREATE TABLE IF NOT EXISTS `menu_items_new` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `businessid` int(11) NOT NULL,
  `pos_processed` tinyint(1) NOT NULL DEFAULT '0',
  `pos_id` int(10) unsigned NULL,
  `group_id` int(11) unsigned DEFAULT NULL,
  `manufacturer` int(5) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ordertype` int(4) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `mic_id` int(10) unsigned NOT NULL COMMENT 'id from menu_item_company',
  `item_code` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `upc` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `onhand` double NOT NULL,
  `reorder` double NOT NULL,
  `max` double NOT NULL,
  `reorder_point` double NOT NULL,
  `reorder_amount` double NOT NULL,
  `shelf_life_days` int(11) NOT NULL,
  `is_button` tinyint(1) NOT NULL DEFAULT '0',
  `rollover` float NOT NULL DEFAULT '0',
  `sale_unit` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unit',
  `tare` decimal(7,5) NOT NULL DEFAULT '0.00000',
  `cond_group` int(10) unsigned DEFAULT NULL,
  `force_condiment` tinyint(1) NOT NULL DEFAULT '0',
  `requires_activation` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `pos_id` (`pos_id`,`businessid`),
  KEY `businessid` (`businessid`,`item_code`),
  KEY `active` (`businessid`,`active`,`ordertype`),
  KEY `group_id` (`group_id`,`pos_id`),
  KEY `businessid_2` (`businessid`,`pos_processed`),
  KEY `cond_group_index` (`cond_group`),
  KEY `upc` (`upc`),
  KEY `requires_activation` (`requires_activation`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1451569 ;

--
-- Triggers `menu_items_new`
--
DROP TRIGGER IF EXISTS `mitnew_bi`;
DELIMITER //
CREATE TRIGGER `mitnew_bi` BEFORE INSERT ON `menu_items_new`
 FOR EACH ROW BEGIN
    
    DECLARE bid_item_code_totals, bid_upc_totals, pos_id_value INT DEFAULT 0;
	DECLARE item_code_new, upc_new VARCHAR(30) DEFAULT NULL;

    SET @businessid = NEW.businessid; 
    SET @pos_id = NEW.pos_id; 
    SET @group_id = NEW.group_id; 
    SET @name = NEW.name; 
    SET @item_code = NEW.item_code; 
    SET @upc = NEW.upc; 
	
	SELECT REPLACE(@item_code, '&', '') INTO item_code_new;
	SELECT REPLACE(@upc, '&', '') INTO upc_new;
    
    SELECT MAX(pos_id) + 1 INTO pos_id_value FROM jsantos_ee.menu_items_new;

    SELECT COUNT(*) INTO bid_item_code_totals FROM jsantos_ee.menu_items_new WHERE (businessid = @businessid) AND (item_code = @item_code);
    SELECT COUNT(*) INTO bid_upc_totals FROM jsantos_ee.menu_items_new WHERE (businessid = @businessid) AND (upc = @upc);

    IF (bid_item_code_totals > 0) OR (bid_upc_totals > 0) THEN 
        SET NEW.businessid = 0; 
        SET NEW.pos_id = pos_id_value; 
        SET NEW.group_id = @group_id; 
        SET NEW.name = 'delete';
        SET NEW.item_code = 'delete'; 
        SET NEW.upc = 'delete'; 
        
        INSERT INTO jsantos_ee.menu_items_new_duplicates (pos_id_value, businessid, item_code, upc) VALUES (NEW.pos_id, @businessid, @item_code, @upc);
    ELSE
        SET NEW.pos_id = pos_id_value;
		SET NEW.item_code = item_code_new;
		SET NEW.upc = upc_new;
    END IF;
    
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `menu_items_new_ai`;
DELIMITER //
CREATE TRIGGER `menu_items_new_ai` AFTER INSERT ON `menu_items_new`
 FOR EACH ROW BEGIN 
    INSERT IGNORE INTO posreports_menu_items_new 
    (id, businessid, pos_processed, pos_id, group_id, manufacturer, name, ordertype, active, mic_id, item_code, upc, onhand, reorder, max, reorder_point, reorder_amount, shelf_life_days, is_button, rollover, sale_unit, tare, cond_group, force_condiment, requires_activation) 
    VALUES 
    (NEW.id, NEW.businessid, NEW.pos_processed, NEW.pos_id, NEW.group_id, NEW.manufacturer, NEW.name, NEW.ordertype, NEW.active, NEW.mic_id, NEW.item_code, NEW.upc, NEW.onhand, NEW.reorder, NEW.max, NEW.reorder_point, NEW.reorder_amount, NEW.shelf_life_days, NEW.is_button, NEW.rollover, NEW.sale_unit, NEW.tare, NEW.cond_group, NEW.force_condiment, NEW.requires_activation);
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `mitnew_bu`;
DELIMITER //
CREATE TRIGGER `mitnew_bu` BEFORE UPDATE ON `menu_items_new`
 FOR EACH ROW BEGIN
    
    DECLARE bid_item_code_totals, bid_upc_totals, pos_id_value, duplicate_bid_item_code, duplicate_bid_upc INT DEFAULT 0;
	DECLARE item_code_new, upc_new VARCHAR(30) DEFAULT NULL;

	SET @item_code = NEW.item_code; 
	SET @upc = NEW.upc; 
	
	SELECT REPLACE(@item_code, '&', '') INTO item_code_new;
	SELECT REPLACE(@upc, '&', '') INTO upc_new;
	
	SET NEW.item_code = item_code_new;
	SET NEW.upc = upc_new;
	
    SET @mitn_id_value = OLD.id;
    SET @businessid = NEW.businessid; 
    SET @bid_value = NEW.businessid; 
    SET @pos_id = NEW.pos_id; 
	SET @name = NEW.name; 
        
    IF (NEW.item_code != OLD.item_code) OR (NEW.upc != OLD.upc) THEN
    
        SELECT COUNT(*) INTO bid_item_code_totals FROM jsantos_ee.menu_items_new WHERE (businessid = @businessid) AND (item_code = NEW.item_code);
        SELECT COUNT(*) INTO bid_upc_totals FROM jsantos_ee.menu_items_new WHERE (businessid = @businessid) AND (upc = @upc);

        IF (bid_item_code_totals > 0) THEN 
            SET NEW.item_code = OLD.item_code;
            SELECT 1 INTO duplicate_bid_item_code;
        ELSE
            SELECT 0 INTO duplicate_bid_item_code;
        END IF;
        
        IF (bid_upc_totals > 0) THEN 
            SET NEW.upc = OLD.upc;
            SELECT 1 INTO duplicate_bid_upc;
        ELSE
            SELECT 0 INTO duplicate_bid_upc;
        END IF;
        
        IF (duplicate_bid_item_code > 0) OR (duplicate_bid_upc > 0) THEN 
            INSERT INTO jsantos_ee.menu_items_new_duplicates (menu_item_new_id_value, pos_id_value, item_name, businessid, item_code, upc) VALUES (@mitn_id_value, NEW.pos_id, @name, @bid_value, @item_code, @upc);
        END IF;
        
    END IF;
    
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `update_MINClearGroupPosition`;
DELIMITER //
CREATE TRIGGER `update_MINClearGroupPosition` AFTER UPDATE ON `menu_items_new`
 FOR EACH ROW BEGIN
 CALL trigMINClearGroupPosition( NEW.id, NEW.is_button );
 	DELETE FROM posreports_menu_items_new WHERE id = OLD.id;
	
    INSERT IGNORE INTO posreports_menu_items_new 
    (id, businessid, pos_processed, pos_id, group_id, manufacturer, name, ordertype, active, mic_id, item_code, upc, onhand, reorder, max, reorder_point, reorder_amount, shelf_life_days, is_button, rollover, sale_unit, tare, cond_group, force_condiment, requires_activation) 
    VALUES 
    (OLD.id, NEW.businessid, NEW.pos_processed, NEW.pos_id, NEW.group_id, NEW.manufacturer, NEW.name, NEW.ordertype, NEW.active, NEW.mic_id, NEW.item_code, NEW.upc, NEW.onhand, NEW.reorder, NEW.max, NEW.reorder_point, NEW.reorder_amount, NEW.shelf_life_days, NEW.is_button, NEW.rollover, NEW.sale_unit, NEW.tare, NEW.cond_group, NEW.force_condiment, NEW.requires_activation);
    END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `menu_items_new_ad`;
DELIMITER //
CREATE TRIGGER `menu_items_new_ad` AFTER DELETE ON `menu_items_new`
 FOR EACH ROW BEGIN 
   DELETE FROM posreports_menu_items_new WHERE id = OLD.id LIMIT 1;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `menu_items_new_duplicates`
--

DROP TABLE IF EXISTS `menu_items_new_duplicates`;
CREATE TABLE IF NOT EXISTS `menu_items_new_duplicates` (
  `valueid` int(11) NOT NULL AUTO_INCREMENT,
  `menu_item_new_id_value` int(11) DEFAULT '0',
  `pos_id_value` int(11) DEFAULT '0',
  `item_name` varchar(100) NOT NULL,
  `businessid` int(11) DEFAULT '0',
  `item_code` varchar(30) NOT NULL,
  `upc` varchar(30) NOT NULL,
  PRIMARY KEY (`valueid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=64073 ;

-- --------------------------------------------------------

--
-- Table structure for table `menu_items_new_modifier_groups`
--

DROP TABLE IF EXISTS `menu_items_new_modifier_groups`;
CREATE TABLE IF NOT EXISTS `menu_items_new_modifier_groups` (
  `menu_item_id` int(10) unsigned NOT NULL,
  `modifier_group_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`menu_item_id`,`modifier_group_id`),
  KEY `fk_aeris_modifier_group` (`modifier_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menu_items_ordertype`
--

DROP TABLE IF EXISTS `menu_items_ordertype`;
CREATE TABLE IF NOT EXISTS `menu_items_ordertype` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `menu_items_price`
--

DROP TABLE IF EXISTS `menu_items_price`;
CREATE TABLE IF NOT EXISTS `menu_items_price` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `businessid` int(11) unsigned NOT NULL,
  `menu_item_id` int(11) unsigned NOT NULL,
  `pos_price_id` tinyint(2) unsigned NOT NULL,
  `price` decimal(5,2) NOT NULL,
  `new_price` decimal(5,2) DEFAULT NULL,
  `cost` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menu_item_id` (`menu_item_id`,`pos_price_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1570176 ;

--
-- Triggers `menu_items_price`
--
DROP TRIGGER IF EXISTS `menu_items_price_ai`;
DELIMITER //
CREATE TRIGGER `menu_items_price_ai` AFTER INSERT ON `menu_items_price`
 FOR EACH ROW BEGIN 
    INSERT IGNORE INTO posreports_menu_items_price 
    (id, businessid, menu_item_id, pos_price_id, price, new_price, cost) 
    VALUES 
    (NEW.id, NEW.businessid, NEW.menu_item_id, NEW.pos_price_id, NEW.price, NEW.new_price, NEW.cost);
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `menu_items_price_au`;
DELIMITER //
CREATE TRIGGER `menu_items_price_au` AFTER UPDATE ON `menu_items_price`
 FOR EACH ROW BEGIN 
	
	DELETE FROM posreports_menu_items_price WHERE id = OLD.id;
	
    INSERT IGNORE INTO posreports_menu_items_price 
    (id, businessid, menu_item_id, pos_price_id, price, new_price, cost) 
    VALUES 
    (OLD.id, NEW.businessid, NEW.menu_item_id, NEW.pos_price_id, NEW.price, NEW.new_price, NEW.cost);
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `menu_items_price_ad`;
DELIMITER //
CREATE TRIGGER `menu_items_price_ad` AFTER DELETE ON `menu_items_price`
 FOR EACH ROW BEGIN 
   DELETE FROM posreports_menu_items_price WHERE id = OLD.id LIMIT 1;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `menu_items_price_copy`
--

DROP TABLE IF EXISTS `menu_items_price_copy`;
CREATE TABLE IF NOT EXISTS `menu_items_price_copy` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `businessid` int(11) unsigned NOT NULL,
  `menu_item_id` int(11) unsigned NOT NULL,
  `pos_price_id` tinyint(2) unsigned NOT NULL,
  `price` decimal(5,2) NOT NULL,
  `new_price` decimal(5,2) DEFAULT NULL,
  `cost` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menu_item_id` (`menu_item_id`,`pos_price_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1192332 ;

-- --------------------------------------------------------

--
-- Table structure for table `menu_portion`
--

DROP TABLE IF EXISTS `menu_portion`;
CREATE TABLE IF NOT EXISTS `menu_portion` (
  `menu_portionid` int(10) NOT NULL AUTO_INCREMENT,
  `menu_portionname` varchar(30) NOT NULL,
  `menu_typeid` int(10) NOT NULL,
  `orderid` int(2) NOT NULL,
  `businessid` int(10) NOT NULL,
  PRIMARY KEY (`menu_portionid`),
  KEY `menu_typeid` (`menu_typeid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=73 ;

-- --------------------------------------------------------

--
-- Table structure for table `menu_portiondetail`
--

DROP TABLE IF EXISTS `menu_portiondetail`;
CREATE TABLE IF NOT EXISTS `menu_portiondetail` (
  `menu_portiondetailid` int(10) NOT NULL AUTO_INCREMENT,
  `menu_typeid` int(3) NOT NULL,
  `menu_portionid` int(10) NOT NULL,
  `menu_daypartid` int(10) NOT NULL,
  `menu_itemid` int(10) NOT NULL,
  `portion` float NOT NULL,
  `srv_size` varchar(10) NOT NULL,
  PRIMARY KEY (`menu_portiondetailid`),
  KEY `menu_typeid` (`menu_typeid`,`menu_portionid`,`menu_daypartid`,`menu_itemid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=81613 ;

-- --------------------------------------------------------

--
-- Table structure for table `menu_pricegroup`
--

DROP TABLE IF EXISTS `menu_pricegroup`;
CREATE TABLE IF NOT EXISTS `menu_pricegroup` (
  `menu_pricegroupid` int(10) NOT NULL AUTO_INCREMENT,
  `menu_pricegroupname` varchar(30) NOT NULL,
  `menu_typeid` int(3) NOT NULL,
  `menu_groupid` int(5) NOT NULL,
  `orderid` int(2) NOT NULL,
  `price` double NOT NULL,
  `percent` tinyint(1) NOT NULL DEFAULT '0',
  `account` varchar(10) NOT NULL,
  `old_account` varchar(10) NOT NULL,
  `foodcost` double NOT NULL DEFAULT '60',
  PRIMARY KEY (`menu_pricegroupid`),
  KEY `menu_pricegroupid` (`menu_pricegroupid`,`menu_groupid`),
  KEY `menu_typeid` (`menu_typeid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=175 ;

-- --------------------------------------------------------

--
-- Table structure for table `menu_screen_order`
--

DROP TABLE IF EXISTS `menu_screen_order`;
CREATE TABLE IF NOT EXISTS `menu_screen_order` (
  `businessid` int(10) unsigned NOT NULL,
  `itemId` int(10) unsigned NOT NULL,
  `display_order` int(11) NOT NULL DEFAULT '99999',
  `pos_color` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`businessid`,`itemId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu_tax`
--

DROP TABLE IF EXISTS `menu_tax`;
CREATE TABLE IF NOT EXISTS `menu_tax` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pos_processed` tinyint(1) NOT NULL DEFAULT '0',
  `pos_id` int(10) NOT NULL,
  `businessid` int(10) NOT NULL,
  `name` varchar(25) NOT NULL,
  `percent` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `businessid` (`businessid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=782 ;

-- --------------------------------------------------------

--
-- Table structure for table `menu_type`
--

DROP TABLE IF EXISTS `menu_type`;
CREATE TABLE IF NOT EXISTS `menu_type` (
  `menu_typeid` int(5) NOT NULL AUTO_INCREMENT,
  `menu_typename` varchar(50) NOT NULL,
  `companyid` int(3) NOT NULL,
  `businessid` int(10) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `type` int(1) NOT NULL DEFAULT '0',
  `parent` int(3) NOT NULL,
  `static_menu` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`menu_typeid`),
  KEY `companyid` (`companyid`,`businessid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=53 ;

-- --------------------------------------------------------

--
-- Table structure for table `migrate_promo_groups`
--

DROP TABLE IF EXISTS `migrate_promo_groups`;
CREATE TABLE IF NOT EXISTS `migrate_promo_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `businessid` int(10) unsigned NOT NULL,
  `uuid` char(36) NOT NULL DEFAULT '',
  `name` varchar(128) NOT NULL DEFAULT '',
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `routing` tinyint(1) NOT NULL DEFAULT '1',
  `display_order` int(11) NOT NULL DEFAULT '99999',
  `display_y` int(11) DEFAULT NULL,
  `display_x` int(11) DEFAULT NULL,
  `pos_color` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  KEY `businessid` (`businessid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=690 ;

-- --------------------------------------------------------

--
-- Table structure for table `migrate_promo_groups_detail`
--

DROP TABLE IF EXISTS `migrate_promo_groups_detail`;
CREATE TABLE IF NOT EXISTS `migrate_promo_groups_detail` (
  `product_id` int(10) unsigned NOT NULL,
  `promo_group_id` int(10) unsigned NOT NULL,
  `display_order` int(10) unsigned NOT NULL DEFAULT '99999',
  `display_y` int(11) DEFAULT NULL,
  `display_x` int(11) DEFAULT NULL,
  `pos_color` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`,`promo_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mirror_accounts`
--

DROP TABLE IF EXISTS `mirror_accounts`;
CREATE TABLE IF NOT EXISTS `mirror_accounts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `accountid` int(10) NOT NULL,
  `type` int(3) NOT NULL,
  `accountnum` varchar(25) NOT NULL,
  `category` int(3) NOT NULL,
  `bank_rec` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `accountid` (`accountid`,`type`,`category`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19382 ;

-- --------------------------------------------------------

--
-- Table structure for table `mirror_submit`
--

DROP TABLE IF EXISTS `mirror_submit`;
CREATE TABLE IF NOT EXISTS `mirror_submit` (
  `businessid` int(10) NOT NULL,
  `date` date NOT NULL,
  `export` tinyint(1) NOT NULL DEFAULT '0',
  `category` int(3) NOT NULL DEFAULT '0',
  UNIQUE KEY `businessid` (`businessid`,`date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `modifier`
--

DROP TABLE IF EXISTS `modifier`;
CREATE TABLE IF NOT EXISTS `modifier` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL DEFAULT '0',
  `companyid` int(2) NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `total` int(6) NOT NULL DEFAULT '0',
  `detail` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50084 ;

-- --------------------------------------------------------

--
-- Table structure for table `mygadgets`
--

DROP TABLE IF EXISTS `mygadgets`;
CREATE TABLE IF NOT EXISTS `mygadgets` (
  `mygadgetid` int(10) NOT NULL AUTO_INCREMENT,
  `loginid` int(10) NOT NULL,
  `gadget_name` varchar(40) NOT NULL,
  `code` text NOT NULL,
  PRIMARY KEY (`mygadgetid`),
  KEY `loginid` (`loginid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=89 ;

-- --------------------------------------------------------

--
-- Table structure for table `mygroup_detail`
--

DROP TABLE IF EXISTS `mygroup_detail`;
CREATE TABLE IF NOT EXISTS `mygroup_detail` (
  `mygroup_detailid` int(10) NOT NULL AUTO_INCREMENT,
  `mygroup_id` int(10) NOT NULL,
  `loginid` int(10) NOT NULL,
  PRIMARY KEY (`mygroup_detailid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

-- --------------------------------------------------------

--
-- Table structure for table `mygroups`
--

DROP TABLE IF EXISTS `mygroups`;
CREATE TABLE IF NOT EXISTS `mygroups` (
  `mygroup_id` int(10) NOT NULL AUTO_INCREMENT,
  `mygroup_name` varchar(50) NOT NULL,
  `loginid` int(10) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`mygroup_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

-- --------------------------------------------------------

--
-- Table structure for table `mylinks`
--

DROP TABLE IF EXISTS `mylinks`;
CREATE TABLE IF NOT EXISTS `mylinks` (
  `mylinksid` int(10) NOT NULL AUTO_INCREMENT,
  `loginid` int(10) NOT NULL,
  `url` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`mylinksid`),
  KEY `loginid` (`loginid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=434 ;

-- --------------------------------------------------------

--
-- Table structure for table `nutrition`
--

DROP TABLE IF EXISTS `nutrition`;
CREATE TABLE IF NOT EXISTS `nutrition` (
  `item_code` varchar(15) NOT NULL,
  `mfr_code` varchar(15) NOT NULL,
  `mfr_name` varchar(15) NOT NULL,
  `item_name` varchar(40) NOT NULL,
  `vendor` int(10) NOT NULL,
  `calories` double NOT NULL,
  `protein` double NOT NULL,
  `unsat_fat` double NOT NULL,
  `sat_fat` double NOT NULL,
  `total_fat` double NOT NULL,
  `fiber` double NOT NULL,
  `cholesterol` double NOT NULL,
  `sodium` double NOT NULL,
  `iron` double NOT NULL,
  `calcium` double NOT NULL,
  `vit_A` double NOT NULL,
  `thiamine` double NOT NULL,
  `riboflavin` double NOT NULL,
  `vit_C` double NOT NULL,
  `niacin` double NOT NULL,
  `magnesium` double NOT NULL,
  `zinc` double NOT NULL,
  `phosphorus` double NOT NULL,
  `potassium` double NOT NULL,
  `complex_cabs` double NOT NULL,
  `sugar` double NOT NULL,
  `cal_from_fat` double NOT NULL,
  `weight_density` double NOT NULL,
  `serv_size_gram` double NOT NULL,
  `serv_size_hh` double NOT NULL,
  `hh_units` varchar(20) NOT NULL,
  `rec_size` int(3) NOT NULL,
  `rec_size2` int(3) NOT NULL,
  `inv_rec_size` int(3) NOT NULL,
  `inv_rec_num` double NOT NULL,
  `inv_rec_size2` int(3) NOT NULL,
  `inv_rec_num2` double NOT NULL,
  `notes` varchar(100) NOT NULL,
  `user` varchar(20) NOT NULL,
  `date` date NOT NULL,
  `category` double NOT NULL,
  PRIMARY KEY (`item_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nutrition_types`
--

DROP TABLE IF EXISTS `nutrition_types`;
CREATE TABLE IF NOT EXISTS `nutrition_types` (
  `nutritionid` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `units` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `column_key` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `sort_order` int(2) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`nutritionid`),
  UNIQUE KEY `column_key` (`column_key`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=24 ;

-- --------------------------------------------------------

--
-- Table structure for table `nutrition_xref_company`
--

DROP TABLE IF EXISTS `nutrition_xref_company`;
CREATE TABLE IF NOT EXISTS `nutrition_xref_company` (
  `mic_id` int(10) unsigned NOT NULL COMMENT 'menu_item_company.id',
  `nutrition_type_id` int(3) unsigned NOT NULL COMMENT 'nutrition_types.nutritionid',
  `value` double NOT NULL,
  PRIMARY KEY (`mic_id`,`nutrition_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='crossreference between menu_items_company and nutrition_type';

-- --------------------------------------------------------

--
-- Table structure for table `nutrition_xref_master`
--

DROP TABLE IF EXISTS `nutrition_xref_master`;
CREATE TABLE IF NOT EXISTS `nutrition_xref_master` (
  `mim_id` int(10) unsigned NOT NULL COMMENT 'menu_item_master.id',
  `nutrition_type_id` int(3) unsigned NOT NULL COMMENT 'nutrition_types.nutritionid',
  `value` double NOT NULL,
  PRIMARY KEY (`mim_id`,`nutrition_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='crossreference between menu_item_master and nutrition_type';

-- --------------------------------------------------------

--
-- Table structure for table `nutrition_xref_types`
--

DROP TABLE IF EXISTS `nutrition_xref_types`;
CREATE TABLE IF NOT EXISTS `nutrition_xref_types` (
  `item_code` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `nutrition_types_id` int(11) NOT NULL,
  `value` double NOT NULL,
  PRIMARY KEY (`item_code`,`nutrition_types_id`),
  KEY `nutrition_types_id` (`nutrition_types_id`,`value`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `optiongroup`
--

DROP TABLE IF EXISTS `optiongroup`;
CREATE TABLE IF NOT EXISTS `optiongroup` (
  `optiongroupid` int(10) NOT NULL AUTO_INCREMENT,
  `menu_item_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(50) NOT NULL DEFAULT '',
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `orderid` int(2) NOT NULL,
  PRIMARY KEY (`optiongroupid`),
  KEY `menu_item_id` (`menu_item_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6365 ;

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

DROP TABLE IF EXISTS `options`;
CREATE TABLE IF NOT EXISTS `options` (
  `optionid` int(10) NOT NULL AUTO_INCREMENT,
  `optiongroupid` int(10) NOT NULL DEFAULT '0',
  `description` varchar(50) NOT NULL DEFAULT '',
  `price` float NOT NULL DEFAULT '0',
  `orderid` int(2) NOT NULL,
  `noprint` tinyint(1) NOT NULL,
  `menu_itemid` int(10) NOT NULL,
  PRIMARY KEY (`optionid`),
  KEY `optiongroupid` (`optiongroupid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31614 ;

-- --------------------------------------------------------

--
-- Table structure for table `order_detail`
--

DROP TABLE IF EXISTS `order_detail`;
CREATE TABLE IF NOT EXISTS `order_detail` (
  `order_detailid` int(12) NOT NULL AUTO_INCREMENT,
  `menu_daypartid` int(10) NOT NULL,
  `date` date NOT NULL,
  `menu_portionid` int(10) NOT NULL,
  `qty` double NOT NULL,
  `accountid` int(10) NOT NULL,
  `billed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_detailid`),
  KEY `menu_daypartid` (`menu_daypartid`,`menu_portionid`,`accountid`,`date`,`billed`),
  KEY `accountid` (`accountid`,`date`,`menu_daypartid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1174075 ;

-- --------------------------------------------------------

--
-- Table structure for table `order_item`
--

DROP TABLE IF EXISTS `order_item`;
CREATE TABLE IF NOT EXISTS `order_item` (
  `order_itemid` int(10) NOT NULL AUTO_INCREMENT,
  `menu_itemid` int(10) NOT NULL,
  `daypartid` int(10) NOT NULL,
  `date` date NOT NULL,
  `businessid` int(10) NOT NULL,
  `orderid` int(1) NOT NULL,
  PRIMARY KEY (`order_itemid`),
  KEY `daypartid` (`daypartid`,`date`,`businessid`),
  KEY `businessid` (`businessid`,`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=562566 ;

-- --------------------------------------------------------

--
-- Table structure for table `order_matrix`
--

DROP TABLE IF EXISTS `order_matrix`;
CREATE TABLE IF NOT EXISTS `order_matrix` (
  `order_matrixid` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL,
  `menu_typeid` int(5) NOT NULL,
  `daypartid` int(10) NOT NULL,
  `portionid` int(10) NOT NULL,
  `price` double NOT NULL,
  PRIMARY KEY (`order_matrixid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=326 ;

-- --------------------------------------------------------

--
-- Table structure for table `order_matrixcust`
--

DROP TABLE IF EXISTS `order_matrixcust`;
CREATE TABLE IF NOT EXISTS `order_matrixcust` (
  `order_matrixcustid` int(10) NOT NULL AUTO_INCREMENT,
  `accountid` int(10) NOT NULL,
  `menu_typeid` int(5) NOT NULL,
  `daypartid` int(5) NOT NULL,
  `portionid` int(5) NOT NULL,
  `price` double NOT NULL,
  PRIMARY KEY (`order_matrixcustid`),
  KEY `daypartid` (`daypartid`,`portionid`,`accountid`,`menu_typeid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=59938 ;

-- --------------------------------------------------------

--
-- Table structure for table `order_payments`
--

DROP TABLE IF EXISTS `order_payments`;
CREATE TABLE IF NOT EXISTS `order_payments` (
  `id` varchar(250) NOT NULL,
  `order_id` varchar(250) DEFAULT NULL,
  `order_items_count` int(11) DEFAULT NULL,
  `order_total` float DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `memo1` text,
  `memo2` text,
  `created` int(11) DEFAULT NULL,
  `modified` int(11) DEFAULT NULL,
  `origin_amount` float DEFAULT NULL,
  `service_clerk` varchar(250) DEFAULT NULL,
  `proceeds_clerk` varchar(250) DEFAULT NULL,
  `service_clerk_displayname` varchar(250) DEFAULT NULL,
  `proceeds_clerk_displayname` varchar(250) DEFAULT NULL,
  `change` float DEFAULT NULL,
  `sale_period` int(11) DEFAULT NULL,
  `shift_number` int(11) DEFAULT NULL,
  `terminal_no` varchar(250) DEFAULT NULL,
  `is_groupable` tinyint(1) DEFAULT '0',
  `customer_card_number` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_payments_oid` (`order_id`),
  KEY `order_payments_sp_tn_sn` (`sale_period`,`terminal_no`,`shift_number`),
  KEY `terminal_no` (`terminal_no`(10),`memo1`(25),`memo2`(25))
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_subs`
--

DROP TABLE IF EXISTS `order_subs`;
CREATE TABLE IF NOT EXISTS `order_subs` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `order_itemid` int(10) NOT NULL,
  `accountid` int(10) NOT NULL,
  `amount` double NOT NULL,
  `daypartid` int(5) NOT NULL,
  `portionid` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `daypartid` (`daypartid`,`portionid`,`accountid`,`order_itemid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=76031 ;

-- --------------------------------------------------------

--
-- Table structure for table `ordertype`
--

DROP TABLE IF EXISTS `ordertype`;
CREATE TABLE IF NOT EXISTS `ordertype` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `participation_businesses`
--

DROP TABLE IF EXISTS `participation_businesses`;
CREATE TABLE IF NOT EXISTS `participation_businesses` (
  `participation_id` int(5) NOT NULL,
  `business_id` int(10) NOT NULL,
  UNIQUE KEY `participation_id` (`participation_id`,`business_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `participation_program`
--

DROP TABLE IF EXISTS `participation_program`;
CREATE TABLE IF NOT EXISTS `participation_program` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `participation_whitelist_upc`
--

DROP TABLE IF EXISTS `participation_whitelist_upc`;
CREATE TABLE IF NOT EXISTS `participation_whitelist_upc` (
  `participation_id` int(5) NOT NULL,
  `UPC` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `participation_id` (`participation_id`,`UPC`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_detail`
--

DROP TABLE IF EXISTS `payment_detail`;
CREATE TABLE IF NOT EXISTS `payment_detail` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `businessid` int(11) NOT NULL,
  `check_number` bigint(20) unsigned NOT NULL,
  `payment_type` int(11) NOT NULL,
  `received` double(6,2) NOT NULL,
  `base_amount` double(6,2) NOT NULL,
  `tip_amount` double(6,2) NOT NULL,
  `scancode` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `check_number` (`check_number`),
  KEY `scancode` (`scancode`),
  KEY `businessid` (`businessid`,`check_number`,`payment_type`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=102531875 ;

--
-- Triggers `payment_detail`
--
DROP TRIGGER IF EXISTS `payment_detail_ai`;
DELIMITER //
CREATE TRIGGER `payment_detail_ai` AFTER INSERT ON `payment_detail`
 FOR EACH ROW BEGIN 
    INSERT IGNORE INTO posreports_payment_detail 
    (id, businessid, check_number, payment_type, received, base_amount, tip_amount, scancode) 
    VALUES 
    (NEW.id, NEW.businessid, NEW.check_number, NEW.payment_type, NEW.received, NEW.base_amount, NEW.tip_amount, NEW.scancode);
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `payment_detail_au`;
DELIMITER //
CREATE TRIGGER `payment_detail_au` AFTER UPDATE ON `payment_detail`
 FOR EACH ROW BEGIN 
	
	DELETE FROM posreports_payment_detail WHERE id = OLD.id;
	
    INSERT IGNORE INTO posreports_payment_detail 
    (id, businessid, check_number, payment_type, received, base_amount, tip_amount, scancode) 
    VALUES 
    (OLD.id, NEW.businessid, NEW.check_number, NEW.payment_type, NEW.received, NEW.base_amount, NEW.tip_amount, NEW.scancode);
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `payment_detail_ad`;
DELIMITER //
CREATE TRIGGER `payment_detail_ad` AFTER DELETE ON `payment_detail`
 FOR EACH ROW BEGIN 
   DELETE FROM posreports_payment_detail WHERE id = OLD.id LIMIT 1;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `payroll`
--

DROP TABLE IF EXISTS `payroll`;
CREATE TABLE IF NOT EXISTS `payroll` (
  `payrollid` int(10) NOT NULL AUTO_INCREMENT,
  `unit_num` int(5) NOT NULL,
  `date` date NOT NULL,
  `employee` varchar(50) NOT NULL,
  `empl_id` int(10) NOT NULL,
  `rate` float NOT NULL,
  `reg1` double NOT NULL,
  `ot1` double NOT NULL,
  `coded1` varchar(30) NOT NULL,
  `reg2` float NOT NULL,
  `ot2` double NOT NULL,
  `coded2` varchar(30) NOT NULL,
  `gross` double NOT NULL,
  PRIMARY KEY (`payrollid`),
  KEY `unit_num` (`unit_num`,`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=210335 ;

-- --------------------------------------------------------

--
-- Table structure for table `payroll_code`
--

DROP TABLE IF EXISTS `payroll_code`;
CREATE TABLE IF NOT EXISTS `payroll_code` (
  `codeid` int(5) NOT NULL AUTO_INCREMENT,
  `companyid` int(3) NOT NULL,
  `code` varchar(15) NOT NULL,
  `orderid` int(2) NOT NULL,
  `charge` tinyint(1) NOT NULL DEFAULT '0',
  `export_name` varchar(20) NOT NULL,
  `special` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`codeid`),
  KEY `codeid` (`codeid`,`code`),
  KEY `companyid` (`companyid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

-- --------------------------------------------------------

--
-- Table structure for table `payroll_code_comm`
--

DROP TABLE IF EXISTS `payroll_code_comm`;
CREATE TABLE IF NOT EXISTS `payroll_code_comm` (
  `codeid` int(5) NOT NULL AUTO_INCREMENT,
  `companyid` int(3) NOT NULL,
  `code_name` varchar(10) NOT NULL,
  `export_name` varchar(20) NOT NULL,
  `no_export` tinyint(1) NOT NULL DEFAULT '0',
  `orderid` int(2) NOT NULL,
  `hours` tinyint(1) NOT NULL,
  `nocharge` tinyint(1) NOT NULL,
  `noedit` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`codeid`),
  KEY `companyid` (`companyid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

-- --------------------------------------------------------

--
-- Table structure for table `payroll_code_control`
--

DROP TABLE IF EXISTS `payroll_code_control`;
CREATE TABLE IF NOT EXISTS `payroll_code_control` (
  `companyid` int(3) NOT NULL,
  `code` varchar(50) NOT NULL,
  `charge` tinyint(1) NOT NULL,
  UNIQUE KEY `companyid` (`companyid`,`code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payroll_date`
--

DROP TABLE IF EXISTS `payroll_date`;
CREATE TABLE IF NOT EXISTS `payroll_date` (
  `payroll_dateid` int(10) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `companyid` int(3) NOT NULL,
  PRIMARY KEY (`payroll_dateid`),
  KEY `date` (`date`,`companyid`),
  KEY `companyid` (`companyid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=361 ;

-- --------------------------------------------------------

--
-- Table structure for table `payroll_department`
--

DROP TABLE IF EXISTS `payroll_department`;
CREATE TABLE IF NOT EXISTS `payroll_department` (
  `dept_id` int(3) NOT NULL AUTO_INCREMENT,
  `dept_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `dept_code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`dept_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

-- --------------------------------------------------------

--
-- Table structure for table `payroll_departmentdetail`
--

DROP TABLE IF EXISTS `payroll_departmentdetail`;
CREATE TABLE IF NOT EXISTS `payroll_departmentdetail` (
  `dept_detail_id` int(10) NOT NULL AUTO_INCREMENT,
  `loginid` int(10) NOT NULL,
  `dept_id` int(3) NOT NULL,
  PRIMARY KEY (`dept_detail_id`),
  KEY `loginid` (`loginid`,`dept_id`),
  KEY `dept_id` (`dept_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4415 ;

-- --------------------------------------------------------

--
-- Table structure for table `pm_access`
--

DROP TABLE IF EXISTS `pm_access`;
CREATE TABLE IF NOT EXISTS `pm_access` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `jobid` int(5) NOT NULL,
  `loginid` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=69 ;

-- --------------------------------------------------------

--
-- Table structure for table `pm_accounts`
--

DROP TABLE IF EXISTS `pm_accounts`;
CREATE TABLE IF NOT EXISTS `pm_accounts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `accountid` int(10) NOT NULL,
  `account_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `pm_category`
--

DROP TABLE IF EXISTS `pm_category`;
CREATE TABLE IF NOT EXISTS `pm_category` (
  `categoryid` int(3) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`categoryid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

-- --------------------------------------------------------

--
-- Table structure for table `pm_comment`
--

DROP TABLE IF EXISTS `pm_comment`;
CREATE TABLE IF NOT EXISTS `pm_comment` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pm_id` int(10) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `user` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=70 ;

-- --------------------------------------------------------

--
-- Table structure for table `pm_detail`
--

DROP TABLE IF EXISTS `pm_detail`;
CREATE TABLE IF NOT EXISTS `pm_detail` (
  `pm_detailid` int(10) NOT NULL AUTO_INCREMENT,
  `pm_id` int(10) NOT NULL,
  `level` int(2) NOT NULL DEFAULT '1',
  `detail` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `propose_date` date NOT NULL,
  `date` date NOT NULL,
  `assign_to` int(5) NOT NULL,
  `hours` double NOT NULL,
  `eoc` tinyint(1) NOT NULL,
  PRIMARY KEY (`pm_detailid`),
  KEY `pm_id` (`pm_id`,`level`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1060 ;

-- --------------------------------------------------------

--
-- Table structure for table `pm_division`
--

DROP TABLE IF EXISTS `pm_division`;
CREATE TABLE IF NOT EXISTS `pm_division` (
  `divisionid` int(2) NOT NULL AUTO_INCREMENT,
  `division_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`divisionid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `pm_list`
--

DROP TABLE IF EXISTS `pm_list`;
CREATE TABLE IF NOT EXISTS `pm_list` (
  `pm_id` int(10) NOT NULL AUTO_INCREMENT,
  `pm_job` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `pm_detail` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(2) NOT NULL,
  `priority` int(3) NOT NULL,
  `q_order` int(2) NOT NULL,
  `date` date NOT NULL,
  `complete_date` date NOT NULL,
  `accountid` int(10) NOT NULL,
  `assign_to` int(5) NOT NULL,
  `eoc` tinyint(1) NOT NULL,
  `category` int(2) NOT NULL,
  `divisionid` int(2) NOT NULL,
  PRIMARY KEY (`pm_id`),
  KEY `status` (`status`,`date`,`assign_to`,`category`),
  KEY `divisionid` (`divisionid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=109 ;

-- --------------------------------------------------------

--
-- Table structure for table `pm_message`
--

DROP TABLE IF EXISTS `pm_message`;
CREATE TABLE IF NOT EXISTS `pm_message` (
  `message` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pm_status`
--

DROP TABLE IF EXISTS `pm_status`;
CREATE TABLE IF NOT EXISTS `pm_status` (
  `pm_statusid` int(3) NOT NULL AUTO_INCREMENT,
  `pm_statusname` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`pm_statusid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Table structure for table `pm_users`
--

DROP TABLE IF EXISTS `pm_users`;
CREATE TABLE IF NOT EXISTS `pm_users` (
  `userid` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `security` int(1) NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `accountid` int(5) NOT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `po_category`
--

DROP TABLE IF EXISTS `po_category`;
CREATE TABLE IF NOT EXISTS `po_category` (
  `categoryid` int(3) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(35) NOT NULL,
  `budget_type` int(2) NOT NULL,
  PRIMARY KEY (`categoryid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Table structure for table `po_note`
--

DROP TABLE IF EXISTS `po_note`;
CREATE TABLE IF NOT EXISTS `po_note` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `apinvoiceid` int(10) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=553 ;

-- --------------------------------------------------------

--
-- Table structure for table `pos_report`
--

DROP TABLE IF EXISTS `pos_report`;
CREATE TABLE IF NOT EXISTS `pos_report` (
  `report_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `on_demand` tinyint(1) NOT NULL DEFAULT '0',
  `how_often` tinyint(3) unsigned NOT NULL COMMENT 'how_often flag for on_demand reports, otherwise use how_often in the pos_report_runlist table',
  `slug` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT 'use this for programming off of',
  `class` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`report_id`),
  KEY `on_demand` (`on_demand`),
  KEY `on_demand_2` (`on_demand`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

-- --------------------------------------------------------

--
-- Table structure for table `pos_report_cache`
--

DROP TABLE IF EXISTS `pos_report_cache`;
CREATE TABLE IF NOT EXISTS `pos_report_cache` (
  `report_cache_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(10) unsigned NOT NULL,
  `report_id` tinyint(3) unsigned NOT NULL,
  `how_often` tinyint(3) unsigned NOT NULL,
  `day_of_week` enum('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday') COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_run` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `filename` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `archived` enum('Yes','No') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  PRIMARY KEY (`report_cache_id`),
  UNIQUE KEY `report_by_company` (`report_id`,`company_id`,`how_often`,`date_run`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8979 ;

-- --------------------------------------------------------

--
-- Table structure for table `pos_report_runlist`
--

DROP TABLE IF EXISTS `pos_report_runlist`;
CREATE TABLE IF NOT EXISTS `pos_report_runlist` (
  `report_runlist_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(10) unsigned NOT NULL,
  `report_id` tinyint(3) unsigned NOT NULL,
  `login_id` int(10) unsigned NOT NULL,
  `how_often` tinyint(3) unsigned NOT NULL,
  `day_of_week` enum('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday') COLLATE utf8_unicode_ci DEFAULT NULL,
  `day_of_month` int(11) DEFAULT NULL,
  PRIMARY KEY (`report_runlist_id`),
  KEY `report_id` (`report_id`,`how_often`,`day_of_week`,`day_of_month`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1001 ;

-- --------------------------------------------------------

--
-- Table structure for table `posreports_business`
--

DROP TABLE IF EXISTS `posreports_business`;
CREATE TABLE IF NOT EXISTS `posreports_business` (
  `businessid` int(10) NOT NULL AUTO_INCREMENT,
  `companyid` int(3) unsigned NOT NULL DEFAULT '1',
  `businessname` varchar(50) NOT NULL DEFAULT '<font color=red>NewUnit',
  `companyname` varchar(50) NOT NULL,
  `street` varchar(50) NOT NULL DEFAULT '',
  `city` varchar(30) NOT NULL DEFAULT '',
  `state` char(2) NOT NULL DEFAULT '',
  `zip` varchar(10) NOT NULL DEFAULT '',
  `manager` int(10) NOT NULL DEFAULT '0',
  `phone` varchar(50) NOT NULL DEFAULT '',
  `paystart` date NOT NULL DEFAULT '2000-01-01',
  `paytype` tinyint(1) NOT NULL DEFAULT '0',
  `paydays` int(2) NOT NULL DEFAULT '7',
  `location` varchar(5) NOT NULL DEFAULT '',
  `unit` int(5) NOT NULL,
  `ar_unit` varchar(3) NOT NULL,
  `tax` float NOT NULL DEFAULT '0',
  `over_short_acct` varchar(20) NOT NULL,
  `inventor_acct` varchar(20) NOT NULL,
  `inventor_goal` double NOT NULL,
  `caternum` int(3) NOT NULL DEFAULT '5',
  `caterdays` tinyint(2) NOT NULL DEFAULT '2',
  `districtid` int(3) NOT NULL DEFAULT '0',
  `catering` tinyint(1) NOT NULL DEFAULT '0',
  `srvchrg` tinyint(1) NOT NULL DEFAULT '0',
  `srvchrgpcnt` float NOT NULL DEFAULT '0',
  `srvchrg_name` varchar(50) NOT NULL DEFAULT 'SERVICE CHRG',
  `weekend` tinyint(1) NOT NULL DEFAULT '0',
  `cater_min` int(3) NOT NULL,
  `cater_spread` int(2) NOT NULL,
  `cater_costcenter` int(1) NOT NULL DEFAULT '0',
  `count1` varchar(10) NOT NULL DEFAULT 'Breakfast',
  `count2` varchar(10) NOT NULL DEFAULT 'Lunch',
  `count3` varchar(10) NOT NULL DEFAULT 'Dinner',
  `multitax` tinyint(1) NOT NULL DEFAULT '0',
  `exp_sales` tinyint(1) NOT NULL DEFAULT '0',
  `exp_trans` tinyint(1) NOT NULL DEFAULT '0',
  `exp_ar` tinyint(1) NOT NULL DEFAULT '0',
  `exp_ap` tinyint(1) NOT NULL DEFAULT '0',
  `exp_inv` tinyint(1) NOT NULL DEFAULT '0',
  `exp_payroll` tinyint(1) NOT NULL DEFAULT '0',
  `ap_export_num` int(2) NOT NULL,
  `restrict_transfer_to` tinyint(1) NOT NULL DEFAULT '0',
  `skpi` tinyint(1) NOT NULL DEFAULT '1',
  `db_restrict` tinyint(1) NOT NULL DEFAULT '0',
  `benefit` float NOT NULL DEFAULT '34',
  `closed` tinyint(1) NOT NULL DEFAULT '0',
  `closed_reason` varchar(100) NOT NULL DEFAULT '',
  `inv_note` varchar(250) NOT NULL DEFAULT '',
  `operate` int(1) NOT NULL DEFAULT '5',
  `default_menu` int(3) NOT NULL DEFAULT '1',
  `cafe_menu` int(5) NOT NULL,
  `vend_menu` int(5) NOT NULL,
  `default_menu_alias` int(5) NOT NULL,
  `cafe_menu_alias` int(5) NOT NULL,
  `vend_menu_alias` int(5) NOT NULL,
  `default_only` tinyint(1) NOT NULL DEFAULT '0',
  `static_menu` tinyint(1) NOT NULL,
  `no_nutrition` tinyint(1) NOT NULL DEFAULT '0',
  `multi_cafe_menu` tinyint(1) NOT NULL DEFAULT '0',
  `allow_sub_orders` tinyint(1) NOT NULL DEFAULT '0',
  `unit_add` tinyint(1) NOT NULL DEFAULT '0',
  `transfer` int(10) NOT NULL,
  `po_num` int(5) NOT NULL DEFAULT '1001',
  `population` int(10) NOT NULL,
  `contract` tinyint(1) NOT NULL DEFAULT '0',
  `contract_num` int(10) NOT NULL DEFAULT '1',
  `show_inv_price` tinyint(1) NOT NULL DEFAULT '0',
  `show_inv_count` tinyint(1) NOT NULL DEFAULT '6',
  `show_last_count` tinyint(1) NOT NULL DEFAULT '0',
  `show_inv_par` tinyint(1) NOT NULL DEFAULT '0',
  `use_nutrition` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'uses nutrition table instead of inv_items table',
  `count_type` tinyint(1) NOT NULL DEFAULT '0',
  `vend_start` date NOT NULL,
  `vend_end` date NOT NULL,
  `interco` tinyint(1) NOT NULL DEFAULT '0',
  `categoryid` int(3) NOT NULL,
  `labor_notify_email` varchar(100) NOT NULL,
  `po_notify_email` int(10) NOT NULL COMMENT 'not an email, loginid',
  `timezone` tinyint(2) NOT NULL,
  `acct_counter` int(5) NOT NULL DEFAULT '1000',
  `cs_amounttype` tinyint(1) NOT NULL COMMENT '0None,1%,2Fixed',
  `cs_amount` double NOT NULL,
  `cs_special` double NOT NULL,
  `petty_cash` double NOT NULL,
  `budget_type` tinyint(1) NOT NULL DEFAULT '0',
  `use_mei_sales` tinyint(1) NOT NULL,
  `auto_submit_labor` tinyint(1) NOT NULL DEFAULT '0',
  `auto_submit_sales` tinyint(1) NOT NULL DEFAULT '0',
  `over_time_type` tinyint(1) NOT NULL DEFAULT '0',
  `sort` varchar(4) NOT NULL,
  `po_prefix` varchar(2) NOT NULL,
  `po_approval` int(10) NOT NULL DEFAULT '53',
  `import_pl` tinyint(1) NOT NULL,
  `setup` tinyint(4) NOT NULL DEFAULT '0',
  `kiosk_multi_inv` tinyint(1) NOT NULL DEFAULT '0',
  `business_logo` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`businessid`),
  KEY `unit` (`unit`) USING BTREE,
  KEY `companyid` (`companyid`,`businessid`) USING BTREE,
  KEY `districtid_2` (`districtid`,`businessid`) USING BTREE,
  KEY `businessid` (`businessid`,`db_restrict`) USING BTREE,
  KEY `po_notify_email` (`po_notify_email`) USING BTREE,
  KEY `restrict_transfer_to` (`restrict_transfer_to`) USING BTREE,
  KEY `exp_sales` (`exp_sales`) USING BTREE,
  KEY `exp_ar` (`exp_ar`) USING BTREE,
  KEY `companyid_2` (`companyid`,`exp_payroll`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1247 ;

-- --------------------------------------------------------

--
-- Table structure for table `posreports_checkdetail`
--

DROP TABLE IF EXISTS `posreports_checkdetail`;
CREATE TABLE IF NOT EXISTS `posreports_checkdetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `businessid` int(11) NOT NULL,
  `bill_number` bigint(20) unsigned NOT NULL,
  `item_number` int(11) NOT NULL,
  `quantity` tinyint(1) NOT NULL,
  `price` double(10,2) NOT NULL,
  `cost` double(10,2) NOT NULL,
  `is_void` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `bill_number` (`bill_number`) USING BTREE,
  KEY `businessid` (`businessid`) USING BTREE,
  KEY `item_number` (`item_number`) USING BTREE,
  KEY `is_void` (`is_void`) USING BTREE,
  KEY `item_number2` (`item_number`,`businessid`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=127855224 ;

-- --------------------------------------------------------

--
-- Table structure for table `posreports_checks`
--

DROP TABLE IF EXISTS `posreports_checks`;
CREATE TABLE IF NOT EXISTS `posreports_checks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `businessid` int(11) NOT NULL,
  `check_number` bigint(20) unsigned NOT NULL,
  `session_number` int(11) NOT NULL,
  `table_number` int(5) NOT NULL,
  `seat_number` int(5) NOT NULL,
  `emp_no` int(11) NOT NULL,
  `people` int(5) NOT NULL,
  `bill_datetime` datetime NOT NULL,
  `bill_posted` datetime NOT NULL,
  `total` double(10,2) NOT NULL,
  `taxes` double(10,2) NOT NULL,
  `received` decimal(10,2) NOT NULL,
  `sale_type` int(5) NOT NULL,
  `account_number` bigint(20) NOT NULL,
  `terminal_group` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `is_void` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `check_number` (`check_number`) USING BTREE,
  KEY `bill_datetime` (`bill_datetime`) USING BTREE,
  KEY `is_void` (`is_void`) USING BTREE,
  KEY `total` (`total`) USING BTREE,
  KEY `account_number` (`account_number`) USING BTREE,
  KEY `businessid_2` (`businessid`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18121818 ;

-- --------------------------------------------------------

--
-- Table structure for table `posreports_company`
--

DROP TABLE IF EXISTS `posreports_company`;
CREATE TABLE IF NOT EXISTS `posreports_company` (
  `companyid` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `company_type` int(3) NOT NULL,
  `companyname` varchar(50) NOT NULL DEFAULT '',
  `reference` varchar(40) NOT NULL,
  `street` varchar(40) NOT NULL,
  `city` varchar(30) NOT NULL DEFAULT '',
  `state` char(2) NOT NULL DEFAULT '',
  `zip` varchar(10) NOT NULL DEFAULT '',
  `phone` varchar(25) NOT NULL DEFAULT '',
  `code` varchar(5) NOT NULL,
  `exportby` varchar(30) NOT NULL DEFAULT '',
  `exportdate` date NOT NULL DEFAULT '0000-00-00',
  `last_export` varchar(2) NOT NULL,
  `import_num` int(2) NOT NULL DEFAULT '0',
  `import_num2` int(2) NOT NULL DEFAULT '0',
  `reply` int(10) NOT NULL,
  `rec_accesscode` varchar(20) NOT NULL,
  `exporting` tinyint(1) NOT NULL DEFAULT '0',
  `week_start` varchar(10) NOT NULL DEFAULT 'Friday',
  `week_end` varchar(10) NOT NULL DEFAULT 'Thursday',
  `payroll_date` date NOT NULL,
  `ar_update` date NOT NULL,
  `base_pay` double NOT NULL DEFAULT '0',
  `holiday_pay` double NOT NULL DEFAULT '0',
  `invoice_name` varchar(50) NOT NULL DEFAULT 'Treat America Dining',
  `invoice_remit` varchar(50) NOT NULL DEFAULT 'Treat America Food Services',
  `logo` varchar(50) NOT NULL,
  `backdrop` varchar(50) NOT NULL,
  `business_hierarchy` int(11) DEFAULT NULL,
  `ul_budget` tinyint(1) NOT NULL DEFAULT '0',
  `dl_budget` tinyint(1) NOT NULL DEFAULT '0',
  `ul_estimate` tinyint(1) NOT NULL DEFAULT '0',
  `dl_estimate` tinyint(1) NOT NULL DEFAULT '0',
  `master_vendor_id` int(10) NOT NULL,
  `company_code` int(10) NOT NULL,
  `client_program_id` int(10) NOT NULL,
  PRIMARY KEY (`companyid`),
  KEY `companyid` (`companyid`,`company_type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=72 ;

-- --------------------------------------------------------

--
-- Table structure for table `posreports_district`
--

DROP TABLE IF EXISTS `posreports_district`;
CREATE TABLE IF NOT EXISTS `posreports_district` (
  `districtid` int(3) NOT NULL AUTO_INCREMENT,
  `companyid` int(3) NOT NULL,
  `divisionid` int(3) NOT NULL,
  `districtname` varchar(30) NOT NULL DEFAULT '',
  `arrequest` int(5) NOT NULL DEFAULT '0',
  `aprequest` int(5) NOT NULL DEFAULT '0',
  `labor_request` varchar(100) NOT NULL,
  `dm` int(10) NOT NULL DEFAULT '0',
  `submit` tinyint(1) NOT NULL DEFAULT '0',
  `email_submit` varchar(200) NOT NULL,
  `labor_submit` tinyint(1) NOT NULL DEFAULT '0',
  `labor_email` varchar(100) NOT NULL,
  `bad_ar` tinyint(1) NOT NULL,
  `bad_ar_email` varchar(100) NOT NULL,
  `email_orders` varchar(255) NOT NULL,
  `production` int(5) NOT NULL,
  `deactivate_items` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`districtid`),
  KEY `districtid` (`districtid`,`companyid`,`divisionid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=270 ;

-- --------------------------------------------------------

--
-- Table structure for table `posreports_inv_count_vend`
--

DROP TABLE IF EXISTS `posreports_inv_count_vend`;
CREATE TABLE IF NOT EXISTS `posreports_inv_count_vend` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `machine_num` varchar(10) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `item_code` varchar(15) NOT NULL,
  `onhand` double NOT NULL,
  `unit_cost` double NOT NULL,
  `pulled` tinyint(1) NOT NULL,
  `fills` double NOT NULL,
  `waste` double NOT NULL,
  `is_inv` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `machine_num` (`machine_num`) USING BTREE,
  KEY `item_code` (`item_code`) USING BTREE,
  KEY `is_inv` (`is_inv`) USING BTREE,
  KEY `date_time` (`date_time`) USING BTREE,
  KEY `item_code2` (`item_code`,`is_inv`,`date_time`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5483161 ;

-- --------------------------------------------------------

--
-- Table structure for table `posreports_machine_bus_link`
--

DROP TABLE IF EXISTS `posreports_machine_bus_link`;
CREATE TABLE IF NOT EXISTS `posreports_machine_bus_link` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL,
  `machine_num` varchar(30) NOT NULL,
  `ordertype` int(3) NOT NULL,
  `creditid` int(10) NOT NULL DEFAULT '0',
  `client_programid` int(10) NOT NULL DEFAULT '0',
  `leadtime` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `businessid` (`businessid`) USING BTREE,
  KEY `machine_num` (`machine_num`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1961 ;

-- --------------------------------------------------------

--
-- Table structure for table `posreports_menu_items_new`
--

DROP TABLE IF EXISTS `posreports_menu_items_new`;
CREATE TABLE IF NOT EXISTS `posreports_menu_items_new` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `businessid` int(11) NOT NULL,
  `pos_processed` tinyint(1) NOT NULL DEFAULT '0',
  `pos_id` int(10) unsigned NOT NULL,
  `group_id` int(11) unsigned DEFAULT NULL,
  `manufacturer` int(5) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ordertype` int(4) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `mic_id` int(10) unsigned NOT NULL COMMENT 'id from menu_item_company',
  `item_code` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `upc` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `onhand` double NOT NULL,
  `reorder` double NOT NULL,
  `max` double NOT NULL,
  `reorder_point` double NOT NULL,
  `reorder_amount` double NOT NULL,
  `shelf_life_days` int(11) NOT NULL,
  `is_button` tinyint(1) NOT NULL DEFAULT '0',
  `rollover` float NOT NULL DEFAULT '0',
  `sale_unit` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unit',
  `tare` decimal(7,5) NOT NULL DEFAULT '0.00000',
  `cond_group` int(10) unsigned DEFAULT NULL,
  `force_condiment` tinyint(1) NOT NULL DEFAULT '0',
  `requires_activation` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `pos_id` (`pos_id`) USING BTREE,
  KEY `businessid` (`businessid`) USING BTREE,
  KEY `item_code` (`item_code`) USING BTREE,
  KEY `active` (`active`) USING BTREE,
  KEY `ordertype` (`ordertype`) USING BTREE,
  KEY `group_id` (`group_id`) USING BTREE,
  KEY `pos_processed` (`pos_processed`) USING BTREE,
  KEY `cond_group` (`cond_group`) USING BTREE,
  KEY `upc` (`upc`) USING BTREE,
  KEY `requires_activation` (`requires_activation`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1451569 ;

-- --------------------------------------------------------

--
-- Table structure for table `posreports_menu_items_price`
--

DROP TABLE IF EXISTS `posreports_menu_items_price`;
CREATE TABLE IF NOT EXISTS `posreports_menu_items_price` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `businessid` int(11) unsigned NOT NULL,
  `menu_item_id` int(11) unsigned NOT NULL,
  `pos_price_id` tinyint(2) unsigned NOT NULL,
  `price` decimal(5,2) NOT NULL,
  `new_price` decimal(5,2) DEFAULT NULL,
  `cost` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `menu_item_id` (`menu_item_id`) USING BTREE,
  KEY `pos_price_id` (`pos_price_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1570176 ;

-- --------------------------------------------------------

--
-- Table structure for table `posreports_payment_detail`
--

DROP TABLE IF EXISTS `posreports_payment_detail`;
CREATE TABLE IF NOT EXISTS `posreports_payment_detail` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `businessid` int(11) NOT NULL,
  `check_number` bigint(20) unsigned NOT NULL,
  `payment_type` int(11) NOT NULL,
  `received` double(6,2) NOT NULL,
  `base_amount` double(6,2) NOT NULL,
  `tip_amount` double(6,2) NOT NULL,
  `scancode` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `check_number` (`check_number`) USING BTREE,
  KEY `scancode` (`scancode`) USING BTREE,
  KEY `businessid` (`businessid`,`check_number`,`payment_type`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=102531864 ;

-- --------------------------------------------------------

--
-- Table structure for table `price_history`
--

DROP TABLE IF EXISTS `price_history`;
CREATE TABLE IF NOT EXISTS `price_history` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `menu_itemid` int(10) NOT NULL,
  `date` date NOT NULL,
  `price` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `menu_itemid` (`menu_itemid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=81004 ;

-- --------------------------------------------------------

--
-- Table structure for table `promo_detail`
--

DROP TABLE IF EXISTS `promo_detail`;
CREATE TABLE IF NOT EXISTS `promo_detail` (
  `promo_id` int(10) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `id` int(10) NOT NULL,
  PRIMARY KEY (`promo_id`,`type`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `promo_groups`
--

DROP TABLE IF EXISTS `promo_groups`;
CREATE TABLE IF NOT EXISTS `promo_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `businessid` int(11) unsigned DEFAULT NULL,
  `uuid` char(36) NOT NULL DEFAULT '',
  `name` varchar(128) NOT NULL DEFAULT '',
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `routing` tinyint(1) NOT NULL DEFAULT '1',
  `display_order` int(11) NOT NULL DEFAULT '99999',
  `display_y` int(11) DEFAULT NULL,
  `display_x` int(11) DEFAULT NULL,
  `pos_color` smallint(6) NOT NULL DEFAULT '0',
  `group_rows` tinyint(4) DEFAULT NULL,
  `group_cols` tinyint(4) DEFAULT NULL,
  `display_on` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  KEY `businessid` (`businessid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2788 ;

-- --------------------------------------------------------

--
-- Table structure for table `promo_groups_detail`
--

DROP TABLE IF EXISTS `promo_groups_detail`;
CREATE TABLE IF NOT EXISTS `promo_groups_detail` (
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'corresponds to menu_items_new.id',
  `promo_group_id` int(10) unsigned NOT NULL COMMENT 'promo_groups.id',
  `master_product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'menu_items_master.Id',
  `display_order` int(10) unsigned NOT NULL DEFAULT '99999',
  `display_y` int(11) DEFAULT NULL,
  `display_x` int(11) DEFAULT NULL,
  `pos_color` smallint(5) unsigned NOT NULL DEFAULT '0',
  `processed` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`product_id`,`promo_group_id`,`master_product_id`),
  KEY `promo_group_id` (`promo_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `promo_groups_mapping`
--

DROP TABLE IF EXISTS `promo_groups_mapping`;
CREATE TABLE IF NOT EXISTS `promo_groups_mapping` (
  `PromoGroupId` int(10) unsigned NOT NULL,
  `BusinessId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`PromoGroupId`,`BusinessId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `promo_mapping`
--

DROP TABLE IF EXISTS `promo_mapping`;
CREATE TABLE IF NOT EXISTS `promo_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `PromotionId` int(10) NOT NULL,
  `BusinessId` int(10) NOT NULL,
  `Utilized` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Nonzero if this promotion has been used at least once from a client machine with this business id',
  `UtilizedChecked` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `PromotionId` (`PromotionId`,`BusinessId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Keep track of the businesses that a (global) promotion appli' AUTO_INCREMENT=3647 ;

-- --------------------------------------------------------

--
-- Table structure for table `promo_mapping_temp`
--

DROP TABLE IF EXISTS `promo_mapping_temp`;
CREATE TABLE IF NOT EXISTS `promo_mapping_temp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `PromotionId` int(10) NOT NULL,
  `BusinessId` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `PromotionId` (`PromotionId`,`BusinessId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Keep track of the businesses that a (global) promotion appli' AUTO_INCREMENT=1019 ;

-- --------------------------------------------------------

--
-- Table structure for table `promo_target`
--

DROP TABLE IF EXISTS `promo_target`;
CREATE TABLE IF NOT EXISTS `promo_target` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `db_table` varchar(50) NOT NULL,
  `order_by` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `promo_trigger`
--

DROP TABLE IF EXISTS `promo_trigger`;
CREATE TABLE IF NOT EXISTS `promo_trigger` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `vivipos_name` varchar(32) NOT NULL DEFAULT '',
  `target_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `promo_type`
--

DROP TABLE IF EXISTS `promo_type`;
CREATE TABLE IF NOT EXISTS `promo_type` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(35) NOT NULL,
  `vivipos_name` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `promotions`
--

DROP TABLE IF EXISTS `promotions`;
CREATE TABLE IF NOT EXISTS `promotions` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(11) unsigned DEFAULT NULL,
  `pos_id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `type` int(3) NOT NULL,
  `target` int(3) NOT NULL,
  `promo_trigger` int(3) NOT NULL,
  `value` double NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `start_time` varchar(5) NOT NULL,
  `end_time` varchar(5) NOT NULL,
  `monday` tinyint(1) NOT NULL,
  `tuesday` tinyint(1) NOT NULL,
  `wednesday` tinyint(1) NOT NULL,
  `thursday` tinyint(1) NOT NULL,
  `friday` tinyint(1) NOT NULL,
  `saturday` tinyint(1) NOT NULL,
  `sunday` tinyint(1) NOT NULL,
  `activeFlag` tinyint(1) NOT NULL DEFAULT '1',
  `promo_discount` varchar(24) DEFAULT NULL,
  `promo_discount_type` varchar(24) DEFAULT NULL,
  `promo_discount_n` smallint(6) DEFAULT NULL,
  `promo_discount_limit` smallint(6) DEFAULT NULL,
  `promo_trigger_amount_limit` varchar(16) NOT NULL DEFAULT 'multiple',
  `promo_trigger_amount_type` varchar(24) DEFAULT NULL,
  `promo_trigger_amount` decimal(8,2) DEFAULT NULL,
  `rule_order` smallint(6) DEFAULT NULL,
  `pos_processed` tinyint(1) NOT NULL DEFAULT '0',
  `tax_id` int(11) DEFAULT NULL,
  `promo_trigger_amount_2` smallint(6) DEFAULT NULL,
  `promo_trigger_amount_3` smallint(6) DEFAULT NULL,
  `promo_reserve` tinyint(1) NOT NULL DEFAULT '0',
  `subsidy` tinyint(1) NOT NULL DEFAULT '0',
  `reporting_category_id` int(11) DEFAULT NULL COMMENT 'references reporting_category',
  PRIMARY KEY (`id`),
  KEY `businessid` (`businessid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1467 ;

-- --------------------------------------------------------

--
-- Table structure for table `pub_db_custom`
--

DROP TABLE IF EXISTS `pub_db_custom`;
CREATE TABLE IF NOT EXISTS `pub_db_custom` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `viewid` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `custom_field` varchar(50) NOT NULL,
  `future_dates` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `viewid` (`viewid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=235 ;

-- --------------------------------------------------------

--
-- Table structure for table `pub_db_trendlines`
--

DROP TABLE IF EXISTS `pub_db_trendlines`;
CREATE TABLE IF NOT EXISTS `pub_db_trendlines` (
  `trendid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `viewid` int(10) unsigned NOT NULL,
  `lineStart` int(11) NOT NULL DEFAULT '0',
  `lineEnd` int(11) NOT NULL DEFAULT '0',
  `trendCaption` varchar(128) DEFAULT '',
  `lineColor` char(6) DEFAULT 'ff0000',
  `lineWidth` int(10) unsigned NOT NULL DEFAULT '1',
  `linePosition` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`trendid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `pub_db_view`
--

DROP TABLE IF EXISTS `pub_db_view`;
CREATE TABLE IF NOT EXISTS `pub_db_view` (
  `viewid` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `loginid` int(10) NOT NULL,
  `graph_type` int(3) NOT NULL,
  `size` tinyint(1) NOT NULL DEFAULT '1',
  `db_height` smallint(6) DEFAULT '250',
  `xaxis` tinyint(4) DEFAULT '0',
  `db_order` int(2) NOT NULL,
  `period` tinyint(1) NOT NULL,
  `period_num` tinyint(1) NOT NULL,
  `period_group` tinyint(1) NOT NULL,
  `current` tinyint(1) NOT NULL,
  `week_ends` varchar(8) NOT NULL DEFAULT 'Thursday',
  `db_mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`viewid`),
  KEY `loginid` (`loginid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=288 ;

-- --------------------------------------------------------

--
-- Table structure for table `pub_db_what`
--

DROP TABLE IF EXISTS `pub_db_what`;
CREATE TABLE IF NOT EXISTS `pub_db_what` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `viewid` int(10) NOT NULL,
  `type` int(2) NOT NULL,
  `what` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `viewid` (`viewid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=647 ;

-- --------------------------------------------------------

--
-- Table structure for table `pub_db_who`
--

DROP TABLE IF EXISTS `pub_db_who`;
CREATE TABLE IF NOT EXISTS `pub_db_who` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `viewid` int(10) NOT NULL,
  `type` int(2) NOT NULL,
  `viewtype` int(3) NOT NULL,
  `viewbus` int(10) NOT NULL,
  `viewbus2` int(5) NOT NULL,
  `expand` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `viewid` (`viewid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=802 ;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_card`
--

DROP TABLE IF EXISTS `purchase_card`;
CREATE TABLE IF NOT EXISTS `purchase_card` (
  `purchase_cardid` int(10) NOT NULL AUTO_INCREMENT,
  `companyid` int(3) NOT NULL,
  `businessid` int(10) NOT NULL,
  `loginid` int(10) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `vendorid` int(10) NOT NULL,
  `approve_loginid` int(5) NOT NULL,
  `cash_only` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `del_date` date NOT NULL,
  PRIMARY KEY (`purchase_cardid`),
  KEY `businessid` (`businessid`,`loginid`,`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=631 ;

-- --------------------------------------------------------

--
-- Table structure for table `recipe`
--

DROP TABLE IF EXISTS `recipe`;
CREATE TABLE IF NOT EXISTS `recipe` (
  `recipeid` int(10) NOT NULL AUTO_INCREMENT,
  `menu_itemid` int(10) NOT NULL,
  `inv_itemid` int(10) NOT NULL,
  `rec_num` float NOT NULL,
  `rec_size` int(5) NOT NULL,
  `srv_num` double NOT NULL,
  `rec_order` varchar(1) NOT NULL DEFAULT '1',
  `sort` int(5) NOT NULL DEFAULT '1',
  PRIMARY KEY (`recipeid`),
  KEY `menu_itemid` (`menu_itemid`,`inv_itemid`,`sort`),
  KEY `menu_itemid_2` (`menu_itemid`),
  KEY `inv_itemid` (`inv_itemid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1085944 ;

-- --------------------------------------------------------

--
-- Table structure for table `recipe_comment`
--

DROP TABLE IF EXISTS `recipe_comment`;
CREATE TABLE IF NOT EXISTS `recipe_comment` (
  `recipe_commentid` int(10) NOT NULL AUTO_INCREMENT,
  `user` varchar(25) NOT NULL,
  `date` date NOT NULL,
  `menu_itemid` int(10) NOT NULL,
  `comment` text NOT NULL,
  PRIMARY KEY (`recipe_commentid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

-- --------------------------------------------------------

--
-- Table structure for table `recipe_prep`
--

DROP TABLE IF EXISTS `recipe_prep`;
CREATE TABLE IF NOT EXISTS `recipe_prep` (
  `recipe_prepid` int(10) NOT NULL AUTO_INCREMENT,
  `menu_itemid` int(10) NOT NULL,
  `user` varchar(25) NOT NULL,
  `rating` int(1) NOT NULL,
  PRIMARY KEY (`recipe_prepid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

-- --------------------------------------------------------

--
-- Table structure for table `recipe_rating`
--

DROP TABLE IF EXISTS `recipe_rating`;
CREATE TABLE IF NOT EXISTS `recipe_rating` (
  `recipe_ratingid` int(10) NOT NULL AUTO_INCREMENT,
  `user` varchar(25) NOT NULL,
  `menu_itemid` int(10) NOT NULL,
  `rating` int(1) NOT NULL,
  PRIMARY KEY (`recipe_ratingid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

-- --------------------------------------------------------

--
-- Table structure for table `reconcile_user`
--

DROP TABLE IF EXISTS `reconcile_user`;
CREATE TABLE IF NOT EXISTS `reconcile_user` (
  `recid` int(10) NOT NULL AUTO_INCREMENT,
  `loginid` int(10) NOT NULL,
  `user` varchar(30) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `account` varchar(25) NOT NULL,
  `totusers` int(2) NOT NULL,
  PRIMARY KEY (`recid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1552 ;

-- --------------------------------------------------------

--
-- Table structure for table `recur`
--

DROP TABLE IF EXISTS `recur`;
CREATE TABLE IF NOT EXISTS `recur` (
  `recurid` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL,
  `companyid` int(3) NOT NULL,
  `year` int(4) NOT NULL,
  `apaccountid` int(10) NOT NULL,
  `acct_num` varchar(20) NOT NULL,
  `01` double NOT NULL,
  `02` double NOT NULL,
  `03` double NOT NULL,
  `04` double NOT NULL,
  `05` double NOT NULL,
  `06` double NOT NULL,
  `07` double NOT NULL,
  `08` double NOT NULL,
  `09` double NOT NULL,
  `10` double NOT NULL,
  `11` double NOT NULL,
  `12` double NOT NULL,
  PRIMARY KEY (`recurid`),
  KEY `businessid` (`businessid`,`apaccountid`,`year`),
  KEY `year` (`year`,`apaccountid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4249 ;

-- --------------------------------------------------------

--
-- Table structure for table `reportdetail`
--

DROP TABLE IF EXISTS `reportdetail`;
CREATE TABLE IF NOT EXISTS `reportdetail` (
  `reportdetailid` int(10) NOT NULL AUTO_INCREMENT,
  `reportid` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `type` int(2) NOT NULL,
  `query` varchar(100) NOT NULL,
  `orderid` int(4) NOT NULL,
  `avg` tinyint(1) NOT NULL DEFAULT '0',
  `bold` tinyint(1) NOT NULL DEFAULT '0',
  `g_color` varchar(12) NOT NULL,
  `graph_it` varchar(1) NOT NULL DEFAULT '0',
  `on_pie` varchar(1) NOT NULL DEFAULT '0',
  `hide` tinyint(1) NOT NULL,
  `num_decimal` tinyint(1) NOT NULL DEFAULT '2',
  `suffix` varchar(2) NOT NULL,
  `wtdb` tinyint(1) NOT NULL DEFAULT '0',
  `table` varchar(20) NOT NULL,
  `field` varchar(20) NOT NULL,
  PRIMARY KEY (`reportdetailid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=330 ;

-- --------------------------------------------------------

--
-- Table structure for table `reporting_category`
--

DROP TABLE IF EXISTS `reporting_category`;
CREATE TABLE IF NOT EXISTS `reporting_category` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `category` varchar(50) NOT NULL,
  `type` varchar(10) NOT NULL DEFAULT 'debits',
  `type_key` varchar(50) NOT NULL,
  `type_detail` varchar(50) NOT NULL,
  `type_detail_key` varchar(50) NOT NULL,
  `orderid` int(5) NOT NULL,
  `promo_mapping` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

DROP TABLE IF EXISTS `reports`;
CREATE TABLE IF NOT EXISTS `reports` (
  `reportid` int(10) NOT NULL AUTO_INCREMENT,
  `report_name` varchar(30) NOT NULL,
  `companyid` int(3) NOT NULL,
  `loginid` int(10) NOT NULL DEFAULT '0',
  `seperate` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`reportid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

-- --------------------------------------------------------

--
-- Table structure for table `reserve`
--

DROP TABLE IF EXISTS `reserve`;
CREATE TABLE IF NOT EXISTS `reserve` (
  `reserveid` int(10) NOT NULL AUTO_INCREMENT,
  `parent` int(10) NOT NULL,
  `accountid` int(10) NOT NULL DEFAULT '0',
  `businessid` int(10) NOT NULL DEFAULT '0',
  `companyid` int(3) NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `start_hour` int(2) NOT NULL DEFAULT '0',
  `end_hour` int(2) NOT NULL DEFAULT '0',
  `peoplenum` int(6) NOT NULL DEFAULT '0',
  `roomid` int(10) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `roomcomment` text NOT NULL,
  `comment` varchar(200) NOT NULL,
  `taxed` char(3) NOT NULL DEFAULT 'on',
  `service` int(2) NOT NULL,
  `costcenter` varchar(30) NOT NULL,
  `curmenu` int(3) NOT NULL,
  `customid` varchar(250) NOT NULL,
  PRIMARY KEY (`reserveid`),
  KEY `accountid` (`accountid`,`businessid`,`companyid`,`date`,`curmenu`),
  KEY `businessid` (`businessid`),
  KEY `status` (`status`),
  KEY `date` (`date`),
  KEY `parent` (`parent`,`accountid`,`curmenu`,`date`),
  KEY `customid` (`customid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=261756 ;

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

DROP TABLE IF EXISTS `rooms`;
CREATE TABLE IF NOT EXISTS `rooms` (
  `roomid` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL DEFAULT '0',
  `companyid` int(5) NOT NULL DEFAULT '0',
  `buildingnum` varchar(10) NOT NULL DEFAULT '',
  `floornum` varchar(10) NOT NULL DEFAULT '',
  `roomnum` varchar(10) NOT NULL DEFAULT '',
  `roomname` varchar(50) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`roomid`),
  KEY `businessid` (`businessid`,`deleted`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=253534 ;

-- --------------------------------------------------------

--
-- Table structure for table `route_inv`
--

DROP TABLE IF EXISTS `route_inv`;
CREATE TABLE IF NOT EXISTS `route_inv` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `login_routeid` int(10) NOT NULL,
  `menu_itemid` int(10) NOT NULL,
  `count` double NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_routeid` (`login_routeid`,`menu_itemid`,`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=230 ;

-- --------------------------------------------------------

--
-- Table structure for table `route_parlevel`
--

DROP TABLE IF EXISTS `route_parlevel`;
CREATE TABLE IF NOT EXISTS `route_parlevel` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `login_routeid` int(10) NOT NULL,
  `menu_itemid` int(10) NOT NULL,
  `par_level` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_routeid` (`login_routeid`,`menu_itemid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

-- --------------------------------------------------------

--
-- Table structure for table `sales_calc`
--

DROP TABLE IF EXISTS `sales_calc`;
CREATE TABLE IF NOT EXISTS `sales_calc` (
  `calc_id` int(10) NOT NULL AUTO_INCREMENT,
  `creditid` int(10) NOT NULL,
  `formula` varchar(30) NOT NULL,
  PRIMARY KEY (`calc_id`),
  KEY `creditid` (`creditid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Table structure for table `sales_calc_detail`
--

DROP TABLE IF EXISTS `sales_calc_detail`;
CREATE TABLE IF NOT EXISTS `sales_calc_detail` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `calc_id` int(10) NOT NULL,
  `formula` tinyint(1) NOT NULL,
  `number` double NOT NULL,
  `glid` int(10) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `update` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

-- --------------------------------------------------------

--
-- Table structure for table `security`
--

DROP TABLE IF EXISTS `security`;
CREATE TABLE IF NOT EXISTS `security` (
  `securityid` int(5) NOT NULL AUTO_INCREMENT,
  `companyid` int(3) NOT NULL,
  `security_name` varchar(50) NOT NULL,
  `custom` tinyint(1) NOT NULL DEFAULT '0',
  `multi_company` int(1) NOT NULL,
  `com_setup` tinyint(1) NOT NULL,
  `user` tinyint(1) NOT NULL,
  `securities` tinyint(1) NOT NULL,
  `routes` tinyint(1) NOT NULL,
  `invoice` tinyint(1) NOT NULL,
  `payable` tinyint(1) NOT NULL,
  `item_list` tinyint(1) NOT NULL,
  `nutrition` tinyint(1) NOT NULL DEFAULT '0',
  `request` tinyint(1) NOT NULL,
  `transfer` tinyint(1) NOT NULL DEFAULT '0',
  `export` tinyint(1) NOT NULL,
  `utility` tinyint(1) NOT NULL,
  `kpi` tinyint(1) NOT NULL,
  `kpi_graph` tinyint(1) NOT NULL,
  `labor` tinyint(1) NOT NULL,
  `reconcile` tinyint(1) NOT NULL,
  `emp_labor` tinyint(1) NOT NULL DEFAULT '0',
  `project_manager` tinyint(4) NOT NULL,
  `budget_calc` tinyint(1) NOT NULL DEFAULT '0',
  `rebate` tinyint(1) NOT NULL DEFAULT '0',
  `reporting` tinyint(1) NOT NULL DEFAULT '1',
  `calendar` tinyint(1) NOT NULL DEFAULT '1',
  `faq` tinyint(1) NOT NULL DEFAULT '1',
  `comrecipe` tinyint(1) NOT NULL DEFAULT '1',
  `comdirectory` tinyint(1) NOT NULL DEFAULT '1',
  `messages` tinyint(1) NOT NULL DEFAULT '1',
  `dashboard` tinyint(1) NOT NULL DEFAULT '1',
  `setup_mgr` tinyint(1) NOT NULL DEFAULT '0',
  `customer_utility` tinyint(1) NOT NULL DEFAULT '0',
  `survey` tinyint(1) NOT NULL DEFAULT '0',
  `bus_setup` tinyint(1) NOT NULL,
  `bus_sales` tinyint(1) NOT NULL,
  `bus_invoice` tinyint(1) NOT NULL,
  `bus_order` tinyint(1) NOT NULL,
  `bus_payable` tinyint(1) NOT NULL,
  `bus_payable2` tinyint(1) NOT NULL DEFAULT '0',
  `bus_payable3` tinyint(1) NOT NULL DEFAULT '0',
  `bus_inventor1` tinyint(1) NOT NULL,
  `bus_inventor2` tinyint(1) NOT NULL,
  `bus_inventor3` tinyint(1) NOT NULL,
  `bus_inventor4` tinyint(1) NOT NULL,
  `bus_inventor5` tinyint(1) NOT NULL,
  `bus_control` tinyint(1) NOT NULL,
  `bus_menu` tinyint(1) NOT NULL,
  `menu_nutrition` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `bus_menu_linking` tinyint(1) NOT NULL,
  `bus_menu_pos` tinyint(1) NOT NULL,
  `bus_menu_pos_mgmt` tinyint(1) NOT NULL DEFAULT '0',
  `bus_menu_pos_report` tinyint(1) NOT NULL,
  `bus_order_setup` tinyint(1) NOT NULL,
  `bus_request` tinyint(1) NOT NULL,
  `route_collection` tinyint(1) NOT NULL,
  `route_labor` tinyint(1) NOT NULL,
  `route_detail` float NOT NULL DEFAULT '0',
  `route_machine` tinyint(1) NOT NULL DEFAULT '0',
  `bus_labor` float NOT NULL DEFAULT '0',
  `bus_timeclock` tinyint(1) NOT NULL DEFAULT '0',
  `bus_budget` tinyint(1) NOT NULL DEFAULT '0',
  `bus_promo_manager` tinyint(1) NOT NULL DEFAULT '0',
  `bus_kiosk_route_mgmt` tinyint(1) NOT NULL DEFAULT '0',
  `bus_kiosk_menu_mgmt` tinyint(1) NOT NULL DEFAULT '0',
  `pos_settings` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`securityid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=175 ;

-- --------------------------------------------------------

--
-- Table structure for table `security_control`
--

DROP TABLE IF EXISTS `security_control`;
CREATE TABLE IF NOT EXISTS `security_control` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `loginid` int(10) NOT NULL,
  `businessid` int(10) NOT NULL,
  `startdate` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `loginid` (`loginid`,`businessid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

-- --------------------------------------------------------

--
-- Table structure for table `security_departments`
--

DROP TABLE IF EXISTS `security_departments`;
CREATE TABLE IF NOT EXISTS `security_departments` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `securityid` int(5) NOT NULL,
  `dept_id` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `securityid` (`securityid`,`dept_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=628 ;

-- --------------------------------------------------------

--
-- Table structure for table `security_labor`
--

DROP TABLE IF EXISTS `security_labor`;
CREATE TABLE IF NOT EXISTS `security_labor` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `loginid` int(10) NOT NULL,
  `businessid` int(10) NOT NULL,
  `startdate` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `loginid` (`loginid`,`businessid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

-- --------------------------------------------------------

--
-- Table structure for table `servery_station`
--

DROP TABLE IF EXISTS `servery_station`;
CREATE TABLE IF NOT EXISTS `servery_station` (
  `stationid` int(5) NOT NULL AUTO_INCREMENT,
  `station_name` varchar(30) NOT NULL,
  `orderid` int(2) NOT NULL,
  PRIMARY KEY (`stationid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

-- --------------------------------------------------------

--
-- Table structure for table `setup`
--

DROP TABLE IF EXISTS `setup`;
CREATE TABLE IF NOT EXISTS `setup` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `groupid` int(3) NOT NULL,
  `orderid` int(3) NOT NULL,
  `mapping` varchar(200) NOT NULL,
  `map_from` varchar(200) NOT NULL,
  `where_query` varchar(254) NOT NULL,
  `type` tinyint(1) NOT NULL COMMENT '1:varchar,2:text,3:date,4:dropdown',
  `link` varchar(20) NOT NULL,
  `setup_status` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=73 ;

-- --------------------------------------------------------

--
-- Table structure for table `setup_detail`
--

DROP TABLE IF EXISTS `setup_detail`;
CREATE TABLE IF NOT EXISTS `setup_detail` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `setupid` int(4) NOT NULL,
  `businessid` int(10) NOT NULL,
  `value` varchar(254) NOT NULL,
  `complete` tinyint(1) NOT NULL DEFAULT '0',
  `editby` varchar(25) NOT NULL,
  `edittime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `edit_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `setupid` (`setupid`,`businessid`),
  KEY `businessid` (`businessid`,`complete`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=48640 ;

-- --------------------------------------------------------

--
-- Table structure for table `setup_groups`
--

DROP TABLE IF EXISTS `setup_groups`;
CREATE TABLE IF NOT EXISTS `setup_groups` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(30) NOT NULL,
  `order` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `size`
--

DROP TABLE IF EXISTS `size`;
CREATE TABLE IF NOT EXISTS `size` (
  `sizeid` int(3) NOT NULL AUTO_INCREMENT,
  `sizename` varchar(10) NOT NULL,
  PRIMARY KEY (`sizeid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

-- --------------------------------------------------------

--
-- Table structure for table `smartMenuAuth`
--

DROP TABLE IF EXISTS `smartMenuAuth`;
CREATE TABLE IF NOT EXISTS `smartMenuAuth` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `loginKey` char(30) DEFAULT NULL,
  `loginSecret` char(30) DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `loginid` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `loginKey_index` (`loginKey`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Table structure for table `smartMenuBoards`
--

DROP TABLE IF EXISTS `smartMenuBoards`;
CREATE TABLE IF NOT EXISTS `smartMenuBoards` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `boardGuid` char(32) NOT NULL,
  `boardName` varchar(64) NOT NULL DEFAULT 'board',
  `locationGuid` char(32) NOT NULL,
  `authId` int(10) unsigned NOT NULL,
  `currentPackage` int(10) unsigned DEFAULT NULL,
  `screenResolution` float unsigned NOT NULL,
  `activeFlag` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `boardGuid_index` (`boardGuid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `smartMenuBoardsLibraries`
--

DROP TABLE IF EXISTS `smartMenuBoardsLibraries`;
CREATE TABLE IF NOT EXISTS `smartMenuBoardsLibraries` (
  `BoardId` int(11) NOT NULL,
  `AssemblyName` varchar(45) NOT NULL,
  `AssemblyVersion` varchar(20) NOT NULL,
  `AssemblyFileVersion` varchar(45) NOT NULL,
  PRIMARY KEY (`BoardId`,`AssemblyName`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `smartMenuBoardsScreens`
--

DROP TABLE IF EXISTS `smartMenuBoardsScreens`;
CREATE TABLE IF NOT EXISTS `smartMenuBoardsScreens` (
  `BoardId` int(11) NOT NULL,
  `Name` varchar(45) NOT NULL,
  `Width` int(11) NOT NULL,
  `Height` int(11) NOT NULL,
  `OffsetX` int(11) NOT NULL DEFAULT '0',
  `OffsetY` int(11) NOT NULL DEFAULT '0',
  `isPrimary` tinyint(1) NOT NULL DEFAULT '0',
  KEY `BoardId` (`BoardId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `smartMenuClients`
--

DROP TABLE IF EXISTS `smartMenuClients`;
CREATE TABLE IF NOT EXISTS `smartMenuClients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `businessid` int(10) unsigned NOT NULL,
  `clientName` varchar(64) NOT NULL DEFAULT 'client',
  `authId` int(10) unsigned NOT NULL,
  `activeFlag` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `businessid_clientName_index` (`businessid`,`clientName`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `smartMenuControl`
--

DROP TABLE IF EXISTS `smartMenuControl`;
CREATE TABLE IF NOT EXISTS `smartMenuControl` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `targetGuid` char(32) NOT NULL,
  `authority` varchar(64) NOT NULL,
  `command` varchar(32) NOT NULL,
  `argumentList` varchar(512) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `targetGuid_index` (`targetGuid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `smartMenuFiles`
--

DROP TABLE IF EXISTS `smartMenuFiles`;
CREATE TABLE IF NOT EXISTS `smartMenuFiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dataFile` char(32) NOT NULL,
  `packageId` int(10) unsigned NOT NULL,
  `fileName` varchar(128) NOT NULL,
  `createdTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `createdBy` int(10) unsigned NOT NULL,
  `modifiedBy` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dataFile_index` (`dataFile`),
  KEY `packageId_fk` (`packageId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `smartMenuFonts`
--

DROP TABLE IF EXISTS `smartMenuFonts`;
CREATE TABLE IF NOT EXISTS `smartMenuFonts` (
  `fontFile` char(32) NOT NULL,
  `createdTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(10) unsigned NOT NULL,
  PRIMARY KEY (`fontFile`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `smartMenuLocations`
--

DROP TABLE IF EXISTS `smartMenuLocations`;
CREATE TABLE IF NOT EXISTS `smartMenuLocations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `locationGuid` char(32) NOT NULL,
  `businessid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `locationGuid_index` (`locationGuid`),
  KEY `businessid` (`businessid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=65112 ;

-- --------------------------------------------------------

--
-- Table structure for table `smartMenuMenuGroups`
--

DROP TABLE IF EXISTS `smartMenuMenuGroups`;
CREATE TABLE IF NOT EXISTS `smartMenuMenuGroups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `businessid` int(10) unsigned NOT NULL,
  `groupName` varchar(64) NOT NULL DEFAULT '',
  `automaticName` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `smartMenuMenuItems`
--

DROP TABLE IF EXISTS `smartMenuMenuItems`;
CREATE TABLE IF NOT EXISTS `smartMenuMenuItems` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menuId` int(10) unsigned NOT NULL,
  `menuItemId` int(10) unsigned NOT NULL,
  `menuGroupId` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menuId_index` (`menuId`),
  KEY `menuGroupId_index` (`menuGroupId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `smartMenuMenus`
--

DROP TABLE IF EXISTS `smartMenuMenus`;
CREATE TABLE IF NOT EXISTS `smartMenuMenus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `businessid` int(10) unsigned NOT NULL,
  `menuName` varchar(64) NOT NULL DEFAULT 'menu',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `smartMenuPackageLinks`
--

DROP TABLE IF EXISTS `smartMenuPackageLinks`;
CREATE TABLE IF NOT EXISTS `smartMenuPackageLinks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `boardGuid` char(32) NOT NULL,
  `packageFile` char(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `boardGuid_packageFile_index` (`boardGuid`,`packageFile`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

-- --------------------------------------------------------

--
-- Table structure for table `smartMenuPackageRequests`
--

DROP TABLE IF EXISTS `smartMenuPackageRequests`;
CREATE TABLE IF NOT EXISTS `smartMenuPackageRequests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `packageFile` char(32) DEFAULT NULL,
  `expirationDate` datetime DEFAULT NULL,
  `fileType` varchar(20) NOT NULL DEFAULT 'package',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2141873199 ;

-- --------------------------------------------------------

--
-- Table structure for table `smartMenuPackages`
--

DROP TABLE IF EXISTS `smartMenuPackages`;
CREATE TABLE IF NOT EXISTS `smartMenuPackages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `packageFile` char(32) NOT NULL,
  `companyid` int(10) unsigned NOT NULL,
  `createdTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `createdBy` int(10) unsigned NOT NULL,
  `modifiedBy` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `packageFile_index` (`packageFile`),
  KEY `companyid` (`companyid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Triggers `smartMenuPackages`
--
DROP TRIGGER IF EXISTS `smartMenuPackages_setTimestamp`;
DELIMITER //
CREATE TRIGGER `smartMenuPackages_setTimestamp` BEFORE INSERT ON `smartMenuPackages`
 FOR EACH ROW begin
		set NEW.modifiedTime = NEW.createdTime;
	end
//
DELIMITER ;
DROP TRIGGER IF EXISTS `smartMenuPackages_updateTimestamp`;
DELIMITER //
CREATE TRIGGER `smartMenuPackages_updateTimestamp` BEFORE UPDATE ON `smartMenuPackages`
 FOR EACH ROW begin
		set NEW.modifiedTime = current_timestamp;
	end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `smartMenuTypes`
--

DROP TABLE IF EXISTS `smartMenuTypes`;
CREATE TABLE IF NOT EXISTS `smartMenuTypes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `smartMenuUpload`
--

DROP TABLE IF EXISTS `smartMenuUpload`;
CREATE TABLE IF NOT EXISTS `smartMenuUpload` (
  `packageFile` varchar(45) NOT NULL,
  `createdTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `createdBy` int(11) NOT NULL,
  `fileType` varchar(20) NOT NULL DEFAULT 'package',
  `uploadOptions` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`createdTime`,`packageFile`),
  KEY `createdTime` (`createdTime`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `smartMenus`
--

DROP TABLE IF EXISTS `smartMenus`;
CREATE TABLE IF NOT EXISTS `smartMenus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `businessid` int(10) unsigned NOT NULL,
  `name` varchar(64) NOT NULL DEFAULT '',
  `type` smallint(5) unsigned NOT NULL DEFAULT '0',
  `activeFlag` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `stats`
--

DROP TABLE IF EXISTS `stats`;
CREATE TABLE IF NOT EXISTS `stats` (
  `statid` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL DEFAULT '0',
  `companyid` int(3) NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `number` int(7) NOT NULL DEFAULT '0',
  `lunch` int(5) NOT NULL DEFAULT '0',
  `dinner` int(5) NOT NULL DEFAULT '0',
  `labor` float NOT NULL DEFAULT '0',
  `labor_vend` double NOT NULL,
  `labor_hour` double NOT NULL DEFAULT '0',
  `void` float NOT NULL DEFAULT '0',
  `closed` tinyint(1) NOT NULL DEFAULT '0',
  `closed_reason` text NOT NULL,
  PRIMARY KEY (`statid`),
  UNIQUE KEY `businessid` (`businessid`,`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=229084 ;

-- --------------------------------------------------------

--
-- Table structure for table `streamware_checks_sent`
--

DROP TABLE IF EXISTS `streamware_checks_sent`;
CREATE TABLE IF NOT EXISTS `streamware_checks_sent` (
  `scs_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'streamware_checks_sent id',
  `businessid` int(10) unsigned NOT NULL,
  `check_number` bigint(20) unsigned NOT NULL,
  `timestamp_sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `confirmed` tinyint(1) NOT NULL,
  PRIMARY KEY (`scs_id`),
  UNIQUE KEY `businessid` (`businessid`,`check_number`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `streamware_debug`
--

DROP TABLE IF EXISTS `streamware_debug`;
CREATE TABLE IF NOT EXISTS `streamware_debug` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `headers_received` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'headers received from client',
  `get_data` text COLLATE utf8_unicode_ci NOT NULL COMMENT '$_GET',
  `post_data` text COLLATE utf8_unicode_ci NOT NULL COMMENT '$_POST',
  `input_data` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'php://input',
  `is_xml_valid` tinyint(1) DEFAULT NULL COMMENT 'whether the data in xmlData is valid according to the DataExchange.xsd',
  `xml_errors` text COLLATE utf8_unicode_ci NOT NULL,
  `debug_data` text COLLATE utf8_unicode_ci NOT NULL,
  `debug_exceptions` text COLLATE utf8_unicode_ci NOT NULL,
  `debug_log` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'debug info from entering and exiting methods',
  `headers_response` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'response headers sent back to client',
  `soap_response` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'return value from Zend_Soap_Server->handle()',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=354 ;

-- --------------------------------------------------------

--
-- Table structure for table `submit`
--

DROP TABLE IF EXISTS `submit`;
CREATE TABLE IF NOT EXISTS `submit` (
  `companyid` int(5) NOT NULL DEFAULT '0',
  `businessid` int(10) NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `submitid` int(10) NOT NULL AUTO_INCREMENT,
  `user` varchar(25) NOT NULL DEFAULT '',
  `export` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`submitid`),
  UNIQUE KEY `businessid` (`businessid`,`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=447926 ;

-- --------------------------------------------------------

--
-- Table structure for table `submit_labor`
--

DROP TABLE IF EXISTS `submit_labor`;
CREATE TABLE IF NOT EXISTS `submit_labor` (
  `businessid` int(5) NOT NULL,
  `date` date NOT NULL,
  `export` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`businessid`,`date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `submit_labor_dept`
--

DROP TABLE IF EXISTS `submit_labor_dept`;
CREATE TABLE IF NOT EXISTS `submit_labor_dept` (
  `businessid` int(5) NOT NULL,
  `date` date NOT NULL,
  `export` tinyint(1) NOT NULL,
  `dept_id` int(5) NOT NULL,
  UNIQUE KEY `businessid` (`businessid`,`date`,`dept_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `survey`
--

DROP TABLE IF EXISTS `survey`;
CREATE TABLE IF NOT EXISTS `survey` (
  `survey_id` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL,
  `date` date NOT NULL,
  `rating` tinyint(1) NOT NULL,
  `added_by` varchar(50) NOT NULL,
  `added_when` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`survey_id`),
  KEY `businessid` (`businessid`),
  KEY `date` (`date`,`rating`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

-- --------------------------------------------------------

--
-- Table structure for table `survey_category`
--

DROP TABLE IF EXISTS `survey_category`;
CREATE TABLE IF NOT EXISTS `survey_category` (
  `category_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `survey_detail`
--

DROP TABLE IF EXISTS `survey_detail`;
CREATE TABLE IF NOT EXISTS `survey_detail` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `survey_id` int(10) NOT NULL,
  `category_id` int(10) NOT NULL,
  `notes` text NOT NULL,
  `action` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `survey_id` (`survey_id`),
  KEY `category_id` (`category_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=148 ;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_log`
--

DROP TABLE IF EXISTS `transaction_log`;
CREATE TABLE IF NOT EXISTS `transaction_log` (
  `transactionId` varchar(25) NOT NULL,
  `clientProgramId` int(11) NOT NULL,
  `customerCardNum` bigint(20) NOT NULL,
  `amount` double NOT NULL,
  `transactionDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`transactionId`,`clientProgramId`,`customerCardNum`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_log22`
--

DROP TABLE IF EXISTS `transaction_log22`;
CREATE TABLE IF NOT EXISTS `transaction_log22` (
  `transactionId` varchar(25) NOT NULL,
  `clientProgramId` int(11) NOT NULL,
  `customerCardNum` bigint(20) NOT NULL,
  `amount` double NOT NULL,
  `transactionDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `is_processed` int(11) NOT NULL
) ENGINE=MEMORY DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_log_copy`
--

DROP TABLE IF EXISTS `transaction_log_copy`;
CREATE TABLE IF NOT EXISTS `transaction_log_copy` (
  `transactionId` varchar(25) NOT NULL,
  `clientProgramId` int(11) NOT NULL,
  `customerCardNum` bigint(20) NOT NULL,
  `amount` double NOT NULL,
  `transactionDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`transactionId`,`clientProgramId`,`customerCardNum`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `vCashierUsers`
--
DROP VIEW IF EXISTS `vCashierUsers`;
CREATE TABLE IF NOT EXISTS `vCashierUsers` (
`username` varchar(30)
,`password` varchar(20)
,`description` varchar(52)
,`groupId` varchar(20)
,`businessid` int(10) unsigned
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vDbCustom`
--
DROP VIEW IF EXISTS `vDbCustom`;
CREATE TABLE IF NOT EXISTS `vDbCustom` (
`vGroupId` varchar(6)
,`viewid` int(10)
,`name` varchar(100)
,`loginid` int(10)
,`graph_type` int(3)
,`size` tinyint(1)
,`db_height` smallint(6)
,`xaxis` tinyint(4)
,`db_order` int(2)
,`period` tinyint(1)
,`period_num` tinyint(1)
,`period_group` tinyint(1)
,`current` tinyint(1)
,`week_ends` varchar(8)
,`db_mtime` timestamp
,`mtime` bigint(10)
,`ptime` bigint(10)
,`dbSide` int(11)
,`dbHeight` int(11)
,`dbOrder` int(11)
,`cacheId` int(10) unsigned
,`cacheDate` timestamp
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vDbGroup`
--
DROP VIEW IF EXISTS `vDbGroup`;
CREATE TABLE IF NOT EXISTS `vDbGroup` (
`vGroupId` varchar(11)
,`viewid` int(10)
,`name` varchar(100)
,`loginid` int(10)
,`graph_type` int(3)
,`size` tinyint(1)
,`db_height` smallint(6)
,`xaxis` tinyint(4)
,`db_order` int(2)
,`period` tinyint(1)
,`period_num` tinyint(1)
,`period_group` tinyint(1)
,`current` tinyint(1)
,`week_ends` varchar(8)
,`db_mtime` timestamp
,`dbSide` smallint(6)
,`dbOrder` int(11)
,`dbHeight` int(11)
,`cacheId` int(10) unsigned
,`cacheDate` timestamp
,`dbPublic` int(0)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vDbGroupList`
--
DROP VIEW IF EXISTS `vDbGroupList`;
CREATE TABLE IF NOT EXISTS `vDbGroupList` (
`groupid` varchar(11)
,`groupname` varchar(30)
,`loginid` int(11)
,`groupowner` varchar(11)
,`dbDefault` bigint(20)
,`floater` bigint(20)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vDbGroupMembersOwners`
--
DROP VIEW IF EXISTS `vDbGroupMembersOwners`;
CREATE TABLE IF NOT EXISTS `vDbGroupMembersOwners` (
`groupid` int(11) unsigned
,`loginid` int(11) unsigned
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vMenuItems_District`
--
DROP VIEW IF EXISTS `vMenuItems_District`;
CREATE TABLE IF NOT EXISTS `vMenuItems_District` (
`menu_item_id` int(10)
,`businessid` int(10)
,`companyid` int(3)
,`item_name` varchar(45)
,`upc` varchar(20)
,`price` float
,`retail` double
,`parent` int(10)
,`deleted` tinyint(1)
,`description` text
,`groupid` int(5)
,`unit` varchar(15)
,`menu_typeid` int(3)
,`true_unit` int(3)
,`serving_size` double
,`serving_size2` varchar(20)
,`order_unit` int(3)
,`su_in_ou` double
,`label` tinyint(1)
,`order_min` int(2)
,`item_code` varchar(15)
,`sales_acct` int(10)
,`tax_acct` int(10)
,`nontax_acct` int(10)
,`recipe` text
,`recipe_active` tinyint(1)
,`recipe_active_cust` tinyint(1)
,`recipe_station` int(5)
,`batch_recipe` tinyint(1)
,`contract_option` tinyint(1)
,`heart_healthy` tinyint(1)
,`image` varchar(20)
,`keeper` tinyint(1)
,`districtname` varchar(30)
,`businessname` varchar(50)
,`menu_typename` varchar(50)
,`station_name` varchar(30)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vPromoDays`
--
DROP VIEW IF EXISTS `vPromoDays`;
CREATE TABLE IF NOT EXISTS `vPromoDays` (
`day` bigint(20)
,`id` int(11)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vPromoVivipos`
--
DROP VIEW IF EXISTS `vPromoVivipos`;
CREATE TABLE IF NOT EXISTS `vPromoVivipos` (
`businessid` int(11) unsigned
,`promo_id` int(11)
,`promo_target` int(11)
,`vivipos_name` varchar(50)
,`pos_id` int(11)
,`value` double
,`rule_order` smallint(6)
,`promo_reserve` tinyint(4)
,`promo_discount` varchar(24)
,`promo_discount_type` varchar(24)
,`promo_discount_n` smallint(6)
,`promo_discount_limit` smallint(6)
,`promo_trigger_amount_limit` varchar(16)
,`promo_trigger_amount_type` varchar(24)
,`promo_trigger_amount` decimal(8,2)
,`promo_trigger_amount_2` smallint(6)
,`promo_trigger_amount_3` smallint(6)
,`vivipos_taxno` varchar(11)
,`vivipos_taxname` varchar(25)
,`vivipos_startdate` bigint(20)
,`vivipos_starttime` bigint(20)
,`vivipos_enddate` bigint(20)
,`vivipos_endtime` bigint(20)
,`vivipos_days` blob
,`vivipos_type` varchar(32)
,`vivipos_trigger` varchar(32)
,`promotarget_table` varchar(50)
,`promotarget_order` varchar(50)
,`vivipos_modified` bigint(20)
,`vivipos_active` bigint(20)
,`is_global` bigint(20)
,`alt_name1` tinyint(4)
);
-- --------------------------------------------------------

--
-- Table structure for table `vehicle_maintenance`
--

DROP TABLE IF EXISTS `vehicle_maintenance`;
CREATE TABLE IF NOT EXISTS `vehicle_maintenance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `mileage` int(6) unsigned NOT NULL,
  `service_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `vehicle_id` (`vehicle_id`,`service_id`,`service_date`),
  KEY `service_id` (`service_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_routes`
--

DROP TABLE IF EXISTS `vehicle_routes`;
CREATE TABLE IF NOT EXISTS `vehicle_routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL,
  `route_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `vehicle_id` (`vehicle_id`,`route_id`,`route_date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_services`
--

DROP TABLE IF EXISTS `vehicle_services`;
CREATE TABLE IF NOT EXISTS `vehicle_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `mileage` int(6) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

DROP TABLE IF EXISTS `vehicles`;
CREATE TABLE IF NOT EXISTS `vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `businessid` int(11) NOT NULL,
  `vehicle_num` int(11) NOT NULL,
  `vehicle_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `make` varchar(50) COLLATE utf8_bin NOT NULL,
  `model` varchar(50) COLLATE utf8_bin NOT NULL,
  `year` int(4) unsigned NOT NULL,
  `mileage` int(6) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `businessid` (`businessid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `vend_budget`
--

DROP TABLE IF EXISTS `vend_budget`;
CREATE TABLE IF NOT EXISTS `vend_budget` (
  `budgetid` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(5) NOT NULL,
  `date` date NOT NULL,
  `amount` double NOT NULL,
  `type` int(2) NOT NULL,
  PRIMARY KEY (`budgetid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2615 ;

-- --------------------------------------------------------

--
-- Table structure for table `vend_bulletin`
--

DROP TABLE IF EXISTS `vend_bulletin`;
CREATE TABLE IF NOT EXISTS `vend_bulletin` (
  `bulletinid` int(10) NOT NULL AUTO_INCREMENT,
  `login_routeid` int(10) NOT NULL,
  `date` date NOT NULL,
  `message` text NOT NULL,
  `new` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`bulletinid`),
  KEY `login_routeid` (`login_routeid`,`new`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4803 ;

-- --------------------------------------------------------

--
-- Table structure for table `vend_channel`
--

DROP TABLE IF EXISTS `vend_channel`;
CREATE TABLE IF NOT EXISTS `vend_channel` (
  `channelid` int(10) NOT NULL AUTO_INCREMENT,
  `channel_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `businessid` int(10) NOT NULL,
  PRIMARY KEY (`channelid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=33 ;

-- --------------------------------------------------------

--
-- Table structure for table `vend_collection`
--

DROP TABLE IF EXISTS `vend_collection`;
CREATE TABLE IF NOT EXISTS `vend_collection` (
  `vend_collectionid` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(5) NOT NULL,
  `date` date NOT NULL,
  `nickel_dime` double NOT NULL,
  `coin` double NOT NULL,
  `currency` double NOT NULL,
  `checks` double NOT NULL,
  `changers_made` double NOT NULL,
  `in_room` double NOT NULL,
  `out_room` double NOT NULL,
  `escrow_in` double NOT NULL,
  `escrow_out` double NOT NULL,
  `brinks` double NOT NULL,
  `bal_on_hand` double NOT NULL,
  `b_bag1` double NOT NULL,
  `b_bag2` double NOT NULL,
  `b_escrow_qty` double NOT NULL,
  `b_escrow_amt` double NOT NULL,
  `b_nickel_qty` double NOT NULL,
  `b_dime_qty` double NOT NULL,
  `b_quarter_qty` double NOT NULL,
  `b_dollar_qty` double NOT NULL,
  `b_bill_qty` double NOT NULL,
  `b_changers_made` double NOT NULL,
  `b_bag3` double NOT NULL,
  `b_bag4` double NOT NULL,
  `b_escrow_qty2` double NOT NULL,
  `b_escrow_amt2` double NOT NULL,
  `b_nickel_qty2` double NOT NULL,
  `b_dime_qty2` double NOT NULL,
  `b_quarter_qty2` double NOT NULL,
  `b_dollar_qty2` double NOT NULL,
  `b_bill_qty2` double NOT NULL,
  `posted` tinyint(1) NOT NULL DEFAULT '0',
  `notes` text NOT NULL,
  PRIMARY KEY (`vend_collectionid`),
  KEY `businessid` (`businessid`,`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3801 ;

-- --------------------------------------------------------

--
-- Table structure for table `vend_commission`
--

DROP TABLE IF EXISTS `vend_commission`;
CREATE TABLE IF NOT EXISTS `vend_commission` (
  `commissionid` int(5) NOT NULL AUTO_INCREMENT,
  `companyid` int(3) NOT NULL,
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `fixed_amount` tinyint(1) NOT NULL,
  `base_link` int(5) NOT NULL,
  `commission_link` int(5) NOT NULL,
  `freevend_link` int(5) NOT NULL,
  `csv_link` int(5) NOT NULL,
  `weekend_link` int(5) NOT NULL,
  `pto_link` int(5) NOT NULL,
  `pto_dollar_link` int(11) NOT NULL,
  `holiday_link` int(5) NOT NULL,
  `snp_link` int(5) NOT NULL,
  `base` double NOT NULL,
  `holiday_pay` double NOT NULL DEFAULT '0',
  `route_emp` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 to link to empl',
  `base_commission` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '1 is base or commission, 2 is base+commission',
  PRIMARY KEY (`commissionid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

-- --------------------------------------------------------

--
-- Table structure for table `vend_commission_detail`
--

DROP TABLE IF EXISTS `vend_commission_detail`;
CREATE TABLE IF NOT EXISTS `vend_commission_detail` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `commissionid` int(5) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `calc_type` int(2) NOT NULL,
  `lower` double NOT NULL,
  `upper` double NOT NULL,
  `percent` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=139 ;

-- --------------------------------------------------------

--
-- Table structure for table `vend_commission_source`
--

DROP TABLE IF EXISTS `vend_commission_source`;
CREATE TABLE IF NOT EXISTS `vend_commission_source` (
  `sourceid` int(2) NOT NULL AUTO_INCREMENT,
  `source_name` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `action` int(1) NOT NULL COMMENT '0-BT,1-MEI(net),2-Fixed,3-MEI(gross)',
  PRIMARY KEY (`sourceid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `vend_family`
--

DROP TABLE IF EXISTS `vend_family`;
CREATE TABLE IF NOT EXISTS `vend_family` (
  `familyid` int(10) NOT NULL AUTO_INCREMENT,
  `family_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `businessid` int(10) NOT NULL,
  PRIMARY KEY (`familyid`),
  KEY `familyid` (`familyid`,`businessid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=318 ;

-- --------------------------------------------------------

--
-- Table structure for table `vend_gl_account_budgets`
--

DROP TABLE IF EXISTS `vend_gl_account_budgets`;
CREATE TABLE IF NOT EXISTS `vend_gl_account_budgets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` int(10) unsigned NOT NULL COMMENT 'matches vend_gl_accounts primary key',
  `budget_month` date NOT NULL,
  `amount` double(10,2) NOT NULL,
  `final` double(10,2) DEFAULT NULL COMMENT 'final value for the month',
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_id` (`account_id`,`budget_month`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1704 ;

-- --------------------------------------------------------

--
-- Table structure for table `vend_gl_account_details`
--

DROP TABLE IF EXISTS `vend_gl_account_details`;
CREATE TABLE IF NOT EXISTS `vend_gl_account_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` int(10) unsigned NOT NULL COMMENT 'matches vend_gl_accounts primary key',
  `friday_date` date NOT NULL,
  `amount` double(10,2) NOT NULL,
  `budget` double NOT NULL,
  `import_number` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'is this the primary or secondary file import',
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_id` (`account_id`,`friday_date`,`import_number`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15102 ;

-- --------------------------------------------------------

--
-- Table structure for table `vend_gl_account_groups`
--

DROP TABLE IF EXISTS `vend_gl_account_groups`;
CREATE TABLE IF NOT EXISTS `vend_gl_account_groups` (
  `id` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `account_type_id` smallint(5) unsigned NOT NULL COMMENT 'matches vend_gl_account_types primary key',
  `sort_order` smallint(6) NOT NULL,
  `account_group_class` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `vend_gl_account_items`
--

DROP TABLE IF EXISTS `vend_gl_account_items`;
CREATE TABLE IF NOT EXISTS `vend_gl_account_items` (
  `account_id` int(10) unsigned NOT NULL COMMENT 'matches vend_gl_accounts primary key',
  `sequence_id` int(10) unsigned NOT NULL COMMENT 'array id within the account_id - we could use auto_increment if we had some way of knowing when we could flush the table',
  `friday_date` date NOT NULL,
  `import_number` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'is this the primary or secondary file import',
  `amount` double(10,2) NOT NULL,
  `label1` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `label2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`account_id`,`sequence_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vend_gl_account_types`
--

DROP TABLE IF EXISTS `vend_gl_account_types`;
CREATE TABLE IF NOT EXISTS `vend_gl_account_types` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `sort_order` smallint(6) NOT NULL DEFAULT '0',
  `account_class` varchar(255) COLLATE utf8_unicode_ci DEFAULT '' COMMENT 'Class to instantiate for this account',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Table structure for table `vend_gl_account_xhrefs`
--

DROP TABLE IF EXISTS `vend_gl_account_xhrefs`;
CREATE TABLE IF NOT EXISTS `vend_gl_account_xhrefs` (
  `account_id` int(10) unsigned NOT NULL,
  `business_id` int(10) unsigned NOT NULL,
  `foreign_id` int(10) unsigned NOT NULL,
  `table_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'name of the table the foreign_id belongs to',
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `old_account_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`account_id`,`business_id`,`table_name`,`foreign_id`),
  KEY `account_id` (`account_id`,`business_id`),
  KEY `account_id_2` (`account_id`,`foreign_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vend_gl_accounts`
--

DROP TABLE IF EXISTS `vend_gl_accounts`;
CREATE TABLE IF NOT EXISTS `vend_gl_accounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `business_id` int(10) unsigned NOT NULL COMMENT 'matches business primary key',
  `account_group_id` int(3) unsigned NOT NULL COMMENT 'matches vend_gl_account_groups primary key',
  `is_deleted` smallint(6) NOT NULL DEFAULT '0',
  `account_id` int(10) unsigned NOT NULL COMMENT 'matches vend_gl_accounts primary key',
  `number` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `account_type_id` smallint(5) unsigned NOT NULL COMMENT 'matches vend_gl_account_types primary key',
  `sort_order` smallint(6) NOT NULL,
  `creditid` int(10) NOT NULL,
  `multiplier` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `businessid` (`business_id`,`account_group_id`),
  KEY `number` (`number`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=687 ;

-- --------------------------------------------------------

--
-- Table structure for table `vend_item`
--

DROP TABLE IF EXISTS `vend_item`;
CREATE TABLE IF NOT EXISTS `vend_item` (
  `vend_itemid` int(10) NOT NULL AUTO_INCREMENT,
  `menu_itemid` int(10) NOT NULL,
  `date` date NOT NULL,
  `groupid` int(5) NOT NULL,
  `businessid` int(5) NOT NULL,
  PRIMARY KEY (`vend_itemid`),
  KEY `groupid` (`date`,`businessid`,`groupid`),
  KEY `vend_itemid_2` (`vend_itemid`,`groupid`),
  KEY `vend_itemid` (`vend_itemid`,`menu_itemid`,`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=200050 ;

-- --------------------------------------------------------

--
-- Table structure for table `vend_locations`
--

DROP TABLE IF EXISTS `vend_locations`;
CREATE TABLE IF NOT EXISTS `vend_locations` (
  `locationid` int(5) NOT NULL AUTO_INCREMENT,
  `location_name` varchar(40) NOT NULL,
  `businessid` int(5) NOT NULL,
  `companyid` int(3) NOT NULL,
  `menu_typeid` int(5) NOT NULL,
  `accountid` int(10) NOT NULL,
  `unitid` int(5) NOT NULL,
  `op_unitid` int(10) NOT NULL,
  `debitid` int(10) NOT NULL,
  `creditid` int(10) NOT NULL,
  `op_creditid` int(10) NOT NULL,
  `vendor` int(10) NOT NULL COMMENT 'for commissary retail',
  `apaccountid` int(10) NOT NULL COMMENT 'for commissary retail',
  `AR_id` int(10) NOT NULL,
  `AR_credit` int(10) NOT NULL,
  `AR_debit` int(10) NOT NULL,
  `catchall_creditid` int(10) NOT NULL,
  `catchall_debitid` int(10) NOT NULL,
  PRIMARY KEY (`locationid`),
  KEY `businessid` (`businessid`,`unitid`),
  KEY `unitid` (`unitid`),
  KEY `op_unitid` (`op_unitid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=90 ;

-- --------------------------------------------------------

--
-- Table structure for table `vend_machine`
--

DROP TABLE IF EXISTS `vend_machine`;
CREATE TABLE IF NOT EXISTS `vend_machine` (
  `machineid` int(10) NOT NULL AUTO_INCREMENT,
  `machine_num` varchar(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `login_routeid` int(10) NOT NULL,
  `accountid` int(10) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `del_date` date NOT NULL,
  `menu_typeid` int(5) NOT NULL,
  PRIMARY KEY (`machineid`),
  KEY `login_routeid` (`login_routeid`),
  KEY `machine_num` (`machine_num`,`is_deleted`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5163 ;

-- --------------------------------------------------------

--
-- Table structure for table `vend_machine_complaint`
--

DROP TABLE IF EXISTS `vend_machine_complaint`;
CREATE TABLE IF NOT EXISTS `vend_machine_complaint` (
  `complaintid` int(5) NOT NULL AUTO_INCREMENT,
  `complaint_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `businessid` int(10) NOT NULL,
  `route_related` tinyint(1) NOT NULL,
  PRIMARY KEY (`complaintid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=255 ;

-- --------------------------------------------------------

--
-- Table structure for table `vend_machine_repair`
--

DROP TABLE IF EXISTS `vend_machine_repair`;
CREATE TABLE IF NOT EXISTS `vend_machine_repair` (
  `repairid` int(5) NOT NULL AUTO_INCREMENT,
  `repair_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `businessid` int(10) NOT NULL,
  PRIMARY KEY (`repairid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2615 ;

-- --------------------------------------------------------

--
-- Table structure for table `vend_machine_repairman`
--

DROP TABLE IF EXISTS `vend_machine_repairman`;
CREATE TABLE IF NOT EXISTS `vend_machine_repairman` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `repairman` varchar(40) NOT NULL,
  `businessid` int(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=361 ;

-- --------------------------------------------------------

--
-- Table structure for table `vend_machine_sales`
--

DROP TABLE IF EXISTS `vend_machine_sales`;
CREATE TABLE IF NOT EXISTS `vend_machine_sales` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `machineid` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `businessid` int(10) NOT NULL,
  `date` date NOT NULL,
  `accountid` int(10) NOT NULL,
  `install_date` date NOT NULL,
  `route` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `has_vend` tinyint(1) NOT NULL,
  `has_coffee` tinyint(1) NOT NULL,
  `channel` int(3) NOT NULL,
  `family` int(5) NOT NULL,
  `salesman` int(3) NOT NULL,
  `fills` double NOT NULL,
  `collects` double NOT NULL,
  `vgroup` int(5) NOT NULL,
  `waste` double NOT NULL,
  `refunds` double NOT NULL,
  `over_short` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `date` (`route`,`date`,`businessid`),
  KEY `businessid` (`machineid`,`businessid`,`date`),
  KEY `businessid_2` (`businessid`,`date`),
  KEY `accountid` (`accountid`,`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5018488 ;

-- --------------------------------------------------------

--
-- Table structure for table `vend_machine_schedule`
--

DROP TABLE IF EXISTS `vend_machine_schedule`;
CREATE TABLE IF NOT EXISTS `vend_machine_schedule` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `machineid` int(10) NOT NULL,
  `day` int(1) NOT NULL COMMENT '1-M,2-T,3-W,4-T,5-F,6-S,7-S',
  PRIMARY KEY (`id`),
  UNIQUE KEY `machineid` (`machineid`,`day`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17786 ;

-- --------------------------------------------------------

--
-- Table structure for table `vend_machine_service`
--

DROP TABLE IF EXISTS `vend_machine_service`;
CREATE TABLE IF NOT EXISTS `vend_machine_service` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `machineid` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `accountid` int(10) NOT NULL,
  `complaint` int(5) NOT NULL,
  `repair` int(5) NOT NULL,
  `repairman` int(5) NOT NULL,
  `businessid` int(10) NOT NULL,
  `login_routeid` int(8) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `machineid` (`machineid`,`businessid`,`date`),
  KEY `complaint` (`complaint`,`date`,`businessid`),
  KEY `date` (`date`,`login_routeid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=385147 ;

-- --------------------------------------------------------

--
-- Table structure for table `vend_machine_vgroup`
--

DROP TABLE IF EXISTS `vend_machine_vgroup`;
CREATE TABLE IF NOT EXISTS `vend_machine_vgroup` (
  `vgroupid` int(5) NOT NULL AUTO_INCREMENT,
  `vgroup_num` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `vgroup_name` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `businessid` int(10) NOT NULL,
  `vgroup_type` int(3) NOT NULL DEFAULT '1',
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`vgroupid`),
  KEY `vgroupid` (`vgroupid`,`vgroup_type`,`hidden`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=386 ;

-- --------------------------------------------------------

--
-- Table structure for table `vend_machine_vgroup_type`
--

DROP TABLE IF EXISTS `vend_machine_vgroup_type`;
CREATE TABLE IF NOT EXISTS `vend_machine_vgroup_type` (
  `vgroup_typeid` int(3) NOT NULL AUTO_INCREMENT,
  `vgroup_typename` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(7) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`vgroup_typeid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `vend_mei_sales`
--

DROP TABLE IF EXISTS `vend_mei_sales`;
CREATE TABLE IF NOT EXISTS `vend_mei_sales` (
  `mei_id` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(5) NOT NULL,
  `date` date NOT NULL,
  `route` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `amount` double NOT NULL,
  `tax` double NOT NULL,
  PRIMARY KEY (`mei_id`),
  KEY `businessid` (`businessid`,`route`,`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=24849 ;

-- --------------------------------------------------------

--
-- Table structure for table `vend_order`
--

DROP TABLE IF EXISTS `vend_order`;
CREATE TABLE IF NOT EXISTS `vend_order` (
  `vend_orderid` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(5) NOT NULL,
  `amount` double NOT NULL,
  `qi_price` double NOT NULL,
  `shipped` double NOT NULL,
  `received` double NOT NULL,
  `returned` double NOT NULL,
  `qo_price` double NOT NULL,
  `date` date NOT NULL,
  `login_routeid` int(10) NOT NULL,
  `vend_itemid` int(10) NOT NULL,
  `locationid` int(5) NOT NULL,
  `machineid` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`vend_orderid`),
  KEY `vend_itemid` (`vend_itemid`,`login_routeid`,`date`,`machineid`),
  KEY `locationid` (`locationid`),
  KEY `login_routeid` (`login_routeid`),
  KEY `date` (`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3922641 ;

-- --------------------------------------------------------

--
-- Table structure for table `vend_pckg`
--

DROP TABLE IF EXISTS `vend_pckg`;
CREATE TABLE IF NOT EXISTS `vend_pckg` (
  `vend_pckgid` int(5) NOT NULL AUTO_INCREMENT,
  `pckg_name` varchar(25) NOT NULL,
  PRIMARY KEY (`vend_pckgid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

-- --------------------------------------------------------

--
-- Table structure for table `vend_route_collection`
--

DROP TABLE IF EXISTS `vend_route_collection`;
CREATE TABLE IF NOT EXISTS `vend_route_collection` (
  `route_col_id` int(10) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `login_routeid` int(10) NOT NULL,
  `route_total` double NOT NULL,
  `route_currency` double NOT NULL,
  `changer_expected` double NOT NULL,
  `changer_total` double NOT NULL,
  `changer_currency` double NOT NULL,
  PRIMARY KEY (`route_col_id`),
  KEY `login_routeid` (`login_routeid`,`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=141558 ;

-- --------------------------------------------------------

--
-- Table structure for table `vend_salesman`
--

DROP TABLE IF EXISTS `vend_salesman`;
CREATE TABLE IF NOT EXISTS `vend_salesman` (
  `salesid` int(5) NOT NULL AUTO_INCREMENT,
  `businessid` int(5) NOT NULL,
  `sales_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`salesid`),
  KEY `businessid` (`businessid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=49 ;

-- --------------------------------------------------------

--
-- Table structure for table `vend_settings`
--

DROP TABLE IF EXISTS `vend_settings`;
CREATE TABLE IF NOT EXISTS `vend_settings` (
  `businessid` int(10) NOT NULL,
  `nickelx` double NOT NULL,
  `dimex` double NOT NULL,
  `quarterx` double NOT NULL,
  `dollarx` double NOT NULL,
  `d_coinx` double NOT NULL,
  `email_labor` varchar(100) NOT NULL,
  `changer_total_creditid` int(10) NOT NULL,
  `changer_expected_debitid` int(10) NOT NULL,
  `brinks_creditid` int(10) NOT NULL,
  `brinks_debitid` int(10) NOT NULL,
  `kiosk_cash` int(10) NOT NULL,
  `kiosk_onlinecc` int(10) NOT NULL,
  `kiosk_giftcard` int(10) NOT NULL,
  PRIMARY KEY (`businessid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vend_supervisor`
--

DROP TABLE IF EXISTS `vend_supervisor`;
CREATE TABLE IF NOT EXISTS `vend_supervisor` (
  `supervisorid` int(4) NOT NULL AUTO_INCREMENT,
  `supervisor_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `businessid` int(10) NOT NULL,
  PRIMARY KEY (`supervisorid`),
  KEY `supervisorid` (`supervisorid`,`businessid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=23 ;

-- --------------------------------------------------------

--
-- Table structure for table `vend_waste`
--

DROP TABLE IF EXISTS `vend_waste`;
CREATE TABLE IF NOT EXISTS `vend_waste` (
  `vend_wasteid` int(10) NOT NULL AUTO_INCREMENT,
  `login_routeid` int(10) NOT NULL,
  `locationid` int(5) NOT NULL,
  `date` date NOT NULL,
  `total` double NOT NULL DEFAULT '0',
  `user` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`vend_wasteid`),
  KEY `login_routeid` (`login_routeid`,`date`),
  KEY `date` (`date`),
  KEY `locationid` (`locationid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4226 ;

-- --------------------------------------------------------

--
-- Table structure for table `vend_waste_detail`
--

DROP TABLE IF EXISTS `vend_waste_detail`;
CREATE TABLE IF NOT EXISTS `vend_waste_detail` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `vend_wasteid` int(10) NOT NULL,
  `menu_itemid` int(10) NOT NULL,
  `qty` double NOT NULL,
  `price` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `vend_wasteid` (`vend_wasteid`,`menu_itemid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=95504 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_date`
--

DROP TABLE IF EXISTS `vendor_date`;
CREATE TABLE IF NOT EXISTS `vendor_date` (
  `vendor_dateid` int(10) NOT NULL AUTO_INCREMENT,
  `vendorid` int(10) NOT NULL,
  `businessid` int(10) NOT NULL,
  `day` tinyint(1) NOT NULL,
  `type` tinyint(1) NOT NULL,
  PRIMARY KEY (`vendor_dateid`),
  KEY `businessid` (`businessid`,`day`,`type`),
  KEY `vendorid` (`vendorid`,`businessid`,`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=916 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_import`
--

DROP TABLE IF EXISTS `vendor_import`;
CREATE TABLE IF NOT EXISTS `vendor_import` (
  `vendor_importid` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL,
  `companyid` int(3) NOT NULL,
  `vendor` int(10) NOT NULL,
  `customerid` varchar(20) NOT NULL,
  `last_update` date NOT NULL,
  PRIMARY KEY (`vendor_importid`),
  KEY `customerid` (`customerid`,`vendor`),
  KEY `businessid` (`businessid`,`vendor`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=194 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_import_dates`
--

DROP TABLE IF EXISTS `vendor_import_dates`;
CREATE TABLE IF NOT EXISTS `vendor_import_dates` (
  `importId` char(3) NOT NULL,
  `fileDate` date NOT NULL,
  PRIMARY KEY (`importId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_master`
--

DROP TABLE IF EXISTS `vendor_master`;
CREATE TABLE IF NOT EXISTS `vendor_master` (
  `vendor_master_id` int(10) NOT NULL AUTO_INCREMENT,
  `companyid` int(3) NOT NULL DEFAULT '1',
  `vendor_name` varchar(50) NOT NULL,
  `vendor_code` varchar(20) NOT NULL DEFAULT '',
  `street` varchar(75) NOT NULL,
  `street2` varchar(50) NOT NULL,
  `street3` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(5) NOT NULL,
  `zip` varchar(10) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `del_date` date NOT NULL,
  PRIMARY KEY (`vendor_master_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17373 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendorpend`
--

DROP TABLE IF EXISTS `vendorpend`;
CREATE TABLE IF NOT EXISTS `vendorpend` (
  `vendorpendid` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL DEFAULT '0',
  `companyid` int(3) NOT NULL DEFAULT '0',
  `vendorname` varchar(30) NOT NULL DEFAULT '',
  `attn` varchar(30) NOT NULL DEFAULT '',
  `street` varchar(30) NOT NULL DEFAULT '',
  `city` varchar(30) NOT NULL DEFAULT '',
  `state` char(2) NOT NULL DEFAULT '',
  `zip` varchar(12) NOT NULL DEFAULT '',
  `phone` varchar(25) NOT NULL DEFAULT '',
  PRIMARY KEY (`vendorpendid`),
  KEY `companyid` (`companyid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3463 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

DROP TABLE IF EXISTS `vendors`;
CREATE TABLE IF NOT EXISTS `vendors` (
  `vendorid` int(10) NOT NULL AUTO_INCREMENT,
  `businessid` int(10) NOT NULL DEFAULT '0',
  `name` varchar(30) NOT NULL DEFAULT '',
  `vendor_num` varchar(15) NOT NULL DEFAULT '',
  `master` int(10) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `notes` text NOT NULL,
  `orderid` int(2) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `del_date` date NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`vendorid`),
  KEY `businessid` (`businessid`,`is_deleted`,`del_date`),
  KEY `businessid_2` (`businessid`,`is_deleted`),
  KEY `businessid_3` (`businessid`,`is_deleted`,`master`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13252 ;

-- --------------------------------------------------------

--
-- Structure for view `vCashierUsers`
--
DROP TABLE IF EXISTS `vCashierUsers`;

CREATE VIEW `vCashierUsers` AS select lcase(coalesce(nullif(`login`.`username`,''),concat(left(trim(`login`.`firstname`),1),trim(`login`.`lastname`)))) AS `username`,`login_cashier`.`passcode` AS `password`,concat(trim(`login`.`lastname`),', ',trim(`login`.`firstname`)) AS `description`,`login_cashier_link`.`groupName` AS `groupId`,`login_cashier_link`.`businessid` AS `businessid` from ((`login` join `login_cashier` on((`login`.`loginid` = `login_cashier`.`loginid`))) join `login_cashier_link` on((`login`.`loginid` = `login_cashier_link`.`loginid`)));

-- --------------------------------------------------------

--
-- Structure for view `vDbCustom`
--
DROP TABLE IF EXISTS `vDbCustom`;

CREATE VIEW `vDbCustom` AS select cast('custom' as char charset latin1) AS `vGroupId`,`db_view`.`viewid` AS `viewid`,`db_view`.`name` AS `name`,`db_view`.`loginid` AS `loginid`,`db_view`.`graph_type` AS `graph_type`,`db_view`.`size` AS `size`,`db_view`.`db_height` AS `db_height`,`db_view`.`xaxis` AS `xaxis`,`db_view`.`db_order` AS `db_order`,`db_view`.`period` AS `period`,`db_view`.`period_num` AS `period_num`,`db_view`.`period_group` AS `period_group`,`db_view`.`current` AS `current`,`db_view`.`week_ends` AS `week_ends`,`db_view`.`db_mtime` AS `db_mtime`,unix_timestamp(`db_view`.`db_mtime`) AS `mtime`,unix_timestamp(`pub_db_view`.`db_mtime`) AS `ptime`,`db_view_order`.`dbSide` AS `dbSide`,`db_view_order`.`dbHeight` AS `dbHeight`,`db_view_order`.`dbOrder` AS `dbOrder`,`db_cache`.`cacheId` AS `cacheId`,`db_cache`.`cacheDate` AS `cacheDate` from (((`db_view` left join `pub_db_view` on((`db_view`.`viewid` = `pub_db_view`.`viewid`))) left join `db_view_order` on((`db_view`.`viewid` = `db_view_order`.`viewid`))) left join `db_cache` on((`db_view`.`viewid` = `db_cache`.`viewid`)));

-- --------------------------------------------------------

--
-- Structure for view `vDbGroup`
--
DROP TABLE IF EXISTS `vDbGroup`;

CREATE VIEW `vDbGroup` AS select cast(`db_group_graphs`.`groupid` as char charset latin1) AS `vGroupId`,`pub_db_view`.`viewid` AS `viewid`,`pub_db_view`.`name` AS `name`,`pub_db_view`.`loginid` AS `loginid`,`pub_db_view`.`graph_type` AS `graph_type`,`pub_db_view`.`size` AS `size`,`pub_db_view`.`db_height` AS `db_height`,`pub_db_view`.`xaxis` AS `xaxis`,`pub_db_view`.`db_order` AS `db_order`,`pub_db_view`.`period` AS `period`,`pub_db_view`.`period_num` AS `period_num`,`pub_db_view`.`period_group` AS `period_group`,`pub_db_view`.`current` AS `current`,`pub_db_view`.`week_ends` AS `week_ends`,`pub_db_view`.`db_mtime` AS `db_mtime`,`db_group_graphs`.`dbSide` AS `dbSide`,`db_group_graphs`.`dbOrder` AS `dbOrder`,`db_group_graphs`.`dbHeight` AS `dbHeight`,`db_cache`.`cacheId` AS `cacheId`,`db_cache`.`cacheDate` AS `cacheDate`,(case when (`db_public`.`groupname` is not null) then 1 else 0 end) AS `dbPublic` from ((((`pub_db_view` join `db_group_graphs` on((`pub_db_view`.`viewid` = `db_group_graphs`.`viewid`))) left join `db_cache` on((`pub_db_view`.`viewid` = `db_cache`.`viewid`))) left join `db_group` on((`db_group_graphs`.`groupid` = `db_group`.`groupid`))) left join `db_public` on((`db_group`.`groupname` = `db_public`.`groupname`)));

-- --------------------------------------------------------

--
-- Structure for view `vDbGroupList`
--
DROP TABLE IF EXISTS `vDbGroupList`;

CREATE VIEW `vDbGroupList` AS (select cast('custom' as char charset latin1) AS `groupid`,cast('My Charts' as char charset latin1) AS `groupname`,`login`.`loginid` AS `loginid`,cast('custom' as char charset latin1) AS `groupowner`,0 AS `dbDefault`,1 AS `floater` from (`login` join `security` on(((`login`.`security` = `security`.`securityid`) and (`security`.`dashboard` = 4))))) union (select cast(`db_group`.`groupid` as char charset latin1) AS `groupid`,`db_group`.`groupname` AS `groupname`,`vDbGroupMembersOwners`.`loginid` AS `loginid`,cast(`db_group`.`groupowner` as char charset latin1) AS `groupowner`,coalesce(`db_public`.`dbDefault`,0) AS `dbDefault`,0 AS `floater` from ((`db_group` join `vDbGroupMembersOwners` on((`db_group`.`groupid` = `vDbGroupMembersOwners`.`groupid`))) left join `db_public` on((`db_group`.`groupid` = `db_public`.`id`)))) order by `floater` desc,`groupname`;

-- --------------------------------------------------------

--
-- Structure for view `vDbGroupMembersOwners`
--
DROP TABLE IF EXISTS `vDbGroupMembersOwners`;

CREATE VIEW `vDbGroupMembersOwners` AS select `db_group_members`.`groupid` AS `groupid`,`db_group_members`.`loginid` AS `loginid` from `db_group_members` union select `db_group`.`groupid` AS `groupid`,`db_group`.`groupowner` AS `loginid` from `db_group`;

-- --------------------------------------------------------

--
-- Structure for view `vMenuItems_District`
--
DROP TABLE IF EXISTS `vMenuItems_District`;

CREATE VIEW `vMenuItems_District` AS select `menu_items`.`menu_item_id` AS `menu_item_id`,`menu_items`.`businessid` AS `businessid`,`menu_items`.`companyid` AS `companyid`,`menu_items`.`item_name` AS `item_name`,`menu_items`.`upc` AS `upc`,`menu_items`.`price` AS `price`,`menu_items`.`retail` AS `retail`,`menu_items`.`parent` AS `parent`,`menu_items`.`deleted` AS `deleted`,`menu_items`.`description` AS `description`,`menu_items`.`groupid` AS `groupid`,`menu_items`.`unit` AS `unit`,`menu_items`.`menu_typeid` AS `menu_typeid`,`menu_items`.`true_unit` AS `true_unit`,`menu_items`.`serving_size` AS `serving_size`,`menu_items`.`serving_size2` AS `serving_size2`,`menu_items`.`order_unit` AS `order_unit`,`menu_items`.`su_in_ou` AS `su_in_ou`,`menu_items`.`label` AS `label`,`menu_items`.`order_min` AS `order_min`,`menu_items`.`item_code` AS `item_code`,`menu_items`.`sales_acct` AS `sales_acct`,`menu_items`.`tax_acct` AS `tax_acct`,`menu_items`.`nontax_acct` AS `nontax_acct`,`menu_items`.`recipe` AS `recipe`,`menu_items`.`recipe_active` AS `recipe_active`,`menu_items`.`recipe_active_cust` AS `recipe_active_cust`,`menu_items`.`recipe_station` AS `recipe_station`,`menu_items`.`batch_recipe` AS `batch_recipe`,`menu_items`.`contract_option` AS `contract_option`,`menu_items`.`heart_healthy` AS `heart_healthy`,`menu_items`.`image` AS `image`,`menu_items`.`keeper` AS `keeper`,`district`.`districtname` AS `districtname`,`business`.`businessname` AS `businessname`,`menu_type`.`menu_typename` AS `menu_typename`,`servery_station`.`station_name` AS `station_name` from ((((`menu_items` left join `business` on((`menu_items`.`businessid` = `business`.`businessid`))) left join `district` on((`business`.`districtid` = `district`.`districtid`))) left join `menu_type` on((`menu_items`.`menu_typeid` = `menu_type`.`menu_typeid`))) left join `servery_station` on((`servery_station`.`stationid` = `menu_items`.`recipe_station`)));

-- --------------------------------------------------------

--
-- Structure for view `vPromoDays`
--
DROP TABLE IF EXISTS `vPromoDays`;

CREATE VIEW `vPromoDays` AS select 0 AS `day`,`psu`.`id` AS `id` from `promotions` `psu` where (`psu`.`sunday` = 1) union select 1 AS `day`,`pm`.`id` AS `id` from `promotions` `pm` where (`pm`.`monday` = 1) union select 2 AS `day`,`pt`.`id` AS `id` from `promotions` `pt` where (`pt`.`tuesday` = 1) union select 3 AS `day`,`pw`.`id` AS `id` from `promotions` `pw` where (`pw`.`wednesday` = 1) union select 4 AS `day`,`pth`.`id` AS `id` from `promotions` `pth` where (`pth`.`thursday` = 1) union select 5 AS `day`,`pf`.`id` AS `id` from `promotions` `pf` where (`pf`.`friday` = 1) union select 6 AS `day`,`ps`.`id` AS `id` from `promotions` `ps` where (`ps`.`saturday` = 1);

-- --------------------------------------------------------

--
-- Structure for view `vPromoVivipos`
--
DROP TABLE IF EXISTS `vPromoVivipos`;

CREATE VIEW `vPromoVivipos` AS select `promotions`.`businessid` AS `businessid`,`promotions`.`id` AS `promo_id`,`promo_target`.`id` AS `promo_target`,`promotions`.`name` AS `vivipos_name`,`promotions`.`pos_id` AS `pos_id`,`promotions`.`value` AS `value`,`promotions`.`rule_order` AS `rule_order`,`promotions`.`promo_reserve` AS `promo_reserve`,`promotions`.`promo_discount` AS `promo_discount`,`promotions`.`promo_discount_type` AS `promo_discount_type`,`promotions`.`promo_discount_n` AS `promo_discount_n`,`promotions`.`promo_discount_limit` AS `promo_discount_limit`,`promotions`.`promo_trigger_amount_limit` AS `promo_trigger_amount_limit`,`promotions`.`promo_trigger_amount_type` AS `promo_trigger_amount_type`,`promotions`.`promo_trigger_amount` AS `promo_trigger_amount`,`promotions`.`promo_trigger_amount_2` AS `promo_trigger_amount_2`,`promotions`.`promo_trigger_amount_3` AS `promo_trigger_amount_3`,coalesce(cast(`menu_tax`.`pos_id` as char charset utf8),'') AS `vivipos_taxno`,coalesce(`menu_tax`.`name`,'') AS `vivipos_taxname`,(unix_timestamp(`promotions`.`start_date`) - (3600 * `business`.`timezone`)) AS `vivipos_startdate`,(unix_timestamp(concat(`promotions`.`start_date`,' ',`promotions`.`start_time`)) - (3600 * `business`.`timezone`)) AS `vivipos_starttime`,((unix_timestamp(`promotions`.`end_date`) - (3600 * `business`.`timezone`)) + 59) AS `vivipos_enddate`,((unix_timestamp(concat(`promotions`.`start_date`,' ',`promotions`.`end_time`)) - (3600 * `business`.`timezone`)) + 59) AS `vivipos_endtime`,group_concat(`vPromoDays`.`day` order by `vPromoDays`.`day` ASC separator ',') AS `vivipos_days`,`promo_type`.`vivipos_name` AS `vivipos_type`,`promo_trigger`.`vivipos_name` AS `vivipos_trigger`,`promo_target`.`db_table` AS `promotarget_table`,`promo_target`.`order_by` AS `promotarget_order`,unix_timestamp(now()) AS `vivipos_modified`,`promotions`.`activeFlag` AS `vivipos_active`,0 AS `is_global`,`promotions`.`subsidy` AS `alt_name1` from ((((((`promotions` join `promo_type` on((`promotions`.`type` = `promo_type`.`id`))) join `promo_trigger` on((`promotions`.`promo_trigger` = `promo_trigger`.`id`))) join `promo_target` on((`promo_trigger`.`target_id` = `promo_target`.`id`))) left join `menu_tax` on((`promotions`.`tax_id` = `menu_tax`.`id`))) left join `vPromoDays` on((`promotions`.`id` = `vPromoDays`.`id`))) join `business` on((`business`.`businessid` = `promotions`.`businessid`))) group by `promotions`.`id` union select `promo_mapping`.`BusinessId` AS `businessid`,`promotions`.`id` AS `promo_id`,`promo_target`.`id` AS `promo_target`,`promotions`.`name` AS `vivipos_name`,`promotions`.`pos_id` AS `pos_id`,`promotions`.`value` AS `value`,`promotions`.`rule_order` AS `rule_order`,`promotions`.`promo_reserve` AS `promo_reserve`,`promotions`.`promo_discount` AS `promo_discount`,`promotions`.`promo_discount_type` AS `promo_discount_type`,`promotions`.`promo_discount_n` AS `promo_discount_n`,`promotions`.`promo_discount_limit` AS `promo_discount_limit`,`promotions`.`promo_trigger_amount_limit` AS `promo_trigger_amount_limit`,`promotions`.`promo_trigger_amount_type` AS `promo_trigger_amount_type`,`promotions`.`promo_trigger_amount` AS `promo_trigger_amount`,`promotions`.`promo_trigger_amount_2` AS `promo_trigger_amount_2`,`promotions`.`promo_trigger_amount_3` AS `promo_trigger_amount_3`,coalesce(cast(`menu_tax`.`pos_id` as char charset utf8),'') AS `vivipos_taxno`,coalesce(`menu_tax`.`name`,'') AS `vivipos_taxname`,(unix_timestamp(`promotions`.`start_date`) - (3600 * `business`.`timezone`)) AS `vivipos_startdate`,(unix_timestamp(concat(`promotions`.`start_date`,' ',`promotions`.`start_time`)) - (3600 * `business`.`timezone`)) AS `vivipos_starttime`,((unix_timestamp(`promotions`.`end_date`) - (3600 * `business`.`timezone`)) + 59) AS `vivipos_enddate`,((unix_timestamp(concat(`promotions`.`start_date`,' ',`promotions`.`end_time`)) - (3600 * `business`.`timezone`)) + 59) AS `vivipos_endtime`,group_concat(`vPromoDays`.`day` order by `vPromoDays`.`day` ASC separator ',') AS `vivipos_days`,`promo_type`.`vivipos_name` AS `vivipos_type`,`promo_trigger`.`vivipos_name` AS `vivipos_trigger`,`promo_target`.`db_table` AS `promotarget_table`,`promo_target`.`order_by` AS `promotarget_order`,unix_timestamp(now()) AS `vivipos_modified`,if((`promo_mapping`.`active` = 0),0,`promotions`.`activeFlag`) AS `vivipos_active`,1 AS `is_global`,`promotions`.`subsidy` AS `alt_name1` from (((((((`promotions` join `promo_type` on((`promotions`.`type` = `promo_type`.`id`))) join `promo_trigger` on((`promotions`.`promo_trigger` = `promo_trigger`.`id`))) join `promo_target` on((`promo_trigger`.`target_id` = `promo_target`.`id`))) left join `menu_tax` on((`promotions`.`tax_id` = `menu_tax`.`id`))) left join `vPromoDays` on((`promotions`.`id` = `vPromoDays`.`id`))) join `promo_mapping` on((`promo_mapping`.`PromotionId` = `promotions`.`id`))) join `business` on((`business`.`businessid` = `promo_mapping`.`BusinessId`))) group by `business`.`businessid`,`promotions`.`id`;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
