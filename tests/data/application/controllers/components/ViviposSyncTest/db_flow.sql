DROP DATABASE
CREATE DATABASE IF NOT EXISTS `mburris_businesstrack` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
SOURCE db_ee.sql;
SOURCE db_manage.sql;
SOURCE db_vivipos_order.sql;
SOURCE db_vivipos_order_tbl_data.sql;
SOURCE db_ee_data.sql;