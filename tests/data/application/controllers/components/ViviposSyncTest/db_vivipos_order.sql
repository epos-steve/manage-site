-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: vivipos
-- Generation Time: Jan 03, 2013 at 11:29 AM
-- Server version: 5.5.22
-- PHP Version: 5.3.10-1ubuntu3.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `vivipos_order`
--
CREATE DATABASE IF NOT EXISTS `vivipos_order` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `vivipos_order`;

-- --------------------------------------------------------

--
-- Table structure for table `cc_transactions`
--

DROP TABLE IF EXISTS `cc_transactions`;
CREATE TABLE IF NOT EXISTS `cc_transactions` (
  `id` varchar(36) NOT NULL,
  `cc_transaction_id` varchar(20) DEFAULT NULL,
  `time_stamp` int(15) DEFAULT NULL,
  `response_code` varchar(5) DEFAULT NULL,
  `response_text` varchar(20) DEFAULT NULL,
  `auth_code` varchar(30) DEFAULT NULL,
  `avsrsltcode` varchar(30) DEFAULT NULL,
  `ref_number` varchar(30) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `is_processed` tinyint(1) NOT NULL DEFAULT '0',
  `recordId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `time_stamp` (`time_stamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_item_taxes`
--

DROP TABLE IF EXISTS `order_item_taxes`;
CREATE TABLE IF NOT EXISTS `order_item_taxes` (
  `id` varchar(250) NOT NULL,
  `order_id` varchar(250) NOT NULL,
  `order_item_id` varchar(250) DEFAULT NULL,
  `promotion_id` varchar(250) DEFAULT NULL,
  `tax_no` varchar(250) NOT NULL,
  `tax_name` varchar(250) NOT NULL,
  `tax_type` varchar(250) NOT NULL,
  `tax_rate` float NOT NULL,
  `tax_rate_type` varchar(250) NOT NULL,
  `tax_threshold` float NOT NULL,
  `tax_subtotal` float DEFAULT NULL,
  `included_tax_subtotal` float DEFAULT NULL,
  `item_count` int(11) DEFAULT '0',
  `taxable_amount` float DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `order_item_taxes_oid` (`order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

DROP TABLE IF EXISTS `order_items`;
CREATE TABLE IF NOT EXISTS `order_items` (
  `id` varchar(250) NOT NULL,
  `order_id` varchar(250) DEFAULT NULL,
  `cate_no` varchar(250) DEFAULT NULL,
  `included_tax` float DEFAULT NULL,
  `cate_name` varchar(250) DEFAULT NULL,
  `product_no` varchar(250) DEFAULT NULL,
  `product_barcode` varchar(250) DEFAULT NULL,
  `product_name` varchar(250) DEFAULT NULL,
  `current_qty` float DEFAULT NULL,
  `current_price` float DEFAULT NULL,
  `current_subtotal` float DEFAULT NULL,
  `tax_name` varchar(250) DEFAULT NULL,
  `tax_rate` float DEFAULT NULL,
  `tax_type` varchar(250) DEFAULT NULL,
  `current_tax` float DEFAULT NULL,
  `discount_name` varchar(250) DEFAULT NULL,
  `discount_rate` float DEFAULT NULL,
  `discount_type` varchar(250) DEFAULT NULL,
  `current_discount` float DEFAULT NULL,
  `surcharge_name` varchar(250) DEFAULT NULL,
  `surcharge_rate` float DEFAULT NULL,
  `surcharge_type` varchar(250) DEFAULT NULL,
  `current_surcharge` float DEFAULT NULL,
  `condiments` varchar(250) DEFAULT NULL,
  `current_condiment` float DEFAULT NULL,
  `memo` text,
  `has_discount` tinyint(1) DEFAULT NULL,
  `has_surcharge` tinyint(1) DEFAULT NULL,
  `has_marker` tinyint(1) DEFAULT NULL,
  `created` int(11) DEFAULT NULL,
  `modified` int(11) DEFAULT NULL,
  `destination` varchar(250) DEFAULT NULL,
  `parent_no` varchar(250) DEFAULT NULL,
  `weight` float DEFAULT '0',
  `sale_unit` varchar(250) DEFAULT 'unit',
  `seat_no` varchar(250) DEFAULT NULL,
  `parent_index` varchar(250) DEFAULT NULL,
  `has_setitems` tinyint(1) DEFAULT '0',
  `stock_maintained` tinyint(1) DEFAULT NULL,
  `counter` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `order_items_order_id` (`order_id`),
  KEY `order_items_product_no` (`product_no`),
  KEY `modified` (`modified`,`current_tax`),
  KEY `modified_2` (`modified`),
  KEY `cate_no` (`cate_no`,`tax_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_payments`
--

DROP TABLE IF EXISTS `order_payments`;
CREATE TABLE IF NOT EXISTS `order_payments` (
  `id` varchar(250) NOT NULL,
  `order_id` varchar(250) DEFAULT NULL,
  `order_items_count` int(11) DEFAULT NULL,
  `order_total` float DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `memo1` text,
  `memo2` text,
  `created` int(11) DEFAULT NULL,
  `modified` int(11) DEFAULT NULL,
  `origin_amount` float DEFAULT NULL,
  `service_clerk` varchar(250) DEFAULT NULL,
  `proceeds_clerk` varchar(250) DEFAULT NULL,
  `service_clerk_displayname` varchar(250) DEFAULT NULL,
  `proceeds_clerk_displayname` varchar(250) DEFAULT NULL,
  `change` float DEFAULT NULL,
  `sale_period` int(11) DEFAULT NULL,
  `shift_number` int(11) DEFAULT NULL,
  `terminal_no` varchar(250) DEFAULT NULL,
  `is_groupable` tinyint(1) DEFAULT '0',
  `customer_card_number` varchar(30) DEFAULT NULL,
  `is_processed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `order_payments_oid` (`order_id`),
  KEY `order_payments_sp_tn_sn` (`sale_period`,`terminal_no`,`shift_number`),
  KEY `terminal_no` (`terminal_no`(10),`memo1`(25),`memo2`(25)),
  KEY `modified` (`modified`,`terminal_no`),
  KEY `customer_card_number` (`customer_card_number`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_promotions`
--

DROP TABLE IF EXISTS `order_promotions`;
CREATE TABLE IF NOT EXISTS `order_promotions` (
  `id` varchar(255) NOT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `promotion_id` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `alt_name1` varchar(255) DEFAULT NULL,
  `alt_name2` varchar(255) DEFAULT NULL,
  `trigger` varchar(255) DEFAULT NULL,
  `trigger_name` varchar(255) DEFAULT NULL,
  `trigger_label` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `type_name` varchar(255) DEFAULT NULL,
  `type_label` varchar(255) DEFAULT NULL,
  `matched_amount` int(11) DEFAULT NULL,
  `matched_items_qty` int(11) DEFAULT NULL,
  `matched_items_subtotal` float DEFAULT NULL,
  `discount_subtotal` float DEFAULT NULL,
  `tax_name` varchar(255) DEFAULT NULL,
  `current_tax` float DEFAULT NULL,
  `included_tax` float DEFAULT NULL,
  `created` int(11) DEFAULT NULL,
  `modified` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `modified` (`modified`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_receipts`
--

DROP TABLE IF EXISTS `order_receipts`;
CREATE TABLE IF NOT EXISTS `order_receipts` (
  `id` char(36) NOT NULL,
  `order_id` char(36) DEFAULT NULL,
  `sequence` varchar(16) DEFAULT NULL,
  `device` smallint(6) DEFAULT NULL,
  `created` int(11) DEFAULT NULL,
  `modified` int(11) DEFAULT NULL,
  `batch` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` varchar(36) NOT NULL,
  `sequence` varchar(250) DEFAULT NULL,
  `items_count` int(11) DEFAULT NULL,
  `total` float DEFAULT NULL,
  `change` float DEFAULT NULL,
  `tax_subtotal` float DEFAULT NULL,
  `surcharge_subtotal` float DEFAULT NULL,
  `discount_subtotal` float DEFAULT NULL,
  `payment_subtotal` float DEFAULT NULL,
  `rounding_prices` varchar(250) DEFAULT NULL,
  `precision_prices` varchar(250) DEFAULT NULL,
  `rounding_taxes` varchar(250) DEFAULT NULL,
  `precision_taxes` varchar(250) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `service_clerk` varchar(250) DEFAULT NULL,
  `service_clerk_displayname` varchar(250) DEFAULT NULL,
  `proceeds_clerk` varchar(250) DEFAULT NULL,
  `proceeds_clerk_displayname` varchar(250) DEFAULT NULL,
  `member` varchar(250) DEFAULT NULL,
  `member_displayname` varchar(250) DEFAULT NULL,
  `member_email` varchar(250) DEFAULT NULL,
  `member_cellphone` varchar(250) DEFAULT NULL,
  `invoice_type` varchar(250) DEFAULT NULL,
  `invoice_title` varchar(250) DEFAULT NULL,
  `invoice_no` varchar(250) DEFAULT NULL,
  `invoice_count` int(11) DEFAULT NULL,
  `destination` varchar(250) DEFAULT NULL,
  `table_no` int(11) DEFAULT NULL,
  `check_no` int(11) DEFAULT NULL,
  `no_of_customers` int(11) DEFAULT NULL,
  `terminal_no` varchar(250) DEFAULT NULL,
  `transaction_created` int(11) DEFAULT NULL,
  `transaction_submitted` int(11) DEFAULT NULL,
  `created` int(11) DEFAULT NULL,
  `modified` int(11) DEFAULT NULL,
  `included_tax_subtotal` float DEFAULT NULL,
  `item_subtotal` float DEFAULT NULL,
  `sale_period` int(11) DEFAULT NULL,
  `shift_number` int(11) DEFAULT NULL,
  `branch_id` varchar(250) DEFAULT NULL,
  `branch` varchar(250) DEFAULT NULL,
  `transaction_voided` int(11) DEFAULT NULL,
  `promotion_subtotal` float DEFAULT NULL,
  `void_clerk` varchar(250) DEFAULT NULL,
  `void_clerk_displayname` varchar(250) DEFAULT NULL,
  `void_sale_period` int(11) DEFAULT NULL,
  `void_shift_number` int(11) DEFAULT NULL,
  `revalue_subtotal` float DEFAULT '0',
  `qty_subtotal` int(11) DEFAULT '0',
  `inherited_order_id` varchar(250) DEFAULT NULL,
  `inherited_desc` varchar(250) DEFAULT NULL,
  `item_surcharge_subtotal` float DEFAULT '0',
  `trans_surcharge_subtotal` float DEFAULT '0',
  `item_discount_subtotal` float DEFAULT '0',
  `trans_discount_subtotal` float DEFAULT '0',
  `customer_card_number` varchar(20) DEFAULT NULL,
  `date_posted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `items_counter` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `order_member` (`member`),
  KEY `orders_check_no` (`check_no`),
  KEY `orders_created` (`transaction_created`),
  KEY `orders_sale_period` (`sale_period`),
  KEY `orders_sequence` (`sequence`),
  KEY `orders_status_created` (`status`,`created`),
  KEY `orders_status_sale_period` (`status`,`sale_period`),
  KEY `orders_status_transaction_created` (`status`,`transaction_created`),
  KEY `orders_status_transaction_submittedd` (`status`,`transaction_submitted`),
  KEY `orders_status_transaction_voided` (`status`,`transaction_voided`),
  KEY `orders_submitted` (`transaction_submitted`),
  KEY `orders_table_no` (`table_no`),
  KEY `orders_terminal_no` (`terminal_no`),
  KEY `modified` (`modified`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
