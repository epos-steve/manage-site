<?php
/**
 * Trait file for helper to make HTTP requests.
 *
 * @copyright  Copyright © 2012 Essential Elements, LLC (http://www.essential-elements.net)
 * @license    EE-Proprietary
 * @package    Manage\Application\Tests
 * @subpackage Tests\Traits
 */

/**
 * Part of Manage\Tests\Traits
 *
 * @category Manage\Tests\Traits;
 */
namespace Manage\Tests\Traits;

/**
 * Trait adds only requestHttp() method with class that is returned.
 *
 * Uses Curl.
 * @package    Manage\Application\Tests
 * @subpackage Tests\Traits
 */
trait Http
{
	/**
	 * Make HTTP request to test site.
	 * @param string $path URL path based off of base domain in config.
	 * @return \Manage\Tests\Traits\HttpResponse
	 */
	protected function requestHttp($path)
	{
		if ( !function_exists('curl_init') )
		{
			$this->markTestSkipped('cURL not installed.');
			return new HttpResponse("\r\n\r\n");
		}

		$curl = \curl_init();
		\curl_setopt($curl, \CURLOPT_URL, implode('/', array(\HTTP_TEST_SITE, ltrim($path, '/'))));
		\curl_setopt($curl, \CURLOPT_HEADER, 1);
		\curl_setopt($curl, \CURLOPT_RETURNTRANSFER, 1);

		$response = \curl_exec($curl);

		\curl_close($curl);

		return new HttpResponse($response);
	}
}

/**
 * Handles response by splitting contents into header and footer and processing.
 *
 * Helps with making processing HTTP requests.
 *
 * @package    Manage\Application\Tests
 * @subpackage Tests\Traits
 */
class HttpResponse
{
	protected $_http_version = 1.0;
	protected $_rawResponse = '';
	protected $_body = '';
	protected $_rawHeaders = '';
	protected $_headers = array();
	protected $_response = array(
		'code' => 0,
		'message' => '',
	);

	public function __construct($response)
	{
		list($headers, $body) = explode("\r\n\r\n", $response);
		$this->_body = $body;
		$this->_rawHeaders = $headers;
		$this->_processHeaders();
	}

	protected function _processHeaders()
	{
		$headers = explode("\r\n", $this->_rawHeaders);
		$lastHeader = '';
		$lastHeaderCount = 0;
		foreach ($headers as $header)
		{
			if (false !== strpos($header, ':'))
			{
				list($name, $value) = explode(':', $header, 2);
				$name = strtolower($name);
				if ( !array_key_exists($name, $this->_headers) )
				{
					$this->_headers[$name] = array();
				}
				$this->_headers[$name][] = trim($value);
				$lastHeaderCount = count($this->_headers[$name]) - 1;
				$lastHeader = $name;
			}
			else if ( preg_match('|http/([0-9.]{2,3})\s+([0-9]{3}\s+(.*))|i', $header, $response_line))
			{
				$this->_http_version = (float) $response_line[1];
				$this->_response['code'] = (int) $response_line[2];
				$this->_response['message'] = $response_line[3];
			}
			else
			{
				$this->_headers[$lastHeader][$lastHeaderCount] .= $value;
			}
		}
	}

	public function hasHeader($name)
	{
		return array_key_exists(strtolower($name), $this->_headers);
	}

	public function header($name)
	{
		if ( ! $this->hasHeader($name) )
		{
			return;
		}
		// I forget the spec for this, but I believe it is a ';' that is used.
		// However, it might simply be whitespace. Oh well.
		return implode('; ', $this->_headers[strtolower($name)]);
	}

	public function body()
	{
		return $this->_body;
	}

	public function responseCode()
	{
		return (int) $this->_response['code'];
	}

	public function responseMessage()
	{
		return (int) $this->_response['message'];
	}

	public function httpVersion()
	{
		return (float) $this->_http_version;
	}
}