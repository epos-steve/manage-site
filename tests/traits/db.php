<?php
/**
 * Trait file for helper to setting up DB connections with PHPUnit.
 *
 * @copyright  Copyright © 2012 Essential Elements, LLC (http://www.essential-elements.net)
 * @license    EE-Proprietary
 * @package    Manage\Application\Tests
 * @subpackage Tests\Traits
 */

/**
 * Part of Manage\Tests\Traits
 *
 * @category Manage\Tests\Traits;
 */
namespace Manage\Tests\Traits;


define('DATA_PATH', __DIR__.'/../data/');

trait SetupDatabase
{
	protected $_pdo_manage = null;

	protected $_pdo_businesstrack = null;

	protected function setupDatabases()
	{
		try {
			$this->_pdo_manage = new \PDO(
				'mysql:dbname=mburris_manage;host='.$GLOBALS['DB_TEST_HOST'],
				$GLOBALS['DB_TEST_USERNAME'],
				$GLOBALS['DB_TEST_PASSWORD']
			);
			$this->_pdo_manage->setAttribute(\PDO::ATTR_EMULATE_PREPARES, true);
		} catch(\PDOException $e) {
			throw new \Exception($e->getMessage());
		}

		try {
			$this->_pdo_businesstrack = new \PDO(
				'mysql:dbname=mburris_businesstrack;host='.$GLOBALS['DB_TEST_HOST'],
				$GLOBALS['DB_TEST_USERNAME'],
				$GLOBALS['DB_TEST_PASSWORD']
			);
			$this->_pdo_businesstrack->setAttribute(\PDO::ATTR_EMULATE_PREPARES, true);
		} catch(\PDOException $e) {
			throw new \Exception($e->getMessage());
		}
	}

	protected function loadManageDb($config = 'treat db')
	{
		\SiTech_Model_Abstract::db(\Manage_DB_Proxy::singleton($config));
	}

	protected function closeDatabases()
	{
		$this->_pdo_manage = null;
		$this->_pdo_businesstrack = null;
	}
}

/**
 * Trait Implements PHPUnit DbTestCase for Manage Database.
 *
 * Uses PDO_MYSQL.
 * @package    Manage\Application\Tests
 * @subpackage Tests\Traits
 */
trait DbConnectionManage
{
	private $connection = null;

	public function getConnection()
	{
		if ( preg_match('|192[.]168[.][0-9]{1,3}[.][0-9]{1,3}|', $GLOBALS['DB_TEST_HOST']) )
		{
			throw new Exception('Must not access 192.168.x.x databases. This will truncate data!');
		}
		if ( $this->connection === null )
		{
			if ( static::$pdo === null )
			{
				static::$pdo = new \PDO('mysql:dbname=mburris_manage;host='.$GLOBALS['DB_TEST_HOST'], $GLOBALS['DB_TEST_USERNAME'], $GLOBALS['DB_TEST_PASSWORD']);
				static::$pdo->exec('SET FOREIGN_KEY_CHECKS=0');
			}
			$this->connection = $this->createDefaultDBConnection(static::$pdo, 'mburris_manage');
		}
		return $this->connection;
	}
}

/**
 * Trait Implements PHPUnit DbTestCase for BusinessTrack Database.
 *
 * Uses PDO_MYSQL.
 * @package    Manage\Application\Tests
 * @subpackage Tests\Traits
 */
trait DbConnectionBusinessTrack
{
	private $connection = null;

	public function getConnection()
	{
		if ( preg_match('|192[.]168[.][0-9]{1,3}[.][0-9]{1,3}|', $GLOBALS['DB_TEST_HOST']) )
		{
			throw new Exception('Must not access 192.168.x.x databases. This will truncate data!');
		}
		if ( $this->connection === null )
		{
			if ( static::$pdo === null )
			{
				static::$pdo = new \PDO('mysql:dbname=mburris_businesstrack;host='.$GLOBALS['DB_TEST_HOST'], $GLOBALS['DB_TEST_USERNAME'], $GLOBALS['DB_TEST_PASSWORD']);
			}
			$this->connection = $this->createDefaultDBConnection(static::$pdo, 'mburris_businesstrack');
		}
		return $this->connection;
	}
}

/**
 * Trait implements getDataSet for YAML set in the class using the trait.
 *
 * Please set $_yaml_file, path is from data directory. For example, the
 * data/example/example.yml would only need to have 'example/example.yml' in the
 * class property.
 *
 * @package    Manage\Application\Tests
 * @subpackage Tests\Traits
 */
trait DbDataSetYaml
{
	public function getDataSet()
	{
		if ( '' === $this->_yaml_file )
		{
			$this->markTestSkipped('Missing Yaml file.');
			return;
		}
		return new \PHPUnit_Extensions_Database_DataSet_YamlDataSet(
			DATA_PATH . ltrim($this->_yaml_file, '/\\')
		);
	}

}
