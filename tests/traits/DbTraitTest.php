<?php
/**
 * Tests for application/controllers/kiosksync.php
 *
 * HTTP requests for test site, must not be run against main site!
 *
 * Each file tests different method or page. May also mock the main controller
 * and testing methods inside of the class.
 *
 * @copyright  Copyright © 2012 Essential Elements, LLC (http://www.essential-elements.net)
 * @license    EE-Proprietary
 * @package    Manage\Application\Tests
 * @subpackage Tests\Application\Controllers\KioskSync\Action
 */

/**
 * Part of Manage\Application\Tests
 *
 * @category Manage\Tests\Application\Controllers\Kiosksync\Action;
 */
namespace Manage\Tests\Traits;


/**
 * Tests for the preg_match in Db Trait to test IP addresses.
 *
 * @package    Manage\Application\Tests
 * @subpackage Tests\Traits
 */
class DbTraitTest extends \PHPUnit_Framework_TestCase
{

	/**
	 * @dataProvider matchIps
	 */
	public function testIpMatches($result, $ip)
	{
		$this->assertEquals($result, \preg_match('|192[.]168[.][0-9]{1,3}[.][0-9]{1,3}|', $ip));
	}

	public function matchIps()
	{
		return array(
			array(1, '192.168.2.2'),
			array(1, '192.168.1.50'),
			array(1, '192.168.1.51'),
			array(1, '192.168.1.52'),
			array(1, '192.168.1.160'),
			array(1, '192.168.1.37'),
			array(0, 'localhost'),
			array(0, 'dev.essential-elements.net'),
			array(0, '127.0.0.1'),
		);
	}


}

