<?php
/**
 * Tests for application/controllers/kiosksync.php
 *
 * HTTP requests for test site, must not be run against main site!
 *
 * Each file tests different method or page. May also mock the main controller
 * and testing methods inside of the class.
 *
 * NOTE: You must include the boostrap in the base tests directory to ensure
 * that the correct files and mockcontroller works.
 *
 * @copyright  Copyright © 2012 Essential Elements, LLC (http://www.essential-elements.net)
 * @license    EE-Proprietary
 * @package    Manage\Application\Tests
 * @subpackage Tests\Application\Controllers\KioskSync\Action
 */

/**
 * Part of Manage\Application\Tests
 *
 * @category Manage\Tests\Application\Controllers\Kiosksync\Action;
 */
namespace Manage\Tests\Application\Controllers\Kiosksync;


/**
 * Global Controller tests.
 *
 * @package    Manage\Application\Tests
 * @subpackage Tests\Application\Controllers\KioskSync
 *
 * @requires extension curl
 * @requires PHP 5.4
 * @requires PHPUnit 3.7
 */
class ViviposSyncTest extends \PHPUnit_Framework_TestCase
{
	protected $db = null;
	protected $_siblings = array();
	protected $start = null;
	protected $end = null;
	protected $client_programid = 0;

	public function __construct()
	{
		$this->db = new \Manage_DB('vivipos db');
		$this->start = strtotime('2013-01-02 00:00:00');
		$this->end = strtotime('2013-01-03 00:00:00');
	}

	public function setUp()
	{
		$sourcePath = array(
			\str_replace('/', \DIRECTORY_SEPARATOR, TEST_DATA_PATH),
			'application',
			'controllers',
			'components',
			'ViviposSyncTest',
		);
		$sqlFiles = array(
			'db_ee.sql',
			'db_manage.sql',
			'db_vivipos_order.sql',
			'db_vivipos_order_tbl_data.sql',
		);
		$this->db = new \Manage_DB('vivipos db');
		$this->db->exec('SET FOREIGN_KEY_CHECKS = 0');
		// Reload DB to prevent changes from interferring with each other.
		$basePath = implode(\DIRECTORY_SEPARATOR, $sourcePath).\DIRECTORY_SEPARATOR;
		foreach($sqlFiles as $filename)
		{
			echo \exec('mysql -h '.$GLOBALS['DB_TEST_HOST'].' --user='.$GLOBALS['DB_TEST_USERNAME'].' --pass='.$GLOBALS['DB_TEST_PASSWORD'].' < '.$basePath.$filename);
		}
		$this->db->exec('SET FOREIGN_KEY_CHECKS = 1');

		$this->db->exec('USE vivipos_order');
		$this->db->exec('UPDATE orders SET date_posted = FROM_UNIXTIME(modified) WHERE date_posted = NULL OR date_posted = \'\' OR date_posted < \'1970-01-01 00:00:00\'');
		// Make sure siblings is set.
		$statement = $this->db->query(
			'SELECT DISTINCT terminal_no FROM orders'
		);
		if($statement->rowCount() > 0)
		{
			$this->_siblings = implode(', ', array_map('intval', $statement->fetchAll(\PDO::FETCH_COLUMN, 0)));
		}
	}

	public function tearDown()
	{
		if ( $this->client_programid > 0 )
		{
			$dataFile = \SITECH_APP_PATH.'/files/'.$this->client_programid.'.old.txt';
			if ( file_exists($dataFile) ) {
				unlink($dataFile);
			}
			$dataFile = \SITECH_APP_PATH.'/files/'.$this->client_programid.'.txt';
			if ( file_exists($dataFile) ) {
				unlink($dataFile);
			}
		}
		$this->db = null;
	}

	/**
	 * Regression test
	 */
	public function testDetailCreationFileMatches()
	{
		require_once SITECH_APP_PATH . '/controllers/components/DetailCreation.php';
		$client = \ClientProgramsModel::get(373, true);
		$this->client_programid = $client->client_programid;
		$db = new \Manage_DB('vivipos db');

		$uri = new \SiTech_Uri('http://'.$GLOBALS['HTTP_TEST_SITE'].'/');

		ob_start();
		$detailCreation = new \DetailCreation;
		$this->assertTrue($detailCreation->setClient($client, $uri), 'Could not set client for detail creation.');
		$detailCreation->start = strtotime('2013-01-02 00:00:00');
		$detailCreation->end = strtotime('2013-01-03 00:00:00');

		$detailCreation->createHeaderSection();
		$detailCreation->createSalesSection();
		$detailCreation->createTaxesSection();
		$detailCreation->createTendersSection();
		$detailCreation->createDiscountSection();
		$detailCreation->createPaymentDetailSection();
		$detailCreation->createChecksSection();
		$detailCreation->createCheckDetailSection();
		$detailCreation->createDiscountDetailSection();
		// Finalize the data and send it
		$detailCreation->sendData();
		$contents = ob_get_contents();
		ob_end_clean();

		$fileBit = \FILE_IGNORE_NEW_LINES | \FILE_SKIP_EMPTY_LINES;
		$expectedFile = \TEST_DATA_PATH.'application/controllers/components/ViviposSyncTest/detailcreation.'.$client->client_programid.'.txt';
		if ( !file_exists($expectedFile) ) {
			$this->fail('Expected file does not exist. Please add.');
		}

		$actualFile = \SITECH_APP_PATH.'/files/'.$client->client_programid.'.old.txt';
		if ( !file_exists($expectedFile) ) {
			$actualFile = \SITECH_APP_PATH.'/files/'.$client->client_programid.'.txt';
			if ( !file_exists($expectedFile) ) {
				$this->fail('Detail creation file does not exist.'."\n\n{$contents}");
			}
		}

		$expected = file($expectedFile, $fileBit);
		$actual = file($actualFile, $fileBit);

		$this->assertSame($expected, $actual, 'Files are different.');
	}

	/**
	 * Regression test
	 */
	public function testDetailCreationDbSyncMatches()
	{
		require_once SITECH_APP_PATH . '/controllers/components/DetailCreation.php';
		$db = new \Manage_DB('vivipos db');
		$client = \ClientProgramsModel::get(373, true);

		$uri = new \SiTech_Uri('http://'.$GLOBALS['HTTP_TEST_SITE'].'/');

		ob_start();
		$detailCreation = new \DetailCreation;
		$this->assertTrue($detailCreation->setClient($client, $uri), 'Could not set client for detail creation.');
		$detailCreation->start = strtotime('2013-01-02 00:00:00');
		$detailCreation->end = strtotime('2013-01-03 00:00:00');

		$detailCreation->createHeaderSection();
		$detailCreation->createSalesSection();
		$detailCreation->createTaxesSection();
		$detailCreation->createTendersSection();
		$detailCreation->createDiscountSection();
		$detailCreation->createPaymentDetailSection();
		$detailCreation->createChecksSection();
		$detailCreation->createCheckDetailSection();
		$detailCreation->createDiscountDetailSection();
		// Finalize the data and send it
		$detailCreation->sendData();
		ob_end_clean();

		$db->exec('USE mburris_businesstrack');
		$statement = $db->query("
			SELECT
				check_number, bill_datetime, bill_posted
			FROM
				checks
			WHERE
				bill_datetime != bill_posted
				AND bill_datetime >= FROM_UNIXTIME(?)
				AND bill_datetime < FROM_UNIXTIME(?)
			",
			array(
				$detailCreation->start,
				$detailCreation->end
			)
		);
		$diff = $statement->fetchAll(\PDO::FETCH_ASSOC);

		$this->assertEquals(0, count($diff), 'Imported failed for these items: '. var_export($diff));
	}

}

