<?php
/**
 * Tests for application/controllers/kiosksync.php
 *
 * HTTP requests for test site, must not be run against main site!
 *
 * Each file tests different method or page. May also mock the main controller
 * and testing methods inside of the class.
 *
 * NOTE: You must include the boostrap in the base tests directory to ensure
 * that the correct files and mockcontroller works.
 *
 * @copyright  Copyright © 2012 Essential Elements, LLC (http://www.essential-elements.net)
 * @license    EE-Proprietary
 * @package    Manage\Application\Tests
 * @subpackage Tests\Application\Controllers\KioskSync\Action
 */

/**
 * Part of Manage\Application\Tests
 *
 * @category Manage\Tests\Application\Controllers\Kiosksync\Action;
 */
namespace Manage\Tests\Application\Controllers\Kiosksync;


/**
 * Global Controller tests.
 *
 * @package    Manage\Application\Tests
 * @subpackage Tests\Application\Controllers\KioskSync
 *
 * @requires extension curl
 * @requires PHP 5.4
 * @requires PHPUnit 3.7
 */
class DetailCreationTest extends \PHPUnit_Framework_TestCase
{
	protected $db = null;
	protected $_siblings = array();
	protected $start = null;
	protected $end = null;

	public function __construct()
	{
		$this->db = new \Manage_DB('vivipos db');
		$this->start = strtotime('2013-01-02 00:00:00');
		$this->end = strtotime('2013-01-03 00:00:00');
	}

	public function setUp()
	{
		$sourcePath = array(
			\str_replace('/', \DIRECTORY_SEPARATOR, TEST_DATA_PATH),
			'application',
			'controllers',
			'components',
			'ViviposSyncTest',
		);
		$sqlFiles = array(
			'db_ee.sql',
			'db_manage.sql',
			'db_vivipos_order.sql',
			'db_vivipos_order_tbl_data.sql',
		);
		$this->db = new \Manage_DB('vivipos db');
		// Reload DB to prevent changes from interferring with each other.
		$basePath = implode(\DIRECTORY_SEPARATOR, $sourcePath).\DIRECTORY_SEPARATOR;
		foreach($sqlFiles as $filename)
		{
			echo \exec('mysql -h '.$GLOBALS['DB_TEST_HOST'].' --user='.$GLOBALS['DB_TEST_USERNAME'].' --pass='.$GLOBALS['DB_TEST_PASSWORD'].' < '.$basePath.$filename);
		}
		$this->db->exec('USE vivipos_order');
		$this->db->exec('UPDATE orders SET date_posted = FROM_UNIXTIME(modified) WHERE date_posted = NULL OR date_posted = \'\' OR date_posted < \'1970-01-01 00:00:00\'');
		// Make sure siblings is set.
		$statement = $this->db->query(
			'SELECT DISTINCT terminal_no FROM orders'
		);
		if($statement->rowCount() > 0)
		{
			$this->_siblings = implode(', ', array_map('intval', $statement->fetchAll(\PDO::FETCH_COLUMN, 0)));
		}
	}

	public function tearDown()
	{
		$this->db = null;
	}

	/**
	 * @dataProvider sqlStatements
	 * @param string $sql
	 */
	public function testSqlStatementHasRows($sql = null)
	{
		if ( !$sql )
		{
			$this->fail('SQL does not have anything! '.var_export($sql, true));
		}
		$stmnt = $this->db->query(
			$sql,
			array(
				'start' => $this->start,
				'end' => $this->end
			)
		);

		$this->assertThat(
			$stmnt->rowCount(),
			$this->greaterThan(0),
			'SQL statement is invalid or date range has no results.'
		);
	}

	/**
	 * @see DetailCreation::createDiscountSection()
	 */
	public function testDiscountsCodeSqlStatementHasRows()
	{
		$date = $this->start;
		$dates = array();

		$stmntDiscounts = $this->db->query(
			'SELECT
				op.code AS ID,
				op.name AS NAME
			FROM
				order_promotions AS op
			JOIN orders AS o
				ON op.order_id = o.id
			WHERE
				o.date_posted >= FROM_UNIXTIME(:start)
				AND o.date_posted < FROM_UNIXTIME(:end)
				AND o.terminal_no IN ('.$this->_siblings.')
			GROUP BY op.code',
			array(
				'start' => $this->start,
				'end' => $this->end
			)
		);
		$discounts = $stmntDiscounts->fetchAll(\PDO::FETCH_ASSOC);

		$this->assertThat(
			count($discounts),
			$this->greaterThan(0),
			'There are no discounts for date range.'
		);

		while ($date < $this->end) {
			foreach ($discounts as &$discount) {
				$stmnt = $this->db->query(
					'SELECT
						ABS(ROUND(SUM(op.discount_subtotal), 2))
					FROM
						order_promotions AS op
					JOIN orders AS o
						ON op.order_id = o.id
					WHERE
						op.code = ?
						AND o.date_posted >= FROM_UNIXTIME(?)
						AND o.date_posted < FROM_UNIXTIME(?)
						AND o.terminal_no IN ('.$this->_siblings.')
					GROUP BY op.code',
					array(
						$discount['ID'],
						$date,
						strtotime('+1 day', $date)
					)
				);
				$discount[date('Y-m-d', $date)] = (double) $stmnt->fetchColumn();
			}

			$dates[] = date('Y-m-d', $date);
			$date = strtotime('+1 day', $date);
		}

		$this->assertThat(
			count($dates),
			$this->greaterThan(0),
			'No discount totals.'
		);
	}

	/**
	 * @see DetailCreation::createSalesSection()
	 */
	public function testSalesSqlStatementHasRows()
	{
		$menu_groups = array();
		$totals = array();
		$dates = array();

		$date1 = $this->start;
		$date2 = strtotime("+1 day", $date1);
		$counter = 1;

		$this->db->exec('
			UPDATE order_items
			SET
				cate_no = 1
			WHERE
				cate_no < 1
				AND tax_name != \'X\'
		');

		$statement = $this->db->prepare(
			'SELECT
				order_items.cate_no,
				order_items.cate_name,
				SUM(
					IF(
						order_items.sale_unit = \'lb\',
						order_items.current_subtotal + order_items.current_discount,
						order_items.current_price * order_items.current_qty + order_items.current_discount + order_items.current_condiment
					)
				) AS total
			FROM
				order_items
			JOIN orders
				ON orders.id = order_items.order_id
			WHERE
				orders.date_posted >= FROM_UNIXTIME(:start)
				AND orders.date_posted < FROM_UNIXTIME(:end)
				AND orders.terminal_no IN (' . $this->_siblings . ')
				AND orders.status = 1
			GROUP BY order_items.cate_no'
		);

		while($date2 <= $this->end) {
			$statement->execute(array(
				'start' => $date1,
				'end' => $date2
			));
			$result = $statement->fetchAll(\PDO::FETCH_ASSOC);

			$this->assertThat(
				count($result),
				$this->greaterThan(0),
				'No order items.'
			);

			foreach($result AS $line){
				$menu_groups[$line["cate_no"]] = $line["cate_name"];
				$totals[$line["cate_no"]]["$date1"] = $line["total"];
			}

			$dates[$counter] = $date1;
			$date1 = $date2;
			$date2 = strtotime("+1 day", $date1);
			$counter++;
		}

		$this->assertThat(
			count($dates),
			$this->greaterThan(0),
			'No dates.'
		);

		$this->assertThat(
			count($menu_groups),
			$this->greaterThan(0),
			'No menu groups.'
		);

		$this->assertThat(
			count($totals),
			$this->greaterThan(0),
			'No totals.'
		);
	}

	/**
	 * @see DetailCreation::createTaxesSection()
	 */
	public function testTaxesSqlStatementHasRows()
	{
		$taxes = array();
		$totals = array();
		$dates = array();

		$date1 = $this->start;
		$date2 = strtotime("+1 day", $date1);
		$counter = 1;

		$orderItemsStatement = $this->db->prepare(
			'SELECT
				order_items.tax_name,
				ROUND( SUM( order_items.current_tax ), 2) AS total
			FROM
				order_items
			JOIN orders
				ON orders.id = order_items.order_id
			WHERE
				orders.date_posted >= FROM_UNIXTIME(:start)
				AND orders.date_posted < FROM_UNIXTIME(:end)
				AND order_items.current_tax != 0
				AND orders.terminal_no IN (' . $this->_siblings . ')
				AND orders.status = 1
			GROUP BY order_items.tax_name'
		);

		$promosStatement = $this->db->prepare(
			'SELECT
				order_promotions.tax_name,
				ROUND( SUM( order_promotions.current_tax ), 2 ) AS total
			FROM
				order_promotions
			JOIN orders
				ON orders.id = order_promotions.order_id
			WHERE
				orders.date_posted >= FROM_UNIXTIME(:start)
				AND orders.date_posted < FROM_UNIXTIME(:end)
				AND orders.terminal_no IN ('.$this->_siblings.')
			GROUP BY order_promotions.tax_name'
		);

		while($date2 <= $this->end) {
			$timestamps = array(
				'start' => $date1,
				'end' => $date2
			);

			$orderItemsStatement->execute($timestamps);
			$orderItems = $orderItemsStatement->fetchAll(\PDO::FETCH_KEY_PAIR);

			$promosStatement->execute($timestamps);
			$promos = $promosStatement->fetchAll(\PDO::FETCH_KEY_PAIR);

			$counter2 = 1;

			foreach($orderItems AS $name => $total) {
				$taxes[$name] = "Tax Rate $counter2";
				if (isset($promos[$name])) {
					$total -= $promos[$name];
				}
				$totals[$name][$date1] = $total;
				$counter2++;
			}

			$dates[$counter] = $date1;
			$date1 = $date2;
			$date2 = strtotime("+1 day", $date1);
			$counter++;
		}

		$this->assertThat(
			count($dates),
			$this->greaterThan(0),
			'No dates.'
		);

		$this->assertThat(
			count($totals),
			$this->greaterThan(0),
			'No totals.'
		);
	}


	public function sqlStatements()
	{
		return array(
			'SELECT
				CAST(CONCAT(SUBSTRING(o.sequence, 2), o.terminal_no) AS UNSIGNED INTEGER) AS sequence,
				oi.product_no,
				oi.cate_no,
				oi.current_qty,
				IF(oi.sale_unit = \'lb\', oi.current_subtotal + oi.current_discount, oi.current_price + oi.current_discount + oi.current_condiment) AS current_price,
				IF(void_clerk IS NULL, 0, 1) AS is_void
			FROM
				order_items AS oi
			JOIN orders AS o
				ON oi.order_id = o.id
			WHERE
				o.date_posted >= FROM_UNIXTIME(:start)
				AND o.date_posted < FROM_UNIXTIME(:end)
				AND o.terminal_no IN (' . $this->_siblings . ')
				AND o.status = 1
			ORDER BY o.sequence'
		);
		return array(
			/** @see DetailCreation::createChecksSection() */
			array(
			'(SELECT
				CAST(CONCAT(SUBSTRING(sequence, 2), terminal_no) AS UNSIGNED INTEGER) AS sequence,
				0 AS session,
				table_no,
				0 AS seat,
				0 AS emp_no,
				no_of_customers,
				FROM_UNIXTIME(modified) AS transaction_created,
				total,
				tax_subtotal,
				payment_subtotal,
				0 AS sale_type,
				IF (status != 1, 1, 0) AS is_void,
				IF (customer_card_number = "null", "", customer_card_number) AS customer_card_number,
				terminal_no,
				date_posted
			FROM
				orders
			WHERE
				date_posted >= FROM_UNIXTIME(:start)
				AND date_posted < FROM_UNIXTIME(:end)
				AND terminal_no IN (' . $this->_siblings . ')
				AND status = 1)
			UNION ALL
			(SELECT
				CAST(CONCAT(SUBSTRING(FROM_UNIXTIME(op.modified, \'%Y%m%d%H%i%s\'), 2), op.terminal_no) AS UNSIGNED INTEGER) AS sequence,
				0 AS session,
				0 AS table_no,
				0 AS seat,
				0 AS emp_no,
				1 AS no_of_customers,
				FROM_UNIXTIME(op.modified) AS transaction_created,
				op.order_total AS total,
				0 AS tax_subtotal,
				op.amount AS payment_subtotal,
				0 AS sale_type,
				0 AS is_void,
				IF (op.customer_card_number = "null", "", op.customer_card_number) AS customer_card_number,
				op.terminal_no,
				date_posted
			FROM
				order_payments AS op
			LEFT JOIN cc_transactions AS cc
				ON op.order_id = cc.cc_transaction_id
			WHERE
				op.date_posted >= FROM_UNIXTIME(:start)
				AND op.date_posted < FROM_UNIXTIME(:end)
				AND op.terminal_no IN ('. $this->_siblings .')
				AND op.name IN (\'CR\', \'CA\'))
			ORDER BY sequence'
			),
			/** @see DetailCreation::createCheckDetailSection() */
			array(
			'SELECT
				CAST(CONCAT(SUBSTRING(o.sequence, 2), o.terminal_no) AS UNSIGNED INTEGER) AS sequence,
				oi.product_no,
				oi.cate_no,
				oi.current_qty,
				IF(oi.sale_unit = \'lb\', oi.current_subtotal + oi.current_discount, oi.current_price + oi.current_discount + oi.current_condiment) AS current_price,
				IF(void_clerk IS NULL, 0, 1) AS is_void
			FROM
				order_items AS oi
			JOIN orders AS o
				ON oi.order_id = o.id
			WHERE
				o.date_posted >= FROM_UNIXTIME(:start)
				AND o.date_posted < FROM_UNIXTIME(:end)
				AND o.terminal_no IN (' . $this->_siblings . ')
				AND o.status = 1
			ORDER BY o.sequence'
			),
			/** @see DetailCreation::createDiscountSection() */
			/*
			array(
			'SELECT
				op.code AS ID,
				op.name AS NAME
			FROM
				order_promotions AS op
			JOIN orders AS o
				ON op.order_id = o.id
			WHERE
				op.date_posted > FROM_UNIXTIME(:start)
				AND op.date_posted <= FROM_UNIXTIME(:end)
				AND o.terminal_no IN ('.$this->_siblings.')
			GROUP BY op.code'
			),
			*/
			/** @see DetailCreation::createDiscountDetailSection() */
			array(
			'SELECT
				CAST(CONCAT(SUBSTRING(o.sequence, 2), o.terminal_no) AS UNSIGNED INTEGER) AS sequence,
				op.code,
				op.discount_subtotal
			FROM
				order_promotions AS op
			JOIN orders AS o
				ON op.order_id = o.id
			WHERE
				o.date_posted >= FROM_UNIXTIME(:start)
				AND o.date_posted < FROM_UNIXTIME(:end)
				AND o.terminal_no IN (' . $this->_siblings . ')
			ORDER BY o.sequence'
			),
			/** @see DetailCreation::createPaymentDetailSection() */
			array(
			'(SELECT
				CAST(CONCAT(SUBSTRING(o.sequence, 2), o.terminal_no) AS UNSIGNED INTEGER) AS sequence,
				IF(
					op.name = \'CA\' OR op.name = \'cash\',
					1,
					IF(
						op.name = \'CC\',
						4,
						IF(
							op.name = \'CK\',
							14,
							IF(
								op.name = \'PD\',
								5,
								IF(
									op.name = \'CR\',
									6,
									IF(
										op.name = \'check\',
										8,
										IF(
											op.name = \'giftcard\',
											9,
											0
										)
									)
								)
							)
						)
					)
				) AS name,
				op.amount,
				op.change,
				0 AS tips,
				IF(
					CAST(oi.product_no AS SIGNED) > 0,
					oi.product_no,
					IF(
						CAST(op.customer_card_number AS SIGNED) > 0,
						op.customer_card_number,
						o.customer_card_number
					)
				) AS customer_card_number
			FROM
				order_payments AS op
			JOIN orders AS o
				ON op.order_id = o.id
			LEFT JOIN order_items AS oi
				ON op.order_id = oi.order_id
				AND oi.tax_name = :tax_name
			WHERE
				op.date_posted >= FROM_UNIXTIME(:start)
				AND op.date_posted < FROM_UNIXTIME(:end)
				AND o.terminal_no IN (' . $this->_siblings . '))
			UNION ALL
			(SELECT
				CAST(CONCAT(SUBSTRING(FROM_UNIXTIME(op.modified, \'%Y%m%d%H%i%s\'), 2), op.terminal_no) AS UNSIGNED INTEGER) AS sequence,
				IF(
					op.name = \'CA\' OR op.name = \'cash\',
					1,
					IF(
						op.name = \'CC\',
						4,
						IF(
							op.name = \'CK\',
							14,
							IF(
								op.name = \'PD\',
								5,
								IF(
									op.name = \'CR\',
									6,
									IF(
										op.name = \'check\',
										8,
										0
									)
								)
							)
						)
					)
				) AS name,
				op.amount,
				op.change,
				0 AS tips,
				IF(op.customer_card_number = "NULL", "", op.customer_card_number) AS customer_card_number
			FROM
				order_payments AS op
			WHERE
				op.date_posted >= FROM_UNIXTIME(:start)
				AND op.date_posted < FROM_UNIXTIME(:end)
				AND op.terminal_no IN ('.$this->_siblings.')
				AND op.name IN (\'CR\', \'CA\'))
			ORDER BY sequence'
			),
		);
	}

}

