<?php
/**
 * Tests for application/controllers/kiosksync.php
 *
 * HTTP requests for test site, must not be run against main site!
 *
 * Each file tests different method or page. May also mock the main controller
 * and testing methods inside of the class.
 *
 * NOTE: You must include the boostrap in the base tests directory to ensure
 * that the correct files and mockcontroller works.
 *
 * @copyright  Copyright © 2012 Essential Elements, LLC (http://www.essential-elements.net)
 * @license    EE-Proprietary
 * @package    Manage\Application\Tests
 * @subpackage Tests\Application\Controllers\KioskSync\Action
 */

/**
 * Part of Manage\Application\Tests
 *
 * @category Manage\Tests\Application\Controllers\Kiosksync\Action;
 */
namespace Manage\Tests\Application\Controllers\Kiosksync\Action;

/** Mock Controller */
require_once __DIR__.'/../MockController.php';

/**
 * Tests for checkRestart controller action, both HTTP and PHP.
 * 
 * Only been able to get tests to work with businesstrack database. Cattrack and
 * others fail. Not sure what setup is required, but it is discerning to say the
 * least.
 *
 * @package    Manage\Application\Tests
 * @subpackage Tests\Application\Controllers\KioskSync
 *
 * @requires extension curl
 * @requires PHP 5.4
 * @requires PHPUnit 3.7
 */
class CheckRestartTest extends \PHPUnit_Framework_TestCase
{
	use \Manage\Tests\Traits\Http;
	use \Manage\Tests\Traits\SetupDatabase;

	protected $_prepared_restart_needed = null;
	protected $_prepared_no_restart = null;
	protected $_prepared_update_revert = null;

	public function setUp()
	{
		$this->setupDatabases();
		$this->_prepared_restart_needed = $this->_pdo_manage->prepare(
			'UPDATE client_programs SET send_status = 3 WHERE client_programid = ?'
		);
		$this->_prepared_no_restart = $this->_pdo_manage->prepare(
			'UPDATE client_programs SET send_status = 0 WHERE client_programid = ?'
		);
		$this->_prepared_update_revert = $this->_pdo_manage->prepare(
			'UPDATE client_programs SET send_status = ? WHERE client_programid = ?'
		);
	}

	/**
	 * @covers KiosksyncController::checkRestart
	 */
	public function testResponseMessageHasCorrectHeaders()
	{
		$statement = $this->_pdo_manage->query(
			'SELECT client_programid FROM client_programs WHERE `db_name` = \'mburris_businesstrack\' LIMIT 1'
		);
		if ( $statement->rowCount() < 1 )
		{
			$this->fail('There are no rows in test table.');
			return;
		}
		
		$client_programid = $statement->fetch(\PDO::FETCH_OBJ)->client_programid;
		$response = $this->requestHttp('/kiosksync/checkRestart/'.$client_programid);
		$this->assertEquals(
			200,
			$response->responseCode(),
			sprintf("Response returned %d, instead of 200.\n\nBody:\n%s", $response->responseCode(), $response->body())
		);
		$this->assertTrue($response->hasHeader('content-type'), 'Missing content-type header message.');
		$this->assertEquals(
			'text/plain; charset=utf-8',
			$response->header('content-type'),
			sprintf('Content type "%s" is not "text/plain; charset=utf-8".', $response->header('content-type'))
		);
	}

	/**
	 * @depends testResponseMessageHasCorrectHeaders
	 * @covers KiosksyncController::checkRestart
	 * @dataProvider provider
	 */
	public function testResponseNeedsRestart($id, $current)
	{
		$this->_prepared_restart_needed->execute(array($id));
		$expected = '1';
		$response = $this->requestHttp('/kiosksync/checkRestart/' . (int)$id);
		$this->assertEquals($expected, $response->body(), 'Response has changed or restart is not needed');
		$this->_prepared_update_revert->execute(array($current, $id));
	}

	/**
	 * @depends testResponseMessageHasCorrectHeaders
	 * @covers KiosksyncController::checkRestart
	 * @dataProvider provider
	 */
	public function testResponseDoesNotNeedRestart($id, $current)
	{
		$this->_prepared_no_restart->execute(array($id));
		$expected = '0';
		$response = $this->requestHttp('/kiosksync/checkRestart/' . (int)$id);
		$this->assertEquals($expected, $response->body(), 'Response has changed or restart is not needed');
		$this->_prepared_update_revert->execute(array($current, $id));
	}

	public function provider()
	{
		$this->setupDatabases();
		$statement = $this->_pdo_manage->query(
			'SELECT client_programid, send_status FROM client_programs WHERE `db_name` = \'mburris_businesstrack\' LIMIT 5'
		);
		if ( $statement->rowCount() < 1 )
		{
			// The client_programid value 2 exists right... right?
			return array(array(2, 0));
		}
		$result = $statement->fetchAll(\PDO::FETCH_OBJ);
		$values = array();
		foreach($result as $object)
		{
			$values[] = array(intval($object->client_programid), intval($object->send_status));
		}
		return $values;
	}

}
