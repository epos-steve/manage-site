<?php
/**
 * Tests for application/controllers/kiosksync.php
 *
 * HTTP requests for test site, must not be run against main site!
 *
 * Each file tests different method or page. May also mock the main controller
 * and testing methods inside of the class.
 *
 * NOTE: You must include the boostrap in the base tests directory to ensure
 * that the correct files and mockcontroller works.
 *
 * @copyright  Copyright © 2012 Essential Elements, LLC (http://www.essential-elements.net)
 * @license    EE-Proprietary
 * @package    Manage\Application\Tests
 * @subpackage Tests\Application\Controllers\KioskSync\Action
 */

/**
 * Part of Manage\Application\Tests
 *
 * @category Manage\Tests\Application\Controllers\Kiosksync\Action;
 */
namespace Manage\Tests\Application\Controllers\Kiosksync\Action;

/** Mock Controller */
require_once __DIR__.'/../MockController.php';

/**
 * Tests for cashier_users Controller action and getCashiers controller method.
 *
 * Uses the mock controller and assumes that Models have tests (which may not be
 * true).
 *
 * @requires extension curl
 * @requires PHP 5.4
 * @requires PHPUnit 3.7
 */
class CashiersTest extends \PHPUnit_Framework_TestCase
{
	use \Manage\Tests\Traits\Http;
	use \Manage\Tests\Traits\SetupDatabase;

	static protected $_select_cashier_users = null;

	public function setUp()
	{
		$this->setupDatabases();
		if ( null === static::$_select_cashier_users)
		{
			static::$_select_cashier_users = $this->_pdo_businesstrack->prepare(
				'
				SELECT 
					`username`, `password`, `description`, `groupId`
				FROM `vCashierUsers`
				WHERE businessid = ?
				'
			);
		}
	}

	public function tearDown() {
		$this->closeDatabases();
		parent::tearDown();
	}

	/**
	 * @covers KiosksyncController::cashier_users
	 * @dataProvider hasUsersProvider
	 */
	public function testHasCashierUsersResponse($client_programid, $businessid)
	{
		$response = $this->requestHttp('/kiosksync/cashier_users/'.$client_programid);
		$this->assertEquals(
			200,
			$response->responseCode(),
			sprintf("Response returned %d, instead of 200.\n\nBody:\n%s", $response->responseCode(), $response->body())
		);
		$this->assertTrue($response->hasHeader('content-type'), 'Missing content-type header message.');
		$this->assertEquals(
			'application/json; charset=utf-8',
			$response->header('content-type'),
			sprintf("Content type '%s' is not 'application/json; charset=utf-8'.\n\n%s", $response->header('content-type'), $response->body())
		);

		static::$_select_cashier_users->execute(array($businessid));
		$expected = json_encode(static::$_select_cashier_users->fetchAll(\PDO::FETCH_ASSOC));
		$this->assertEquals($expected, $response->body(), 'Response has changed');
	}

	/**
	 * @covers KiosksyncController::cashier_users
	 * @dataProvider lacksUsersProvider
	 */
	public function testLacksCashierUsersResponse($client_programid, $businessid)
	{
		$response = $this->requestHttp('/kiosksync/cashier_users/'.$client_programid);
		$this->assertEquals(
			200,
			$response->responseCode(),
			sprintf("Response returned %d, instead of 200. Body:\n\n%s", $response->responseCode(), $response->body())
		);
		$this->assertTrue($response->hasHeader('content-type'), 'Missing content-type header message.');
		$this->assertEquals(
			'application/json; charset=utf-8',
			$response->header('content-type'),
			sprintf("Content type '%s' is not 'application/json; charset=utf-8'.\n\n%s", $response->header('content-type'), $response->body())
		);

		static::$_select_cashier_users->execute(array($businessid));
		$expected = json_encode(static::$_select_cashier_users->fetchAll(\PDO::FETCH_ASSOC));
		$this->assertEquals($expected, $response->body(), 'Response has changed');
	}

	public function hasUsersProvider()
	{
		$this->setupDatabases();
		$statement = $this->_pdo_manage->query(
			"
			SELECT
				client_programid, businessid
			FROM client_programs
			WHERE
				businessid IN (
					SELECT businessid
					FROM `mburris_businesstrack`.`vCashierUsers`
					GROUP BY `mburris_businesstrack`.`vCashierUsers`.`businessid`
					HAVING
						COUNT(`mburris_businesstrack`.`vCashierUsers`.`businessid`) > 2
				)
				AND client_programid > 0
				AND `db_name` = 'mburris_businesstrack'
				AND `db_ip` = '192.168.2.2'
			"
		);

		if ( !$statement || $statement->rowCount() < 1 )
		{
			// The client_programid value 7 exists right... right?
			return array(array(7, 36));
		}
		$result = $statement->fetchAll(\PDO::FETCH_OBJ);
		$values = array();
		foreach($result as $object)
		{
			$values[] = array(intval($object->client_programid), intval($object->businessid));
		}
		return $values;
	}

	public function lacksUsersProvider()
	{
		$this->setupDatabases();
		$statement = $this->_pdo_manage->query(
			"
			SELECT
				client_programid, businessid
			FROM client_programs
			LEFT JOIN `mburris_businesstrack`.`vCashierUsers` USING( `businessid` )
			WHERE 
				client_programid > 0
				AND `db_name` = 'mburris_businesstrack'
				AND `db_ip` = '192.168.2.2'
			GROUP BY client_programs.client_programid
			HAVING COUNT(`mburris_businesstrack`.`vCashierUsers`.`businessid`) < 1
			LIMIT 5
			"
		);
		if ( !$statement || $statement->rowCount() < 1 )
		{
			// The client_programid value 2 exists right... right?
			return array(array(2, 312));
		}
		$result = $statement->fetchAll(\PDO::FETCH_OBJ);
		$values = array();
		foreach($result as $object)
		{
			$values[] = array(intval($object->client_programid), intval($object->businessid));
		}
		return $values;
	}

}

