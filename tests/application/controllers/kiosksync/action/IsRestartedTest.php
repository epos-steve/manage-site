<?php
/**
 * Tests for application/controllers/kiosksync.php
 *
 * HTTP requests for test site, must not be run against main site!
 *
 * Each file tests different method or page. May also mock the main controller
 * and testing methods inside of the class.
 *
 * NOTE: You must include the boostrap in the base tests directory to ensure
 * that the correct files and mockcontroller works.
 *
 * @copyright  Copyright © 2012 Essential Elements, LLC (http://www.essential-elements.net)
 * @license    EE-Proprietary
 * @package    Manage\Application\Tests
 * @subpackage Tests\Application\Controllers\KioskSync\Action
 */

/**
 * Part of Manage\Application\Tests
 *
 * @category Manage\Tests\Application\Controllers\Kiosksync\Action;
 */
namespace Manage\Tests\Application\Controllers\Kiosksync\Action;

/** Mock Controller */
require_once __DIR__.'/../MockController.php';

/**
 * Tests isRestarted controller action.
 *
 * The action does not return any body describing whether or not the action
 * succeeds or fails. Would have to test the database.
 *
 * @package    Manage\Application\Tests
 * @subpackage Tests\Application\Controllers\KioskSync
 *
 * @requires extension curl
 * @requires PHP 5.4
 * @requires PHPUnit 3.7
 */
class IsRestartedTest extends \PHPUnit_Framework_TestCase
{
	use \Manage\Tests\Traits\Http;
	use \Manage\Tests\Traits\SetupDatabase;

	protected $_prepared_restart_needed = null;
	protected $_prepared_test = null;
	protected $_prepared_restore = null;

	public function setUp()
	{
		$this->setupDatabases();
		$this->_prepared_restart_needed = $this->_pdo_manage->prepare(
			'UPDATE client_programs SET send_status = 3 WHERE client_programid = ?'
		);
		$this->_prepared_test = $this->_pdo_manage->prepare(
			'SELECT send_status, receive_status FROM client_programs WHERE client_programid = ?'
		);
		$this->_prepared_restore = $this->_pdo_manage->prepare(
			'UPDATE client_programs SET send_status = ?, receive_status = ?, receive_time = ? WHERE client_programid = ?'
		);
	}

	/**
	 * @covers KiosksyncController::isRestarted
	 * @dataProvider provider
	 */
	public function testIsRestartedDatabaseUpdates($client_programid, $send_status, $receive_status, $receive_time)
	{
		// Set restarted is needed.
		$this->_prepared_restart_needed->execute(array($client_programid));

		// Test headers.
		$response = $this->requestHttp('/kiosksync/isRestarted/'.$client_programid);
		$this->assertEquals(
			200,
			$response->responseCode(),
			sprintf("Response returned %d, instead of 200.\n\nBody:\n%s", $response->responseCode(), $response->body())
		);
		$this->assertTrue($response->hasHeader('content-type'), 'Missing content-type header message.');
		$this->assertEquals(
			'application/json; charset=utf-8',
			$response->header('content-type'),
			sprintf('Content type "%s" is not "text/plain; charset=utf-8".', $response->header('content-type'))
		);

		// Test database for changes.
		$this->_prepared_test->execute(array($client_programid));
		$result = $this->_prepared_test->fetch(\PDO::FETCH_OBJ);
		$this->assertEquals(0, intval($result->send_status), 'Send_status was not updated.');
		$this->assertEquals(5, intval($result->receive_status), 'Receive_status was not updated.');

		// Revert changes to development database.
		$this->_prepared_restore->execute(array($send_status, $receive_status, $receive_time, $client_programid));
	}

	public function provider()
	{
		$this->setupDatabases();
		$statement = $this->_pdo_manage->query(
			'SELECT client_programid, send_status, receive_status, receive_time FROM client_programs LIMIT 5'
		);
		if ( $statement->rowCount() < 1 )
		{
			// The client_programid value 2 exists right... right?
			return array(array(2, 0));
		}
		$result = $statement->fetchAll(\PDO::FETCH_OBJ);
		$values = array();
		foreach($result as $object)
		{
			$values[] = array(
				intval($object->client_programid),
				intval($object->send_status),
				intval($object->receive_status),
				$object->receive_time,
			);
		}
		return $values;
	}

}

