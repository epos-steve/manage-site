<?php
/**
 * Tests for application/controllers/kiosksync.php
 *
 * HTTP requests for test site, must not be run against main site!
 *
 * Each file tests different method or page. May also mock the main controller
 * and testing methods inside of the class.
 *
 * NOTE: You must include the boostrap in the base tests directory to ensure
 * that the correct files and mockcontroller works.
 *
 * @copyright  Copyright © 2012 Essential Elements, LLC (http://www.essential-elements.net)
 * @license    EE-Proprietary
 * @package    Manage\Application\Tests
 * @subpackage Tests\Application\Controllers\KioskSync
 */

/**
 * Part of Manage\Application\Tests
 *
 * @category Manage\Tests\Application\Controllers\Kiosksync;
 */
namespace Manage\Tests\Application\Controllers\Kiosksync;

/** Mock Controller */
require_once __DIR__.'/MockController.php';

/**
 * Tests Mock Controller.
 *
 * @package    Manage\Application\Tests
 * @subpackage Tests\Application\Controllers\KioskSync
 *
 * @requires extension curl
 * @requires PHP 5.4
 * @requires PHPUnit 3.7
 */
class ControllerTest extends \PHPUnit_Framework_TestCase
{
	use \Manage\Tests\Traits\Http;
	use \Manage\Tests\Traits\SetupDatabase;

	public function testCompleteTestCases()
	{
		$this->markTestIncomplete();
	}


}

