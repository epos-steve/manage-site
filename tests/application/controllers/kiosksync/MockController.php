<?php
/**
 * Stub Double Controller for application/controllers/kiosksync.php
 *
 * HTTP requests for test site, must not be run against main site!
 *
 * Each file tests different method or page. May also mock the main controller
 * and testing methods inside of the class.
 *
 * @copyright  Copyright © 2012 Essential Elements, LLC (http://www.essential-elements.net)
 * @license    EE-Proprietary
 * @package    Manage\Application\Tests
 * @subpackage Tests\Application\Controllers\KioskSync
 */

/**
 * Part of Manage\Application\Tests
 *
 * @category Manage\Tests\Application\Controllers\Kiosksync;
 */
namespace Manage\Tests\Application\Controllers\Kiosksync;

/** Kiosksync Controller for application. */
require_once \TEST_APPLICATION_PATH.'/controllers/kiosksync.php';

class KiosksyncControllerMock extends \KiosksyncController
{
	public function dataDefinition()
	{
		return static::$dataDefinition;
	}

	public function getData( $type )
	{
		return parent::getData($type);
	}

	public function getLocation()
	{
		return parent::getLocation();
	}

	public function init()
	{
		return parent::init();
	}

	public function _hasMenuGroups()
	{
		return parent::_hasMenuGroups();
	}

	public function _hasMenuItems()
	{
		return parent::_hasMenuItems();
	}

	public function _hasMenuItemTaxes()
	{
		return parent::_hasMenuItemTaxes();
	}

	public function _hasPromotions()
	{
		return parent::_hasPromotions();
	}

	public function _hasPromotionGroups()
	{
		return parent::_hasPromotionGroups();
	}

	public function _hasTaxTypes()
	{
		return parent::_hasTaxTypes();
	}

	public function _hasRunScripts( )
	{
		return parent::_hasRunScripts();
	}

	public function _hasUserChanges()
	{
		return parent::_hasUserChanges();
	}

	public function _hasCashiers()
	{
		return parent::_hasCashiers();
	}

	public function _hasLocation()
	{
		return parent::_hasLocation();
	}

	public function _hasPosSettings()
	{
		return parent::_hasPosSettings();
	}

	public function _hasModifierGroups()
	{
		return parent::_hasModifierGroups();
	}

	public function _hasModifiers()
	{
		return parent::_hasModifiers();
	}
}