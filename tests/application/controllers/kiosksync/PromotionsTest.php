<?php
/**
 * Tests for application/controllers/kiosksync.php
 *
 * HTTP requests for test site, must not be run against main site!
 *
 * Each file tests different method or page. May also mock the main controller
 * and testing methods inside of the class.
 *
 * NOTE: You must include the boostrap in the base tests directory to ensure
 * that the correct files and mockcontroller works.
 *
 * @copyright  Copyright © 2012 Essential Elements, LLC (http://www.essential-elements.net)
 * @license    EE-Proprietary
 * @package    Manage\Application\Tests
 * @subpackage Tests\Application\Controllers\KioskSync
 */

/**
 * Part of Manage\Application\Tests
 *
 * @category Manage\Tests\Application\Controllers\Kiosksync;
 */
namespace Manage\Tests\Application\Controllers\Kiosksync;

/** Mock Controller */
require_once __DIR__.'/MockController.php';

/**
 * Tests
 *
 * @package    Manage\Application\Tests
 * @subpackage Tests\Application\Controllers\KioskSync
 *
 * @requires extension curl
 * @requires PHP 5.4
 * @requires PHPUnit 3.7
 */
class PromotionsTest extends \PHPUnit_Framework_TestCase
{
	use \Manage\Tests\Traits\Http;
	use \Manage\Tests\Traits\SetupDatabase;

	static protected $displayedActivated = false;
	static protected $displayedDeactivated = false;

	protected $currentBusinesses = array();
	protected $promotion = null;

	public function setUp()
	{
		$this->setupDatabases();
		$this->loadManageDb();
		$this->currentBusinesses = array();
		$this->promotion = null;
	}

	public function tearDown()
	{
		if ( ! is_null( $this->promotion ) )
		{
			$this->promotion->setAppliesTo($this->currentBusinesses);
		}
	}

	/**
	 * Runs through the normal activate business for global promotions.
	 *
	 * Does not use web driver or tests web interface for managing global
	 * promotions. All actions are handled through the normal Treat Models for
	 * handling what the web interface would normally. Not the best solution
	 * since changes to the web interface or Treat models could adversely affect
	 * this test.
	 *
	 * Steps:
	 *     1. Activate random business associated with global promotion for a
	 *        given client and add to kiosksync.
	 *     2. Test promotion display for whether it shows up.
	 *     3. Cleanup.
	 *
	 * @dataProvider providerPromotions
	 */
	public function testActivateBusinessPromotionsNormalResponse($promotionId)
	{
		$this->loadManageDb();
		\Treat_Model_ClientPrograms::db(\Manage_DB::singleton('database'));

		$promotion = \Treat_Model_Promotion::get($promotionId, true);
		$this->promotion = $promotion;
		$businesses = \Treat_Model_Business::get('categoryid = '.\Treat_Model_Business::CATEGORY_KIOSK);
		$active = \Treat_Model_Promotion_Businesses::getColumn("BusinessId", "PromotionId = {$promotionId}");

		$inactive_businesses = array_values(
			array_filter(
				$businesses,
				function ($business) use($active) {
					return !in_array($business->businessid, $active);
				}
			)
		);

		if ( !is_array($inactive_businesses) || count($inactive_businesses) < 1 )
		{
			$this->markTestSkipped('No inactive businesses to set active.');
			return;
		}

		$set_active_businesses = $promotion->getAppliesTo();
		$this->currentBusinesses = $set_active_businesses;
		$client_programids = array();
		if ( count($inactive_businesses) < 5 )
		{
			foreach($inactive_businesses as $business)
			{
				$client_programid = intval(\Treat_Model_ClientPrograms::getColumn('client_programid', "businessid = {$business->businessid}"));
				if (count($client_programid) > 0)
				{
					$set_active_businesses[] = $business->businessid;
					$client_programids = array_merge($client_programids, $client_programid);
				}
			}
		}
		else
		{
			$activationLimitAmount = 5;
			foreach($inactive_businesses as $business)
			{
				if ($activationLimitAmount <= 0)
				{
					break;
				}
				if (false == mt_rand(0, 1))
				{
					continue;
				}

				$client_programid = \Treat_Model_ClientPrograms::getColumn('client_programid', "businessid = {$business->businessid}");
				if (count($client_programid) > 0)
				{
					$set_active_businesses[] = $business->businessid;
					$client_programids = array_merge($client_programids, $client_programid);
					$activationLimitAmount--;
				}
			}
		}

		$promotion->setAppliesTo($set_active_businesses);

		if (count($client_programids) < 1)
		{
			$this->markTestSkipped('Does not have any kiosks attached to business.');
			return;
		}

		foreach($client_programids as $client_programid)
		{
			$client = \ClientProgramsModel::get((int)$client_programid, true);
			$response = $this->requestHttp('/kiosksync/promotions/'.$client_programid.'?debug=true');
			$expectedData = \Treat_Model_KioskSync::getPromotions($client, false);
			// Fix for modified time. Hack shouldn't be needed, but it is
			// throwing failures simply, because the modified timestamp changed.
			$responseData = json_decode($response->body());

			foreach($responseData as $index => $responseDataItem)
			{
				if(!array_key_exists($index, $expectedData))
				{
					$this->fail('Response does not have the same number of items.');
					return;
				}
				$expectedData[$index]['vivipos_modified'] = $responseDataItem->vivipos_modified;
			}

			$expected = utf8_json_encode($expectedData);

			$this->assertEquals(
				200,
				$response->responseCode(),
				sprintf("Response returned %d, instead of 200.\n\nBody:\n%s", $response->responseCode(), $response->body())
			);
			$this->assertTrue($response->hasHeader('content-type'), 'Missing content-type header message.');
			$this->assertEquals(
				'application/json; charset=utf-8',
				$response->header('content-type'),
				sprintf('Content type "%s" is not "application/json; charset=utf-8".', $response->header('content-type'))
			);
			$this->assertSame($expected, $response->body(), sprintf('JSON Response does not match.'));
		}
	}

	/**
	 * Runs through the normal activate business for global promotions.
	 *
	 * Does not use web driver or tests web interface for managing global
	 * promotions. All actions are handled through the normal Treat Models for
	 * handling what the web interface would normally. Not the best solution
	 * since changes to the web interface or Treat models could adversely affect
	 * this test.
	 *
	 * Steps:
	 *     1. Deactivate random business associated with global promotion for a
	 *        given client and add to kiosksync.
	 *     2. Test promotion display for whether it shows up.
	 *     3. Cleanup.
	 *
	 * @dataProvider providerPromotions
	 */
	public function testDeactivateBusinessPromotionsNormalResponse($promotionId)
	{
		$this->loadManageDb();
		\Treat_Model_ClientPrograms::db(\Manage_DB::singleton('database'));

		$promotion = \Treat_Model_Promotion::get($promotionId, true);
		$set_deactivate_businesses = $promotion->getAppliesTo();
		$this->promotion = $promotion;
		$this->currentBusinesses = $set_deactivate_businesses;

		if ( !is_array($set_deactivate_businesses) || count($set_deactivate_businesses) < 1 )
		{
			$this->markTestSkipped('No active businesses to set inactive.');
			return;
		}

		$client_programids = array();
		$activationLimitAmount = 5;
		foreach($set_deactivate_businesses as $index => $businessid)
		{
			if ($activationLimitAmount <= 0)
			{
				break; // Limit the amount of deactivations for testing.
			}
			if (false == mt_rand(0, 1))
			{
				continue;
			}
			$client_programid = \Treat_Model_ClientPrograms::getColumn('client_programid', "businessid = {$businessid}");
			if (count($client_programid) > 0)
			{
				unset($set_deactivate_businesses[$index]);
				$client_programids = array_merge($client_programids, $client_programid);
				$activationLimitAmount--;
			}
		}

		$promotion->setAppliesTo($set_deactivate_businesses);

		if (count($client_programids) < 1)
		{
			$this->markTestSkipped('Does not have any kiosks attached to business.');
			return;
		}

		foreach($client_programids as $client_programid)
		{
			$client = \ClientProgramsModel::get((int)$client_programid, true);
			$response = $this->requestHttp('/kiosksync/promotions/'.$client_programid.'?debug=true');
			$expectedData = \Treat_Model_KioskSync::getPromotions($client, false);
			// Fix for modified time. Hack shouldn't be needed, but it is
			// throwing failures simply, because the modified timestamp changed.
			$responseData = json_decode($response->body());

			foreach($responseData as $index => $responseDataItem)
			{
				if(!array_key_exists($index, $expectedData))
				{
					$this->fail('Response does not have the same number of items.');
					return;
				}
				$expectedData[$index]['vivipos_modified'] = $responseDataItem->vivipos_modified;
			}

			$expected = utf8_json_encode($expectedData);

			$this->assertEquals(
				200,
				$response->responseCode(),
				sprintf("Response returned %d, instead of 200.\n\nBody:\n%s", $response->responseCode(), $response->body())
			);
			$this->assertTrue($response->hasHeader('content-type'), 'Missing content-type header message.');
			$this->assertEquals(
				'application/json; charset=utf-8',
				$response->header('content-type'),
				sprintf('Content type "%s" is not "application/json; charset=utf-8".', $response->header('content-type'))
			);
			$this->assertSame($expected, $response->body(), sprintf('JSON Response does not match.'));
		}
	}

	/**
	 * Runs through the normal activate business for global promotions.
	 *
	 * Does not use web driver or tests web interface for managing global
	 * promotions. All actions are handled through the normal Treat Models for
	 * handling what the web interface would normally. Not the best solution
	 * since changes to the web interface or Treat models could adversely affect
	 * this test.
	 *
	 * Steps:
	 *     1. Activate random business associated with global promotion for a
	 *        given client and add to kiosksync.
	 *     2. Test promotion display for whether it shows up.
	 *     3. Cleanup.
	 *
	 * @dataProvider providerActivePromotions
	 */
	public function testDeactivateBusinessForActivePromotions($promotionId)
	{
		$this->loadManageDb();
		\Treat_Model_ClientPrograms::db(\Manage_DB::singleton('database'));

		$promotion = \Treat_Model_Promotion::get($promotionId, true);
		$set_deactivate_businesses = $promotion->getAppliesTo();
		$this->promotion = $promotion;
		$this->currentBusinesses = $set_deactivate_businesses;

		if ( !is_array($set_deactivate_businesses) || count($set_deactivate_businesses) < 1 )
		{
			$this->markTestSkipped('No active businesses to set inactive.');
			return;
		}

		$client_programids = array();
		$activationLimitAmount = 5;
		foreach($set_deactivate_businesses as $index => $businessid)
		{
			if ($activationLimitAmount <= 0)
			{
				break; // Limit the amount of deactivations for testing.
			}
			if (false == mt_rand(0, 1))
			{
				continue;
			}
			$client_programid = \Treat_Model_ClientPrograms::getColumn('client_programid', "businessid = {$businessid}");
			if (count($client_programid) > 0)
			{
				unset($set_deactivate_businesses[$index]);
				$client_programids = array_merge($client_programids, $client_programid);
				$activationLimitAmount--;
			}
		}

		$promotion->setAppliesTo($set_deactivate_businesses);

		if (count($client_programids) < 1)
		{
			$this->markTestSkipped('Does not have any kiosks attached to business.');
			return;
		}

		foreach ($client_programids as $client_programid)
		{
			$client = \ClientProgramsModel::get((int)$client_programid, true);
			$promo_mapping = \Treat_Model_Promotion_Businesses::get('`BusinessId` = '.intval($client->businessid).' AND `PromotionId` = '.$promotionId);
			$hasPromoMapping = (count($promo_mapping) > 0);
			$promotions = \Treat_Model_KioskSync::getPromotions($client, false);

			$promotion_exists = false;
			foreach ($promotions as $_promotion)
			{
				if ($promotion->id == $_promotion['promo_id'])
				{
					$this->assertTrue($hasPromoMapping, 'Business promo mapping does not exist.');
					$this->assertTrue(
						($_promotion['vivipos_active'] == 0),
						'Promotion is still active when it should be inactive.'
					);
					$promotion_exists = true;
				}
			}

			$this->assertTrue($promotion_exists, 'Promotion is not within the promotions list.');
		}
	}

	public function providerActivePromotions()
	{
		$this->setupDatabases();
		$this->loadManageDb();

		$promotions = \Treat_Model_Promotion::get(
			'`activeFlag` = 1 AND `businessid` IS NULL ORDER BY `rule_order` ASC LIMIT 20'
		);

		$promotionsList = array();
		foreach ($promotions as $promotion)
		{
			$promotionsList[] = array(intval($promotion->id));
		}

		return $promotionsList;
	}

	public function providerPromotions()
	{
		$this->setupDatabases();
		$this->loadManageDb();

		$activePromotions = \Treat_Model_Promotion::get(
			'`activeFlag` = 1 AND `businessid` IS NULL ORDER BY `rule_order` ASC LIMIT 10'
		);
		$inactivePromotions = \Treat_Model_Promotion::get(
			'`activeFlag` = 0 AND `businessid` IS NULL ORDER BY `rule_order` ASC LIMIT 10'
		);
		$promotions = array_merge($activePromotions, $inactivePromotions);

		$promotionsList = array();
		foreach ($promotions as $promotion)
		{
			$promotionsList[] = array(intval($promotion->id));
		}

		return $promotionsList;
	}
}

