<?php

/** Change me to the actual test web site. */
define('HTTP_TEST_SITE', 'manage.localhost');



define('TEST_BASE_PATH', __DIR__);
define('TEST_TRAITS_PATH', __DIR__.'/traits');
define('TEST_DATA_PATH', __DIR__.'/data/');
define('TEST_APPLICATION_PATH', realpath(__DIR__.'/../application/'));
define('TEST_LIB_TREAT_PATH', realpath(__DIR__.'/../vendors/Treat/lib'));

/** PHPUnit Framework Testcase */
require_once 'PHPUnit/Framework/TestCase.php';

/** PHPUnit Extensions Database Testcase */
require_once 'PHPUnit/Extensions/Database/TestCase.php';

/** HTTP Test - has methods for sending HTTP requests */
require_once \TEST_TRAITS_PATH.'/http.php';

/** DB Trait - has methods for PHPUnit DbTest */
require_once \TEST_TRAITS_PATH.'/db.php';


define('SITECH_APP_PATH', rtrim(TEST_APPLICATION_PATH, '/'));

require_once SITECH_APP_PATH.'/bootstrap.php';

