# Tests

A combination of raw HTTP requests and database manipulation to check Kiosk API.
The reason is to check regressions and ensure that the Manage site doesn't have
regressions.

What isn't being checked are external libraries that may also have regressions
causing regressions or errors with the tests. The point is that eventually, the
external dependencies will have enough tests to ensure that regressions and
errors are pinpointed close to the problem.

So really, the tests fall into functional or acceptance tests as well as
regression. Selenium or Web Driver tests probably would be acceptable as well.

## Setup

Assumes phpunit and its dependencies (DbUnit, Web Driver etc) are also
installed.

 1. Copy 'phpunit-dist.xml' and rename 'phpunit.xml'.
 1. Change variables to test site. (''''Must never ever be live database!'''').
 1. Configure live site so that tests can access API and live site.
 1. Navigate to the tests directory.
 1. Run tests using '/path/to/phpunit -c phpunit.xml'.

## Files

The files are currently separated as other test directories. For example,
/tests/application are tests for /application and /tests/lib/ would be tests for
the Manage library located in /lib.

The tests are also separated by 'task', which may be different from other test
case files where tests are combined for a given class or file. The reason for
handling it this way is the organization and improvements. A given section
requires more than one or two tests , which may require 'stories' to better
describe the actions being tested. It is similar organized to Selenium or Web
Driver test cases.

## Data

Some test cases require extra data for database setup or testing libraries
loading files. In this case, the organize is by tests directory instead of base
directory.