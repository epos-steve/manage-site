<?php
/**
 * Tests for application/controllers/kiosksync.php
 *
 * HTTP requests for test site, must not be run against main site!
 *
 * Each file tests different method or page. May also mock the main controller
 * and testing methods inside of the class.
 *
 * NOTE: You must include the boostrap in the base tests directory to ensure
 * that the correct files and mockcontroller works.
 *
 * @copyright  Copyright © 2012 Essential Elements, LLC (http://www.essential-elements.net)
 * @license    EE-Proprietary
 * @package    Manage\Application\Tests
 * @subpackage Tests\Application\Controllers\Kiosksync
 */

/**
 * Part of Manage\Application\Tests
 *
 * @category Manage\Tests\Lib\Manage
 */
namespace Manage\Tests\Lib\Manage;


/**
 * Tests
 *
 * @package    Manage\Application\Tests
 * @subpackage Tests\Application\Controllers\Kiosksync
 *
 * @requires PHP 5.4
 * @requires PHPUnit 3.7
 */
class ConfigParserTest extends \PHPUnit_Framework_TestCase
{

	public function setUp()
	{
		$this->setupDatabases();
		$this->loadManageDb();
	}

	public function testConfigFileExists()
	{
		$this->assertFileExists(TEST_APPLICATION_PATH.'/config.ini', 'Missing config.ini file.');
	}

	public function testInstance()
	{
		$instance = Manage_ConfigParser::singleton();
		$this->assertInstanceOf('Manage_ConfigParser', $instance);
	}

	/**
	 * @depends testConfigFileExists
	 * @dataProvider configSectionList
	 */
	public function testInstanceSectionExists($section)
	{
		$instance = Manage_ConfigParser::singleton();
		$this->assertTrue($instance->hasSection($section));
	}

	/**
	 * @depends testConfigFileExists
	 * @dataProvider configSectionItemList
	 */
	public function testSectionItemExists($section, $item)
	{
		$instance = Manage_ConfigParser::singleton();
		$this->assertTrue($instance->hasOption($section, $item));
	}

	public function configSectionList()
	{
		return array(
			array('database'),
			array('treat db'),
			array('process_host'),
			array('vivipos db'),
			array('main'),
		);
	}

	public function configSectionItemList()
	{
		return array(
			array('database', 'dsn'),
			array('database', 'host'),
			array('database', 'ro_host'),
			array('database', 'port'),
			array('database', 'database'),
			array('database', 'username'),
			array('database', 'password'),

			array('treat db', 'dsn'),
			array('treat db', 'host'),
			array('treat db', 'ro_host'),
			array('treat db', 'port'),
			array('treat db', 'database'),
			array('treat db', 'username'),
			array('treat db', 'password'),

			array('process_host', 'driver'),
			array('process_host', 'host'),
			array('process_host', 'ro_host'),
			array('process_host', 'port'),
			array('process_host', 'database'),
			array('process_host', 'username'),
			array('process_host', 'password'),

			array('vivipos db', 'dsn'),
			array('vivipos db', 'username'),
			array('vivipos db', 'password'),

			array('main', 'path_prefix'),
			array('main', 'base_url'),
			array('main', 'production'),
		);
	}

}

