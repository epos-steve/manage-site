<?php
/**
 * @copyright  Copyright � 2012 Essential Elements, LLC (http://www.essential-elements.net)
 * @license    EE-Proprietary
 * @package    Manage\Application\Tests
 * @subpackage Tests\Lib\Manage\Db
 */

/**
 * Part of Manage\Application\Tests
 *
 * @category Manage\Tests\Lib\Manage\Db
 */
namespace Manage\Tests\Lib\Manage\Db;


class MockDbProxy extends \Manage_DB_Proxy
{
	public static function _configParameters($section = 'database')
	{
		return parent::_configParameters($section);
	}

	public static function getConnectionAttribute($attribute, $section = 'database')
	{
		return array(
			'writer' => static::$_instance[$section]->_writeConn->getAttribute($attribute),
			'reader' => static::$_instance[$section]->_readConn->getAttribute($attribute),
		);
	}

	public static function singleton($section = 'database')
	{
		if ( !isset( static::$_instance[$section])) {
			$data = static::_configParameters($section);
			static::$_instance[$section] = new static($data['config'], $data['writers'], $data['readers']);

			if ($data['config']['driver'] == 'mysql')
			{
				static::$_instance[$section]->setAttribute( \PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true );
			}
		}

		return static::$_instance[$section];
	}

	public static function getReadOnly($statement, $section = 'database')
	{
		return static::$_instance[$section]->_readOnly($statement);
	}
}