<?php
/**
 * @copyright  Copyright � 2012 Essential Elements, LLC (http://www.essential-elements.net)
 * @license    EE-Proprietary
 * @package    Manage\Application\Tests
 * @subpackage Tests\Lib\Manage\Db
 */

/**
 * Part of Manage\Application\Tests
 *
 * @category Manage\Tests\Lib\Manage\Db
 */
namespace Manage\Tests\Lib\Manage\Db;

require 'MockDbProxy.php';
/**
 * Tests
 *
 * @package    Manage\Application\Tests
 * @subpackage Tests\Application\Controllers\Kiosksync
 *
 * @requires PHP 5.4
 * @requires PHPUnit 3.7
 */
class ProxyTest extends \PHPUnit_Framework_TestCase
{
	public function testConfigHasHostReplacement()
	{
		/*
dsn="mysql:host=192.168.2.2;dbname=mburris_manage"
host = 192.168.2.2
ro_host[]= 192.168.2.2
ro_host[]= 192.168.2.3
port     = 3306
database = mburris_manage
username="mburris_user"
password="bigdog"
		 */
		$config = array(
			'dsn' => 'mysql:host=192.168.2.2;dbname=mburris_manage',
			'driver' => 'mysql',
			'host' => '192.168.2.2',
			'database' => 'mburris_manage',
			'username' => 'test',
			'password' => 'test',
			'port' => '3306',
			'ro_host' => array(
				'192.168.2.2',
				'192.168.2.3',
			),
		);

		$expectedConfig = $config;
		$expectedConfig['host'] = '%s';
		$actualConfig = \array_merge($config, array('host' => '%s'));

		$this->assertSame($expectedConfig, $actualConfig);

		$expected = 'mysql:host=%s;port=3306;dbname=mburris_manage';
		$actual = \Manage_DB_Proxy::getDsn( $actualConfig );

		$this->assertEquals($expected, $actual);
	}

	/**
	 * @dataProvider dbSectionConfig
	 */
	public function testLiveConfigHasHostReplacement($section)
	{
		$config = \Manage_ConfigParser::singleton()->items( $section );

		$expected = 'mysql:host=%s;port=3306;dbname='.$config['database'];
		$actual = \Manage_DB_Proxy::getDsn( array_merge($config, array('host' => '%s')) );

		$this->assertEquals($expected, $actual);
	}

	public function dbSectionConfig()
	{
		return array(
			array('database'),
			array('treat db'),
			array('process_host'),
		);
	}

	public function testDatabaseReadOnlyHosts()
	{
		MockDbProxy::singleton();

		$connections = array();
		for ($attempts = 100; $attempts > 0; $attempts--)
		{
			$connection = MockDbProxy::getConnectionAttribute(\PDO::ATTR_CONNECTION_STATUS);
			if (!in_array($connection['reader'], $connections))
			{
				$connections[] = $connection['reader'];
			}
		}

		$this->assertCount(1, $connections, var_export($connections, true));
	}

	/**
	 * @dataProvider readOnlySqlStatements
	 */
	public function testReadOnlySQLStatementsAreReadOnly($sql)
	{
		MockDbProxy::singleton();
		$this->assertTrue(MockDbProxy::getReadOnly($sql));
	}

	/**
	 * @dataProvider writeOnlySqlStatements
	 */
	public function testWriteOnlySQLStatementsAreReadOnly($sql)
	{
		MockDbProxy::singleton();
		$this->assertFalse(MockDbProxy::getReadOnly($sql));
	}

	public function readOnlySqlStatements()
	{
		return array(
			array('SELECT * FROM table'),
			array('DESC table'),
			array('DESCRIBE table'),
			array('EXPLAIN SELECT * FROM table'),
			array("HELP 'contents'"),
			array('SHOW COLUMNS FROM table'),
		);
	}

	public function writeOnlySqlStatements()
	{
		return array(
			array('INSERT INTO `table` (`stuff`, `column`) VALUES (1, 2)'),
			array('UPDATE `table` SET `stuff` = 1 WHERE `column` = 2'),
			array('DELETE FROM `table` WHERE `column` = 2'),
		);
	}
}

