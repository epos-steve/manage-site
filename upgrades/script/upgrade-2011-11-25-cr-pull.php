<?php
/**
 * This script takes all the previous credit reloads and puts them into the
 * checks table.
 */

define('SITECH_APP_PATH', dirname(dirname(dirname(__FILE__))).'/application');
require_once(dirname(SITECH_APP_PATH).'/vendors/SiTech/lib/SiTech/Loader.php');
SiTech_Loader::loadBootstrap();
ClientProgramsModel::db(Manage_DB::singleton('database', true));
$clients = array();
$totals = array('missing' => array(), 'inserted' => array(), 'skipped' => array());

// Connect to the database server then switch to the vivipos_order database
$vdb = Manage_DB::singleton('vivipos db');
$vdb->exec('USE vivipos_order');
// Now grab all past charges.
$stmnt = $vdb->query(
'SELECT
	CAST(CONCAT(SUBSTRING(FROM_UNIXTIME(op.modified, \'%Y%m%d%H%i%s\'), 2), op.terminal_no) AS UNSIGNED INTEGER) AS sequence,
	0 AS session,
	0 AS table_no,
	0 AS seat,
	0 AS emp_no,
	1 AS no_of_customers,
	FROM_UNIXTIME(op.modified) AS transaction_created,
	op.order_total AS total,
	0 AS tax_subtotal,
	op.amount AS payment_subtotal,
	IF(op.name = \'CA\' OR op.name = \'CR\', 1, 0) AS sale_type,
	0 AS is_void,
	IF (op.customer_card_number = "null", "", op.customer_card_number) AS customer_card_number,
	op.terminal_no,
	IF(op.name = \'CA\', 1, IF(op.name = \'CC\', 4, IF(op.name = \'CK\', 14, IF(op.name = \'PD\', 15, IF(op.name = \'CR\', 4, 0))))) AS payment_type
FROM
	order_payments AS op
		LEFT JOIN orders AS o
			ON o.id = op.order_id
WHERE
	op.modified < :time
	AND op.name IN (\'CR\', \'CA\')
	AND o.id IS NULL', array('time' => time()));

while ($payment = $stmnt->fetch(PDO::FETCH_OBJ)) {
	$accessDenied = false;
	foreach ($clients as $c) {
		// le-sigh
		if (is_object($c['obj'])) {
			$c['obj']->db->keepAlive();
			ClientProgramsModel::db()->keepAlive();
		}
	}

	if (!isset($clients[$payment->terminal_no])) {
		$clients[$payment->terminal_no] = array();
		$clients[$payment->terminal_no]['obj'] = ClientProgramsModel::get((int)$payment->terminal_no, true);
		
		$totals['inserted'][$payment->terminal_no] = 0;
		$totals['missing'][$payment->terminal_no] = 0;
		$totals['updated'][$payment->terminal_no] = 0;
		$totals['skipped'][$payment->terminal_no] = 0;

		if ($clients[$payment->terminal_no]['obj'] !== false) {
			try {
				$clients[$payment->terminal_no]['checks'] = $clients[$payment->terminal_no]['obj']->db->prepare('INSERT INTO checks
				(
					businessid,
					check_number,
					session_number,
					table_number,
					seat_number,
					emp_no,
					people,
					bill_datetime,
					total,
					taxes,
					received,
					sale_type,
					account_number,
					is_void
				)
				VALUES(
					:bid, :checkno, :snum, :tnum, :stnum, :empno, :people, :date, :total, :tax, :recv, :type, :acct, :void
				) ON DUPLICATE KEY UPDATE sale_type = :type');
				$clients[$payment->terminal_no]['payment'] = $clients[$payment->terminal_no]['obj']->db->prepare('INSERT INTO payment_detail
				(
					businessid,
					check_number,
					payment_type,
					received,
					base_amount,
					tip_amount,
					scancode
				)
				VALUES(
					?, ?, ?, ?, ?, 0, ?
				)');
				$clients[$payment->terminal_no]['update'] = $clients[$payment->terminal_no]['obj']->db->prepare('UPDATE
					payment_detail
				SET
					payment_type = ?
				WHERE
					businessid = ?
					AND check_number = ?');
			} catch (PDOException $e) {
				if (strstr($e->getMessage(), '[1045] Access denied') === false) {
					throw $e;
				}

				$clients[$payment->terminal_no]['obj'] = false;
				continue;
			}
		}
	}

	$client = $clients[$payment->terminal_no]['obj'];
	
	if ($client === false) {
		$totals['missing'][$payment->terminal_no]++;
		continue;
	}

	$newCheck = $clients[$payment->terminal_no]['checks'];
	$newPayment = $clients[$payment->terminal_no]['payment'];
	$updatePayment = $clients[$payment->terminal_no]['update'];

	try {
		// Now put the data into the tables ... checks and payment details
		$newCheck->execute(array(
			'bid'     => $client->businessid,
			'checkno' => $payment->sequence,
			'snum'    => $payment->session,
			'tnum'    => $payment->table_no,
			'stnum'   => $payment->seat,
			'empno'   => $payment->emp_no,
			'people'  => $payment->no_of_customers,
			'date'    => $payment->transaction_created,
			'total'   => $payment->total,
			'tax'     => $payment->tax_subtotal,
			'recv'    => $payment->payment_subtotal,
			'type'    => $payment->sale_type,
			'acct'    => $payment->customer_card_number,
			'void'    => $payment->is_void
		));
		if (($_updated = $newCheck->rowCount()) > 1) {
			$newCheck->closeCursor();
			$totals['updated'][$payment->terminal_no] += 1;
			$updatePayment->execute(array($payment->payment_type, $client->businessid, $payment->sequence));
			$totals['updated'][$payment->terminal_no] += $updatePayment->rowCount();
			continue; // Updated... don't insert a payment
		}
		$totals['inserted'][$payment->terminal_no] += $_updated;
		$newCheck->closeCursor();
	} catch (PDOException $e) {
		$newCheck->closeCursor();
		if (strstr($e->getMessage(), '1062 Duplicate entry') === false) {
			var_dump($e->getMessage(), $e);
			throw $e;
		}
		
		$totals['skipped'][$payment->terminal_no] += 1;
		continue;
	}

	try {
		$newPayment->execute(array(
			$client->businessid,
			$payment->sequence,
			$payment->payment_type,
			$payment->payment_subtotal,
			$payment->payment_subtotal,
			$payment->customer_card_number
		));
		$totals['inserted'][$payment->terminal_no] += $newPayment->rowCount();
		$newPayment->closeCursor();
	} catch (PDOException $e) {
		$newPayment->closeCursor();
		if (strstr($e->getMessage(), '1062 Duplicate entry') === false) {
			throw $e;
		}
		$totals['skipped'][$payment->terminal_no] += 1;
	}
}

var_dump($totals['missing']);

echo 'Import of previous Credit Reloads complete'.PHP_EOL;
echo 'Inserted: '.array_sum($totals['inserted']).PHP_EOL;
echo 'Updated:  '.array_sum($totals['updated']).PHP_EOL;
echo 'Missing:  '.array_sum($totals['missing']).PHP_EOL;
echo 'Skipped:  '.array_sum($totals['skipped']).PHP_EOL;
