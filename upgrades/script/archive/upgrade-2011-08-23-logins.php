<?php
define('SITECH_APP_PATH', dirname(dirname(dirname(__FILE__))).'/../application');
require_once(SITECH_APP_PATH.'/bootstrap.php');

$db = Manage_DB::singleton();
$db->beginTransaction();
$total = 0;

echo 'Updating logins ...';
try {
	$db->exec('ALTER TABLE login ADD COLUMN npassword CHAR(40) NOT NULL, ENGINE=INNODB');
	$logins = $db->query('SELECT loginid, password FROM login');
	$update = $db->prepare('UPDATE login SET npassword = :npassword WHERE loginid = :loginid');
	foreach ($logins->fetchAll(PDO::FETCH_ASSOC) as $login) {
		$login['npassword'] = sha1($login['password']);
		unset($login['password']);
		$update->execute($login);
		$update->closeCursor();
		$total++;
	}

	$logins->closeCursor();
	$db->exec('ALTER TABLE login DROP COLUMN password, CHANGE npassword password CHAR(40) NOT NULL AFTER username, ADD COLUMN permissions INT NOT NULL DEFAULT 0');
} catch (PDOException $e) {
	$db->rollBack();
	echo ' FAILED!'.PHP_EOL;
	echo 'MySQL Said: '.$e->getMessage().PHP_EOL;
	exit;
}

$db->commit();
echo ' DONE!'.PHP_EOL;
echo 'Successfully updated '.$total.' login records'.PHP_EOL;
