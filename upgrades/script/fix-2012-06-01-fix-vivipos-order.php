<?php
/**
 * Since I totally broke the syncs for a bit... this tries to fix it. sigh.
 *
 * @author Eric Gach <eric@essential-elements.net>
 */

define('SITECH_APP_PATH', dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR.'application');
set_include_path(dirname(SITECH_APP_PATH).DIRECTORY_SEPARATOR.'vendors'.DIRECTORY_SEPARATOR.'SiTech'.DIRECTORY_SEPARATOR.'lib'.PATH_SEPARATOR.dirname(SITECH_APP_PATH).DIRECTORY_SEPARATOR.'lib'.PATH_SEPARATOR.get_include_path());
require_once('SiTech/Loader.php');
SiTech_Loader::registerAutoload();
$odb = Manage_DB::singleton();
$odb->exec('USE vivipos_order');
$vdb = Manage_DB::singleton('vivipos db');
$vdb->exec('USE vivipos_order');

$records = array();
$timestamp = (empty($_SERVER['argv'][1]))? '1338529695' : $_SERVER['argv'][1];

function build_array($stmnt, $table, $records) {
	if ($stmnt === false) return($records);
	$records[$table] = array();
	foreach ($stmnt->fetchAll(PDO::FETCH_ASSOC) as $r) {
		$records[$table][] = $r;
	}

	return($records);
}

$records = build_array($odb->query('SELECT * FROM orders WHERE modified > ?', array($timestamp)), 'orders', $records);
$records = build_array($odb->query('SELECT order_items.* FROM order_items JOIN orders ON order_items.order_id = orders.id WHERE orders.modified > ?', array($timestamp)), 'order_items', $records);
$records = build_array($odb->query('SELECT order_item_taxes.* FROM order_item_taxes JOIN orders ON order_item_taxes.order_id = orders.id WHERE orders.modified > ?', array($timestamp)), 'order_item_taxes', $records);
$records = build_array($odb->query('SELECT * FROM order_payments WHERE order_payments.modified > ?', array($timestamp)), 'order_payments', $records);
$records = build_array($odb->query('SELECT * FROM cc_transactions WHERE time_stamp > ?', array($timestamp)), 'cc_transactions', $records);
$records = build_array($odb->query('SELECT * FROM cashdrawer_records WHERE modified > ?', array($timestamp)), 'cashdrawer_records', $records);
$records = build_array($odb->query('SELECT * FROM order_promotions WHERE modified > ?', array($timestamp)), 'order_promotions', $records);

foreach ($records as $table => $rows) {
	if (!sizeof($rows)) continue;
	foreach ($rows as $k => $row) {
		array_walk($row, function(&$item, $key, $vdb) {
			$item = $vdb->quote($item);
		}, $vdb);
		$rows[$k] = '('.implode(',', $row).')';
	}
	$sql = 'INSERT IGNORE INTO '.$table.' VALUES'.implode(',', $rows).'';
	try {
		echo 'Rows Inserted into '.$table.': '.$vdb->exec($sql).' of '.sizeof($rows).' found'.PHP_EOL;
	} catch (PDOException $e) {
		var_dump($sql);
		echo $e->getMessage().PHP_EOL;
	}
}