
DROP TABLE IF EXISTS `syncData`;
CREATE TABLE IF NOT EXISTS `syncData` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_programid` int(10) unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `client_programid` (`client_programid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `syncLog`;
CREATE TABLE IF NOT EXISTS `syncLog` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `sequence` bigint(20) unsigned NOT NULL,
  `businessid` int(10) unsigned NOT NULL,
  `level` enum('DEBUG','INFO','NOTICE','WARNING','ERROR','CRITICAL','EXCEPTION') NOT NULL DEFAULT 'INFO',
  `message` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
