--
-- Triggers `cashdraw_records`
--
DROP TRIGGER IF EXISTS `cashdrawer_records_archive`;
DELIMITER //
CREATE TRIGGER `cashdrawer_records_archive` AFTER INSERT ON `cashdrawer_records`
 FOR EACH ROW BEGIN
	INSERT INTO `cashdrawer_records_archive` VALUES(NEW.`id`, NEW.`terminal_no`, NEW.`drawer_no`, NEW.`clerk`, NEW.`clerk_displayname`, NEW.`event_type`, NEW.`status`, NEW.`created`, NEW.`modified`, NEW.`payment_type`, NEW.`amount`, NEW.`sequence`, NEW.`order_id`);
END
//
DELIMITER ;


--
-- Triggers `cc_transactions`
--
DROP TRIGGER IF EXISTS `cc_transactions_archive`;
DELIMITER //
CREATE TRIGGER `cc_transactions_archive` AFTER INSERT ON `cc_transactions`
 FOR EACH ROW BEGIN
    INSERT INTO `cc_transactions_archive` VALUES(NEW.`id`, NEW.`cc_transaction_id`, NEW.`time_stamp`, NEW.`response_code`, NEW.`response_text`, NEW.`auth_code`, NEW.`avsrsltcode`, NEW.`ref_number`, NEW.`email`, NEW.`is_processed`);
END
//
DELIMITER ;

--
-- Triggers `kiosk_logs`
--
DROP TRIGGER IF EXISTS `kiosk_logs_archive`;
DELIMITER //
CREATE TRIGGER `kiosk_logs_archive` AFTER INSERT ON `kiosk_logs`
 FOR EACH ROW BEGIN
    INSERT INTO `kiosk_logs_archive` VALUES(NEW.`Id`, NEW.`TerminalNumber`, NEW.`Level`, NEW.`Message`, NEW.`created`);
END
//
DELIMITER ;

--
-- Triggers `orders`
--
DROP TRIGGER IF EXISTS `orders_archive`;
DELIMITER //
CREATE TRIGGER `orders_archive` AFTER INSERT ON `orders`
 FOR EACH ROW BEGIN
        /* Do items first since this is a linked table */
        DELETE FROM `order_items` WHERE `order_items`.`order_id` = NEW.`id`;
        /* Now we can archive the order */
        INSERT INTO `orders_archive` VALUES(NEW.`id`, NEW.`sequence`, NEW.`items_count`, NEW.`total`, NEW.`change`, NEW.`tax_subtotal`, NEW.`surcharge_subtotal`, NEW.`discount_subtotal`, NEW.`payment_subtotal`, NEW.`rounding_prices`, NEW.`precision_prices`, NEW.`rounding_taxes`, NEW.`precision_taxes`, NEW.`status`, NEW.`service_clerk`, NEW.`service_clerk_displayname`, NEW.`proceeds_clerk`, NEW.`proceeds_clerk_displayname`, NEW.`member`, NEW.`member_displayname`, NEW.`member_email`, NEW.`member_cellphone`, NEW.`invoice_type`, NEW.`invoice_title`, NEW.`invoice_no`, NEW.`invoice_count`, NEW.`destination`, NEW.`table_no`, NEW.`check_no`, NEW.`no_of_customers`, NEW.`terminal_no`, NEW.`transaction_created`, NEW.`transaction_submitted`, NEW.`created`, NEW.`modified`, NEW.`included_tax_subtotal`, NEW.`item_subtotal`, NEW.`sale_period`, NEW.`shift_number`, NEW.`branch_id`, NEW.`branch`, NEW.`transaction_voided`, NEW.`promotion_subtotal`, NEW.`void_clerk`, NEW.`void_clerk_displayname`, NEW.`void_sale_period`, NEW.`void_shift_number`, NEW.`revalue_subtotal`, NEW.`qty_subtotal`, NEW.`inherited_order_id`, NEW.`inherited_desc`, NEW.`item_surcharge_subtotal`, NEW.`trans_surcharge_subtotal`, NEW.`item_discount_subtotal`, NEW.`trans_discount_subtotal`, NEW.`customer_card_number`);
    END
//
DELIMITER ;

--
-- Triggers `order_items`
--
DROP TRIGGER IF EXISTS `order_items_archive`;
DELIMITER //
CREATE TRIGGER `order_items_archive` AFTER INSERT ON `order_items`
 FOR EACH ROW BEGIN
        /* Clear the tax records */
        DELETE FROM `order_item_taxes` WHERE `order_item_id` = NEW.`id`;
        /* Now copy the item to the archive table */
        INSERT INTO `order_items_archive` VALUES(NEW.`id`, NEW.`order_id`, NEW.`cate_no`, NEW.`included_tax`, NEW.`cate_name`, NEW.`product_no`, NEW.`product_barcode`, NEW.`product_name`, NEW.`current_qty`, NEW.`current_price`, NEW.`current_subtotal`, NEW.`tax_name`, NEW.`tax_rate`, NEW.`tax_type`, NEW.`current_tax`, NEW.`discount_name`, NEW.`discount_rate`, NEW.`discount_type`, NEW.`current_discount`, NEW.`surcharge_name`, NEW.`surcharge_type`, NEW.`surcharge_rate`, NEW.`current_surcharge`, NEW.`condiments`, NEW.`current_condiment`, NEW.`memo`, NEW.`has_discount`, NEW.`has_surcharge`, NEW.`has_marker`, NEW.`created`, NEW.`modified`, NEW.`destination`, NEW.`parent_no`, NEW.`weight`, NEW.`sale_unit`, NEW.`seat_no`, NEW.`parent_index`, NEW.`has_setitems`, NEW.`stock_maintained`);
    END
//
DELIMITER ;

--
-- Triggers `order_item_condiments`
--
DROP TRIGGER IF EXISTS `order_item_condiments_archive`;
DELIMITER //
CREATE TRIGGER `order_item_condiments_archive` AFTER INSERT ON `order_item_condiments`
 FOR EACH ROW BEGIN
		INSERT INTO `order_item_condiments_archive` VALUES(NEW.`id`, NEW.`order_id`, NEW.`item_id`, NEW.`name`, NEW.`price`, NEW.`created`, NEW.`modified`);
	END
//
DELIMITER ;

--
-- Triggers `order_item_taxes`
--
DROP TRIGGER IF EXISTS `order_item_taxes_archive`;
DELIMITER //
CREATE TRIGGER `order_item_taxes_archive` AFTER INSERT ON `order_item_taxes`
 FOR EACH ROW BEGIN
        INSERT INTO `order_item_taxes_archive` VALUES(NEW.`id`, NEW.`order_id`, NEW.`order_item_id`, NEW.`promotion_id`, NEW.`tax_no`, NEW.`tax_name`, NEW.`tax_type`, NEW.`tax_rate`, NEW.`tax_rate_type`, NEW.`tax_threshold`, NEW.`tax_subtotal`, NEW.`included_tax_subtotal`, NEW.`item_count`, NEW.`taxable_amount`);
    END
//
DELIMITER ;

--
-- Triggers `order_objects`
--
DROP TRIGGER IF EXISTS `order_objects_archive`;
DELIMITER //
CREATE TRIGGER `order_objects_archive` AFTER INSERT ON `order_objects`
 FOR EACH ROW BEGIN
    INSERT INTO `order_objects_archive` VALUES(NEW.`id`, NEW.`order_id`, NEW.`object`, NEW.`created`, NEW.`modified`);
END
//
DELIMITER ;
--
-- Triggers `order_payments`
--
DROP TRIGGER IF EXISTS `order_payments_archive`;
DELIMITER //
CREATE TRIGGER `order_payments_archive` AFTER INSERT ON `order_payments`
 FOR EACH ROW BEGIN
        INSERT INTO `order_payments_archive` VALUES(NEW.`id`, NEW.`order_id`, NEW.`order_items_count`, NEW.`order_total`, NEW.`name`, NEW.`amount`, NEW.`memo1`, NEW.`memo2`, NEW.`created`, NEW.`modified`, NEW.`origin_amount`, NEW.`service_clerk`, NEW.`proceeds_clerk`, NEW.`service_clerk_displayname`, NEW.`proceeds_clerk_displayname`, NEW.`change`, NEW.`sale_period`, NEW.`shift_number`, NEW.`terminal_no`, NEW.`is_groupable`, NEW.`customer_card_number`);
    END
//
DELIMITER ;

--
-- Triggers `syncs`
--
DROP TRIGGER IF EXISTS `syncs_archive`;
DELIMITER //
CREATE TRIGGER `syncs_archive` AFTER INSERT ON `syncs`
 FOR EACH ROW BEGIN
        INSERT INTO `syncs_archive` VALUES(NEW.`crud`, NEW.`machine_id`, NEW.`from_machine_id`, NEW.`method_id`, NEW.`method_type`, NEW.`method_table`, NEW.`created`, NEW.`modified`);
    END
//
DELIMITER ;
