DROP TABLE IF EXISTS `client_checkin`;
CREATE TABLE `client_checkin` (
  `client_programid` int(10) unsigned NOT NULL,
  `total` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`client_programid`),
  UNIQUE KEY `clientprogram_id_UNIQUE` (`client_programid`),
  KEY `fk_client_checkin_1_idx` (`client_programid`),
  CONSTRAINT `fk_client_checkin_1` FOREIGN KEY (`client_programid`) REFERENCES `client_programs` (`client_programid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
